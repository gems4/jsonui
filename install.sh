#!/bin/bash

#./install-dependencies.sh

threads=3
BRANCH_JSON=master
BuildType=Release
INSTALL_JSON=/usr/local
workfolder=${PWD}
QT_PATH=$1
echo $QT_PATH

mkdir -p build
cd build
cmake .. -DCMAKE_CXX_FLAGS=-fPIC -DCMAKE_BUILD_TYPE=$BuildType -DCMAKE_INSTALL_PREFIX=$INSTALL_JSON -DCMAKE_PREFIX_PATH=$QT_PATH
make -j $threads 
sudo make install

if [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
   sudo ldconfig
fi
