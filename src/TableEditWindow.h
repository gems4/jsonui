//  This is JSONUI library+API (https://bitbucket.org/gems4/jsonui)
//
/// \file TableEditWindow.h
/// Declarations of class TableEditWidget widget to work with table data
//
// JSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONUI depends on the following open-source software products:
// JSONIO (https://bitbucket.org/gems4/jsonio); Qt5 (https://qt.io).
//

#ifndef TABLEEDITWIDGET_H
#define TABLEEDITWIDGET_H

#include <memory>
#include <QMainWindow>
#include "model_table.h"
#include "graph_data.h"

namespace Ui {
class TableEditWidget;
}

class QSortFilterProxyModel;

namespace jsonui {

class TCSVPage;
class TCSVContainer;
class GraphDialog;

extern const QString csvFilters;

/// \class TableEditWidget  widget to work with table data.
class TableEditWidget : public QMainWindow
{
    Q_OBJECT

   /// Title of window
   QString _title;
   /// Mode of using ( see TMatrixTable::TABLE_TYPES )
   int _modeUsing;

   //Graphic
   std::shared_ptr<TCSVContainer> _contn;

   /// Set up menu commands
   void setActions();
   /// Set up widget
   void setWidgetData(TAbstractDataContainer *datacont);

   // get graph info
   void showGraphData( const std::string& title );
   void closeGraph();

   void closeEvent(QCloseEvent* e);


public slots:

    void slotPopupContextMenu(const QPoint &pos);
    void updateTable();

    // File
    void CmExportCSV();
    void CmExportSelectedCSV();
    void CmImportCSV();

    // Graphics
    void ToggleX();
    void ToggleY();
    void CmPlotTable();
    //void CmPrintTable(){}
    //void CmFindFromPlot(){}

    void CmHelp();

public:

    /// Constractor connect to extern table model
    explicit TableEditWidget( const char* title,
                TAbstractDataContainer *data, int mode = TMatrixTable::tbEdit, QWidget *parent = nullptr);
    /// Constructor from csv file
    explicit TableEditWidget( const char* title,
                const std::string& fname, int mode = TMatrixTable::tbEdit, QWidget *parent = nullptr);
    // Constructor
    // TableEditWidget( const char* title,
    // std::shared_ptr<TCSVContainer> data, int mode = TMatrixTable::tbEdit, QWidget *parent = 0);

    /// Destructor
    ~TableEditWidget();

    /// Read new CSV file ( now only in case Constructor from csv file )
    void openNewCSV( const std::string& file );

    /// Set the X axis column index and a list of Y axis indexes
    void setXYaxis(int ndxX, std::vector<int> ndxY);

private:

    /// Menu description
    Ui::TableEditWidget *ui;
    /// Internal table view widget
    TCSVPage* _page;
    /// Graphic window
    GraphDialog* _graph_dlg;

};

} // namespace jsonui

#endif // TABLEEDITWIDGET_H
