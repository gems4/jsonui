//  This is JSONUI library+API (https://bitbucket.org/gems4/jsonui)
//
/// \file model_query.h
/// Declaration of TQueryModel, TQueryDelegate and TQueryView
/// implements a tree view of query structure
//
// JSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONUI depends on the following open-source software products:
// JSONIO (https://bitbucket.org/gems4/jsonio); Qt5 (https://qt.io).
//

#ifndef _model_query_h
#define _model_query_h

#include <QAbstractItemModel>
#include <QTreeView>
#include <QItemDelegate>
#include "jsonio/dbquerydef.h"

namespace jsonui {

struct AQLline;

struct ThriftFieldData
{
    int thrifttype = 0;        ///< Thrift type of field
    std::string enumname = ""; ///< Name of enum
};

/// \class QueryLine represents a query item in a tree view.
/// Item data defines one query object
struct AQLline
{
    enum fieldType
    { Top, Field, LogicalTwo, LogicalNot, Operator };
    //{ tTop, tField, tConditionAdd, tConditionOne, tList, tObject, tString };

    AQLline( size_t andx, const std::string& akey, fieldType atype, AQLline* parentline );
    ~AQLline();

    /// Return json std::string with template query
    std::string toTemplate();
    /// Return std::string with AQL query
    std::string toAQL(const std::string& collection);
    /// Return json dom object
    void toDom( jsonio::JsonDom* domobj );
    /// Set json dom object
    void fromDom( const jsonio::JsonDom* domobj );
    /// Json value type
    int valueType();

    int ndx;
    fieldType type;                     ///< Type of field
    std::string keyname;                ///< Name of field
    std::string value;                  ///< Value of field
    ThriftFieldData field_data;         ///< Internal data from thrift schema
    //jsonio::ThriftFieldDef* field_data; ///< Internal data from thrift schema

    AQLline *parent;
    std::vector<std::unique_ptr<AQLline>> children;

    /// Get Value from std::string
    /// If field is not type T, the false will be returned.
    template <class T>
    bool getValue( T& val  )
    {   return  jsonio::TArray2Base::string2value( value, val );  }

protected:

    fieldType typeFromKey( const std::string& akey );
    void query2dom( jsonio::JsonDom* domobj );
    void value2dom( jsonio::JsonDom* domobj );
    std::string query2AQL();
    std::string value2AQL( const std::string& the_operator );
};


/// \class TQueryModel
/// class for represents the data set and is responsible for fetching the data
/// is needed for viewing and for writing back any changes.
/// Reading/writing data from/to query object
class TQueryModel: public QAbstractItemModel
{
    Q_OBJECT

    QStringList hdData;
    std::unique_ptr<AQLline> rootNode;

public:

    TQueryModel( QObject* parent = nullptr );
    ~TQueryModel(){}

    QModelIndex index(int row, int column, const QModelIndex& parent) const;
    QModelIndex parent(const QModelIndex& child) const;
    int rowCount ( const QModelIndex& parent ) const;     //ok
    int columnCount ( const QModelIndex& parent  ) const; // ok
    QVariant data ( const QModelIndex& index, int role ) const;
    bool setData ( const QModelIndex& index, const QVariant& value, int role );
    QVariant headerData ( int section, Qt::Orientation orientation, int role ) const;
    Qt::ItemFlags flags ( const QModelIndex& index ) const;

    AQLline* lineFromIndex(const QModelIndex& index) const;

    void clearModelData();
    bool canLogical( const QModelIndex& index, bool AQL_mode ) const;
    bool canOperator( const QModelIndex& index, bool AQL_mode ) const;
    bool canField( const QModelIndex& index, bool AQL_mode ) const;

    const QModelIndex addObject( const std::string& akey, bool isoperator, const QModelIndex& index );
    const QModelIndex addField( const std::string& flds_name,
                          jsonio::ThriftFieldDef* fld_data, const QModelIndex& index );
    void delObject( const QModelIndex& index );


    //?? queryType - EJDB query or SparQL or other
    //void setQuery( int /*queryType*/, const std::string& /*oldQuery*/ )
    //{
    //    //queryType; oldQuery;
    //}

    /// Make query std::string from internal model data
    jsonio::DBQueryData getQuery( int queryType, const std::string& collection );

    /// Make FILTER json object from internal model data
    std::string getFILTER();

    /// Set up internal model data from FILTER json string
    void setFILTER( const std::string& filter_generator );

};

/// \class TQueryDelegate
/// individual items in views are rendered and edited using delegates
class TQueryDelegate: public QItemDelegate
{
    Q_OBJECT

public:

    TQueryDelegate( QObject * parent = nullptr );
    QWidget *createEditor( QWidget *parent,
                           const QStyleOptionViewItem &option,
                           const QModelIndex &index) const;
};

///  \class TQueryView implements a tree view of query structure
/// that displays items from a TQueryModel model.
class TQueryView: public QTreeView
{
    Q_OBJECT

protected slots:
    void slotPopupContextMenu(const QPoint& pos);
    void changeCurrent( int section );

public slots:

    void CmAddObjects( const std::vector<std::string>& fields_list,
                       const std::vector<jsonio::ThriftFieldDef*>& fields_types );
    void CmAddField();

    void CmAddAnd()
    {  addObject( "AND", false );  }
    void CmAddOr()
    {  addObject( "OR", false );  }
    void CmAddNot()
    {  addObject( "NOT", false );  }
    void CmAddIn()
    {  addObject( "IN" );  }
    void CmAddNin()
    {  addObject( "NOT IN" );  }
    void CmAddLike()
    {  addObject( "LIKE" );  }
    void CmAddRegexp()
    {  addObject( "=~" );  }
    void CmAddNotRegexp()
    {  addObject( "!~" );  }
    void CmAddGt()
    {  addObject( ">" );  }
    void CmAddGte()
    {  addObject( ">=" );  }
    void CmAddLt()
    {  addObject( "<" );  }
    void CmAddLte()
    {  addObject( "<=" );  }
    void CmAddEq()
    {  addObject( "==" );  }
    void CmAddNotEq()
    {  addObject( "!=" );  }

    void CmDelLine();

    void AQLmode(bool checked)
    {
        if(checked != AQL_mode )
        {
            ClearAll();
            AQL_mode = checked;
        }
    }

 public:

    TQueryView( QWidget * parent = nullptr );

    void ClearAll()
    {
        TQueryModel* themodel =  dynamic_cast<TQueryModel*>(model());
        if( themodel)
            themodel->clearModelData();
    }

private:

    bool AQL_mode;
    void keyPressEvent(QKeyEvent* e);

    void addObject( const std::string& akey, bool isoperator = true );
    QMenu* contextMenu( const QModelIndex &index );

};

} // namespace jsonui

#endif   // _model_query_h
