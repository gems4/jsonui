//  This is JSONUI library+API (https://bitbucket.org/gems4/jsonui)
//
/// \file PreferencesJSONUI.cpp
/// Implementation dialog for define preferences into JSONUI
//
// JSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONUI depends on the following open-source software products:
// JSONIO (https://bitbucket.org/gems4/jsonio); Qt5 (https://qt.io).
//

#include <QFileDialog>
#include <QMessageBox>
#include "PreferencesJSONUI.h"
#include "ui_PreferencesJSONUI.h"
#include "HelpMainWindow.h"
#ifndef IMPEX_OFF
  #include "jsonimpex/dbclient.h"
#endif
using namespace jsonio;

namespace jsonui {

PreferencesJSONUI::PreferencesJSONUI( JsonioSettings& aset,QWidget *parent) :
    QDialog(parent),_iosettings(aset),
    ui(new Ui::PreferencesJSONUI)
{
    ui->setupUi(this);
    QValidator *validator = new QIntValidator( this );
    ui->portEdit->setValidator(validator);

    smbGroup = new QButtonGroup();
    smbGroup->setExclusive(true);
    smbGroup->addButton(ui->useLocal, 0);
    smbGroup->addButton(ui->useRemote, 1);
#ifndef IMPEX_OFF
    smbGroup->addButton(ui->useClient, 2);
#else
    ui->useClient->setDisabled(true);
#endif
    {
        ui->resourcesEdit->setText( _iosettings.value("common.ResourcesDirectory", std::string("")).c_str() );
        ui->schemasEdit->setText( _iosettings.value("common.SchemasDirectory", std::string("")).c_str() );
        ui->usersEdit->setText( _iosettings.value("common.WorkDirectoryPath", std::string(".")).c_str() );

        ui->commentBox->setChecked( _iosettings.value("jsonui.ShowComments", false) );
        ui->enumBox->setChecked( _iosettings.value("jsonui.ShowEnumNames", false) );
        ui->idBox->setChecked( _iosettings.value("jsonui.CanEdit_id", false) );
        ui->expandedBox->setChecked( _iosettings.value("jsonui.KeepExpanded", true) );

        ui->localUrl->setText( _iosettings.value("jsonio.ArangoDBLocal.DB_URL", std::string(local_server_endpoint)).c_str() );
        UpdateLists();
        ui->localDBName->setEditText( _iosettings.value("jsonio.ArangoDBLocal.DBName", std::string(local_server_database)).c_str() );
        ui->localUser->setEditText( _iosettings.value("jsonio.ArangoDBLocal.DBUser", std::string(local_server_username)).c_str() );
        ui->localPassword->setText( _iosettings.value("jsonio.ArangoDBLocal.DBUserPassword", std::string(local_server_password)).c_str() );
        ui->localRO->setChecked( _iosettings.value("jsonio.ArangoDBLocal.DBAccess", std::string("rw"))== "ro" );
        ui->newDatabase->setChecked( _iosettings.value("jsonio.ArangoDBLocal.DBCreate", false) );

        ui->remoteUrl->setText( _iosettings.value("jsonio.ArangoDBRemote.DB_URL", std::string(remote_server_endpoint)).c_str() );
        ui->remoteDBName->setText( _iosettings.value("jsonio.ArangoDBRemote.DBName", std::string(remote_server_database)).c_str() );
        ui->remoteUser->setText( _iosettings.value("jsonio.ArangoDBRemote.DBUser", std::string(remote_server_username)).c_str() );
        ui->remotePassword->setText( _iosettings.value("jsonio.ArangoDBRemote.DBUserPassword",
                                                      std::string(remote_server_password)).c_str() );
        ui->remoteRO->setChecked( _iosettings.value("jsonio.ArangoDBRemote.DBAccess", std::string("ro")) == "ro" );
#ifndef IMPEX_OFF
        ui->hostEdit->setText( _iosettings.value( "jsonimpex.ThriftDBSocketHost", defHost ).c_str() );
        ui->portEdit->setText( QString::number(_iosettings.value("jsonimpex.ThriftDBSocketPort", defPort )) );
#endif
        oldDBconnection = uiSettings().getCurrentDBConnection(); //_iosettings.value("jsonui.CurrentDBConnection", 0);
        smbGroup->button( oldDBconnection )->setChecked( true );
    }

    DBStateChanged(oldDBconnection);

    QObject::connect( ui->buttonBox, SIGNAL(accepted()), this, SLOT(CmSave()));
    QObject::connect( ui->buttonBox, SIGNAL(helpRequested()), this, SLOT(CmHelp()));
    QObject::connect( ui->usersButton, SIGNAL(clicked()), this, SLOT(CmProjectDir()));
    QObject::connect( ui->schemasButton, SIGNAL(clicked()), this, SLOT(CmSchemasDir()));
    QObject::connect( ui->resourcesButton, SIGNAL(clicked()), this, SLOT(CmResourcesDir()));
    QObject::connect( smbGroup, SIGNAL(buttonClicked(int)), this, SLOT(DBStateChanged(int)));
    QObject::connect( ui->localUrl, SIGNAL(editingFinished()), this, SLOT(UpdateLists()));

}

PreferencesJSONUI::~PreferencesJSONUI()
{
    delete ui;
}

void PreferencesJSONUI::DBStateChanged(int state)
{
    ui->localUrl->setEnabled(state==0);
    ui->localDBName->setEnabled(state==0);
    ui->localUser->setEnabled(state==0);
    ui->localPassword->setEnabled(state==0);
    ui->newDatabase->setEnabled(state==0);
    ui->localRO->setEnabled(state==0);

    ui->remoteUrl->setEnabled(state==1);
    ui->remoteDBName->setEnabled(state==1);
    ui->remoteUser->setEnabled(state==1);
    ui->remotePassword->setEnabled(state==1);
    ui->remoteRO->setEnabled(state==1);

    ui->hostEdit->setEnabled(state==2);
    ui->portEdit->setEnabled(state==2);
}

void PreferencesJSONUI::UpdateLists()
{
    try {  // could be not exist local ArangoDB or illegal root password

        ArangoDBConnect rootConnect( local_server_endpoint, local_root_username,  local_root_password,  local_root_database  );
        rootConnect.getFromSettings( _iosettings.groupArangoDB("ArangoDBLocal"), true );
        _rootclient.reset(  new jsonio::TArangoDBRootClient( rootConnect ) );

        auto users = _rootclient->getUserNames();
        ui->localUser->clear();
        for( auto user : users )
            ui->localUser->addItem(user.c_str());

        auto dbnames = _rootclient->getDatabaseNames();
        ui->localDBName->clear();
        for( auto name : dbnames )
            ui->localDBName->addItem(name.c_str());

    }
    catch(std::exception& e)
    {
        std::cout << "Error connection to localhost: " <<  e.what() << std::endl;
    }
}

void PreferencesJSONUI::CmSave()
{
    try {  // could be not exist local ArangoDB or illegal root password

        // save current settings
        _iosettings.setValue("common.ResourcesDirectory", ui->resourcesEdit->text().toStdString() );
        _iosettings.setValue("common.SchemasDirectory", ui->schemasEdit->text().toStdString() );
        _iosettings.setValue("jsonui.ShowComments",  ui->commentBox->isChecked() );
        _iosettings.setValue("jsonui.ShowEnumNames",  ui->enumBox->isChecked() );
        _iosettings.setValue("jsonui.CanEdit_id",  ui->idBox->isChecked() );
        _iosettings.setValue("jsonui.KeepExpanded",  ui->expandedBox->isChecked() );
        _iosettings.setValue("jsonui.CanEditDocPages", HelpMainWindow::editHelp );

        // setup global values
        _iosettings.setUserDir( ui->usersEdit->text().toStdString() );
        uiSettings().updateUiSettings();

        std::string localDatabase = ui->localDBName->currentText().toStdString();
        std::string localUser = ui->localUser->currentText().toStdString();
        std::string localPassword = ui->localPassword->text().toStdString();
        bool createlocalDB = ui->newDatabase->isChecked();
        int newDBConnection = smbGroup->checkedId();

        if( newDBConnection == 0  && createlocalDB )
        {
            // root url must be the same as  LocalArangoDBUrl
            _rootclient->CreateDatabase( localDatabase, { ArangoDBUser(localUser, localPassword) });
        }

        // do not change if exeption up

        _iosettings.setValue("jsonio.ArangoDBLocal.DB_URL",   ui->localUrl->text().toStdString() );
        _iosettings.setValue("jsonio.ArangoDBLocal.DBName",  localDatabase );
        _iosettings.setValue("jsonio.ArangoDBLocal.DBUser",   localUser );
        _iosettings.setValue("jsonio.ArangoDBLocal.DBUserPassword",   localPassword );
        std::string localUserAccess = ( ui->localRO->isChecked()? "ro":"rw");
        _iosettings.setValue("jsonio.ArangoDBLocal.DBAccess",   localUserAccess );
        _iosettings.setValue("jsonio.ArangoDBLocal.DBCreate", createlocalDB );

        _iosettings.setValue("jsonio.ArangoDBRemote.DB_URL",   ui->remoteUrl->text().toStdString() );
        _iosettings.setValue("jsonio.ArangoDBRemote.DBName",   ui->remoteDBName->text().toStdString() );
        _iosettings.setValue("jsonio.ArangoDBRemote.DBUser",   ui->remoteUser->text().toStdString() );
        _iosettings.setValue("jsonio.ArangoDBRemote.DBUserPassword",   ui->remotePassword->text().toStdString() );
        std::string remoteUserAccess = ( ui->remoteRO->isChecked()? "ro":"rw");
        _iosettings.setValue("jsonio.ArangoDBRemote.DBAccess",   remoteUserAccess );
        _iosettings.setValue("jsonimpex.ThriftDBSocketHost",  ui->hostEdit->text().toStdString() );
        _iosettings.setValue("jsonimpex.ThriftDBSocketPort",   ui->portEdit->text().toInt() );

        uiSettings().setCurrentDBConnection( newDBConnection );
        //_iosettings.setValue("jsonui.CurrentDBConnection", newDBConnection);

        // test to reset db driver => close all windows (closeAll) or only (updateDB)
        if( (newDBConnection != oldDBconnection) ||        // Move to new Driver type
                uiSettings().testCurrentDriver(oldDBconnection) )   // Move to new dbname or user
        {
            uiSettings().updateDatabase();
            emit dbdriveChanged();
        }
    }
   catch(std::exception& e)
    {
        QMessageBox::critical( this, "Preferences update", (std::string("Error connection to localhost: \n")+e.what()).c_str() );
    }
    accept();
}

void PreferencesJSONUI::CmProjectDir()
{
    QString dir = QFileDialog::getExistingDirectory(this, "Select Project Directory",
     "",  QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks );
    ui->usersEdit->setText( dir );
}

void PreferencesJSONUI::CmSchemasDir()
{
    QString dir = QFileDialog::getExistingDirectory(this, "Select Schemas Directory",
     "",  QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks );
    ui->schemasEdit->setText( dir );
}

void PreferencesJSONUI::CmResourcesDir()
{
    QString dir = QFileDialog::getExistingDirectory(this, "Select Resource Directory",
     "",  QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks );
    ui->resourcesEdit->setText( dir );
}


void PreferencesJSONUI::CmHelp()
{
   helpWin( "PreferencesJsonui", "" );
}

} // namespace jsonui

// ------------------------ end of PreferencesJSONUI.cpp ------------------------------------------
