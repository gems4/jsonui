//  This is JSONUI library+API (https://bitbucket.org/gems4/jsonui)
//
/// \file model_table.h
/// Declarations of classes TAbstractDataContainer, TMatrixModel,
/// TMatrixDelegate and TMatrixTable - implements flexible approach
/// provided by Qt's model/view architecture according TAbstractDataContainer
/// interface.
//
// JSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytrieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONUI depends on the following open-source software products:
// JSONIO (https://bitbucket.org/gems4/jsonio); Qt5 (https://qt.io).
//

#ifndef model_table_h
#define model_table_h

#include <QAbstractTableModel>
#include <QItemDelegate>
#include <QTableView>
#include "jsonio/nservice.h"


namespace jsonui {

QVector<QString> fromStdStringVector(const std::vector<std::string>& strvector );
std::vector<std::string> toStdStringVector(const QVector<QString>& strvector );
QVector<QVector<QString>> fromStdStringTable(const jsonio::ValuesTable& strtable );
jsonio::ValuesTable toStdStringTable(const QVector<QVector<QString>>& strtable );

class Selection;

/// Item types into table
enum DATA_TYPES {
    ftString = 0,
    ftDouble = 1,
    ftNumeric = 2
};

/// Interface for table data container
class TAbstractDataContainer
{
protected:
    QString tableName;

public:

    TAbstractDataContainer( const char * aname ):
        tableName(aname)
    { }

    virtual ~TAbstractDataContainer();


    // data to model
    virtual int rowCount() const = 0;
    virtual int columnCount() const = 0;
    virtual QVariant data( int line, int column ) const = 0;
    virtual bool setData( int line, int column, const QVariant & value ) = 0;
    virtual QString headerData ( int section ) const = 0;
    virtual bool  IsEditable( int /*line*/, int /*column*/ ) const
    { return true; }
    virtual int getType( int /*line*/, int /*column*/ ) const
    { return ftString; }
    virtual QString getToolTip( int line, int column ) const
    { return QString("%1[%2,%3]").arg(tableName).arg(line).arg(column); }
    virtual bool useColor( int /*line*/, int /*column*/ ) const
    { return false; }
    virtual QColor getColor( int /*line*/, int /*column*/ ) const
    { return QColor(Qt::black); }

    virtual void resetData() = 0;

};


enum NewRole{

    TypeRole = Qt::UserRole
};

///  \class TMatrixModel
/// class for represents the data set and is responsible for fetchin
/// the data is neaded for viewing and for writing back any changes.
/// Reading/writing data from/to inherited classes from TAbstractDataContainer interface
class TMatrixModel: public QAbstractTableModel
{
    friend class TMatrixDelegate;
    friend class TMatrixTable;

    Q_OBJECT

    TAbstractDataContainer *datacontainer;

public:

    TMatrixModel( TAbstractDataContainer *adata, QObject * parent = nullptr ):
        QAbstractTableModel(parent), datacontainer(adata)
    {
        if( parent )
            connect( this, SIGNAL(dataChanged( const QModelIndex&, const QModelIndex& )),
                     parent,  SLOT(objectChanged()) );
    }
    ~TMatrixModel()
    { }

    int rowCount( const QModelIndex& /*parent*/ ) const
    {
        return datacontainer->rowCount();
    }
    int columnCount ( const QModelIndex& /*parent*/  ) const
    {
        return datacontainer->columnCount();
    }

    QVariant data ( const QModelIndex & index, int role ) const;
    bool setData ( const QModelIndex & index, const QVariant & value, int role );
    QVariant headerData ( int section, Qt::Orientation orientation, int role ) const;
    Qt::ItemFlags flags( const QModelIndex & index ) const;

    void resetMatrixData();
};


///  \class TMatrixDelegate
/// individual items in views are rendered and edited using delegates
class TMatrixDelegate: public QItemDelegate
{
    Q_OBJECT

public:

    TMatrixDelegate(    QObject * parent = nullptr ):
        QItemDelegate( parent )
    { }
    QWidget *createEditor(QWidget *parent,
                          const QStyleOptionViewItem &option,
                          const QModelIndex &index) const;
};


///  \class TMatrixTable implements a table view
/// that displays items from a TMatrixModel model.
class TMatrixTable: public QTableView
{
    Q_OBJECT

    Selection getSelectionRange( bool paste_ = false );
    QString createString( Selection& sel, char _splitCol );
    QString createHeader(char _splitCol);
    void pasteIntoArea( Selection& sel, bool transpose, char _splitCol);
    void setFromString(char splitrow, const QString& str,
                       Selection sel, bool transpose);

protected:
    int _modeUsing;
    void keyPressEvent(QKeyEvent* e);

protected slots:
    void slotPopupContextMenu(const QPoint& pos);

public slots:
    void SelectRow();
    void SelectColumn();
    void SelectAll();
    void CopyData();
    void CopyDataHeader();
    // only if editable
    void CmCalc();
    void CutData();
    void ClearData();
    void PasteData();
    void PasteTransposedData();

public:

    /// Types of Matrix table mode
    enum TABLE_TYPES {
        tbSort = 0x0010,  ///< Added sorting into colums
        tbGraph = 0x0004, ///< Connect 2d graphic for columns
        tbEdit = 0x0002,  ///< Enable editing
        tbNoMenu = 0x0001, ///< Disable context menu
        tbShow = 0         ///< Use only show mode
    };


    TMatrixTable( QWidget * parent = nullptr, int mode = tbEdit );

    /// Save selected data to csv format file
    void saveSelectedToCSV(const std::string& fileName);
    /// Save all table data to csv format file
    void saveAllToCSV(const std::string& fileName)
    {
        selectAll();
        saveSelectedToCSV( fileName );
        clearSelection();
    }
    /// Load new table from csv format file
    void loadAllFromCSV(const std::string& /*fileName*/)
    {}

    void makePopupContextMenu(QMenu *menu, QModelIndex index );

    /// Get header data
    QVector< QString > headerData() const;

    /// Get table data
    QVector< QVector<QVariant> > tableValues() const;

};

///  \class TMatrixUniqueSelection implements a table view
/// that displays items from a TMatrixModel model.
/// Can be selected only unique values into defined column.
class TMatrixTableProxy: public TMatrixTable
{
    Q_OBJECT

public:

    TMatrixTableProxy(  QWidget * parent = nullptr, int mode = tbEdit );

    /// Add proxy model
    void setModel(QAbstractItemModel *model);

    /// Set&select current row
    virtual void setCurrentRow( int row );
    /// Returns single selection,
    /// Throw exeption if nothing selected
    virtual std::size_t getCurrentRow() const;
    /// Returns single selection,
    /// returns '-1' if nothing selected
    virtual int selectedRow() const;


    /// Set selected rows
    virtual void selectRows( const std::set<std::size_t>& rows );
    /// Return all selected rows
    virtual std::set<std::size_t> allSelectedRows();

    /// Set Multiple selection
    template<typename T,
             template <typename, typename = std::allocator<T>> class Container>
    void setSelection( const Container<T>& selrows )
    {
        std::set<size_t> sellst;
        for (auto it = selrows.begin(); it != selrows.end(); ++it)
        {
            sellst.insert(static_cast<size_t>(*it));
        }
        selectRows( sellst );
    }

};

///  \class TMatrixUniqueSelection implements a table view
/// that displays items from a TMatrixModel model.
/// Can be selected only unique values into defined column.
class TMatrixUniqueSelection: public TMatrixTableProxy
{
    Q_OBJECT

    std::vector<int> _columnUnique;

protected slots:

    void selectionChanged(const QItemSelection &selected, const QItemSelection &deselected)
    {
        TMatrixTable::selectionChanged( selected, deselected);
        slotSelectionChange(selected, deselected);

    }
    void slotSelectionChange(const QItemSelection &, const QItemSelection &);

public:

    TMatrixUniqueSelection(  const std::vector<int>& colUniq, QWidget * parent = nullptr, int mode = tbEdit );

    // Return all selected rows
    //std::set<std::size_t> allSelectedRows();
};


/// Class for numeric vector container
template< class T>
class TVectorContainer : public TAbstractDataContainer
{
    int type;
    QString  _key;
    QVector<double> _fields;

public:

    TVectorContainer( const char * aname, const std::string& akey, std::vector<T>& afields ):
        TAbstractDataContainer(aname),
        _key(akey.c_str()), _fields( QVector<T>::fromStdVector(afields) )
    {
        if( std::is_integral<T>::value )
            type = ftNumeric;
        else if( std::is_floating_point<T>::value )
            type = ftDouble;
        else
            type = ftString;

    }

    virtual ~TVectorContainer() {}

    int rowCount() const
    { return _fields.size();  }

    int columnCount() const
    { return 1; }

    QVariant data( int line, int /*column*/ ) const
    { return _fields[line];  }

    bool setData( int line, int /*column*/, const QVariant &value )
    {
        _fields[line] = value;
        return true;
    }

    virtual QString headerData ( int /*section*/ ) const
    {  return _key;   }

    virtual bool  IsEditable( int /*line*/, int /*column*/ ) const
    {  return true;  }

    virtual int getType( int /*line*/, int /*column*/ ) const
    {  return type;  }

    virtual QString getToolTip( int /*line*/, int /*column*/ ) const
    { return _key; }

    void resetData()
    { }
};

/// Class for internal table data container
class StringTable : public TAbstractDataContainer
{
    QVector< QString > colHeads;
    jsonio::ValuesTable matrix;
    bool isEditable = false;

public:

    /// Construct table from list of headers.
    StringTable( const char * aname, const std::vector<std::string>& heads ):
        TAbstractDataContainer(aname), colHeads (fromStdStringVector(heads))
    {}

    /// Construct table from 2D array.
    StringTable( const char * aname, const std::vector<std::string>& heads,
                 const jsonio::ValuesTable& values ):
        TAbstractDataContainer(aname), colHeads(fromStdStringVector(heads))
    {
        for( auto val : values )
            addRow( val );
    }

    virtual ~StringTable();

    void setEditable( bool editable )
    {
        isEditable = editable;
    }

    const jsonio::ValuesTable&  getValues() const
    {
        return matrix;
    }

    void updateValues( const jsonio::ValuesTable& table )
    {
        matrix = table;
    }

    void updateHeads( const std::vector<std::string>& heads )
    {
        colHeads = fromStdStringVector(heads);
    }

    void addRow( const std::vector<std::string>& linevalues )
    {
        if( static_cast<int>(linevalues.size()) >= columnCount() )
            matrix.push_back( linevalues );
    }

    void addRow( std::initializer_list<std::string> list )
    {
        std::vector<std::string> vec(list);
        addRow(vec);
    }

    template<typename... T>
    void addRow(T&&... params)
    {
        std::vector<std::string> vec = {params...};
        addRow(vec);
    }

    std::vector<std::string> getColumn( int columni ) const
    {
        std::vector<std::string> keys;
        size_t column =static_cast<size_t>( columni);

        for( auto row: matrix)
            if( column >= row.size() )
                keys.push_back( "" );
            else
                keys.push_back( row[column] );
        return keys;
    }

    const std::vector<std::string>& getRow( int row ) const
    {
        if( row >= rowCount() )
            jsonio::jsonioErr( "getRow", "Illegal row");
        return  matrix[static_cast<size_t>(row)];
    }

    // data to model ---------------------------

    int rowCount() const
    { return static_cast<int>(matrix.size());  }

    int columnCount() const
    { return colHeads.size(); }

    QVariant data( int line, int column ) const
    {  return matrix[static_cast<size_t>(line)][static_cast<size_t>(column)].c_str(); }

    bool setData( int line, int column, const QVariant & value )
    {
        matrix[static_cast<size_t>(line)][static_cast<size_t>(column)] = value.toString().toStdString();
        return true;
    }

    virtual QString headerData ( int section ) const
    { return colHeads[section]; }

    virtual bool  IsEditable( int /*line*/, int /*column*/ ) const
    { return isEditable; }

    virtual int getType( int /*line*/, int /*column*/ ) const
    { return ftString; }

    virtual QString getToolTip( int line, int /*column*/ ) const
    { return QString("%1").arg(line); }

    void resetData()
    { }

};


/// Function fetching document from a collection that match the specified condition
using  GetColorFunction_f = std::function<QColor( int row, int col )>;

/// Class for colorized internal table data container
class ColorStringTable : public StringTable
{

    bool use_color_function = false;
    GetColorFunction_f color_function;

public:

    /// Construct table from list of headers.
    ColorStringTable( const char * aname, const std::vector<std::string>& heads ):
        StringTable(aname, heads)
    {
        color_function =  []( int, int ) {  return QColor(Qt::black);  };
    }

    /// Construct table from 2D array.
    ColorStringTable( const char * aname, const std::vector<std::string>& heads,
                 const jsonio::ValuesTable& values ):
        StringTable(aname, heads, values )
    {
        color_function =  []( int, int ) {  return QColor(Qt::black);  };
    }

    void setColorFunction( GetColorFunction_f func )
    {
      use_color_function = true;
      color_function = func;
    }

    virtual bool useColor( int /*line*/, int /*column*/ ) const
    { return use_color_function; }
    virtual QColor getColor( int line, int column ) const
    { return color_function(line, column); }

};

// -----------------------------------------------  List model


///  \class TStringVectorModel
/// class for represents the data set and is responsible for fetchin
/// the data is neaded for viewing and for writing back any changes.
/// Reading/writing data from/to string vector
class TStringVectorModel : public QAbstractListModel
{
    Q_OBJECT

    QVector<QString> lst;

public:

    explicit TStringVectorModel(const std::vector<std::string>& strs, QObject *parent = nullptr):
        QAbstractListModel(parent), lst(fromStdStringVector(strs))
    {}

    int rowCount(const QModelIndex &parent = QModelIndex()) const
    {
        if (parent.isValid())
            return 0;
        return lst.size();
    }

    QVariant data(const QModelIndex &index, int role) const;

    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);

    std::vector<std::string> stringList() const
    {
        return toStdStringVector(lst);
    }
    void setStringList(const std::vector<std::string>& strs);

};


} // namespace jsonui

#endif   // model_table_h
