//  This is JSONUI library+API (https://bitbucket.org/gems4/jsonui)
//
/// \file VertexWidget.h
/// Declaration of class VertexWidget - Widget to work with vertex -
/// internal DOM based on our JSON schemas data
//
// JSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONUI depends on the following open-source software products:
// JSONIO (https://bitbucket.org/gems4/jsonio); Qt5 (https://qt.io).
//

#ifndef VERTEXWIDGET_H
#define VERTEXWIDGET_H

#include <QCloseEvent>
#include <QThread>
#include "JSONUIBase.h"
#include "model_schema.h"
#include "dbdocumentmodel.h"

namespace Ui {
class VertexWidget;
}

class WaitingSpinnerWidget;

namespace jsonui {

/// \class VertexWidget - window to work with Vertexes (TDBGraph)
class VertexWidget : public JSONUIBase
{
    Q_OBJECT

    void closeEvent(QCloseEvent* e);

    bool event(QEvent * e); // overloading event(QEvent*) method of QMainWindow

    // update after change preferences
    virtual void updtViewMenu();
    virtual void updtModel();
    virtual void updtTable();
    virtual void updtDB();

signals:

    /// Emit signal, when delete record(s)
    void vertexDeleted();

    /// Emit signal, when loaded graph
    void graphLoaded();

    // signals to DBDocumentModel

    /// Reset current DB client
    void cmResetClient(  QString aschemaName );

    // Reload query
    void cmReloadQuery();

    /// Open document
    void cmReadDocument(  QString reckey  );

    /// Save json Document to database
    void cmUpdate( QString jsonDocument );

    /// Delete keyDocument document from database
    void cmDelete( QString keyDocument );

    /// Delete documents from database
    void cmDeleteList( QVector<QString> keysDocument );

    /// Read multiple records from file fileName
    void cmRestoreMultipleRecordsfromFile( QString fileName );

    /// Write multiple records to file fileName
    void cmBackupQueriedRecordstoFile( QString fileName, QVector<QString> keysDocument);

#ifndef IMPEX_OFF
    /// Read json records from format file fileName
    void cmImportFormat( std::shared_ptr<jsonio::ImpexFormatFile> inputIEFile,
                         QString dataFile, bool overwrite  );

    /// Write json record to format file fileName
    void cmExportFormat( std::shared_ptr<jsonio::ImpexFormatFile> inputIEFile,
                         QString dataFile, QVector<QString> keysDocument );
#endif

    /// Write graph to file fileName
    void cmBackupGraphtoFile(QString fileName, std::vector<std::string> keysDocument);

    /// Read multiple records from file fileName
    void cmRestoreGraphfromFile( QString fileName );

    /// Update query
    void cmChangeQueryToId( QString id );

    /// Update query
    void cmResetInOutQuery( QString id );


public slots:

    void openRecordKey(  const std::string& key, bool resetInOutQuery = false  );
    void openRecordKey(  const QModelIndex& index );
    void changeKeyList()
    {
       std::string key = getKeyFromDom( model_schema->getDom() );
       dbKeysModel->resetModel( key.c_str() );
    }
    void updateQuery()
    {
      startProcess();
      emit cmReloadQuery();
    }

    // Record
    void CmRead();
    void CmUpdate();
    void CmDelete();
#ifndef IMPEX_OFF
    void CmImportFormat();
    void CmExportFormat();
#endif
    void CmDeleteSelect();
    void CmDisplaySearchResult();
    void CmSearchQuery();
    void CmBackupQueriedRecordstoFile();
    void CmRestoreMultipleRecordsfromFile();
    void CmBackupGraphtoFile();
    void CmRestoreGraphfromFile();

    // File
    void CmNew();
    void CmClone();
    void CmExportJSON();
    void CmImportJSON();

    // View
    void CmFollowInEdges();
    void CmFollowOutEdges();

protected slots:

    void objectChanged()
    {
        contentsChanged = true;
    }

    /// Start long database process
    void startProcess();

    /// Finish long database process
    void finishProcess();

    /// Exception when database command execution
    void get_exception( const QString& titl, const QString& msg );

    /// Update oid
    void updateOid( QString oid )
    {
        model_schema->setOid(oid.toStdString());
        if( oid.isEmpty() )
            model_schema->setValue("_rev", std::string(""));
        fieldTable->hide();
        fieldTable->show();
    }

    /// After open new document
    void openDocument( QString jsonDocument  );

    /// Update db document query
    void changeDBClientData(  QString query );

    /// Possible delete edges
    void afterDeleteDocument()
    {
        contentsChanged = false;
        emit vertexDeleted();
    }

    /// Load records to different collections
    void afterLoadGraph()
    {
         emit graphLoaded();
    }

public:

    explicit VertexWidget( const std::string& aschemaName, QWidget *parent = nullptr);
    ~VertexWidget();

    /// Update query
    void setQuery( QueryWidget* queryW  );

    std::string currentKey() const
    {
      return  getKeyFromDom( model_schema->getDom() );
    }

private:

    // widget data

    /// VertexWidget.ui description
    Ui::VertexWidget *ui;

    // tree view editor

    /// Tree view editor model
    TJsonSchemaModel* model_schema;
    /// Tree view editor delegate
    QItemDelegate *deleg_schema;
    /// Tree view editor widget
    TJsonView* fieldTable;

    // database API

    /// Database connection object
    DBDocumentModel* dbDocumentModel=nullptr;
    // std::shared_ptr<jsonio::TDBVertexDocument> dbgraph;
    QThread dbThread;

    /// Database keys list model
    DBKeysListModel *dbKeysModel;
    /// Keys list table
    TKeyTable *keysTable;

    // Internal data
    std::string curRecord_;
    bool contentsChanged = false;
    WaitingSpinnerWidget *waitDialog;

    /// Set up menu commands
    void setActions();

    /// Set up current bson data to view model
    bool resetBson( const std::string& curRecord, const std::string& schemaName );

    /// Reset current DB client
    void resetDBClient(const std::string& schemaName );

    /// Get current record key from dom structure
    std::string getKeyFromDom( const jsonio::JsonDom* domdata ) const;

};

} // namespace jsonui

#endif // VERTEXWIDGET_H
