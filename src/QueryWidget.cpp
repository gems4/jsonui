//  This is JSONUI library+API (https://bitbucket.org/gems4/jsonui)
//
/// \file QueryWidget.cpp
/// Implementation of class QueryWidget  - widget to work with database queries
//
// JSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONUI depends on the following open-source software products:
// JSONIO (https://bitbucket.org/gems4/jsonio); Qt5 (https://qt.io).
//

#include <QMessageBox>
#include "QueryWidget.h"
#include "ui_QueryWidget.h"
#include "SchemaSelectDialog.h"
#include "SelectionKeysForm.h"
#include "SelectDialog.h"
#include "JSONUIBase.h"
#include "model_query.h"
#ifndef IMPEX_OFF
  #include "jsonimpex/yamlxml2file.h"

#include "QueryWidget.h"
#endif
using namespace jsonio;

namespace jsonui {

QueryWidget::QueryWidget(const char* title, const std::vector<std::string>& schemaNames,
                         const DBQueryDef& query,  QWidget *parent):
    QMainWindow(parent),  schema_name(schemaNames[0]),
    default_query( query),  query_description( query ),
    ui(new Ui::QueryWidget), resultWidget(parent)
{
    ui->setupUi(this);
    setWindowTitle(title);

    ui->splitterSelect->setStretchFactor(0, 1);
    ui->splitterSelect->setStretchFactor(1, 2);

    for( uint ii=0; ii<schemaNames.size(); ii++ )
        ui->qSchema->addItem(schemaNames[ii].c_str());

    // set up select model
    lstmodel = new TStringVectorModel( query_description.getFieldsCollect(), this );
    ui->fieldsList->setModel(lstmodel);
    //ui->fieldsList->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->fieldsList->setSelectionMode(QAbstractItemView::MultiSelection);
    ui->fieldsList->setEditTriggers( QAbstractItemView::NoEditTriggers );

    // set up query edit mode
    queryEdit =  new TQueryView( ui->frameTree  );
    queryModel = new TQueryModel( this );
    TQueryDelegate*  deleg_ =  new TQueryDelegate( this );
    queryEdit->setModel(queryModel);
    queryEdit->setItemDelegate(deleg_);
    queryEdit->setColumnWidth( 0, 350 );
    ui->gridLayout_2->addWidget(queryEdit, 1, 0, 1, 1);

    // connect commands
    setActions();
    resetDBClient();

    // set up where part
    showQuery();
}

QueryWidget::~QueryWidget()
{
    delete queryEdit;
    delete ui;
}


void QueryWidget::UpdateQuery( const std::vector<std::string>& schemaNames, const DBQueryDef& query )
{
    default_query = query;
    query_description = query;

    ui->qSchema->clear();
    for( uint ii=0; ii<schemaNames.size(); ii++ )
        ui->qSchema->addItem(schemaNames[ii].c_str());

    showQuery();
}

//  Connect all actions
void QueryWidget::setActions()
{
    connect( ui->qSchema, SIGNAL(currentIndexChanged(const QString&)),
             this, SLOT(schemaChanged(const QString&)));
    connect( ui->pAQL, SIGNAL(toggled(bool)), this, SLOT(AQLtoggled(bool)));
    connect( ui->pAQL,  &QRadioButton::toggled,  [&](bool) { ui->queryText->setText(""); });
    connect( ui->pTemplate,  &QRadioButton::toggled,  [&](bool) { ui->queryText->setText(default_query.getQueryCondition().getQueryString().c_str());});

    // File
    connect( ui->action_Save_Current_Query_to_File, SIGNAL(triggered()),this, SLOT(CmSaveQuery()));
    connect( ui->action_Load_Query_from_File, SIGNAL( triggered()), this, SLOT(CmLoadQuery()));
    connect( ui->action_Execute_Current_Query, SIGNAL( triggered()), this, SLOT(CmExecute()));
    connect( ui->action_Accept_curent_Query, SIGNAL( triggered()), this, SLOT(CmApplay()));
    connect( ui->action_Close_Cancel, SIGNAL( triggered()), this, SLOT(close()));

    // Select part
    connect( ui->action_Select_Fieldpaths, SIGNAL( triggered()), this, SLOT(CmSelectFields()));
    connect( ui->action_Add_Fieldpaths, SIGNAL( triggered()), this, SLOT(CmAddSelectFields()));
    connect( ui->action_restote_select, SIGNAL( triggered()), this, SLOT(CmSelectRestore()));

    // Generate part
    connect(ui->action_Apply_from_Tree, SIGNAL(triggered()), this, SLOT(CmWhereApply()));
    connect(ui->action_Restore_Query, SIGNAL(triggered()), this, SLOT(CmWhereRestore()));
    connect(ui->action_Clear_Tree, SIGNAL(triggered()), this, SLOT(CmClearTree()));

    // FILTER
    connect(ui->actionAdd_List_Fieldpaths, SIGNAL(triggered()), this, SLOT(CmAddWhereFields()));
    connect(ui->actionAdd_Fieldpath, SIGNAL(triggered()), queryEdit, SLOT(CmAddField()));

    connect(ui->actionAdd_and, SIGNAL(triggered()), queryEdit, SLOT(CmAddAnd()));
    connect(ui->actionAdd_or, SIGNAL(triggered()), queryEdit, SLOT(CmAddOr()));
    connect(ui->actionAdd_not, SIGNAL(triggered()), queryEdit, SLOT(CmAddNot()));

    connect(ui->actionAdd_eq, SIGNAL(triggered()), queryEdit, SLOT(CmAddEq()));
    connect(ui->actionAdd_noteq, SIGNAL(triggered()), queryEdit, SLOT(CmAddNotEq()));
    connect(ui->actionAdd_gt, SIGNAL(triggered()), queryEdit, SLOT(CmAddGt()));
    connect(ui->actionAdd_gte, SIGNAL(triggered()), queryEdit, SLOT(CmAddGte()));
    connect(ui->actionAdd_lt, SIGNAL(triggered()), queryEdit, SLOT(CmAddLt()));
    connect(ui->actionAdd_lte, SIGNAL(triggered()), queryEdit, SLOT(CmAddLte()));

    connect(ui->actionAdd_in, SIGNAL(triggered()), queryEdit, SLOT(CmAddIn()));
    connect(ui->actionAdd_nin, SIGNAL(triggered()), queryEdit, SLOT(CmAddNin()));
    connect(ui->actionAdd_like, SIGNAL(triggered()), queryEdit, SLOT(CmAddLike()));
    connect(ui->actionAdd_regexp, SIGNAL(triggered()), queryEdit, SLOT(CmAddRegexp()));
    connect(ui->actionAdd_not_regexp, SIGNAL(triggered()), queryEdit, SLOT(CmAddNotRegexp()));

    connect(ui->actionDelete_Line, SIGNAL(triggered()),  queryEdit, SLOT(CmDelLine()));

    // Database
    connect( ui->action_Update, SIGNAL( triggered()), this, SLOT(CmUpdate()));
    connect( ui->action_Clone, SIGNAL( triggered()), this, SLOT(CmClone()));
    connect( ui->action_DeleteList, SIGNAL( triggered()), this, SLOT(CmDeleteList()));
    connect( ui->action_Read, SIGNAL( triggered()), this, SLOT(CmRead()));

    // Help
    connect(ui->actionHelp_About, SIGNAL(triggered()), this, SLOT(CmHelpAbout()));
    connect(ui->action_Help, SIGNAL(triggered()), this, SLOT(CmHelp()));

    qId = new QLineEdit( ui->nameToolBar );
    qId->setEnabled( true );
    qId->setReadOnly( true );
    qId->setFocusPolicy( Qt::StrongFocus );
    qId->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed );
    QString title =  "";
    qId->setText(title);
    ui->nameToolBar->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed );
    QLabel *aname = new QLabel( "_id",ui->nameToolBar );
    ui->nameToolBar->addWidget( aname );
    ui->nameToolBar->addWidget( qId ); // setStretchableWidget( pLine );
}


bool QueryWidget::selectLine( std::string& fldpath, jsonio::ThriftFieldDef** flddata )
{
    SchemaSelectDialog dlg( this,
                            "Please, mark data fields for the query table", schema_name );
    if( !dlg.exec() )
        return false;
    fldpath = dlg.selIndex(flddata);
    return !fldpath.empty();
}


void QueryWidget::schemaChanged(const QString & text)
{
    if( schema_name != text.toStdString())
    {
        schema_name = text.toStdString();
        dbquery->SetQuery(newSchemaQuery());
    }
}

void QueryWidget::AQLtoggled(bool checked)
{
    ui->actionAdd_and->setEnabled(checked);
    ui->actionAdd_or->setEnabled(checked);
    ui->actionAdd_not->setEnabled(checked);

    ui->actionAdd_gt->setEnabled(checked);
    ui->actionAdd_gte->setEnabled(checked);
    ui->actionAdd_lt->setEnabled(checked);
    ui->actionAdd_lte->setEnabled(checked);
    ui->actionAdd_noteq->setEnabled(checked);
    ui->actionAdd_eq->setEnabled(checked);

    ui->actionAdd_in->setEnabled(checked);
    ui->actionAdd_nin->setEnabled(checked);
    ui->actionAdd_like->setEnabled(checked);
    ui->actionAdd_regexp->setEnabled(checked);
    ui->actionAdd_not_regexp->setEnabled(checked);

    ui->actionAdd_List_Fieldpaths->setEnabled(!checked);
    queryEdit->AQLmode(checked);
}

// menu ----------------------------------------------------------

void QueryWidget::CmSaveQuery()
{
    try {
        // set data to dom
        std::string jsonquery = queryToJson();
        std::shared_ptr<JsonDomFree> domdata(JsonDomFree::newObject());
        parseJsonToNode( jsonquery, domdata.get());

        // save query to file
        std::string fileName = "*query.json";
        if(  ChooseFileSave( this, fileName,
                             "Please, select a file to save current query", domFilters, fileName  ))
        {
#ifndef IMPEX_OFF
            FJsonYamlXml file( fileName);
#else
            FJson file( fileName);
#endif
            file.SaveJson( domdata.get() );
        }
    }
    catch(jsonio_exception& e)
    {
        QMessageBox::critical( this, e.title(), e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}


void QueryWidget::CmLoadQuery()
{
    try{
        // Sellect file name
        std::string fileName;
        if(  !ChooseFileOpen( this, fileName,
                              "Please, select a file with saved query to execute", domFilters  ))
            return;

        // read data to dom
        std::shared_ptr<JsonDomFree> domdata(JsonDomFree::newObject());
#ifndef IMPEX_OFF
        FJsonYamlXml file( fileName);
#else
        FJson file( fileName);
#endif
        file.LoadJson( domdata.get() );
        // setup data from dom
        queryFromNode( domdata.get() );
    }
    catch(jsonio_exception& e)
    {
        QMessageBox::critical( this, e.title(), e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Clone current query
void QueryWidget::CmClone()
{
    try
    {
        qId->setText("");
    }
    catch(jsonio_exception& e)
    {
        QMessageBox::critical( this, e.title(), e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}


/// Read new record from DB
void QueryWidget::CmRead()
{
    try
    {
        if( dbquery.get() == nullptr )
            return;

        // Select keys to load
        std::vector<std::string> aKeyList;
        std::vector<std::vector<std::string>> aValList;
        dbquery->lastQueryData()->getKeyValueList( aKeyList, aValList );
        if( aKeyList.empty() )
            return;

        SelectDialog selDlg( false, this, "Please, select a record to read/view", aValList );
        if( !selDlg.exec() )
            return;

        std::string reckey = aKeyList[selDlg.getSelectedIndex()];
        // Read Record
        dbquery->Read( reckey );
        queryFromNode( dbquery->getDom() );
    }
    catch(jsonio_exception& e)
    {
        QMessageBox::critical( this, e.title(), e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Save current record to DB file
void QueryWidget::CmUpdate()
{
    try
    {
        if( dbquery.get() == nullptr )
            return;
        std::string jsonquery = queryToJson();
        dbquery->UpdateFromJson( jsonquery );
    }
    catch(jsonio_exception& e)
    {
        QMessageBox::critical( this, e.title(), e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Delete current record from DB
void QueryWidget::CmDeleteList()
{
    try
    {
        if( dbquery.get() == nullptr )
            return;

        // Select keys to delete
        std::vector<std::string> aKeyList;
        std::vector<std::vector<std::string>> aValList;
        dbquery->lastQueryData()->getKeyValueList( aKeyList, aValList );
        if( aKeyList.empty() )
            return;

        SelectDialog selDlg( true, this, "Please, select documents to delete", aValList,{},
                             TMatrixTable::tbNoMenu|TMatrixTable::tbSort );
        if( !selDlg.exec() )
            return;

        std::vector<std::size_t> selNdx;
        selDlg.getSelection( selNdx );
        for( std::size_t ii=0; ii<selNdx.size(); ii++ )
        {
            dbquery->Delete( aKeyList[selNdx[ii]] );
        }
    }
    catch(jsonio_exception& e)
    {
        QMessageBox::critical( this, e.title(), e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}


void QueryWidget::CmExecute()
{
    try
    {
        JSONUIBase* parentdlg = dynamic_cast<JSONUIBase*>(resultWidget);
        if(parentdlg)
            parentdlg->setQuery( this );
        // Temporaly
        SelectionKeysForm* parentdlg1 = dynamic_cast<SelectionKeysForm*>(resultWidget);
        if(parentdlg1)
            parentdlg1->setQuery( this );

    }
    catch(jsonio_exception& e)
    {
        QMessageBox::critical( this, e.title(), e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }

}

void QueryWidget::CmSelectFields()
{
    try
    {
        SchemaSelectDialog dlg( this, "Please, mark data fields for the ouput table",
                                schema_name, query_description.getFieldsCollect());
        if( !dlg.exec() )
            return;

        query_description.setFieldsCollect( dlg.allSelected());
        lstmodel->setStringList( query_description.getFieldsCollect() );
    }
    catch(jsonio_exception& e)
    {
        QMessageBox::critical( this, e.title(), e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }

}

void QueryWidget::CmAddSelectFields()
{
    try
    {
        std::vector<std::string> _flist;
        SchemaSelectDialog dlg(this, "Please, mark addition data fields for the ouput table",
                               schema_name, _flist);
        if( !dlg.exec() )
            return;

        _flist = dlg.allSelected();
        query_description.addFieldsCollect(_flist);
        lstmodel->setStringList( query_description.getFieldsCollect() );
    }
    catch(jsonio_exception& e)
    {
        QMessageBox::critical( this, e.title(), e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

void QueryWidget::CmSelectRestore()
{
    query_description.setFieldsCollect( default_query.getFieldsCollect() );
    lstmodel->setStringList( query_description.getFieldsCollect() );
}

void QueryWidget::CmWhereApply()
{
    try
    {
        auto collection = ui->qSchema->currentText().toStdString();
        collection = collectionNameFromSchema( collection );
        auto qrstr = queryModel->getQuery( getQueryType(), collection );
        if( !qrstr.empty() )
            ui->queryText->setText(qrstr.getQueryString().c_str());

    }
    catch(jsonio_exception& e)
    {
        QMessageBox::critical( this, e.title(), e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

void QueryWidget::CmWhereRestore()
{
    auto selection = query_description.getFieldsCollect();
    query_description = default_query;
    query_description.setFieldsCollect( selection );
    showQuery();
}

void QueryWidget::CmAddWhereFields()
{
    try
    {
        std::vector<std::string> _flist;
        std::vector<jsonio::ThriftFieldDef*> _ftype;
        SchemaSelectDialog dlg( this, "Please, mark data fields for the query table",
                                schema_name, _flist);
        if( !dlg.exec() )
            return;

        _flist = dlg.allSelected();
        _ftype = dlg.allSelectedTypes();
        queryEdit->CmAddObjects( _flist, _ftype );
    }
    catch(jsonio_exception& e)
    {
        QMessageBox::critical( this, e.title(), e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

void QueryWidget::CmClearTree()
{
    queryEdit->ClearAll();
}

std::string QueryWidget::getQuerySchema() const
{
    return ui->qSchema->currentText().toStdString();
}


/// Return query ( Where )
const DBQueryDef& QueryWidget::getQueryDef()
{
    query_description.setKeyName( ui->qName->text().toStdString());
    query_description.setComment( ui->qComment->text().toStdString());
    query_description.setToSchema( ui->qSchema->currentText().toStdString());
    std::string queryText = ui->queryText->toPlainText().toStdString();
    query_description.setQueryCondition(DBQueryData(queryText, getQueryType() ));

    return query_description;
}


//-------------internal


void QueryWidget::showQuery()
{
    ui->qName->setText(query_description.getKeyName().c_str());
    ui->qComment->setText(query_description.getComment().c_str());
    ui->qSchema->setCurrentText( query_description.getToSchema().c_str());

    int type = query_description.getQueryCondition().getType();
    if( type ==  DBQueryData::qAQL )
        ui->pAQL->setChecked(true);
    /* else
        if( type ==  DBQueryData::qEJDB )
            ui->pEJDB->setChecked(true);*/
        else
            ui->pTemplate->setChecked(true);
    AQLtoggled(ui->pAQL->isChecked());

    lstmodel->setStringList( query_description.getFieldsCollect() );
    ui->queryText->setText(query_description.getQueryCondition().getQueryString().c_str());

    // clear internal
    qId->setText("");
    CmClearTree();
}


int QueryWidget::getQueryType() const
{
    int type = DBQueryData::qTemplate;
    if( ui->pAQL->isChecked() )
        type = DBQueryData::qAQL;
    return type;
}


// set data to bson
std::string QueryWidget::queryToJson()
{
    std::string jsonquery;
    DBQueryDef quer = getQueryDef();
    std::shared_ptr<JsonDomFree> domdata(JsonDomFree::newObject());
    quer.toJsonNode(domdata.get());
    domdata->setOid_(qId->text().toStdString());
    // Filter
    domdata->appendString("filter_generator", queryModel->getFILTER() );
    printNodeToJson( jsonquery, domdata.get() );
    return jsonquery;
}


void QueryWidget::queryFromNode(  const JsonDom *object )
{
    query_description.fromJsonNode(object);
    showQuery();
    std::string stroid;
    object->findValue( "_id", stroid );
    qId->setText( stroid.c_str() );

    // Filter
    std::string filter;
    if( object->findValue( "filter_generator", filter ) )
    {
       queryModel->setFILTER(filter);
    }
}


void QueryWidget::resetDBClient()
{
    try{
        TDBSchemaDocument* newClient = TDBSchemaDocument::newSchemaDocumentQuery(
                    uiSettings().resources_database(), "Query", "queries", newSchemaQuery() );
        // no schema
        if( newClient == nullptr )
            dbquery.reset();
        else
            dbquery.reset( newClient );
    }
    catch(std::exception& e)
    {
        std::cout << "Internal comment: " << e.what() << std::endl;
        throw;
    }
}

DBQueryData QueryWidget::newSchemaQuery()
{
    std::string queryString = "";
    if( !schema_name.empty() )
    {
        queryString = "{ \"qschema\": \"";
        queryString += schema_name;
        queryString += "\" }";
    }
    return DBQueryData(queryString, DBQueryData::qTemplate );
}

void QueryWidget::CmHelp()
{
    helpWin( schema_name, "" );
}

void QueryWidget::CmHelpAbout()
{
    helpWin( "QueryWidgetJsonui", "" );
}

} // namespace jsonui
