//  This is JSONUI library+API (https://bitbucket.org/gems4/jsonui)
//
/// \file JSONUIWidget.h
/// JSONUIWidget - Widget to work with json data or
/// internal DOM based on our JSON schemas data
//
// JSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONUI depends on the following open-source software products:
// JSONIO (https://bitbucket.org/gems4/jsonio); Qt5 (https://qt.io).
//

#ifndef JSONUIWIDGET_H
#define JSONUIWIDGET_H

#include <QCloseEvent>
#include "JSONUIBase.h"
#include "model_view.h"
#include "jsonio/dbschemadoc.h"

namespace Ui {
class JSONUIWidget;
}

namespace jsonui {

/// \class JSONUIWidget - window to work with bson or internal
/// DOM based on our JSON schemas data
class JSONUIWidget : public JSONUIBase
{
    Q_OBJECT

    // Internal data
    std::string curCollectionName = "";
    std::shared_ptr<jsonio::TDBDocumentBase> dbquery;
    std::string curRecord_;
    bool contentsChanged = false;

    // Work functions

    /// Set up menu commands
    void setActions();
    /// Set up current bson data to view model
    bool resetBson( const std::string& curRecord, const std::string& schemaName );

    void closeEvent(QCloseEvent* e);

    // update after change preferences
    virtual void updtViewMenu();
    virtual void updtModel();
    virtual void updtTable();
    virtual void updtDB();

    void resetDBClient();

public slots:

    // internal slots
    void openRecordKey(  const std::string& , bool  = false  ) {}
    void openRecordKey(  const QModelIndex&  ) {}
    void changeKeyList() {}
    void objectChanged()
       { contentsChanged = true; }

    // File
    void CmNew();
    void CmClone();
    void CmExportJSON();
    void CmImportJSON();
#ifndef IMPEX_OFF
    void CmExportYAML();
    void CmImportYAML();
    void CmExportXML();
    void CmImportXML();
#endif

    // Record
    void CmCreateInsert();
    void CmRead();
    void CmUpdate();
    void CmDelete();

public:

    explicit JSONUIWidget( const std::string& aschemaName = "",
                           const std::string& collectName = "", QWidget *parent = nullptr);
    ~JSONUIWidget();

    void setQuery( QueryWidget*  ) {}

private:

    Ui::JSONUIWidget *ui;

    // tree view
    QStringList aHeaderData;
    TJsonAbstractModel* model_schema;
    QItemDelegate *deleg_schema;
    TJsonView* fieldTable;

};

} // namespace jsonui

#endif // TJSONUIWIDGET_H
