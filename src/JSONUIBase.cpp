//  This is JSONUI library+API (https://bitbucket.org/gems4/jsonui)
//
/// \file JSONUIBase.cpp
/// Implementation of class JSONUIBase - Base Widget to work with bson
/// or internal DOM based on our JSON schemas data
//
// JSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONUI depends on the following open-source software products:
// JSONIO (https://bitbucket.org/gems4/jsonio); Qt5 (https://qt.io).
//

#include "JSONUIBase.h"

namespace jsonui {

std::string JSONUIBase::fileShemaExt( const std::string& acurSchemaName,const char* ext )
{
  std::string _ret = "";
  if( !acurSchemaName.empty())
  {
      _ret += "*.";
      _ret += acurSchemaName;
      _ret += ".";//".schema.";
      _ret += ext;
  }
  return _ret;
}

std::string JSONUIBase::schemafromName( const std::string& fname )
{
   std::string _ret = "";
   size_t pose = fname.find_last_of(".");
   if( pose != std::string::npos)
   {
      size_t posb = fname.find_last_of(".", pose-1 );
      if( posb != std::string::npos )
        _ret = fname.substr(posb+1, pose-posb-1 );
   }
  return _ret;
}

void JSONUIBase::CmHelpContens()
{
  helpWin( curSchemaName, "" );
}

void JSONUIBase::CmHelpAbout()
{
    helpWin( "AboutJsonui", "" );
}

void JSONUIBase::CmHelpAuthors()
{
  helpWin( "AuthorsJsonui", "" );
}

void JSONUIBase::CmHelpLicense()
{
  helpWin( "LicenseJsonui", "" );
}

} // namespace jsonui
