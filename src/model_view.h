//  This is JSONUI library+API (https://bitbucket.org/gems4/jsonui)
//
/// \file model_view.h
/// Declaration of TJsonAbstractModel and  TJsonView
/// implements a tree view/edit of json structure
//
// JSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONUI depends on the following open-source software products:
// JSONIO (https://bitbucket.org/gems4/jsonio); Qt5 (https://qt.io).
//

#ifndef _model_view_h
#define _model_view_h

#include <string>
#include <QAbstractItemModel>
#include <QTreeView>
#include <QItemDelegate>


namespace jsonio {
class JsonDom;
}

namespace jsonui {

class ExpandedLevelLine;

#ifdef __APPLE__
const char  splitRow = '\r';
const char  splitCol = '\t';
#else
const char  splitRow = '\n';
const char  splitCol = '\t';
#endif

/// \class TBsonAbstractModel
/// Abstract class for represents the data set and is responsible for fetching the data
/// is needed for viewing and for writing back any changes.
/// Reading/writing data from/to json type objects
class TJsonAbstractModel: public QAbstractItemModel
{
    Q_OBJECT

 public:

  TJsonAbstractModel( QObject* parent = nullptr ):
      QAbstractItemModel(parent)
  { }
  ~TJsonAbstractModel() {}

  virtual std::string helpName( const QModelIndex& index ) const = 0;
  virtual bool isNumber( const QModelIndex& index ) const = 0;
  virtual bool isArray( const QModelIndex&  ) const
  { return false; }
  virtual bool isCanBeRemoved( const QModelIndex& index ) const
  { return isStruct(index); }
  virtual bool isStruct( const QModelIndex&  ) const
  { return false; }
  virtual bool isUnion( const QModelIndex&  ) const
  { return false; }
  virtual bool isCanBeCloned( const QModelIndex&  ) const
  { return false; }

  virtual void resizeArray( QWidget* , const QModelIndex&  ) { }
  virtual void delObject( QWidget* , const QModelIndex&  ) { }
  virtual void delObjects( QWidget* , const QModelIndex&  ) =0;
  virtual void delObjectsUnion( QWidget* , const QModelIndex&  ) { }
  virtual const QModelIndex addObject( QWidget* , const QModelIndex& index )
  { return index; }
  virtual const QModelIndex addObjects( QWidget* , const QModelIndex& index  )
  { return index; }
  virtual const QModelIndex cloneObject( QWidget* , const QModelIndex& index  )
  { return index; }
  virtual void resetObject( QWidget* , const QModelIndex&  ) { }

  virtual std::string  getFieldPath( const QModelIndex&  ) const
   { return "undefined function"; }
  virtual std::string  getFieldData( const QModelIndex&  ) const
  { return "undefined function"; }
  virtual void  setFieldData( const QModelIndex& , const std::string& )
  { //undefined function
  }

  void updateModelData()
  {
      beginResetModel();
      endResetModel();
  }

  /// Extern update data
  virtual void setupModelData( const std::string& ajson, const std::string& schema ) = 0;
  /// Return internal data to json std::string
  virtual std::string saveToJson() = 0 ;

  virtual const jsonio::JsonDom* getDom() const =0;
};

/// \class TJsonView implements a tree view of bson structure
/// that displays items from a TJsonAbstractModel model.
class TJsonView: public QTreeView
{
	Q_OBJECT
	
	void keyPressEvent(QKeyEvent* e);
	QString createString();
    QString createHeader();
    bool testCurrentIndex();
    void selectWithChildren(const QModelIndex& parIndex);
    void copyWithChildren( const QModelIndex& parIndex,
        const QModelIndexList& selIndexes, QString& clipText );

    void saveExpandedState( ExpandedLevelLine *topList );
    void restoreExpandedState( ExpandedLevelLine *topList );
    void saveExpandedOnLevel(const QModelIndex& index, ExpandedLevelLine *parent );
    void restoreExpandedOnLevel(const QModelIndex& index, ExpandedLevelLine *topData );

 protected slots:
    void slotPopupContextMenu(const QPoint& pos);
    void changeCurrent( int section );

 public slots:
    void CmHelp();
    void CmCalc();
    void CmAddObjects();
    void CmAddObject();
    void CmDelObject();
    void CmDelObjects();
    void CmDelObjectsUnion();
    void CmResizeArray();
    void CmCloneObject();
    void CmResetObject();

    void _SelectRow();
    void _SelectColumn();
    void _SelectAll();
    void _SelectGroup();
    void _CopyData();
    void _CopyDataHeader();

    void CopyFieldPath();
    void CopyField();
    void PasteField();

 public:

    static bool expandedFields;	///< Extern flag to keep data fields expanded

     TJsonView( QWidget * parent = nullptr );

     /// Update model data with restore Expanded Level
     void updateModelData( const std::string& ajson, const std::string& schemaname );

};

} // namespace jsonui

#endif   // _model_view_h
