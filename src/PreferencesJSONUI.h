//  This is JSONUI library+API (https://bitbucket.org/gems4/jsonui)
//
/// \file PreferencesJSONUI.h
/// Declaration of PreferencesJSONUI  dialog for define preferences into JSONUI
//
// JSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONUI depends on the following open-source software products:
// JSONIO (https://bitbucket.org/gems4/jsonio); Qt5 (https://qt.io).
//

#ifndef PREFERENCESJSONUI_H
#define PREFERENCESJSONUI_H

#include <QDialog>
#include <QButtonGroup>
#include "preferences.h"
#include "jsonio/dbarango.h"

namespace Ui {
class PreferencesJSONUI;
}

namespace jsonui {

/// \class  PreferencesJSONUI  dialog for insert/edit settings
class PreferencesJSONUI : public QDialog
{
    Q_OBJECT

    jsonio::JsonioSettings& _iosettings;
    /// Current root Database
    std::shared_ptr<jsonio::TArangoDBRootClient> _rootclient;
    int oldDBconnection;

signals:
    void dbdriveChanged();

private slots:
    void CmSave();
    void CmProjectDir();
    void CmSchemasDir();
    void CmResourcesDir();
    void CmHelp();
    void DBStateChanged(int);
    void UpdateLists();

public:
    explicit PreferencesJSONUI( jsonio::JsonioSettings& aset, QWidget *parent = nullptr );
    ~PreferencesJSONUI();

private:

    Ui::PreferencesJSONUI *ui;
    QButtonGroup *smbGroup;

};

} // namespace jsonui

#endif // PREFERENCESJSONUI_H
