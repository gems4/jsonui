//  This is JSONUI library+API (https://bitbucket.org/gems4/jsonui)
//
/// \file TCSVPage.h
/// Declarations of classes TCSVContainer, TCSVPage
/// implements widget to work with csv format data.
//
// JSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONUI depends on the following open-source software products:
// JSONIO (https://bitbucket.org/gems4/jsonio); Qt5 (https://qt.io).
//

#ifndef TCSVPAGE_H
#define TCSVPAGE_H

#include <QVBoxLayout>
#include <QSortFilterProxyModel>
#include "model_table.h"
#include "graph_data.h"

namespace jsonui {

class TSortFilterProxyModel : public QSortFilterProxyModel
 {
     Q_OBJECT

 public:
     TSortFilterProxyModel(QObject *parent = nullptr):
         QSortFilterProxyModel(parent)
     { }

 protected:
     bool lessThan(const QModelIndex &left, const QModelIndex &right) const;

 };


/// \class  TCSVContainer scv table data container
/// with description of one plot in graph screen
class TCSVContainer : public TAbstractDataContainer
{
    QString _csvData;                     ///< Current data
    QVector< QString > _colHeads;         ///< Heards of columns
    QVector< bool > _colDouble;           ///< Numerical column condition
    QVector< QVector<QVariant> > _matrix; ///< CSV matrix

    QVector<int> _xcolms;  ///< Abscissa columns list
    QVector<int> _ycolms;  ///< Ordinate columns list

    void matrixFromCsvString( const QString& valueCsv );
    QString matrixToCsvString();

 public:

   explicit TCSVContainer( const char* aname, QString data ):
      TAbstractDataContainer(aname)//qchart, TPlot()
   {
       matrixFromCsvString( data );
   }

   explicit TCSVContainer(  ):
       TAbstractDataContainer("csv")//qchart, TPlot()
   { }

   virtual ~TCSVContainer();

   int rowCount() const
   { return _matrix.size();  }

   int columnCount() const
   { return _colHeads.size(); }

   QVariant data( int line, int column ) const
   {
       QVariant val = _matrix.at(line).at(column);
       return val;
   }

   bool setData( int line, int column, const QVariant &value )
   {
       _matrix[line].replace(column, value);
       return true;
   }

   virtual QString headerData ( int section ) const
   {
       QString head = _colHeads[section];
       int ii = _xcolms.indexOf( section );
       if( ii!= -1)
           head+="(x)";
       ii = _ycolms.indexOf( section );
       if( ii!= -1)
           head+="(y)";
       return head;
   }

   virtual bool  IsEditable( int /*line*/, int /*column*/ ) const
   { return true; }

   virtual int getType( int /*line*/, int column ) const
   {
      if(_colDouble[column])
          return ftDouble;
      else
          return ftString;
   }

   virtual QString getToolTip( int /*line*/, int column ) const
   {
       return _colHeads[column];
   }

   void resetData()
   {
       matrixFromCsvString( _csvData );
   }

   void setCsvString( const QString& valueCsv )
   {
      _csvData = valueCsv;
   }

   QString getCsvString()
   {
      return  matrixToCsvString();
   }

   void setXColumns( const std::vector<int>& clmns )
   {
       _xcolms.clear();
       for( const auto& val: clmns )
          _xcolms.push_back(val);
       //_xcolms = QVector<int>(clmns.begin(),clmns.end() );
   }

   void setYColumns( const std::vector<int>& clmns )
   {
       _ycolms.clear();
       for( const auto& val: clmns )
          _ycolms.push_back(val);
       //_ycolms = QVector<int>(clmns.begin(),clmns.end() );
   }

};

/// Widget to work with csv format data
class TCSVPage : public QWidget
{
   friend class TableEditWidget;

    Q_OBJECT

    // widget
    QVBoxLayout*   _vLayout;
    TAbstractDataContainer* _data;
    TMatrixModel*  _modelCSV;   ///< Model reading csv data
    TMatrixTable*  _tableCSV;   ///< Table for edit csv data

    bool contentsChanged = false;

    // graph charts information

    /// Abscissa columns list
    std::vector<int> _xColumns;
    /// Ordinate columns list
    std::vector<int> _yColumns;
    /// Descriptions of model extracting data
    std::vector<std::shared_ptr<ChartDataModel>> _chartModels;
    /// Description of 2D plotting widget
    ChartData* _chartData;

signals:

  void updateGraphWindow();

public slots:

  void saveGraphData( ChartData* );
  void updateTable()
     {
       _modelCSV->resetMatrixData();
       /// ??? emit updateGraphWindow();
     }
  void objectChanged()
      { contentsChanged = true; }

  void CmCalc()
    { _tableCSV->CmCalc();  }
  void SelectRow()
    { _tableCSV->SelectRow();  }
  void SelectColumn()
    { _tableCSV->SelectColumn();  }
  void SelectAll()
    { _tableCSV->selectAll();  }
  void CutData()
    { _tableCSV->CutData();  }
  void ClearData()
    { _tableCSV->ClearData();  }
  void CopyData()
    { _tableCSV->CopyData();  }
  void CopyDataHeader()
    { _tableCSV->CopyDataHeader();  }
  void PasteData()
    { _tableCSV->PasteData();  }
  void PasteTransposedData()
    { _tableCSV->PasteTransposedData();  }

public:

    explicit TCSVPage(TAbstractDataContainer* adata, int mode, QWidget* parent = nullptr);
    ~TCSVPage();

    /// Save selected data to csv format file
    void saveSelectedToCSV(const std::string& fileName);
    /// Save all table data to csv format file
    void saveAllToCSV(const std::string& fileName);

    void initGraphic( int mode );
    bool canBeToggled( const QModelIndex& index, bool asX );
    void toggleX( );
    void toggleY( );
    void setXYaxis(int ndxX, std::vector<int> ndxY);

};

} // namespace jsonui

#endif // TCSVPAGE_H
