//  This is JSONUI library+API (https://bitbucket.org/gems4/jsonui)
//
/// \file dbkeysmodel.cpp
/// Implementation of classes TKeyListTableNew, TKeyTable and DBKeysListModel
/// implements data container, widget and model to work with data base record&keys.
//
// JSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2018 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONUI depends on the following open-source software products:
// JSONIO (https://bitbucket.org/gems4/jsonio); Qt5 (https://qt.io).
//

#include <iostream>
#include <QKeyEvent>
#include <QMessageBox>
#include <QSortFilterProxyModel>
#include <QHeaderView>
#include "dbkeysmodel.h"
#include "QueryWidget.h"
#include "TableEditWindow.h"
#include "JSONUIBase.h"
using namespace jsonio;

namespace jsonui {

TKeyListTableNew::~TKeyListTableNew()
{}

DBKeysListModel::DBKeysListModel( jsonio::TDBVertexDocument* adbgraph,  TKeyTable *atable, QWidget *aparent ):
    parent(aparent), table(atable),
    dataTable(nullptr), tableModel(nullptr), proxyModel(nullptr),
    queryWindow(nullptr), resultWindow(nullptr), dbgraph_keys(adbgraph)
{
    defKeysTable();
    changeDBClient( adbgraph );
}

DBKeysListModel::~DBKeysListModel()
{
    Close();

    if(queryWindow)
        delete queryWindow;
    if(resultWindow)
        delete resultWindow;

    if( dataTable )
        delete dataTable;

    if( proxyModel )
        delete proxyModel;

    //cout << "~DBKeysListModel" << endl;
}


// Close addition windows
void DBKeysListModel::Close()
{
    if( queryWindow )
        queryWindow->close();
    if( resultWindow )
        resultWindow->close();
}

void DBKeysListModel::changeDBClient( jsonio::TDBVertexDocument* adbgraph )
{
    dbgraph_keys = adbgraph;

    // if( dbgraph_keys &&  dbgraph_keys != nullptr )
    resetKeysTable();

    if(queryWindow)
        queryWindow->resetDBClient();
}


void DBKeysListModel::movetoKey( const std::string& key )
{
    if( key.empty()  )
        return;

    table->setColumnHidden(0, false);  // ??? not hide to edges
    // search `item` text in model
    QModelIndexList Items =  table->model()->match(  table->model()->index(0, 0),
                                                     Qt::DisplayRole, QString(key.c_str()));
    if (!Items.isEmpty())
    {
        table->selectionModel()->select(Items.first(), QItemSelectionModel::ClearAndSelect |
                                        QItemSelectionModel::Rows);
        table->scrollTo(Items.first(), QAbstractItemView::EnsureVisible );
    }
    table->setColumnHidden(0, true); // ??? not hide to edges

}

void DBKeysListModel::resetModel( QString key )
{
    // reset model data
    tableModel->resetMatrixData();

    if( resultWindow )
        resultWindow->updateTable();

    auto _key = key.toStdString();

    JSONUIBase *aparent = dynamic_cast<JSONUIBase *>(parent);
    if( aparent && _key.empty() )
        _key = aparent->currentKey();

    movetoKey( _key );
}


void DBKeysListModel::showQuery( const char* title, const std::vector<std::string>& schemalst )
{
    if( !dbgraph_keys ||  dbgraph_keys == nullptr )
        return;

    auto query = dbgraph_keys->lastQueryData()->getQuery();
    if(!queryWindow)
    {
        queryWindow = new QueryWidget( title, schemalst, query, parent );
    }
    else
    {
        queryWindow->UpdateQuery( schemalst, query );
        queryWindow->raise();
    }
    queryWindow->show();
}

void DBKeysListModel::setOneQuery( QueryWidget* queryW )
{
    if( queryW != queryWindow )
        return;
    std::shared_ptr<jsonio::DBQueryDef> querydef( new jsonio::DBQueryDef(queryW->getQueryDef()) );
    emit updateQuery( queryW->getQuerySchema().c_str(), querydef  );
}

void DBKeysListModel::showResult( const char* title )
{
    if(!resultWindow)
    {
        resultWindow = new TableEditWidget( title, dataTable, TMatrixTable::tbShow  );
    }
    else
    {
        resultWindow->updateTable();
        resultWindow->raise();
    }
    resultWindow->show();
}

void DBKeysListModel::defKeysTable()
{
    //table->horizontalHeader()->hide();
    table->setSelectionBehavior(QAbstractItemView::SelectRows);
    table->horizontalHeader()->setSectionResizeMode( QHeaderView::Interactive );
    table->setEditTriggers( QAbstractItemView::AnyKeyPressed );
    table->setSortingEnabled(true);
    disconnect( table, SIGNAL(customContextMenuRequested(QPoint)),
                table, SLOT(slotPopupContextMenu(QPoint)));
}

void DBKeysListModel::resetKeysTable()
{
    if( dataTable )
        delete dataTable;
    dataTable = new TKeyListTable("select", dbgraph_keys );

    if( proxyModel )
        delete proxyModel;

    tableModel = new TMatrixModel( dataTable, parent );
    proxyModel = new QSortFilterProxyModel();
    proxyModel->setSourceModel( tableModel );

    table->setModel(proxyModel);
    table->hideColumn(0); // !!!! do not hide for Edges
}


//--------------------------------------------------------------------------------

void TKeyTable::keyPressEvent(QKeyEvent* e)
{
    TMatrixTable::keyPressEvent(e);
    switch( e->key() )
    {
      case Qt::Key_Up:
      case Qt::Key_Down:
      case Qt::Key_PageUp:
      case Qt::Key_PageDown:
        { QModelIndex index = currentIndex();
          if (index.isValid())
           _openf( index ); // parentTask->openRecordKey( index );
          setCurrentIndex(index);
        }
        break;
    }
}

void TKeyTable::CmNext()
{
    QModelIndex index = currentIndex();
    if( !index.isValid() )
    {
      if( model()->rowCount() > 0)
        index = model()->index(0,0);
    }
    else
    { int row = index.row();
      row = std::min( row+1, model()->rowCount()-1);
      index = index.sibling(row, index.column());
    }
    if (index.isValid())
    {
      _openf( index ); //parentTask->openRecordKey( index );
      setCurrentIndex(index);
    }
}

void TKeyTable::CmPrevious()
{
    QModelIndex index = currentIndex();
    if (index.isValid())
    {
      int row = index.row();
      row = std::max( row-1, 0);
      index = index.sibling(row, index.column());
      _openf( index ); //parentTask->openRecordKey( index );
      setCurrentIndex(index);
    }
}


void TKeyListTable::resetData()
{
    uint ii;
    colHeads.clear();
    matrix.clear();

    if( !_dbclient || _dbclient==nullptr )
        return;

    const std::vector<std::string>& fields_ = _dbclient->lastQueryData()->getQuery().getFieldsCollect();
    const TableOfValues& tabl_ = _dbclient->lastQueryData()->getQueryResult();

    // get header
    colHeads.push_back( "key" );
    for( ii=0; ii<fields_.size(); ii++ )
      colHeads.push_back( fields_[ii].c_str() );

    auto it = tabl_.begin();
    while( it != tabl_.end() )
    {
        QVector<QVariant> vec;
        matrix.push_back( vec );
        matrix.last().push_back( it->first.c_str() );
        for( ii=0; ii<it->second.size(); ii++ )
         matrix.last().push_back( it->second[ii].c_str() );
        it++;
    }
}



} // namespace jsonui

//--------------------------------------------------------------------------------
