#ifndef SELECTIONKEYSFORM_H
#define SELECTIONKEYSFORM_H

#include <QDialog>
#include <QWidget>
#include <QThread>
#include "dbdocumentmodel.h"

namespace Ui {
class SelectionKeysForm;
}

class WaitingSpinnerWidget;

namespace jsonui {


class SelectionKeysForm : public QWidget
{
    Q_OBJECT


signals:

    /// Start long database process
    void startProcess();

    /// Finish long database process
    void finishProcess();

    /// Exception when database command execution
    void isException( const QString& titl, const QString& msg );

    /// Signal emits when current schema changed
    void changedSchema( QString newSchemaName );

public slots:

    void selectAll()
    {
        m_keysTable->selectAll();
    }

    void clearSelection()
    {
        m_keysTable->clearSelection();
    }

    void closeQuery()
    {
        if( m_keysModel )
            m_keysModel->Close();
    }

protected slots:

    void changeSchema( QString newSchemaName );

    void resetQuery();

    void changeDBClientData(  QString query );

    void objectChanged() { }

public:

    explicit SelectionKeysForm(  const std::vector<std::string>& aVertexesList, QWidget *parent = nullptr);
    ~SelectionKeysForm();

    /// Define connection to database
    void setDBClient(const std::string& schemaName );

    /// Update query functions ( must be changed )
    void setQuery( QueryWidget* queryW  );

    /// Returns selected keys
    std::vector<std::string> allSelectedKeys() const;

    /// Return selected schema
    std::string vertexSchema() const;

private:


    Ui::SelectionKeysForm *ui;

    // database API

    QThread m_thread;

    /// Database connection object
    DBDocumentModel* m_documentModel=nullptr;

    /// Database keys list model
    DBKeysListModel *m_keysModel = nullptr;
    /// Keys list table
    TKeyTable *m_keysTable = nullptr;

};


class SelectionKeysDialog : public QDialog
{
    Q_OBJECT

public slots:

    /// Start long database process
    void startProcess();

    /// Finish long database process
    void finishProcess();

    /// Exception when database command execution
    void get_exception( const QString& titl, const QString& msg );


public:

    explicit SelectionKeysDialog(QWidget *parent = nullptr);
    ~SelectionKeysDialog();

    /// Returns selected keys
    std::vector<std::string> allSelectedKeys()
    {
      return form->allSelectedKeys();
    }

    /// Return selected schema
    std::string vertexSchema() const
    {
       return form->vertexSchema();
    }

private:

   SelectionKeysForm *form = nullptr;
   WaitingSpinnerWidget *waitDialog = nullptr;

};

} // namespace jsonui

#endif // SELECTIONKEYSFORM_H
