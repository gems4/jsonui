//  This is JSONUI library+API (https://bitbucket.org/gems4/jsonui)
//
/// \file TCSVPage.cpp
/// Implementation of classes TCSVContainer, TCSVPage
/// implements widget to work with csv format data.
//
// JSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONUI depends on the following open-source software products:
// JSONIO (https://bitbucket.org/gems4/jsonio); Qt5 (https://qt.io).
//

#include <iostream>
#include "TCSVPage.h"
#include "jsonio/json2file.h"
//#include "jsonio/jsondomfree.h"

namespace jsonui {

void removeComments( QString& valCsv );

void removeComments( QString& valCsv )
{
    int foundStart = valCsv.indexOf('#');
    int foundEnd;
    while( foundStart >= 0 )
    {
      foundEnd = valCsv.indexOf('\n');
      if( foundEnd > 0 )
        valCsv.remove( foundStart, foundEnd-foundStart+1 );
      else
        {
           valCsv.remove( foundStart);
           break;
        }
      foundStart = valCsv.indexOf('#', foundStart );
    }

}

bool TSortFilterProxyModel::lessThan(const QModelIndex &left,
                                      const QModelIndex &right) const
{
    int typeleft = left.model()->data(left, TypeRole).toInt();
    int typeright = right.model()->data(right, TypeRole).toInt();

    if (typeleft >0 && typeright>0 )
    {
        QVariant leftData = left.model()->data(left);
        QVariant rightData = right.model()->data(right);
        return leftData.toDouble() < rightData.toDouble();
    }
    else
        return QSortFilterProxyModel::lessThan( left, right);
}

// TCSVContainer--------------------------------------------------

TCSVContainer::~TCSVContainer()
{}

// Set std::string to internal data
void TCSVContainer::matrixFromCsvString( const QString& valueCsv )
{
   int ii, jj, nlines;

   // clear old data
  _colHeads.clear();
  _matrix.clear();
  _colDouble.clear();
  _xcolms.clear();
  _ycolms.clear();

  if(valueCsv.isEmpty())
      return;

  // split rows
  const QStringList rows = valueCsv.split('\n', QString::KeepEmptyParts/*Qt::KeepEmptyParts*/);

  // get header
  QStringList cells = rows[0].split(',', QString::KeepEmptyParts/*Qt::KeepEmptyParts*/);
  for( ii=0; ii< cells.count(); ii++ )
    {   _colHeads.push_back( cells[ii] );
        _colDouble.push_back(true);  //number
    }

  // get values
  for( jj=1, nlines=0;  jj<rows.count(); jj++ )
  {
      cells = rows[jj].split(',', QString::KeepEmptyParts/*Qt::KeepEmptyParts*/);
      if(cells.count() < _colHeads.size() )
        continue;
      QVector<QVariant> vec;
      bool ok;
      _matrix.push_back( vec );
      for( ii=0; ii<cells.count(); ii++ )
      {
        if( !cells.empty() ||  _colDouble[ii] )
        { cells[ii].toDouble(&ok);
          if( !ok )
          _colDouble[ii] = false;
        }
        _matrix[nlines].push_back( cells[ii] );
      }
      nlines++;
  }

}

QString TCSVContainer::matrixToCsvString( )
{
    QString valCsv;

    if(_colHeads.size() == 0 )
      return "";

    QVectorIterator<QString> head(_colHeads);
    while (head.hasNext())
    {
        valCsv += head.next();
        if(head.hasNext() )
           valCsv += ",";
    }
    valCsv += "\n";

    QVectorIterator<QVector<QVariant> > line(_matrix);
    while (line.hasNext())
    {
        int col =0;
        QVectorIterator<QVariant> valC(line.next());
        while (valC.hasNext())
        {
          QVariant val = valC.next();
          //if( col  >= numberStringColumns )
          //    valCsv += ValToString(val.toDouble(), 12);
          //  else
              valCsv += val.toString();
          if(valC.hasNext() )
               valCsv += ",";
          col++;
        }
        valCsv += "\n";
    }

   return valCsv;
}


//--------------------------------------------------------------------

TCSVPage::TCSVPage(TAbstractDataContainer *adata, int mode, QWidget *parent):
    QWidget(parent), _data(adata), _chartData(nullptr)
{
   // set up window
   _vLayout = new QVBoxLayout(this);
   _vLayout->setObjectName(QStringLiteral("verticalLayout"));

   //QString valCsv;
   //_data = new TCSVContainer("csv", valCsv);
   _modelCSV = new TMatrixModel( _data, this );
   _tableCSV = new TMatrixTable( this, mode );

   if( mode & TMatrixTable::tbSort)
   { TSortFilterProxyModel *proxyModel = new TSortFilterProxyModel();
     proxyModel->setSourceModel( _modelCSV );
    _tableCSV->setModel(proxyModel/*modelFitResults*/);
   }
   else
    _tableCSV->setModel(_modelCSV);

   TMatrixDelegate *deleg = new TMatrixDelegate( this);
    _tableCSV->setItemDelegate(deleg);
   _vLayout->addWidget(_tableCSV);
}


void TCSVPage::initGraphic( int mode )
{
   if( mode & TMatrixTable::tbGraph )
   {
       _chartModels.push_back( std::shared_ptr<ChartDataModel>( new ChartDataModel( _modelCSV, this )) );
       _chartData = new ChartData( _chartModels, "Graph for window", "x", "y" );

       // read internal cfg
       jsonio::FJson file(  std::string("graph_cfg.json"));
       if( file.Exist() )
       {
          std::shared_ptr<jsonio::JsonDomFree> domdata(jsonio::JsonDomFree::newObject());
          file.LoadJson( domdata.get() );
          _chartData->fromJsonNode( domdata.get());
          _xColumns = _chartModels[0]->XColumns();
          _yColumns = _chartModels[0]->YColumns();
          TCSVContainer* csvdata = dynamic_cast<TCSVContainer*>(_data);
          if( csvdata)
          {
              csvdata->setXColumns(_xColumns);
              csvdata->setYColumns(_yColumns);
          }
       }
   }
}


TCSVPage::~TCSVPage()
{
    delete _tableCSV;
    delete _vLayout;
    delete _chartData;
}

void TCSVPage::saveAllToCSV(const std::string& fileName)
{
    _tableCSV->saveAllToCSV(fileName);
}

void TCSVPage::saveSelectedToCSV(const std::string& fileName)
{
    _tableCSV->saveSelectedToCSV(fileName);
}

// Working with graphic charts -------------------------------------

/// Save graphic definition to internal cfg file
void TCSVPage::saveGraphData( ChartData* grdat )
{
    jsonio::FJson file( std::string("graph_cfg.json"));
    std::shared_ptr<jsonio::JsonDomFree> domdata(jsonio::JsonDomFree::newObject());
    grdat->toJsonNode( domdata.get() );
    file.SaveJson( domdata.get() );
}

bool TCSVPage::canBeToggled( const QModelIndex& index, bool asX )
{
    if(!index.isValid())
        return false;

    // not number
    int type_ = _modelCSV->data(index, TypeRole ).toInt();
    if( type_ != ftNumeric && type_ != ftDouble  )
      return false;

    if( asX )
    {
        // selected as Y
        auto it = std::find( _yColumns.begin(), _yColumns.end(), index.column() );
         if ( it != _yColumns.end() )
            return false;
    }
    else
     {
        // selected as X
        auto it = std::find( _xColumns.begin(), _xColumns.end(), index.column() );
         if ( it != _xColumns.end() )
            return false;
     }
    return true;
}

void TCSVPage::toggleX()
{
    QModelIndex index = _tableCSV->currentIndex();
    if(!canBeToggled( index, true ))
        return;

    int ncolmn = index.column();
    auto it = std::find( _xColumns.begin(), _xColumns.end(), ncolmn );
     if ( it != _xColumns.end() )
       _xColumns.erase( it );
    else
       if( ncolmn>=0 && ncolmn<_data->columnCount() && _data->getType( 0, ncolmn ) == ftDouble )
           _xColumns.push_back( ncolmn );

     TCSVContainer* csvdata = dynamic_cast<TCSVContainer*>(_data);
     if( csvdata)
           csvdata->setXColumns(_xColumns);

     _chartModels[0]->setXColumns( _xColumns );
     emit updateGraphWindow();
}

void TCSVPage::toggleY()
{
    QModelIndex index = _tableCSV->currentIndex();
    if(!canBeToggled( index, false ))
        return;

    int ncolmn = index.column();
    auto it = std::find( _yColumns.begin(), _yColumns.end(), ncolmn );
     if ( it != _yColumns.end() )
       _yColumns.erase( it );
    else
       if( ncolmn>=0 && ncolmn<_data->columnCount() && _data->getType( 0, ncolmn ) == ftDouble )
           _yColumns.push_back( ncolmn );

   TCSVContainer* csvdata = dynamic_cast<TCSVContainer*>(_data);
   if( csvdata)
         csvdata->setYColumns(_yColumns);

   _chartModels[0]->setYColumns( _yColumns, true );
   emit updateGraphWindow();
}

void TCSVPage::setXYaxis(int ndxX, std::vector<int> ndxY)
{
    _xColumns.clear();
    if( ndxX>=0 && ndxX<_data->columnCount() && _data->getType( 0, ndxX ) == ftDouble )
        _xColumns.push_back( ndxX );

    _yColumns.clear();
    for( auto ncolmn: ndxY )
    if( ncolmn>=0 && ncolmn<_data->columnCount() && _data->getType( 0, ncolmn ) == ftDouble )
        _yColumns.push_back( ncolmn );

    _chartModels[0]->setXColumns( _xColumns );
    _chartModels[0]->setYColumns( _yColumns, true );
    emit updateGraphWindow();
}

} // namespace jsonui
