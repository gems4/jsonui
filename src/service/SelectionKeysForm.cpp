#include <QVBoxLayout>
#include <QMessageBox>
#include <QComboBox>
#include "SelectionKeysForm.h"
#include "ui_SelectionKeysForm.h"
#include "jsonio/io_settings.h"
#include "waitingspinnerwidget.h"


namespace jsonui {

/*
{
   "properties.class_": {"2":"SC_AQSOLUTE"}
}
*/

SelectionKeysDialog::SelectionKeysDialog(QWidget *parent):
    QDialog(parent)
{
    auto vertexNames = jsonio::ioSettings().Schema()->getVertexesList();

    QVBoxLayout *mainLayout = new QVBoxLayout;
    form = new SelectionKeysForm(vertexNames, this);
    mainLayout->addWidget(form);
    setLayout(mainLayout);

    waitDialog = new WaitingSpinnerWidget(this, true/*Qt::ApplicationModal*/, true);
    connect( form, &SelectionKeysForm::startProcess, this, &SelectionKeysDialog::startProcess );
    connect( form, &SelectionKeysForm::finishProcess, this, &SelectionKeysDialog::finishProcess );
    connect( form, &SelectionKeysForm::isException, this, &SelectionKeysDialog::get_exception );

    std::string curSchemaName = "VertexElement";
    form->setDBClient( curSchemaName );
}

SelectionKeysDialog::~SelectionKeysDialog()
{
    if( waitDialog )
        delete waitDialog;

    delete form;
}


/// Start long database process
void SelectionKeysDialog::startProcess()
{
    waitDialog->start();
}

/// Finish long database process
void SelectionKeysDialog::finishProcess()
{
    waitDialog->stop();
}

// Exception when database command execution
void SelectionKeysDialog::get_exception( const QString& titl, const QString& msg )
{
    QMessageBox::critical( this, titl, msg );
}

//------------------------------------------------------

SelectionKeysForm::SelectionKeysForm(const std::vector<std::string> &aVertexesList, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SelectionKeysForm)
{
    ui->setupUi(this);
    ui->splitter->setStretchFactor(0, 1);
    ui->splitter->setStretchFactor(1, 4);

    //define schema checkbox
    for( uint ii=0; ii<aVertexesList.size(); ii++ )
        ui->schemaBox->addItem(aVertexesList[ii].c_str());

    if( aVertexesList.size()<2 )
        ui->schemaBox->setDisabled(true);

    // define key table
    m_keysTable = new TKeyTable( ui->widget_2, []( const QModelIndex& ){} );
    m_keysTable->setSelectionMode(QAbstractItemView::MultiSelection);
   // m_keysTable->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    ui->verticalLayout_2->addWidget(m_keysTable);

    //std::string curSchemaName = "VertexElement";
    //resetDBClient( curSchemaName );
}

SelectionKeysForm::~SelectionKeysForm()
{
    m_thread.quit();
    m_thread.wait();

    if( m_keysModel )
        delete m_keysModel;
    if( m_keysTable )
        delete m_keysTable;
    delete ui;
}

void SelectionKeysForm::setDBClient(const std::string& schemaName )
{
    qRegisterMetaType<std::shared_ptr<jsonio::DBQueryDef> >("std::shared_ptr<jsonio::DBQueryDef>");

    try{

        ui->schemaBox->setCurrentText(schemaName.c_str());
        m_keysModel = new DBKeysListModel( nullptr, m_keysTable, this );
        m_documentModel = new DBDocumentModel(  schemaName, false  );


        connect( m_keysModel, &DBKeysListModel::updateQuery, m_documentModel, &DBDocumentModel::dbChangeQuery );
        connect( m_documentModel, &DBDocumentModel::changedDBClient,  m_keysModel, &DBKeysListModel::changeDBClient );
        connect( m_documentModel, &DBDocumentModel::changedModel,  m_keysModel, &DBKeysListModel::resetModel );

        // link from GUI

        connect( this, &SelectionKeysForm::changedSchema, m_documentModel, &DBDocumentModel::dbResetClient );
        connect( ui->schemaBox, &QComboBox::currentTextChanged, this, &SelectionKeysForm::changeSchema );
        connect( ui->queryButton, &QPushButton::clicked, this, &SelectionKeysForm::resetQuery );

        // link to GUI

        connect( m_documentModel, &DBDocumentModel::changedQuery, this, &SelectionKeysForm::changeDBClientData );
        connect( m_documentModel, &DBDocumentModel::finished, this, &SelectionKeysForm::finishProcess );
        connect( m_documentModel, &DBDocumentModel::isException, this, &SelectionKeysForm::isException );

        // thread functions

        m_documentModel->moveToThread(&m_thread);
        connect(&m_thread, &QThread::finished, m_documentModel, &QObject::deleteLater);
        m_thread.start();

        changeSchema(schemaName.c_str());
    }
    catch(std::exception& e)
    {
        std::cout << "Internal comment: " << e.what() << std::endl;
        throw;
    }
}

std::vector<std::string> SelectionKeysForm::allSelectedKeys() const
{
    std::vector<std::string> selectedIds;
    QModelIndexList selection = m_keysTable->selectionModel()->selection().indexes();

    for(int i=0; i< selection.count(); i++)
    {
        QModelIndex index = selection.at(i);
        if(index.column() == 0 )
            selectedIds.push_back( index.data().toString().toStdString() );
    }

    return selectedIds;
}

void SelectionKeysForm::changeSchema(QString newSchemaName)
{
    closeQuery();
    emit startProcess();
    emit changedSchema(newSchemaName);
}

/// Update db document query
void SelectionKeysForm::resetQuery()
{
    try
    {
        std::vector<std::string> schemalst;
        schemalst.push_back(ui->schemaBox->currentText().toStdString());
        m_keysModel->showQuery( "Vertex Query Widget", schemalst );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "CmSearchQuery", e.what() );
    }
}

void SelectionKeysForm::setQuery( QueryWidget* queryW  )
{
    emit startProcess();
    m_keysModel->setOneQuery(queryW);
}

std::string SelectionKeysForm::vertexSchema() const
{
    return ui->schemaBox->currentText().toStdString();
}

void SelectionKeysForm::changeDBClientData(  QString query )
{
    ui->queryEdit->setText( query );
}


} // namespace jsonui
