//  This is JSONUI library+API (https://bitbucket.org/gems4/jsonui)
//
/// \file SchemaSelectDialog.h
/// Declaration of SchemaSelectDialog - dialog for single or multiple
/// selection fields from schema
//
// JSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONUI depends on the following open-source software products:
// JSONIO (https://bitbucket.org/gems4/jsonio); Qt5 (https://qt.io).
//

#ifndef SchemaSelectDialog_included
#define SchemaSelectDialog_included

#include <QDialog>
#include <QAbstractItemModel>
#include "jsonio/dbquerydef.h"

namespace Ui {
class SchemaSelectDialogData;
}

namespace jsonui {


/// \class SelectSchemaLine represents an item in a tree view, and contains several columns of data.
/// Each items data based on thrift field definition
class SelectSchemaLine
{

public:

    /// The constructor is used to record the item's parent and the data associated with each column.
    SelectSchemaLine( jsonio::ThriftFieldDef*  afldDef, SelectSchemaLine* parent );
    ~SelectSchemaLine();

    /// Set up default template of values for map or array
    void addValue( const char* next );

    /// Add path to list of fields
    void addLink( std::vector<std::string>& fieldsList, const std::string& path = "" );

    // internal data
    size_t ndx;                             ///< Internal ndx
    int type;                               ///< Type of field ( 1- array, 2- map, 0 - other)
    jsonio::ThriftFieldDef* fld_thrift;     ///< Thrift description of line

    int levels;                          ///< Number of levels for array or map
    QString defValue;                    ///< Default value of field
    QVector<QString> fldValue;           ///< Values into columns (key, value, comment)

    SelectSchemaLine *parent;
    std::vector<std::unique_ptr<SelectSchemaLine>> children;
};


/// \class TSelectSchemaModel
/// class for represents the data set and is responsible for fetching the data
/// is needed for viewing and for selecting.
/// Each items data based on thrift field definition for current thrift schema
class TSelectSchemaModel: public QAbstractItemModel
{
    Q_OBJECT


public:

    /// The constructor for model based on schemaName thrift schema
    TSelectSchemaModel( const std::string& schemaName,  const QStringList& aHeaderData,   QObject* parent = nullptr );
    ~TSelectSchemaModel();

    QModelIndex index(int row, int column, const QModelIndex& parent) const;
    QModelIndex parent(const QModelIndex& child) const;
    int rowCount ( const QModelIndex& parent ) const;     //ok
    int columnCount ( const QModelIndex& parent  ) const; // ok
    QVariant data ( const QModelIndex& index, int role ) const;
    bool setData ( const QModelIndex& index, const QVariant & value, int role );
    QVariant headerData ( int section, Qt::Orientation orientation, int role ) const;
    Qt::ItemFlags flags ( const QModelIndex& index ) const;

    /// Special function for access to internal item data
    SelectSchemaLine* lineFromIndex(const QModelIndex& index) const;

protected:

    QStringList hdData;
    std::string schemaName;
    jsonio::ThriftStructDef* strDef;
    SelectSchemaLine* rootNode;

    QString getDescription( const QModelIndex& index ) const;

    // Read internal data from json schema
    void struct2model( jsonio::ThriftStructDef* strDef, SelectSchemaLine* parent );
    void field2model( jsonio::ThriftFieldDef* fldDef, SelectSchemaLine* parent );
    void list2model( size_t level, jsonio::ThriftFieldDef* fldDef, SelectSchemaLine* parent );
};

/// \class SchemaSelectDialog class provides a dialog that allow users to select
/// items from thrift schema fields list
class SchemaSelectDialog : public QDialog
{
    Q_OBJECT

public slots:
    void objectChanged() {}

protected slots:
    void CmHelp();
    virtual void CmReset();
    virtual void CmDefault();

public:

    /// Multiple select constructor
    /// Construct tree view of fields thrift schema
    /// \param schemaName.
    /// \param def_fieldsList - default selection
    SchemaSelectDialog( QWidget* parent, const char* title,
                        const std::string& schemaName,
                        const std::vector<std::string>& def_fieldsList );

    /// Single select constructor.
    /// Construct tree view of fields thrift schema schemaName.
    SchemaSelectDialog( QWidget* parent, const char* title,
                        const std::string& schemaName );

    /// Destructor
    virtual ~SchemaSelectDialog();

    /// Returns single selection
    ///    returns empty std::string if nothing selected
    std::string selIndex( jsonio::ThriftFieldDef** flddata = nullptr );

    /// Returns selection array
    ///    array is empty if nothing is selected
    std::vector<std::string> allSelected();

    /// Returns selection array types
    ///    array is empty if nothing is selected
    std::vector<jsonio::ThriftFieldDef*> allSelectedTypes()
    {
        return arr_types;
    }

private:

    bool multi_selection;
    std::string schema_name;
    std::vector<std::string> def_fields;
    std::vector<jsonio::ThriftFieldDef*> arr_types;

    Ui::SchemaSelectDialogData *ui;
    TSelectSchemaModel *model_schema;

    void setupWindow(const char* title );
    void defineMultiSelection( const std::vector<std::string>& sel );
    void selectWithChildren( TSelectSchemaModel* _model, const QModelIndex& parIndex,
                             const QModelIndexList& selIndexes, std::vector<std::string>& arr,
                             std::vector<jsonio::ThriftFieldDef*>& SchData );
    bool selectField(  TSelectSchemaModel* _model,
                       const QModelIndex& parIndex, std::queue<std::string> names );


};

} // namespace jsonui

#endif // SchemaSelectDialog_included
