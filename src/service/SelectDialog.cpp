//  This is JSONUI library+API (https://bitbucket.org/gems4/jsonui)
//
/// \file SelectDialog.cpp
/// Implementation of class SelectDialog - dialog for single or multiple
/// selection from list/table; ChooseFileOpen, ChooseFileSave - service
/// functions
//
// JSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONUI depends on the following open-source software products:
// JSONIO (https://bitbucket.org/gems4/jsonio); Qt5 (https://qt.io).
//

#include <algorithm>
#include <QFileInfo>
#include <QFileDialog>
#include <QHeaderView>
#include <QImageWriter>
#include "SelectDialog.h"
#include "ui_SelectDialog4.h"
#include "preferences.h"
#include "TCSVPage.h"

namespace jsonui {

const QString jsonFilters = "JSON files (*.json);;All files (*.*)";
const QString textFilters = "All files (*);;Text files (*.txt)";

#ifndef IMPEX_OFF
const QString domFilters =  "JSON files (*.json);;YAML files (*.yaml);;XML files (*.xml);;All files (*.*)";
const QString yamlFilters = "YAML files (*.yaml);;All files (*.*)";
const QString xmlFilters =  "XML files (*.xml);;All files (*.*)";
#else
const QString domFilters =  jsonFilters;
#endif

QString imageFilters()
{
    static  QString filterLine;

    if( filterLine.isEmpty() )
    {
        const QList<QByteArray> imageFormats = QImageWriter::supportedImageFormats();

        QStringList filter;
        filter.clear();
        filter += "PDF Documents (*.pdf)";
#ifndef QWT_NO_SVG
        filter += "SVG Documents (*.svg)";
#endif

        if ( imageFormats.size() > 0 )
        {
            QString imageFilter;
            for ( int i = 0; i < imageFormats.size(); i++ )
            {

                imageFilter = imageFormats[i].toUpper();
                imageFilter += "   Image (*.";
                imageFilter +=  imageFormats[i];
                imageFilter += ")";
                filter += imageFilter;
            }
        }
        filterLine = filter.join( ";;" );
    }
    return  filterLine;
}


SelectTable::SelectTable( const char * aname, const std::vector<std::string>& list,
                          const char  splitCol ):
    TAbstractDataContainer(aname)
{
    if(list.size() >0 )
    {
        int jj;
        QString line;
        QStringList cells;

        // get values
        for(size_t ii=0;  ii<list.size(); ii++ )
        {
            line =  list[ii].c_str();
            cells = line.split(splitCol, QString::KeepEmptyParts/*Qt::KeepEmptyParts*/);
            QVector<QVariant> vec;
            matrix.push_back( vec );
            for( jj=0; jj<cells.count(); jj++ )
                matrix.back().push_back( cells[jj] );
        }

        // get header
        for( jj=0; jj<cells.count(); jj++ )
            colHeads.push_back( QString("%1").arg(jj) );
    }
}

SelectTable::SelectTable( const char * aname,
                          const jsonio::ValuesTable& list ):
    TAbstractDataContainer(aname)
{
    if(list.size() >0 )
    {
        size_t ii, jj;

        // get header
        for( jj=0; jj< list[0].size(); jj++ )
            colHeads.push_back( QString("%1").arg(jj) );

        // get values
        for( ii=0;  ii<list.size(); ii++ )
        {
            QVector<QVariant> vec;
            matrix.push_back( vec );
            for( jj=0; jj<list[ii].size(); jj++ )
                matrix.back().push_back( list[ii][jj].c_str() );
        }
    }
}

SelectTable::SelectTable( const char * aname,
                          const jsonio::ValuesTable& list, const std::vector< std::string >& headerData ):
    TAbstractDataContainer(aname)
{

    for(size_t jj=0; jj<headerData.size(); jj++ )
        colHeads.push_back( headerData[jj].c_str() );

    if(list.size() >0 )
    {
        size_t ii, jj;

        // get header
        if( colHeads.empty() )
            for( jj=0; jj< list[0].size(); jj++ )
                colHeads.push_back( QString("%1").arg(jj) );

        // get values
        for( ii=0;  ii<list.size(); ii++ )
        {
            QVector<QVariant> vec;
            matrix.push_back( vec );
            for( jj=0; jj< static_cast<size_t>(colHeads.count()); jj++ )
            {
                if( jj < list[ii].size() )
                    matrix.back().push_back( list[ii][jj].c_str() );
                else
                    matrix.back().push_back( "" );
            }
        }
    }
}


SelectTable::SelectTable( const char * aname,
                          const QVector< QVector<QVariant> >& tableValues, const QVector< QString >& headerData ):
    TAbstractDataContainer(aname), colHeads(headerData), matrix(tableValues)
{
    if(colHeads.empty() && !matrix.empty() )
        for(int jj=0; jj< matrix[0].size(); jj++ )
            colHeads.push_back( QString("%1").arg(jj) );
}

SelectTable::~SelectTable()
{}

//----------------------------------------------------------------

// Single select constructor
SelectDialog::SelectDialog( bool isMultiSelection, QWidget* parent, const char* title,
                            const std::vector<std::string>& list,
                            const char  splitCol, int mode):
    QDialog(parent),
    multi(isMultiSelection), tablemode(mode), data("select",list,splitCol ),
    ui(new Ui::SelectDialogData)
{
    ui->setupUi(this);
    if( multi )
        defineMultiSelectionTable( title );
    else
        defineSingleSelectionTable( title );
}

/* Single select constructor
SelectDialog::SelectDialog( bool isMultiSelection, QWidget* parent, const char* title,
              const jsonio::ValuesTable& list, int mode):
    QDialog(parent),
    multi(isMultiSelection), tablemode(mode), data("select",list ),
    ui(new Ui::SelectDialogData)
{
   ui->setupUi(this);
   if( multi )
       defineMultiSelectionTable( title );
   else
       defineSingleSelectionTable( title );
}*/

SelectDialog::SelectDialog(bool isMultiSelection, QWidget *parent, const char *title,
                           const jsonio::ValuesTable &list, const std::vector<std::string> &headerData, int mode):
    QDialog(parent),
    multi(isMultiSelection), tablemode(mode), data("select",list, headerData ),
    ui(new Ui::SelectDialogData)
{
    ui->setupUi(this);
    if( multi )
        defineMultiSelectionTable( title );
    else
        defineSingleSelectionTable( title );
}

// Single select constructor.
SelectDialog::SelectDialog( bool isMultiSelection, QWidget* parent, const char* title,
                            const QVector< QVector<QVariant> >& tableValues,
                            const QVector< QString >& headerData, int mode ):
    QDialog(parent),
    multi(isMultiSelection), tablemode(mode), data("select", tableValues, headerData ),
    ui(new Ui::SelectDialogData)
{
    ui->setupUi(this);
    if( multi )
        defineMultiSelectionTable( title );
    else
        defineSingleSelectionTable( title );
}

// Destructor
SelectDialog::~SelectDialog()
{
    delete pTable;
    delete ui;
}

void SelectDialog::defSelectionTable()
{
    TMatrixModel* model = new TMatrixModel( &data, this );
    pTable = new TMatrixTableProxy(this, tablemode/*TMatrixTable::tbNoMenu*/ );
    TMatrixDelegate* deleg = new TMatrixDelegate();
    pTable->setItemDelegate(deleg);
    pTable->setModel(model);  // proxy into

    if( !(tablemode & TMatrixTable::tbSort) )
    {
        pTable->horizontalHeader()->hide();
    }
    ui->gridLayout->addWidget(pTable, 1, 0, 1, 5);
    pTable->setSelectionBehavior(QAbstractItemView::SelectRows);
    pTable->setEditTriggers( QAbstractItemView::AnyKeyPressed );
}


void SelectDialog::defineSingleSelectionTable(const char *title)
{
    setWindowTitle(title);

    defSelectionTable();
    pTable->setSelectionMode(QAbstractItemView::SingleSelection);
    pTable->setCurrentRow( 0 );

    ui->pButton3->hide();
    ui->pButton2->hide();
    QObject::connect( ui->bHelp, SIGNAL( clicked() ), this, SLOT( CmHelp() ) );
    QObject::connect(pTable, SIGNAL(doubleClicked(const QModelIndex & )), this, SLOT(accept()));
    pTable->setFocus();
}


void SelectDialog::defineMultiSelectionTable(const char *title)
{
    setWindowTitle(title);

    defSelectionTable();
    pTable->setSelectionMode(QAbstractItemView::MultiSelection);
    QObject::connect( ui->bHelp, SIGNAL( clicked() ), this, SLOT( CmHelp() ) );
    pTable->setFocus();
}


void SelectDialog::CmSelectAll()
{
    if( multi == true )
        pTable->selectAll();
}


void SelectDialog::CmClearAll()
{
    pTable->clearSelection();
}

/// Returns single selection,
/// returns '-1' if nothing selected
int SelectDialog::selIndex() const
{
    if( !result() )
        return -1;
    return pTable->selectedRow();
}

std::set<size_t> SelectDialog::allSelected() const
{
    if( !result() )
        return {};
    return pTable->allSelectedRows();
}

// Returns single selection,
// Throw exeption if nothing selected
size_t SelectDialog::getSelectedIndex() const
{
    auto selndx = selIndex();
    jsonio::jsonioErrIf( selndx<0, "SelectionError", "Undefined single selection" );
    return static_cast<size_t>(selndx);
}


void SelectDialog::CmHelp()
{
    helpWin( "SelectJsonui", "" );
}

// Service functions -----------------------------------


bool ChooseFileOpen(QWidget* par, std::string& path_,
                    const char* title, const QString& filter )
{
    QString path;
    if( path_.find('/') == std::string::npos )
        path  =  QString(jsonio::ioSettings().userDir().c_str())+"/"+path_.c_str();
    else
        path = path_.c_str();

    QString filt;
    if( filter.isEmpty() )
        filt = "All files (*)";
    else
        filt = filter;

    QString fn = QFileDialog::getOpenFileName(  par, title,
                                                path, filt, nullptr, QFileDialog::DontConfirmOverwrite );
#ifdef buildWIN32
    std::replace( fn.begin(), fn.end(), '/', '\\');
#endif
    if ( !fn.isEmpty() )
    {
        QFileInfo flinfo(fn);
        jsonio::ioSettings().setUserDir( flinfo.dir().path().toStdString() );
        path_ = fn.toStdString();
        return true;
    }
    else
    {
        path_ = "";
        return false;
    }

}

bool ChooseFileSave(QWidget* par, std::string& path_,
                    const char* title, const QString& filter,  const std::string& schemafilter )
{
    QString path;
    if( path_.find('/') == std::string::npos )
        path  =  QString(jsonio::ioSettings().userDir().c_str())+"/"+path_.c_str();
    else
        path = path_.c_str();

    //replace(path.begin(), path.end(),'\\', '/');

    QString filt;
    if( !schemafilter.empty() )
        filt += QString("Schema (%1)").arg(schemafilter.c_str())+";;";
    if( filter.isEmpty() )
        filt += "All files (*.*)";
    else
        filt += filter;

    QString selectedFilter;
    QString fn = QFileDialog::getSaveFileName( par, title,
                                               path, filt, &selectedFilter, QFileDialog::DontConfirmOverwrite);

    if ( !fn.isEmpty() )
    {
        QFileInfo flinfo(fn);
        jsonio::ioSettings().setUserDir(flinfo.dir().path().toStdString() );
        if( flinfo.suffix().isEmpty() ) // solwing for linux
        {
            int posb = selectedFilter.lastIndexOf(".");
            if( !schemafilter.empty() )
                posb = selectedFilter.indexOf(".");
            int pose = selectedFilter.indexOf(")", posb);
            if( posb > 0 && pose > posb )
            { QString ext = selectedFilter.mid(posb+1, pose-posb-1);
                fn += "."+ext;
            }
        }
        path_ = fn.toStdString();
        return true;
    }
    else
    {
        path_  = "";
        return false;
    }

}


} // namespace jsonui

//--------------------- End of SelectDialog.cpp ---------------------------

