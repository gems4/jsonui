//  This is JSONUI library+API (https://bitbucket.org/gems4/jsonui)
//
/// \file dbdocumentmodel.h
/// Declarations of classes DBDocumentModel and DBEdgeDocumentModel
/// implements object to work with database document (vertex and edge).
//
// JSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2018 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONUI depends on the following open-source software products:
// JSONIO (https://bitbucket.org/gems4/jsonio); Qt5 (https://qt.io).
//

#ifndef DBDOCUMENTMODEL_H
#define DBDOCUMENTMODEL_H

#include <QObject>
#include "dbkeysmodel.h"

#ifndef IMPEX_OFF
namespace jsonio {
   class ImpexFormatFile;
}
#endif

namespace jsonui {


/// \class DBDocumentModel object to work with database document (vertex)
class DBDocumentModel: public QObject
{
    Q_OBJECT

protected:

    /// Current schema name
    std::string _schemaName;

    /// Current link to Arango Database
    std::shared_ptr<jsonio::TDBVertexDocument> _dbgraph;

    /// Current document query
    jsonio::DBQueryData _defaultQuery;
    bool _isDefaultQuery = false;

signals:

    /// signal when db document changed
    void changedDBClient(  jsonio::TDBVertexDocument* adbgraph );

    /// signal when db document query changed
    void changedQuery(  QString query );

    /// To update keys model data
    void changedModel( QString key );

    /// Finished process
    void finished();

    /// Finished with exception
    void isException( const QString& titl, const QString& msg );

    /// Update oid
    void updatedOid( QString oid );

    /// After open new document
    void openedDocument(  QString jsonDocument  );

    /// Signal of deleting documents
    /// Possible delete edges too ( reload all open edges )
    void deletedDocument();

    /// Signal of loading graph documents
    /// Load records to different collections (reload all open )
    void loadedGraph();

public slots:

    /// Reset current DB client
    virtual void dbResetClient(  QString aschemaName );

    /// Update query
    virtual void dbChangeQuery( QString schema, std::shared_ptr<jsonio::DBQueryDef> querydef );
    /// Reload query data
    virtual void dbReloadQuery();

    /// Open document
    virtual void dbReadDocument(  QString reckey  );

    /// Save json Document to database
    virtual void dbUpdate( QString jsonDocument );

    /// Delete keyDocument document from database
    virtual void dbDelete( QString keyDocument );

    /// Delete documents from database
    virtual void dbDeleteList( QVector<QString> keysDocument );

    /// Read multiple records from file fileName
    virtual void dbRestoreMultipleRecordsfromFile( QString fileName );

    /// Write multiple records to file fileName
    virtual void dbBackupQueriedRecordstoFile( QString fileName, QVector<QString> keysDocument);

#ifndef IMPEX_OFF
    /// Read bson records from format file fileName
    void dbImportFormat( std::shared_ptr<jsonio::ImpexFormatFile> inputIEFile,
                         QString dataFile, bool overwrite  );

    /// Write json record to format file fileName
    void dbExportFormat( std::shared_ptr<jsonio::ImpexFormatFile> inputIEFile,
                         QString fileName, QVector<QString> keysDocument );
#endif

    /// Write graph to file fileName
    void dbBackupGraphtoFile( QString fileName, std::vector<std::string> keysDocument);

    /// Read multiple records from file fileName
    void dbRestoreGraphfromFile( QString fileName );


public:

    DBDocumentModel( const std::string& aschemaName, const jsonio::DBQueryData& query=jsonio::emptyQuery ):
        _schemaName(aschemaName), _dbgraph(nullptr),
        _defaultQuery(query),    _isDefaultQuery(!query.empty())
    {
        // now in main thread
        //dbResetClient( _schemaName );
    }

    ~DBDocumentModel()
    { }

};

/// \class DBEdgeDocumentModel object to work with database edge document
class DBEdgeDocumentModel : public DBDocumentModel
{
    Q_OBJECT

    /// Edge change schema mode
    bool _mode = false;

    // Incoming vertexes list data

    /// Current incoming vertexes query
    jsonio::DBQueryData queryIn;
    /// Database connection incoming vertexes object
    std::shared_ptr<jsonio::TDBVertexDocument> inDB;

    // Outgoing vertexes list data

    /// Current outgoing vertexes query
    jsonio::DBQueryData queryOut;
    /// Database connection outgoing vertexes object
    std::shared_ptr<jsonio::TDBVertexDocument> outDB;

signals:

    /// Fixed incoming query
    void fixIncoming();

    /// signal when incoming document changed
    void changedIncomingDBClient(  jsonio::TDBVertexDocument* adbgraph );

    /// To update Incoming keys model data
    void changedIncomingModel( QString key );

    /// Fixed Outgoing query
    void fixOutgoing();

    /// signal when Outgoing document changed
    void changedOutgoingDBClient(  jsonio::TDBVertexDocument* adbgraph );

    /// To update Outgoing keys model data
    void changedOutgoingModel( QString key );

    /// Reset TypeBox
    void resetTypeBox( QString newschemaName );

public slots:

    /// Reset current DB client
    virtual void dbResetClient(  QString aschemaName );

    /// Reset current DB schema
    virtual void dbResetSchema(  QString aschemaName );

    /// Reload query data
    virtual void dbReloadQuery();

    /// Update Incoming  query
    void dbChangeIncomingQuery( QString schema, std::shared_ptr<jsonio::DBQueryDef> querydef );

    /// Update Outgoing  query
    void dbChangeOutgoingQuery( QString schema, std::shared_ptr<jsonio::DBQueryDef> querydef );

    /// Open document
    virtual void dbReadDocument(  QString reckey  );

    /// Read multiple records from file fileName
    virtual void dbRestoreMultipleRecordsfromFile( QString fileName );

public:

    DBEdgeDocumentModel( const std::string& aschemaName, const jsonio::DBQueryData& query=jsonio::emptyQuery ):
        DBDocumentModel( aschemaName, query ), _mode(_isDefaultQuery)
    {
        // now in main thread
        //dbResetClient( _schemaName );
    }

    ~DBEdgeDocumentModel()
    { }

private:

    /// Update query
    bool changeQueryToId( jsonio::TDBVertexDocument* _dbvertex, const std::string& aid );

    /// Open incoming and outgoing vertex documents
    void resetInOutDBClients();
};


} // namespace jsonui



#endif // DBDOCUMENTMODEL_H
