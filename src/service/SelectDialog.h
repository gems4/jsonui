//  This is JSONUI library+API (https://bitbucket.org/gems4/jsonui)
//
/// \file SelectDialog.h
/// Declaration of SelectDialog - dialog for single or multiple
/// selection from list/table; ChooseFileOpen, ChooseFileSave - service
/// functions
//
// JSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONUI depends on the following open-source software products:
// JSONIO (https://bitbucket.org/gems4/jsonio); Qt5 (https://qt.io).
//

#ifndef SelectDialog_included
#define SelectDialog_included

#include <string>
#include <iostream>
#include <set>
#include <QDialog>
#include "model_table.h"


namespace Ui {
class SelectDialogData;
}

namespace jsonui {

/// Open&Save Image File filters
QString imageFilters();

/// Open&Save text File filters
extern const QString textFilters;

/// Open&Save JSON or YAML or XML File filters
extern const QString domFilters;
/// Open&Save JSON File filters
extern const QString jsonFilters;

#ifndef IMPEX_OFF

/// Open&Save YAML File filters
extern const QString yamlFilters;
/// Open&Save XML File filters
extern const QString xmlFilters;

#endif

/// Function returns true and an existing file selected by the user into param path_.
/// If the user presses Cancel, it returns false
bool ChooseFileOpen(QWidget* par, std::string& path_,
   const char* title, const QString& filter );

/// Function returns true and an existing file selected by the user into param path_.
/// If the user presses Cancel, it returns false
/// The file does not have to exist.
bool ChooseFileSave(QWidget* par, std::string& path_,
   const char* title, const QString& filter,  const std::string& schemafilter = "" );



/// Class for internal table data container
class SelectTable : public TAbstractDataContainer
{
    QVector< QString > colHeads;
    QVector< QVector<QVariant> > matrix;


 public:

   /// Construct table from list of lines.
   /// Split each line into a list of columns by delimiter
   SelectTable( const char * aname, const std::vector<std::string>& list,
                const char  splitCol = '\t' );

   /// Construct table from 2D array.
   SelectTable( const char * aname, const jsonio::ValuesTable& list );

   SelectTable( const char * aname, const jsonio::ValuesTable& list, const std::vector<std::string>& headerData );
   /// Construct table from 2D array.
   SelectTable( const char * aname, const QVector< QVector<QVariant> >& tableValues,
                const QVector< QString >& headerData = {});

   virtual ~SelectTable();

   // data to model

   int rowCount() const
   { return matrix.size();  }

   int columnCount() const
   { return colHeads.size(); }

   QVariant data( int line, int column ) const
   {  return matrix.at( line ).at( column ); }

   bool setData( int /*line*/, int /*column*/, const QVariant & /*value*/ )
   {    //matrix[line].replace(column, value);
        return true;
   }

   virtual QString headerData ( int section ) const
   { return colHeads[section]; }

   virtual bool  IsEditable( int /*line*/, int /*column*/ ) const
   { return false; }

   virtual int getType( int /*line*/, int /*column*/ ) const
   { return ftString; }

   virtual QString getToolTip( int line, int /*column*/ ) const
   { return QString("%1").arg(line); }

   void resetData()
   { }

};

/// The SelectDialog class provides a dialog that allow users to select items from list
class SelectDialog : public QDialog
{
    Q_OBJECT

    bool multi;
    int tablemode;
    SelectTable data;
    TMatrixTableProxy *pTable;

    void defSelectionTable();
    void defineSingleSelectionTable( const char* title );
    void defineMultiSelectionTable( const char* title );

    //void setSingleSelection( int sel );
    //void setMultiSelection( const std::vector<int>& sel );

public slots:

    void objectChanged() { }

protected slots:

    void CmHelp();
    virtual void CmSelectAll();
    virtual void CmClearAll();

public:

    /// Construct table from list of lines.
    /// Split each line into a list of columns by delimiter
    SelectDialog( bool isMultiSelection, QWidget* parent, const char* title,
                  const std::vector<std::string>& list,
                  const char  splitCol = '\t',
                  int mode = TMatrixTable::tbNoMenu );

    /* Construct table from 2D array.
    SelectDialog( bool isMultiSelection, QWidget* parent, const char* title,
                  const jsonio::ValuesTable& list,
                  int mode = TMatrixTable::tbNoMenu );
    */

    /// Construct table from 2D array.
    SelectDialog( bool isMultiSelection, QWidget* parent, const char* title,
                  const jsonio::ValuesTable& list,
                  const std::vector< std::string >& headerData = {},
                  int mode = TMatrixTable::tbNoMenu );

    /// Construct table from 2D array.
    SelectDialog( bool isMultiSelection, QWidget* parent, const char* title,
                  const QVector< QVector<QVariant> >& tableValues,
                  const QVector< QString >& headerData = {},
                  int mode = TMatrixTable::tbNoMenu );

    /// Destructor
    virtual ~SelectDialog();

    /// Returns single selection,
    /// returns '-1' if nothing selected
    int selIndex() const;

    /// Returns single selection,
    /// Throw exeption if nothing selected
    std::size_t getSelectedIndex() const;

    /// Returns selection array
    /// Array is empty if nothing is selected
    std::set<size_t> allSelected() const;

    /// Get Multiple selection
    template<typename T,
        template <typename, typename = std::allocator<T>> class Container>
    void getSelection( Container<T>& selrows )
    {
        selrows.clear();
        auto allrows = allSelected();
        for (auto row: allrows )
        {
            selrows.push_back(static_cast<T>(row));
        }
    }

    /// Set Singler selection
    template <class T>
    void setSelection( const T& selrow )
    {
        pTable->setCurrentRow(  static_cast<int>(selrow) );
    }

    /// Set Multiple selection
    template<typename T,
             template <typename, typename = std::allocator<T>> class Container>
    void setSelection( const Container<T>& selrows )
    {
        pTable->setSelection( selrows );
    }

private:
    Ui::SelectDialogData *ui;

};

} // namespace jsonui

#endif // SelectDialog_included
