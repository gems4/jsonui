//  This is JSONUI library+API (https://bitbucket.org/gems4/jsonui)
//
/// \file SchemaSelectDialog.cpp
/// Implementation of class SchemaSelectDialog - dialog for single or multiple
/// selection from schema tree
//
// JSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONUI depends on the following open-source software products:
// JSONIO (https://bitbucket.org/gems4/jsonio); Qt5 (https://qt.io).
//

#include <QPushButton>
#include "SchemaSelectDialog.h"
#include "ui_SchemaSelectDialog4.h"
#include "model_schema.h"
#include "preferences.h"
using namespace jsonio;

namespace jsonui {

void SchemaSelectDialog::setupWindow(const char* title )
{
    ui->setupUi(this);
    setWindowTitle(title);

    // set up model
    QStringList aHeaderData;
    aHeaderData << "key" << "indexes" << "comment"  ;
    model_schema = new TSelectSchemaModel( schema_name, aHeaderData, this );
    ui->schemaView->setModel(model_schema);
    ui->schemaView->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->schemaView->setEditTriggers( QAbstractItemView::AnyKeyPressed );
    ui->schemaView->header()->resizeSection(0, 250);
    ui->schemaView->expandToDepth(0);

    if(multi_selection)
    {
        QPushButton * btn = ui->buttonBox->addButton(QDialogButtonBox::Reset);
        QObject::connect( btn, SIGNAL(clicked()), this, SLOT(CmReset()));
        btn = ui->buttonBox->addButton(QDialogButtonBox::RestoreDefaults);
        QObject::connect( btn, SIGNAL(clicked()), this, SLOT(CmDefault()));
    }
    else
    {
        ui->schemaView->setSelectionMode(QAbstractItemView::SingleSelection);
    }

    ui->schemaView->setFocus();
    QObject::connect( ui->buttonBox, SIGNAL(helpRequested()), this, SLOT(CmHelp()));
}


// Select constructor
SchemaSelectDialog::SchemaSelectDialog( QWidget* parent, const char* title,
                                        const std::string& schemaName, const std::vector<std::string>& adef_fieldsList ):
    QDialog(parent), multi_selection(true), schema_name(schemaName), def_fields(adef_fieldsList),
    ui(new Ui::SchemaSelectDialogData)
{
    setupWindow( title );
    defineMultiSelection( def_fields );
}


SchemaSelectDialog::SchemaSelectDialog( QWidget* parent, const char* title,
                                        const std::string& schemaName ):
    QDialog(parent), multi_selection(false), schema_name(schemaName),
    ui(new Ui::SchemaSelectDialogData)
{
    setupWindow( title );
}


// Destructor
SchemaSelectDialog::~SchemaSelectDialog()
{
    delete ui;
}

// Select field by fieldpath
bool SchemaSelectDialog::selectField(  TSelectSchemaModel* _model,
                                       const QModelIndex& parIndex, std::queue<std::string> names )
{
    SelectSchemaLine* item =  _model->lineFromIndex(parIndex);
    for( int ii=0; ii<item->levels; ii++ )  //skip array and map names
        if( !names.empty() )
            names.pop();

    // select current line
    if( names.empty() )
    {
        ui->schemaView->selectionModel()->select( parIndex,
                QItemSelectionModel::Rows|QItemSelectionModel::Select );
        return true;
    }

    // test children
    std::string fname = names.front();
    names.pop();

    QModelIndex _index;
    for(int rw = 0; rw < _model->rowCount( parIndex ); rw++ )
    {
        _index = _model->index( rw, 0, parIndex);
        std::string fldname = _model->data( _index, Qt::DisplayRole ).toString().toUtf8().data();
        if( fldname == fname )
            return selectField( _model, _index, names );
    }

    return false;  // not found
}


void SchemaSelectDialog::defineMultiSelection( const std::vector<std::string>& sel )
{
    ui->schemaView->setSelectionMode(QAbstractItemView::MultiSelection);
    TSelectSchemaModel* _model =dynamic_cast<TSelectSchemaModel*>( ui->schemaView->model());
    if( !_model )
        return;

    for( uint jj=0; jj< sel.size(); jj++ )
    {
        std::queue<std::string> names = split(sel[jj], ".");
        selectField( _model, ui->schemaView->rootIndex(), names );
    }
}


void SchemaSelectDialog::CmReset()
{
    ui->schemaView->clearSelection();
    //defineMultiSelection( _database->GetQueryFields() );
}


void SchemaSelectDialog::CmDefault()
{
    ui->schemaView->clearSelection();
    defineMultiSelection( def_fields );
}


void SchemaSelectDialog::selectWithChildren( TSelectSchemaModel* _model,
        const QModelIndex& parIndex, const QModelIndexList& selIndexes,
        std::vector<std::string>& arr, std::vector<jsonio::ThriftFieldDef*>& aschdata )
{
    QModelIndex index;
    for(int rw = 0; rw < _model->rowCount( parIndex ); rw++ )
    {
        index = _model->index( rw, 0, parIndex);
        if( selIndexes.contains( index ) )
        {
            SelectSchemaLine* item =  _model->lineFromIndex(index);
            item->addLink(arr);
            aschdata.push_back( item->fld_thrift );
        }
        selectWithChildren( _model, index, selIndexes, arr, aschdata );
    }
}

std::vector<std::string> SchemaSelectDialog::allSelected()
{
    std::vector<std::string> arr;
    if( !result() )
        return arr;
    QModelIndexList selection = ui->schemaView->selectionModel()->selectedRows();
    TSelectSchemaModel* _model =dynamic_cast<TSelectSchemaModel*>( ui->schemaView->model());
    if( !_model )
        return arr;

    // Multiple rows can be selected (order of  structure)
    selectWithChildren( _model, ui->schemaView->rootIndex(), selection, arr, arr_types );

    return arr;
}

std::string SchemaSelectDialog::selIndex( jsonio::ThriftFieldDef** flddata )
{
    QModelIndex index = ui->schemaView->currentIndex();

    if (index.isValid())
    {
        const TSelectSchemaModel* _model =
                dynamic_cast<const TSelectSchemaModel*>( index.model());
        std::vector<std::string> selected;
        if( _model )
        {
            SelectSchemaLine* item =  _model->lineFromIndex(index);
            item->addLink( selected );
            if( flddata )
                *flddata = item->fld_thrift;
            return selected[0];
        }
    }
    return "";
}


void SchemaSelectDialog::CmHelp()
{
    helpWin( "SchemaSelectJsonui", "" );
}


//------------------------------------------------------------------------

SelectSchemaLine::SelectSchemaLine( ThriftFieldDef*  afldDef, SelectSchemaLine* aparent ):
    ndx(0),  type(0), levels(0), defValue(""), parent(aparent)
{
    if( parent )
    {
        ndx = parent->children.size();
        parent->children.push_back( std::unique_ptr<SelectSchemaLine>(this) );
    }

    if( afldDef )
    {
        fldValue.append(afldDef->fName.c_str());
        fldValue.append("");
        //if( TBsonSchemaModel::showComments  )
        fldValue.append(afldDef->fDoc.c_str());
        fld_thrift = afldDef;
        //thiftdata.thrifttype = afldDef->fTypeId.front(); //afldDef->fTypeId.back();
        //thiftdata.enumname = afldDef->className;
    }
    else
    {
        fldValue.append("root");
        fldValue.append("");
        //if( TBsonSchemaModel::showComments  )
        fldValue.append("");
    }
}

SelectSchemaLine::~SelectSchemaLine()
{
    children.clear();
}

void SelectSchemaLine::addValue(const char *next)
{
    if( defValue.isEmpty() )
        defValue = next;
    else
        defValue += QString(".")+next;
    levels++;
    fldValue[1] = defValue;
}

void SelectSchemaLine::addLink( std::vector<std::string>& fieldsList, const std::string& path )
{
    if( !parent )
    {
        fieldsList.push_back(path);
        return;
    }

    std::string new_path = fldValue[0].toStdString();
    if( type > 0  && ( !fldValue[1].isEmpty() && children.size() > 0 ) )
    {
        std::string fielndx =  fldValue[1].toStdString();
        std::queue<std::string> names = split(fielndx, ";");
        std::string curname;
        while(  !names.empty() )
        {
            if( !names.front().empty() )
            {  curname = new_path + "." + names.front();
                if( !path.empty() )
                    curname += "."+ path;
                parent->addLink( fieldsList, curname );
            }
            names.pop();
        }
    }
    else
    {
        if( !path.empty())
            new_path += "."+ path;
        parent->addLink( fieldsList, new_path );
    }
}

// Set up struct without bson data
void TSelectSchemaModel::struct2model( ThriftStructDef* astrDef, SelectSchemaLine* parent )
{
    ThriftFieldDef*  fldDef;
    for(uint ii=0; ii<astrDef->fields.size(); ii++ )
    {
        fldDef = &astrDef->fields[ii];
        field2model( fldDef, parent );
    }
}

void TSelectSchemaModel::field2model( ThriftFieldDef*  fldDef, SelectSchemaLine* parent )
{
    // add line to tree view
    SelectSchemaLine *line = new SelectSchemaLine( fldDef, parent );
    // add levels for objects and arrays
    list2model( 0, fldDef, line );
}

// Read array
void TSelectSchemaModel::list2model( size_t level,
                                     ThriftFieldDef*  fldDef, SelectSchemaLine* line )
{
    // add levels for objects and arrays
    switch (fldDef->fTypeId[level])
    {
    // main constructions
    case Th_STRUCT:
    {
        // structure
        if( fldDef->className.empty() )
            jsonioErr("bson_to_list" , "Undefined struct name into schema" );
        ThriftStructDef* strDef2 = ioSettings().Schema()->getStruct( fldDef->className);
        if( strDef2 == nullptr )
            jsonioErr( fldDef->className , "Undefined struct definition" );
        struct2model( strDef2,  line );
    }
        break;
    case Th_MAP:
    { // map
        line->type = 2;
        line->addValue("key");
        list2model( level+2, fldDef, line );
    }
        break;
    case Th_LIST:
    case Th_SET:
    { // !! next level
        if(line->type == 0)
            line->type = 1;
        line->addValue("0");
        list2model( level+1, fldDef, line );
    }
        break;
    default:     break;
    }
}


//--------------------------------------------------------------------------------------
// class TSelectSchemaModel
// class for represents the data set and is responsible for fetching the data
// is needed for viewing and for selecting.
// Selecting data from ThriftSchema object
//---------------------------------------------------------------------------------------

TSelectSchemaModel::TSelectSchemaModel( const std::string& aschemaName,
                                        const QStringList& aHeaderData,   QObject* parent ):
    QAbstractItemModel(parent), hdData( aHeaderData),  schemaName(aschemaName), strDef(nullptr)
{
    if( !schemaName.empty() )
        strDef =  ioSettings().Schema()->getStruct(schemaName);
    if( strDef == nullptr )
        jsonioErr( schemaName, "Undefined struct definition" );

    // set up model data
    rootNode = new SelectSchemaLine( nullptr,  nullptr );
    struct2model(strDef,  rootNode );

    //connect( this, SIGNAL(dataChanged( const QModelIndex&, const QModelIndex& )),
    //          parent,  SLOT(objectChanged()) );
}

TSelectSchemaModel::~TSelectSchemaModel()
{
    if(rootNode )
        delete rootNode;
}

SelectSchemaLine *TSelectSchemaModel::lineFromIndex(const QModelIndex &index) const
{
    if (index.isValid()) {
        return static_cast<SelectSchemaLine *>(index.internalPointer());
    } else {
        return rootNode;
    }
}

QModelIndex TSelectSchemaModel::index(int row, int column, const QModelIndex &parent) const
{
    if (!rootNode)
        return QModelIndex();
    SelectSchemaLine *parentItem = lineFromIndex( parent );
    return createIndex(row, column, parentItem->children[row].get());
}

QModelIndex TSelectSchemaModel::parent(const QModelIndex& child) const
{
    if (!child.isValid())
        return QModelIndex();

    SelectSchemaLine *childItem = lineFromIndex(child);
    SelectSchemaLine *parentItem = childItem->parent;
    if (parentItem == rootNode )
        return QModelIndex();
    return createIndex(parentItem->ndx, 0, parentItem);
}


int TSelectSchemaModel::rowCount( const QModelIndex& parent ) const
{
    if (!rootNode)
        return 0;
    if (parent.column() > 0)
        return 0;
    SelectSchemaLine *parentItem = lineFromIndex( parent );
    return parentItem->children.size();
}

int TSelectSchemaModel::columnCount( const QModelIndex& /*parent*/ ) const
{
    if( TJsonSchemaModel::showComments  )
        return 3;
    return 2;
}

Qt::ItemFlags TSelectSchemaModel::flags( const QModelIndex& index ) const
{
    Qt::ItemFlags flags = QAbstractItemModel::flags(index);
    SelectSchemaLine *item = lineFromIndex( index );
    if( index.column() == 1 && (item->type > 0 ) )
    {
        flags |= Qt::ItemIsEditable;
        return flags;
    }
    else
        return (flags & ~Qt::ItemIsEditable);
}

QVariant TSelectSchemaModel::headerData( int section, Qt::Orientation orientation, int role ) const
{
    if( role == Qt::DisplayRole  && orientation == Qt::Horizontal && section<hdData.size())
        return hdData[section];
    return QVariant();
}

QString TSelectSchemaModel::getDescription( const QModelIndex& index ) const
{
    SelectSchemaLine *item = lineFromIndex( index );
    return item->fldValue[2];
}

QVariant TSelectSchemaModel::data( const QModelIndex& index, int role ) const
{
    if(!index.isValid())
        return QVariant();

    switch( role )
    {
    case Qt::DisplayRole:
    {   SelectSchemaLine *item = lineFromIndex( index );
        if( index.column() < item->fldValue.size()  )
            return item->fldValue[index.column()];
    }
        break;
    case Qt::EditRole:
    {  SelectSchemaLine *item = lineFromIndex( index );
        if( index.column() < item->fldValue.size()  )
            return item->fldValue[index.column()];
    }
        break;
    case Qt::ToolTipRole:
    case Qt::StatusTipRole:
        return  getDescription(index );
    case Qt::ForegroundRole:
    {
        if ( index.column() == 0   )
            return QVariant( QColor( Qt::darkCyan ) );
    }
        break;
    default: break;
    }
    return QVariant();
}

bool TSelectSchemaModel::setData( const QModelIndex& index, const QVariant& value, int role)
{
    if( index.isValid() && ( role == Qt::EditRole ) )
    {
        SelectSchemaLine *line =lineFromIndex(index);
        if(  index.column() <  line->fldValue.size() )
        {
            line->fldValue[index.column()] = QVariant(value).toString();
        }
        return true;
    }
    return false;
}

} // namespace jsonui

//--------------------- End of SchemaSelectDialog.cpp ---------------------------

