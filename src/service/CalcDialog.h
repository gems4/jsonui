//  This is JSONUI library+API (https://bitbucket.org/gems4/jsonui)
//
/// \file CalcDialog.h
/// Declaration of CalcDialog class - internal calculator
//
// JSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONUI depends on the following open-source software products:
// JSONIO (https://bitbucket.org/gems4/jsonio); Qt5 (https://qt.io).
//

#ifndef CalcDialog_included
#define CalcDialog_included

#include <QDialog>
#include <QButtonGroup>

namespace Ui {
class CalcDialogData;
}

namespace jsonui {

class CalcDialog : public QDialog
{
    Q_OBJECT

    QButtonGroup *allButtons;
    
protected slots:
    virtual void setMode( int id);
    virtual void ok();
    virtual void unaryMode();
    void help();
    
public:

    CalcDialog(QWidget* parent );
    virtual ~CalcDialog();

    /// The function returns a result of calculating
    /// the selected function from a given value
    double computeFunction( double val );

    /// The function returns a string representation of the result of calculating
    /// the selected function from a given value
    QString computeFunctionString( double val );

    /// Get the selected function and an additional parameter
    int  extructInternal( double& val );

    /// The function returns a string representation of expression of
    /// the selected function with a given parameter text
    QString expression(const char * valText );

private:
    Ui::CalcDialogData *ui;
 };

} // namespace jsonui

#endif // CalcDialog_included
