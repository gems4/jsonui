//  This is JSONUI library+API (https://bitbucket.org/gems4/jsonui)
//
/// \file CalcDialog.cpp
/// Implementation of CalcDialog class - internal calculator
//
// JSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONUI depends on the following open-source software products:
// JSONIO (https://bitbucket.org/gems4/jsonio); Qt5 (https://qt.io).
//

#include <cmath>
#include "CalcDialog.h"
#include "ui_CalcDialog4.h"
#include "jsonio/ar2base.h"
#include "preferences.h"

namespace jsonui {

const double IPNC_DBL_MAX = 1e37;
const double IPNC_DBL_MIN = 1e-37;
const double IPNC_DBL_MAX_10_EXP = 37.;
const double IPNC_DBL_MIN_10_EXP = -37.;
const double IPNC_DBL_MAX_EXP = 85.195648;
const double IPNC_DBL_MIN_EXP = -85.195648;

CalcDialog::CalcDialog(QWidget* parent ):
        QDialog( parent ),
        ui(new Ui::CalcDialogData)
{
    
    ui->setupUi(this);
    
    allButtons = new QButtonGroup( ui->pButtonBox );
    allButtons->addButton(ui->pAssignBtn, 0);
    allButtons->addButton(ui->PushButton2, 1);
    allButtons->addButton(ui->PushButton3, 2);
    allButtons->addButton(ui->PushButton4, 3);
    allButtons->addButton(ui->PushButton5, 4);
    allButtons->addButton(ui->PushButton6, 5);
    allButtons->addButton(ui->PushButton7, 6);
    allButtons->addButton(ui->PushButton8, 7);
    allButtons->addButton(ui->PushButton9, 8);
    allButtons->addButton(ui->PushButton10, 9);
    allButtons->addButton(ui->PushButton11, 10);
    allButtons->addButton(ui->PushButton12, 11);
    allButtons->addButton(ui->PushButton13, 12);
    allButtons->addButton(ui->PushButton14, 13);
    allButtons->addButton(ui->PushButton15, 14);
    allButtons->addButton(ui->pClearBtn, 15);
    ui->pAssignBtn->setChecked( true);

    QObject::connect( allButtons, SIGNAL(buttonClicked(int)), this, SLOT(setMode(int)));

    QObject::connect(ui->PushButton1_3, SIGNAL(clicked()), this, SLOT(ok()));
    QObject::connect(ui->PushButton1_2, SIGNAL(clicked()), this, SLOT(reject()));
  //  QObject::connect(ui->PushButton1_2, SIGNAL(clicked()), this, SLOT(unaryMode()));
    QObject::connect(ui->PushButton42, SIGNAL(clicked()), this, SLOT(help()));
}

CalcDialog::~CalcDialog()
{
    delete allButtons;
    delete ui;
}

void CalcDialog::unaryMode()
{
    ui->pValue->setEnabled(false);
}

void CalcDialog::setMode( int id )
{
 if( id <= 5)
    ui->pValue->setEnabled(true);
 else
    ui->pValue->setEnabled(false);
}

void CalcDialog::ok()
{
    accept();
}

double CalcDialog::computeFunction(double val)
{
    int ii, pos;
    double ret = val;

    ii = allButtons->checkedId();
    double val2=0.;
    if( ii <= 5 )
    {  
      QString str = ui->pValue->currentText();
      pos = str.indexOf('(');
      if( pos >= 0 )
       str.truncate(pos);
      if( (str.indexOf(jsonio::emptiness.c_str()) != -1) || str[0] == '`' )
         val2 = jsonio::DOUBLE_EMPTY;
      else
         val2 = str.toDouble();  /// check for error
    }
    switch( ii )
    {
    case 0:
        ret = val2;
        break;
    case 1:
        ret = val + val2;
        break;
    case 2:
        ret = val - val2;
        break;
    case 3:
        ret = val * val2;
        break;
    case 4:
        if( fabs(val2) >=  IPNC_DBL_MIN ) // Attempt of zerodivide!
          ret = val / val2;
        break;
    case 5:
        if( !(fabs(val)<IPNC_DBL_MIN || fabs(val)>IPNC_DBL_MAX
                 || fabs(val2) < IPNC_DBL_MIN_10_EXP
                 || fabs(val2) > IPNC_DBL_MAX_10_EXP) ) // Attempt of pow() argument out of range
            ret = pow(val, val2);
        break;
    case 6:
        if( fabs(val) >= IPNC_DBL_MIN ) // Attempt of zerodivide!
         ret = 1/val;
        break;
    case 7:
        if( val >= IPNC_DBL_MIN ) // Attempt of sqrt() argument <= 0
         ret = sqrt(val);
        break;
    case 8:
        if( val >= IPNC_DBL_MIN )
         ret = log10(val);
        break;
    case 9:
        if( val >= IPNC_DBL_MIN )
            ret = log(val);
        break;
    case 10:
        if( !( fabs(val) < IPNC_DBL_MIN_10_EXP
             || fabs(val) > IPNC_DBL_MAX_10_EXP) ) // Attempt of pow() argument out of range
            ret = pow(10.,val);
        break;
    case 11:
        if( !(val < IPNC_DBL_MIN_EXP || val > IPNC_DBL_MAX_EXP) ) // Attempt of exp() argument out of range
           ret =  exp(val);
        break;
    case 12:
        ret = sin(val);
        break;
    case 13:
        ret = cos(val);
        break;
    case 14:
        ret = tan(val);
        break;
    case 15:
        ret = jsonio::DOUBLE_EMPTY; //No Data value
    }

    return ret;
}

QString CalcDialog::computeFunctionString(double val)
{
    double ret = computeFunction(val);
    QString retstr;
    if( jsonio::IsDoubleEmpty( ret ) )
        retstr = jsonio::emptiness.c_str();
    else
        retstr = QString::number(  ret, 'g', 6 );//QVariant(ret).toString();
    return retstr;
}

int CalcDialog::extructInternal(double& val)
{
    int ii, pos;

    ii = allButtons->checkedId();

    val=0.;
    if( ii <= 5 )
    {
      QString str = ui->pValue->currentText();
      pos = str.indexOf('(');
      if( pos >= 0 )
       str.truncate(pos);
      if( (str.indexOf(jsonio::emptiness.c_str()) != -1) || str[0] == '`' )
         val = jsonio::DOUBLE_EMPTY;
      else
         val = str.toDouble();  /// check for error
    }
    return ii;
}


QString CalcDialog::expression(const char * valText )
{
    int ii, pos;
    QString res;

    ii = allButtons->checkedId();
    QString strText = "";

    if( ii <= 5 )
    {
      strText = ui->pValue->currentText();
      pos = strText.indexOf('(');
      if( pos >= 0 )
       strText.truncate(pos);
      if( (strText.indexOf(jsonio::emptiness.c_str()) != -1) || strText[0] == '`' )
         strText = "empty()";
    }

   switch( ii )
    {
    case 0:
        res = valText; break;
    case 1:
        res = QString("%1+%2").arg(valText, strText); break;
    case 2:
        res = QString("%1-%2").arg(valText, strText); break;
    case 3:
        res = QString("%1*%2").arg(valText, strText);; break;
    case 4:
        res = QString("%1/%2").arg(valText, strText);; break;
    case 5:
        res = QString("%1^%2").arg(valText, strText);; break;
    case 6:
        res = QString( "( %1 ? (1/%1) : empty() )" ).arg(valText) ; break;
    case 7:
        res = QString( "( %1 > 0. ? sqrt(%1) : empty() )" ).arg(valText) ; break;
    case 8:
        res = QString( "( %1 > 0. ? lg(%1) : empty() )" ).arg(valText) ; break;
    case 9:
        res = QString( "( %1 > 0. ? ln(%1) : empty() )" ).arg(valText) ; break;
    case 10:
        res = QString("10.^%1").arg(valText); break;
    case 11:
        res = QString("exp(%1)").arg(valText); break;
    case 12:
        res = QString("sin(%1)").arg(valText); break;
    case 13:
        res = QString("cos(%1)").arg(valText); break;
    case 14:
        res = QString("tan(%1)").arg(valText); break;
    case 15:
        res = "empty()"; //No Data value
    }
    return res;
}


void CalcDialog::help()
{
     helpWin( "CalcDialogJsonui", "" );
}

} // namespace jsonui

//--------------------- End of CalcDialog.cpp ---------------------------
