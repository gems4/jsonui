//  This is JSONUI library+API (https://bitbucket.org/gems4/jsonui)
//
/// \file dbdocumentmodel.cpp
/// Implementation of classes DBDocumentModel and DBEdgeDocumentModel
/// implements object to work with database document (vertex and edge).
//
// JSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2018 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONUI depends on the following open-source software products:
// JSONIO (https://bitbucket.org/gems4/jsonio); Qt5 (https://qt.io).
//

#include "jsonio/traversal.h"
#ifndef IMPEX_OFF
  #include "jsonimpex/yamlxml2file.h"
  #include "jsonimpex/thrift_impex.h"
#endif
#include "dbdocumentmodel.h"
#include "preferences.h"

namespace jsonui {


// Reset current DB client
void DBDocumentModel::dbResetClient(  QString aschemaName )
{
    try{
        jsonio::TDBVertexDocument* newClient;

        newClient = jsonio::TDBVertexDocument::newVertexDocumentQuery(
                  uiSettings().database(), aschemaName.toStdString()/*, _defaultQuery*/ );

        // no schema
        if( newClient == nullptr  )
            _dbgraph.reset();
        else
            _dbgraph.reset( newClient );

        emit changedDBClient( _dbgraph.get() );
        emit changedQuery( _dbgraph->lastQueryData()->getQuery().getQueryCondition().getQueryString().c_str() );

        dbReadDocument(  _dbgraph->lastQueryData()->getFirstKeyFromList().c_str()  );
    }
    catch(std::exception& e)
    {
        emit isException( "DBDocumentModel::resetDBClient ", e.what() );
    }
    emit finished();
}

void DBDocumentModel::dbChangeQuery( QString schema, std::shared_ptr<jsonio::DBQueryDef> querydef )
{
    if( _dbgraph.get() == nullptr )
    {
        emit finished();
        return;
    }

    auto oldquery = _dbgraph->lastQueryData()->getQuery();
    try
    {
        _dbgraph->resetSchema( schema.toStdString(), true );
        // reset internal query data
        _dbgraph->SetQuery( *querydef );
        _defaultQuery = querydef->getQueryCondition();
    }
    catch(std::exception& e)
    {
        _dbgraph->SetQuery( oldquery );
        emit isException( "DBDocumentModel::changeQuery ", e.what() );
    }
    // update search tables
    emit changedQuery(  querydef->getQueryCondition().getQueryString().c_str() );
    emit changedModel("");
    emit finished();
}

void DBDocumentModel::dbReloadQuery()
{
    try
    {
        if( _dbgraph.get() != nullptr )
        {
          _dbgraph->updateQuery();
           emit changedQuery( _dbgraph->lastQueryData()->getQuery().getQueryCondition().getQueryString().c_str() );
           emit changedModel("");
        }
    }
    catch(std::exception& e)
    {
        emit isException( "DBDocumentModel::dbReloadQuery ", e.what() );
    }
    emit finished();
}

void DBDocumentModel::dbReadDocument(  QString reckey  )
{
    if( _dbgraph.get() == nullptr || reckey.isEmpty() )
        return;

    try{
        _dbgraph->Read( reckey.toStdString() );
        std::string jsonDocument = _dbgraph->GetJson();
        emit openedDocument(  jsonDocument.c_str()  );
    }
    catch( jsonio::jsonio_exception& e)
    {
        emit isException( e.title(), e.what() );
    }
    catch(std::exception& e)
    {
        emit isException( "std::exception", e.what() );
    }
}


// Save current record to DB file
void DBDocumentModel::dbUpdate( QString jsonDocument )
{
    try
    {
        if( _dbgraph.get() == nullptr )
            return;

        std::string  key = _dbgraph->recFromJson( jsonDocument.toStdString() );
        if( !_dbgraph->Find( key ))
        {
            key = _dbgraph->Create( key );
            emit updatedOid(key.c_str());
        }
        else
        {
            _dbgraph->Update( key );
        }

        emit changedModel(key.c_str());
    }
    catch(jsonio::jsonio_exception& e)
    {
        emit isException(  e.title(), e.what() );
    }
    catch(std::exception& e)
    {
        emit isException(  "std::exception", e.what() );
    }
}


// Delete current record from DB
void DBDocumentModel::dbDelete( QString keyDocument )
{
    try
    {
        if( _dbgraph.get() == nullptr || keyDocument.isEmpty())
            return;
        _dbgraph->Delete( keyDocument.toStdString() );

        emit changedModel("");
        emit deletedDocument();
    }
    catch(jsonio::jsonio_exception& e)
    {
        emit isException( e.title(), e.what() );
    }
    catch(std::exception& e)
    {
        emit isException( "std::exception", e.what() );
    }
}

// Delete records from List
void DBDocumentModel::dbDeleteList( QVector<QString> keysDocument )
{
    try
    {
        if( _dbgraph.get() == nullptr )
            return;

        for( int ii=0; ii<keysDocument.size(); ii++ )
            _dbgraph->Delete( keysDocument[ii].toStdString() );

    }
    catch(jsonio::jsonio_exception& e)
    {
        emit isException( e.title(), e.what() );
    }
    catch(std::exception& e)
    {
        emit isException( "std::exception", e.what() );
    }
    emit changedModel("");
    emit deletedDocument();
    emit finished();
}

// Read multiple records from file fileName
void DBDocumentModel::dbRestoreMultipleRecordsfromFile( QString fileName )
{
    if( _dbgraph.get() == nullptr )
        return;

    try{
        std::string curRecord;
#ifndef IMPEX_OFF
        jsonio::FJsonYamlXmlArray file( fileName.toStdString() );
#else
       jsonio::FJsonArray file( fileName.toStdString() );
#endif
        file.Open( jsonio::OpenModeTypes::ReadOnly );
        while( file.LoadNext( curRecord ) )
            _dbgraph->UpdateFromJson( curRecord, false );
        file.Close();
    }
    catch( jsonio::jsonio_exception& e)
    {
        emit isException( e.title(), e.what() );
    }
    catch(std::exception& e)
    {
        emit isException( "std::exception", e.what() );
    }

    emit changedModel("");
    emit finished();
}

// Write multiple records to file fileName
void DBDocumentModel::dbBackupQueriedRecordstoFile( QString fileName, QVector<QString> keysDocument)
{
    if( _dbgraph.get() == nullptr)
        return;

    try {
#ifndef IMPEX_OFF
        jsonio::FJsonYamlXmlArray file( fileName.toStdString() );
#else
        jsonio::FJsonArray file( fileName.toStdString() );
#endif
        file.Open( jsonio::OpenModeTypes::WriteOnly );
        for( int ii=0; ii<keysDocument.size(); ii++ )
        {
            _dbgraph->Read( keysDocument[ii].toStdString() );
            file.SaveNext(_dbgraph->getDom());
        }
        file.Close();
    }
    catch(jsonio::jsonio_exception& e)
    {
        emit isException(  e.title(), e.what() );
    }
    catch(std::exception& e)
    {
        emit isException(  "std::exception", e.what() );
    }

    emit finished();
}

// Read multiple records from file fileName
void DBDocumentModel::dbRestoreGraphfromFile( QString fileName )
{
    try{
        jsonio::GraphTraversal travel( uiSettings().database() );
        travel.RestoreGraphFromFile(fileName.toStdString() );
    }
    catch(jsonio::jsonio_exception& e)
    {
        emit isException( e.title(), e.what() );
    }
    catch(std::exception& e)
    {
        emit isException( "std::exception", e.what() );
    }
    emit changedModel("");
    emit loadedGraph();
    emit finished();
}

// Write multiple records to file fileName
void DBDocumentModel::dbBackupGraphtoFile(QString fileName, std::vector<std::string> keysDocument)
{
    if( keysDocument.empty() )
        return;

    jsonio::FJsonArray* file = nullptr;

    try {
        file = new jsonio::FJsonArray( fileName.toStdString() );
        jsonio::GraphTraversal travel( uiSettings().database() );

        file->Open( jsonio::OpenModeTypes::WriteOnly );

        jsonio::GraphElementFunction afunc =  [file]( bool , const std::string& data )
        {
            file->SaveNext(data);
        };

        travel.Traversal( true, keysDocument, afunc );
        file->Close();
    }
    catch(jsonio::jsonio_exception& e)
    {
        emit isException( e.title(), e.what() );
    }
    catch(std::exception& e)
    {
        emit isException( "std::exception", e.what() );
    }
    delete file;
    emit finished();
}

#ifndef IMPEX_OFF

// Read bson records from format file fileName
void DBDocumentModel::dbImportFormat( std::shared_ptr<jsonio::ImpexFormatFile> inputIEFile,
                                      QString dataFile, bool overwrite  )
{
    if( _dbgraph.get() == nullptr )
        return;

    try{
        std::string curRecord;

        // open file with imported data
        inputIEFile->openFile( dataFile.toStdString() );
        while( inputIEFile->readBlock( curRecord ) )
        {
            _dbgraph->UpdateFromJson( curRecord, overwrite );
        }
        inputIEFile->closeFile();
    }
    catch( jsonio::jsonio_exception& e)
    {
        emit isException( e.title(), e.what() );
    }
    catch(std::exception& e)
    {
        emit isException( "std::exception", e.what() );
    }
    emit changedModel("");
    emit finished();
}

// Write bson record to format file fileName
void DBDocumentModel::dbExportFormat( std::shared_ptr<jsonio::ImpexFormatFile> inputIEFile,
                                      QString dataFile, QVector<QString> keysDocument )
{
    try {

        if( _dbgraph.get() == nullptr || keysDocument.empty() )
            return;

        inputIEFile->openFile( dataFile.toStdString() );  /// open to app

        for( int ii=0; ii<keysDocument.size(); ii++ )
        {
            _dbgraph->Read( keysDocument[ii].toStdString() );
            inputIEFile->writeBlock( _dbgraph->GetJson() );
        }

        inputIEFile->closeFile();
    }
    catch( jsonio::jsonio_exception& e)
    {
        emit isException( e.title(), e.what() );
    }
    catch(std::exception& e)
    {
        emit isException( "std::exception", e.what() );
    }
    emit finished();
}
#endif

// DBEdgeDocumentModel model ----------------------------------------


// Reset current DB client
void DBEdgeDocumentModel::dbResetClient(  QString aschemaName )
{
    try{
        jsonio::TDBEdgeDocument* newClient;

        newClient = jsonio::TDBEdgeDocument::newEdgeDocumentQuery(
                  uiSettings().database(), aschemaName.toStdString(), _defaultQuery );
        newClient->resetMode(_mode);

        // no schema
        if( newClient == nullptr  )
            _dbgraph.reset();
        else
            _dbgraph.reset( newClient );

       emit changedQuery( _dbgraph->lastQueryData()->getQuery().getQueryCondition().getQueryString().c_str() );
       emit changedDBClient( _dbgraph.get() );
       resetInOutDBClients();
       dbReadDocument(  _dbgraph->lastQueryData()->getFirstKeyFromList().c_str()  );
    }
    catch(std::exception& e)
    {
        emit isException( "DBDocumentModel::resetDBClient ", e.what() );
    }
    emit finished();
}


void DBEdgeDocumentModel::dbResetSchema(  QString aschemaName )
{
    try{
        _schemaName = aschemaName.toStdString();

        if( !_isDefaultQuery )
        {
          _dbgraph->resetSchema(_schemaName, true );
          _dbgraph->updateQuery();
          emit changedQuery( _dbgraph->lastQueryData()->getQuery().getQueryCondition().getQueryString().c_str() );
          emit changedModel("");
          dbReadDocument(  _dbgraph->lastQueryData()->getFirstKeyFromList().c_str()  );
        }
        else
          _dbgraph->resetSchema(_schemaName, false );
    }
    catch(std::exception& e)
    {
        emit isException( "DBDocumentModel::resetDBClient ", e.what() );
    }
     emit finished();
}

void DBEdgeDocumentModel::dbReadDocument(  QString reckey  )
{
    if( _dbgraph.get() == nullptr || reckey.isEmpty())
        return;

    try{
        _dbgraph->Read( reckey.toStdString() );

        if( _isDefaultQuery )
        {
            std::string _schema = _dbgraph->getSchemaName();
            if( _schema != _schemaName )
            {
                _schemaName = _schema;
                emit resetTypeBox( _schemaName.c_str() );
            }
        }

        std::string inV, outV;
        // update incoming lists
        _dbgraph->getValue("_to", inV);
        if( !inV.empty() && ( !_isDefaultQuery || queryIn.empty() ) )
        {
          if( changeQueryToId( inDB.get(), inV ) )
             emit changedIncomingModel( inV.c_str() );
        }
        // update outgoing lists
        _dbgraph->getValue("_from", outV );
        if( !outV.empty() && ( !_isDefaultQuery || queryOut.empty() ) )
        {
          if( changeQueryToId( outDB.get(), outV ) )
             emit changedOutgoingModel( outV.c_str() );
        }

       std::string jsonDocument = _dbgraph->GetJson();
       emit openedDocument(  jsonDocument.c_str()  );
    }
    catch( jsonio::jsonio_exception& e)
    {
        emit isException( e.title(), e.what() );
    }
    catch(std::exception& e)
    {
        emit isException( "std::exception", e.what() );
    }
}

// ??????????? must be test
// Read multiple records from file fileName
void DBEdgeDocumentModel::dbRestoreMultipleRecordsfromFile( QString fileName )
{
    if( _dbgraph.get() == nullptr )
        return;

    _dbgraph->resetMode(true);
     DBDocumentModel::dbRestoreMultipleRecordsfromFile( fileName );
    _dbgraph->resetMode(_mode);
     std::string _schema = _dbgraph->getSchemaName();
     if( _schema != _schemaName )
         _dbgraph->resetSchema(_schemaName, false);
}

//----------------------------------------------------------------------

void DBEdgeDocumentModel::resetInOutDBClients()
{
    auto vertexlist = jsonio::ioSettings().Schema()->getVertexesList();
    if( vertexlist.empty() )
        return;

    queryIn = jsonio::emptyQuery;
    queryOut = jsonio::emptyQuery;
    std::string inVertex = vertexlist[0], outVertex = vertexlist[0];

    jsonio::TDBVertexDocument* vertexdoc = jsonio::TDBVertexDocument::newVertexDocumentQuery(
                uiSettings().database(), inVertex, queryIn );
    if( vertexdoc == nullptr )
        return;
    vertexdoc->resetMode(true);

    if( _isDefaultQuery )  // set up fixed query
    {
        std::string _inV_val = jsonio::extractStringField( "_to", _defaultQuery.getQueryString() );
        if( !_inV_val.empty() )
        {
            queryIn = vertexdoc->idQuery( _inV_val );
            std::string newschema = vertexdoc->extractSchemaFromId( _inV_val );
            if( !newschema.empty() )
            {
                inVertex = newschema;
                std::cout << " ########## "<< inVertex << std::endl;
            }
            emit fixIncoming();
            //ui->actionIncoming_Vertex_Query->setDisabled(true);
        }

        std::string _outV_val = jsonio::extractStringField( "_from", _defaultQuery.getQueryString() );
        if( !_outV_val.empty() )
        {
            queryOut = vertexdoc->idQuery( _outV_val );
            std::string newschema = vertexdoc->extractSchemaFromId( _outV_val );
            if( !newschema.empty() )
            {
                outVertex = newschema;
                std::cout << " ########## "<< outVertex << std::endl;
            }
            emit fixOutgoing();
            //ui->actionOutgoing_Vertex_Query->setDisabled(true);
        }

        vertexdoc->resetSchema( inVertex, true );
        vertexdoc->SetQuery(queryIn);
    }

    inDB.reset( vertexdoc );

    jsonio::TDBVertexDocument* outClient = jsonio::TDBVertexDocument::newVertexDocumentQuery(
                uiSettings().database(), outVertex, queryOut );
    outDB.reset( outClient );
    outDB->resetMode(true);

    emit changedIncomingDBClient( inDB.get() );
    emit changedOutgoingDBClient( outDB.get() );
}


bool DBEdgeDocumentModel::changeQueryToId( jsonio::TDBVertexDocument* _dbvertex, const std::string& id )
{
    if( _dbvertex == nullptr )
        return false;

    std::string schemaName = _dbvertex->extractSchemaFromId( id );
    if( !schemaName.empty() && schemaName != _dbvertex->getSchemaName() )
    {
        _dbvertex->resetSchema(schemaName, true );
        if( !_isDefaultQuery )
        {
            _dbvertex->updateQuery();
            return true;
        }
    }
    if( _isDefaultQuery &&  !schemaName.empty() )
    {
        auto qStr = _dbvertex->idQuery( id );
        _dbvertex->SetQuery( qStr, {} );
        return true;
    }
    return false;
}

void DBEdgeDocumentModel::dbChangeOutgoingQuery( QString schema, std::shared_ptr<jsonio::DBQueryDef> querydef )
{
    if( outDB.get() == nullptr )
    {
        emit finished();
        return;
    }

    auto oldquery = outDB->lastQueryData()->getQuery();
    try
    {
        outDB->resetSchema( schema.toStdString(), true );
        // reset internal query data
        outDB->SetQuery( *querydef );
        queryOut = querydef->getQueryCondition();
    }
    catch(std::exception& e)
    {
        outDB->SetQuery( oldquery );
        emit isException( "DBDocumentModel::changeQuery ", e.what() );
    }
    // update search tables
    emit changedOutgoingModel("");
    emit finished();
}

void DBEdgeDocumentModel::dbChangeIncomingQuery( QString schema, std::shared_ptr<jsonio::DBQueryDef> querydef )
{
    if( inDB.get() == nullptr )
    {
        emit finished();
        return;
    }

    auto oldquery = inDB->lastQueryData()->getQuery();
    try
    {
        inDB->resetSchema( schema.toStdString(), true );
        // reset internal query data
        inDB->SetQuery( *querydef );
        queryIn = querydef->getQueryCondition();
    }
    catch(std::exception& e)
    {
        inDB->SetQuery( oldquery );
        emit isException( "DBDocumentModel::changeQuery ", e.what() );
    }
    // update search tables
    emit changedIncomingModel("");
    emit finished();
}


void DBEdgeDocumentModel::dbReloadQuery()
{
    try
    {
        if( _dbgraph.get() != nullptr )
        {
            _dbgraph->updateQuery();
            emit changedQuery( _dbgraph->lastQueryData()->getQuery().getQueryCondition().getQueryString().c_str() );
            emit changedModel("");
        }
        if( inDB.get() != nullptr )
        {
            inDB->updateQuery();
            emit changedIncomingModel("");
        }
        if( outDB.get() != nullptr )
        {
            outDB->updateQuery();
            emit changedOutgoingModel("");
        }
    }
    catch(std::exception& e)
    {
        emit isException( "DBDocumentModel::dbReloadQuery ", e.what() );
    }
    emit finished();
}


/*{
     "_label" :   "element" ,
        "properties.aggregate_state" : 0
}

{
     "_label" :   "substance",
     "properties.sourcetdb.12" :   "THEREDA"
}
*/
} // namespace jsonui
