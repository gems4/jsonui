//  This is JSONUI library+API (https://bitbucket.org/gems4/jsonui)
//
/// \file ModelLineDialog.cpp
/// Implementation of class ModelLineDialog - service to set up field description
/// into model based on bson and our JSON schemas
//
// JSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONUI depends on the following open-source software products:
// JSONIO (https://bitbucket.org/gems4/jsonio); Qt5 (https://qt.io).
//

#include <QSpinBox>
#include "ModelLineDialog.h"
#include "ui_ModelLineDialog.h"
#include "model_schema.h"
#include "model_dom.h"
using namespace jsonio;

namespace jsonui {

QVariant TSizeModel::data( const QModelIndex& index, int role ) const
{
    if(!index.isValid())
     return QVariant();

    switch( role )
    {  case Qt::DisplayRole:
       case Qt::EditRole:
         return QVariant( static_cast<uint>(data_[ static_cast<size_t>(index.row()) ]));
       default: break;
    }
    return QVariant();
}

bool TSizeModel::setData( const QModelIndex &index, const QVariant& value, int role)
{
    if( index.isValid() && ( role == Qt::EditRole ) )
    {
       data_[static_cast<size_t>(index.row())] = value.toUInt();
       return true;
    }
    return false;
}

void TSizeModel::resetData( std::vector<std::size_t>&  sizes )
{
    beginResetModel();
    data_ = sizes;
    endResetModel();
}

QWidget *TSizeDelegate::createEditor(QWidget *parent,
                      const QStyleOptionViewItem&,
                      const QModelIndex&) const
{
  QSpinBox *spin_ = 	new QSpinBox(parent );
    spin_->setMinimum(0);
  return spin_;
}

//==================================================

ModelLineDialog::ModelLineDialog( const TJsonAbstractModel* themodel,
                                  const QModelIndex& index,
                                  const QModelIndex& parentIndex,
                                  const std::string& fldName,
                                  QWidget *parentWidget) :
    QDialog(parentWidget), useSchemas(false), getArrayInf( !fldName.empty() ),
    strDef(nullptr), fldDef(nullptr),
    ui(new Ui::ModelLineDialog)
{
    ui->setupUi(this);
    sizeModel = new TSizeModel( sizes, this);
    ui->sizeView->setModel(sizeModel);
    ui->sizeView->horizontalHeader()->hide();
    TSizeDelegate *sizedlg = new TSizeDelegate(this);
    ui->sizeView->setItemDelegate(sizedlg);

    const TJsonSchemaModel* schemamodel1;
    model = schemamodel1 = dynamic_cast<const TJsonSchemaModel*>(themodel);
    if( model )
    {
       setJsonSchemaModel( schemamodel1, fldName,  index, parentIndex );
       return;
    }
    else
      {
        const TJsonDomModel* dommodel;
        model = dommodel = dynamic_cast<const TJsonDomModel*>(themodel);
        if( dommodel)
            setJsonDomModel( dommodel,fldName, index );
     }
}

ModelLineDialog::~ModelLineDialog()
{
    delete ui;
}

void ModelLineDialog::setJsonSchemaModel( const TJsonSchemaModel* schemamodel,
        const std::string& fldName, const QModelIndex& index, const QModelIndex& parentIndex )
{
    model = schemamodel;
    useSchemas = true;
    auto line_ =  schemamodel->lineFromIndex(parentIndex);
    strDef = schemamodel->lineFromIndex(index)->getSructDef();
    ui->typeBox->setEnabled(false);

    if( getArrayInf )
    {
       ui->nameBox->addItem( fldName.c_str() );
       ui->nameBox->setCurrentText( fldName.c_str() );
       ui->nameBox->setEnabled(false);

       // get old table sizes
       auto itline = schemamodel->lineFromIndex(index);
       fldDef = itline->getFieldDef();
       auto it = fldDef->fTypeId.begin();
       it += itline->getLevel();
       while( (*it == Th_SET ||  *it == Th_LIST ||
               *it == Th_MAP ) &&  it< fldDef->fTypeId.end() )
       {
           if( itline )
           {  size_t sizef = itline->getChildrenCount();
              sizes.push_back(sizef);
              if(sizef>0)
               itline = itline->getChild(0);
           }
           else
             sizes.push_back(0);
           it++;
       }
       sizeModel->resetData(sizes);
    } else
      {
        // set up names box
        auto nonames = line_->getNoUsedKeys();
        for(uint ii=0; ii<nonames.size(); ii++ )
          ui->nameBox->addItem( nonames[ii].c_str() );

        QObject::connect( ui->nameBox, SIGNAL( currentIndexChanged(const QString & ) ),
                         this, SLOT( setDatabyName(const QString & ) ) );

        if( ui->nameBox->count()<1 )
         return;
        ui->nameBox->setCurrentIndex(0);
      }
   setDatabyName(ui->nameBox->currentText() );
}

void ModelLineDialog::setJsonDomModel( const TJsonDomModel* dopmmodel,
             const std::string& fldName, const QModelIndex& index )
{
    model = dopmmodel;
    ui->docLine->hide();//setText("Free bson object");
    if( getArrayInf )
    {
       ui->nameBox->addItem( fldName.c_str() );
       ui->nameBox->setCurrentText( fldName.c_str() );
       ui->nameBox->setEnabled(false);

       // get old table sizes
       auto itline = dopmmodel->lineFromIndex(index);
       sizes.push_back(itline->getChildrenCount());
       sizeModel->resetData(sizes);
       ui->labelDef->setText("Element def");
       ui->labelType->setText("Element type");
       if( itline->getChildrenCount() > 0 )
           setDatabyJsonType(itline->getChild(0)->getType());
       else
           setDatabyJsonType(JSON_STRING);
     } else
      {
         ui->labelSize->hide();
         ui->sizeView->hide();
         ui->nameBox->setEditable(true);
         ui->defEdit->setText("std::string");
      }
    QObject::connect( ui->typeBox, SIGNAL( currentIndexChanged(int ) ),
                    this, SLOT( setDatabyType(int) ) );
}

void ModelLineDialog::setDatabyName(const QString& text)
{
    std::string new_object = text.toStdString();
    fldDef = const_cast<ThriftStructDef*>(strDef)->getFieldDef(new_object);
    if( !fldDef )
     return;

    ui->docLine->setText(fldDef->fDoc.c_str());
    ui->defEdit->setText(fldDef->insertedDefault.c_str());
    int abson_type = jsonTypeFromThrift( fldDef->fTypeId[0] );
    setEditorByType( jsonTypeFromThrift(fldDef->fTypeId.back())  );
    ui->typeBox->setCurrentText( getJsonTypeName( abson_type ).c_str());

    if( !getArrayInf)
      sizes.clear();
    auto it = fldDef->fTypeId.begin();
    if(*it == Th_SET || *it == Th_LIST || *it == Th_MAP )
      {
        // define size table
        if( !getArrayInf )
        { //auto it = fldDef->fTypeId.begin();
          while( (*it == Th_SET || *it == Th_LIST || *it == Th_MAP ) &&
                  it< fldDef->fTypeId.end() )
          {
             sizes.push_back(0);
             it++;
          }
        }
        sizeModel->resetData(sizes);
        ui->labelSize->show();
        ui->sizeView->show();
        ui->labelDef->setText("Element def");
      }
    else
     {
        ui->labelSize->hide();
        ui->sizeView->hide();
        ui->labelDef->setText("Default");
     }
}

void ModelLineDialog::setDatabyJsonType( int jsontype )
{
    setEditorByType( jsontype  );
    ui->defEdit->setText( fixedDefValue( jsontype, "" ).c_str() );
}

void ModelLineDialog::setDatabyType(int /*ndx*/)
{
    int jsontype = getType();
    setDatabyJsonType( jsontype );
}

void ModelLineDialog::setEditorByType( int _type )
{
    ui->defEdit->setEnabled(true);
    switch( _type  )
    {
     case JSON_INT:
         ui->defEdit->setValidator(new QIntValidator(ui->defEdit));
          break;
     case JSON_DOUBLE:
         ui->defEdit->setValidator(new QDoubleValidator(ui->defEdit));
        break;
     case JSON_BOOL:
     case JSON_NULL:
     case JSON_STRING:
         ui->defEdit->setValidator( nullptr );
          break;
    // main constructions
     default:
     case JSON_OBJECT:
     case JSON_ARRAY:
        ui->defEdit->setEnabled(false);
     //   ui->defEdit->setText("");
    }
}

int ModelLineDialog::getType() const
{
  int new_type = JSON_NULL;
  switch( ui->typeBox->currentIndex()  )
  {
   case 0:  new_type = JSON_STRING;
         break;
   case 1:  new_type = JSON_DOUBLE;
         break;
   case 2:  new_type = JSON_INT;
         break;
   case 3:  new_type = JSON_BOOL;
         break;
   case 4:  new_type = JSON_OBJECT;
         break;
   case 5:  new_type = JSON_ARRAY;
         break;
 }
  return new_type;
}

std::string ModelLineDialog::getName() const
{
  return ui->nameBox->currentText().toStdString();
}

std::string ModelLineDialog::getDefValue() const
{
   return ui->defEdit->text().toStdString();
}

} // namespace jsonui
