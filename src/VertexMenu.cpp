//  This is JSONUI library+API (https://bitbucket.org/gems4/jsonui)
//
/// \file VertexMenu.cpp
/// Implementation of class VertexWidget - Widget to work with vertex
/// internal DOM based on our JSON schemas data
//
// JSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2018 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONUI depends on the following open-source software products:
// JSONIO (https://bitbucket.org/gems4/jsonio); Qt5 (https://qt.io).
//

#include <chrono>
#include <QMessageBox>
#include <QLineEdit>
#include "VertexWidget.h"
#include "ui_VertexWidget.h"
#include "SelectDialog.h"
#ifndef IMPEX_OFF
  #include "FormatImpexWidget.h"
  #include "jsonimpex/yamlxml2file.h"
  #include "jsonimpex/thrift_impex.h"
#endif

namespace jsonui {

//  Connect all actions
void VertexWidget::setActions()
{
    // File
    connect( ui->actionNew , SIGNAL( triggered()), this, SLOT(CmNew()));
    connect( ui->action_Clone_Structured_Data_Object , SIGNAL( triggered()), this, SLOT(CmClone()));
    connect( ui->actionE_xit, SIGNAL( triggered()), this, SLOT(close()));
    connect( ui->actionExport_Json_File, SIGNAL( triggered()), this, SLOT(CmExportJSON()));
    connect( ui->actionImport_Json_File, SIGNAL( triggered()), this, SLOT(CmImportJSON()));

    // Edit
    connect(ui->actionAdd_one_field, SIGNAL(triggered()), fieldTable, SLOT(CmAddObject()));
    connect(ui->action_Clone_Current, SIGNAL(triggered()), fieldTable, SLOT(CmCloneObject()));
    connect(ui->action_Delete_field, SIGNAL(triggered()), fieldTable, SLOT(CmDelObject()));
    connect(ui->action_Delete_fields, SIGNAL(triggered()), fieldTable, SLOT(CmDelObjects()));
    connect(ui->actionRemove_Alternatives_Union, SIGNAL(triggered()), fieldTable, SLOT(CmDelObjectsUnion()));
    connect(ui->action_Add_fields, SIGNAL(triggered()), fieldTable, SLOT(CmAddObjects()));
    connect(ui->actionReset_Data_to_Defaults, SIGNAL(triggered()), fieldTable, SLOT(CmResetObject()));
    connect(ui->action_Resize_array, SIGNAL(triggered()), fieldTable, SLOT(CmResizeArray()));
    connect(ui->action_Calculator, SIGNAL(triggered()), fieldTable, SLOT(CmCalc()));
    connect(ui->actionCopy_Field_Path, SIGNAL(triggered()), fieldTable, SLOT(CopyFieldPath()));
    connect(ui->actionCopy_Field, SIGNAL(triggered()), fieldTable, SLOT(CopyField()));
    connect(ui->actionPaste_Field, SIGNAL(triggered()), fieldTable, SLOT(PasteField()));

    // Help
    connect( ui->action_Help_About, SIGNAL( triggered()), this, SLOT(CmHelpAbout()));
    connect( ui->actionContents, SIGNAL( triggered()), this, SLOT(CmHelpContens()));
    connect( ui->actionAuthors, SIGNAL( triggered()), this, SLOT(CmHelpAuthors()));
    connect( ui->actionLicense, SIGNAL( triggered()), this, SLOT(CmHelpLicense()));

    // View
    connect( ui->action_Show_comments, SIGNAL( toggled(bool)), &uiSettings(), SLOT(CmShowComments(bool)));
    connect( ui->action_Display_enums, SIGNAL( toggled(bool)), &uiSettings(), SLOT(CmDisplayEnums(bool)));
    connect( ui->action_Edit_id, SIGNAL(toggled(bool)), &uiSettings(), SLOT(CmEditID(bool)));
    connect( ui->actionKeep_Data_Fields_Expanded, SIGNAL( toggled(bool)), &uiSettings(), SLOT(CmEditExpanded(bool)));
    updateViewMenu();
    connect( ui->actionFollow_Edges, SIGNAL( triggered()), this, SLOT(CmFollowOutEdges()));
    connect( ui->actionFollow_Edges_Incoming, SIGNAL( triggered()), this, SLOT(CmFollowInEdges()));

    // Record
    connect( ui->actionNext_Record, SIGNAL( triggered()), keysTable, SLOT(CmNext()));
    connect( ui->actionPrevious_Record, SIGNAL( triggered()), keysTable, SLOT(CmPrevious()));
    connect( ui->action_Update, SIGNAL( triggered()), this, SLOT(CmUpdate()));
    //connect( ui->action_Create, SIGNAL( triggered()), this, SLOT(CmCreateInsert()));
    connect( ui->action_Delete, SIGNAL( triggered()), this, SLOT(CmDelete()));
    connect( ui->action_Read, SIGNAL( triggered()), this, SLOT(CmRead()));
#ifndef IMPEX_OFF
    connect( ui->actionImport, SIGNAL( triggered()), this, SLOT(CmImportFormat()));
    connect( ui->actionExport, SIGNAL( triggered()), this, SLOT(CmExportFormat()));
#else
    ui->actionImport->setVisible(false);
    ui->actionExport->setVisible(false);
#endif
    connect( ui->actionDeleteMultiple, SIGNAL( triggered()), this, SLOT(CmDeleteSelect()));
    connect( ui->actionSearch_Results, SIGNAL( triggered()), this, SLOT(CmDisplaySearchResult()));
    connect( ui->actionSearch, SIGNAL( triggered()), this, SLOT(CmSearchQuery()));

    connect( ui->actionBackup_Queried_Records_to_File, SIGNAL( triggered()),
             this,  SLOT(CmBackupQueriedRecordstoFile()));
    connect( ui->actionRestore_Multiple_Records_from_File, SIGNAL( triggered()),
             this, SLOT(CmRestoreMultipleRecordsfromFile()));

    connect( ui->actionBackup_Graph_to_File, SIGNAL( triggered()),
             this,  SLOT(CmBackupGraphtoFile()));
    connect( ui->actionRestore_Graph_from_File, SIGNAL( triggered()),
             this, SLOT(CmRestoreGraphfromFile()));

    QLineEdit* pLineTask = new QLineEdit( ui->nameToolBar );
    pLineTask->setEnabled( true );
    pLineTask->setFocusPolicy( Qt::ClickFocus );
    pLineTask->setReadOnly( true );
    pLineTask->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed );
    QString title =  curSchemaName.c_str();
    pLineTask->setText(title);
    ui->nameToolBar->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed );
    ui->nameToolBar->addWidget( pLineTask ); // setStretchableWidget( pLine );
}

// Menu commands -----------------------------------------------------------


/// Set default bson record
void VertexWidget::CmNew()
{
    openDocument( "" );
}

/// Clone bson record (clear _id)
void VertexWidget::CmClone()
{
    updateOid( "" );
}

/// Read new record from DB
void VertexWidget::CmRead()
{
    try
    {
        auto aValList = keysTable->tableValues();
        //auto header = keysTable->headerData();
        if( aValList.empty() )
            return;

        SelectDialog selDlg( false, this, "Please, select a record to read/view", aValList );
        if( !selDlg.exec() )
            return;

        std::string reckey = aValList[selDlg.selIndex()][0].toString().toStdString();
        openRecordKey(  reckey  );
    }
    catch(jsonio::jsonio_exception& e)
    {
        QMessageBox::critical( this, e.title(), e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Save current document to DB file
void VertexWidget::CmUpdate()
{
    try
    {
        curRecord_ = model_schema->saveToJson();
        contentsChanged = false;
        emit cmUpdate(curRecord_.c_str());
    }
    catch(jsonio::jsonio_exception& e)
    {
        QMessageBox::critical( this, e.title(), e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Delete current document from DB
void VertexWidget::CmDelete()
{
    try
    {
        auto key = getKeyFromDom( model_schema->getDom() );

        QString msg = QString("Confirm deletion of %1 record?").arg( key.c_str() );
        QMessageBox::StandardButton reply;
        reply = QMessageBox::question( this, "Record to delete", msg,
                                       QMessageBox::Yes|QMessageBox::No);
        if (reply == QMessageBox::No)
            return;

        emit cmDelete( key.c_str() );
        contentsChanged = false;
    }
    catch(jsonio::jsonio_exception& e)
    {
        QMessageBox::critical( this, e.title(), e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Delete list of documents
void VertexWidget::CmDeleteSelect()
{
    try
    {
        // Select keys to delete
        auto aValList = keysTable->tableValues();
        if( aValList.empty() )
            return;

        SelectDialog selDlg( true, this, "Please, select a records to delete.", aValList,
                             {}, TMatrixTable::tbNoMenu|TMatrixTable::tbSort );
        if( !selDlg.exec() )
            return;

        std::vector<int> selNdx;
        selDlg.getSelection(selNdx);

        QVector<QString> keysDocument;
        for( std::size_t ii=0; ii<selNdx.size(); ii++ )
            keysDocument.push_back( aValList[selNdx[ii]][0].toString() );

        startProcess();
        emit cmDeleteList( keysDocument );
    }
    catch(jsonio::jsonio_exception& e)
    {
        QMessageBox::critical( this, e.title(), e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}


/// Read document from json file fileName
void VertexWidget::CmImportJSON()
{
    try{
        std::string fileName;
        if(  ChooseFileOpen( this, fileName,
                             "Please, select a file with JSON object", domFilters  ))
        {
#ifndef IMPEX_OFF
            jsonio::FJsonYamlXml file( fileName);
#else
            jsonio::FJson file( fileName);
#endif
            curRecord_ =  file.LoadtoJsonString();
            if( !resetBson( curRecord_, schemafromName( fileName )) )
                jsonio::jsonioErr( fileName , "Try to read another schema format file" );
            contentsChanged = true;
        }
    }
    catch(jsonio::jsonio_exception& e)
    {
        QMessageBox::critical( this, e.title(), e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Write current document to json file fileName
void VertexWidget::CmExportJSON()
{
    try {
        std::string fileName = fileShemaExt( curSchemaName, "json");
        if(  ChooseFileSave( this, fileName,
                             "Please, select a file to write the data", domFilters, fileName  ))
        {
#ifndef IMPEX_OFF
            jsonio::FJsonYamlXml file( fileName);
#else
            jsonio::FJson file( fileName);
#endif
            file.SaveJson( model_schema->getDom()  );
        }
    }
    catch(jsonio::jsonio_exception& e)
    {
        QMessageBox::critical( this, e.title(), e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

#ifndef IMPEX_OFF

/// Read bson records from format file fileName
void VertexWidget::CmImportFormat()
{
    try{
        // Select format&input file
        FormatImpexWidget* dlg = new FormatImpexWidget(
                    FormatImpexWidget::runModeImport, curSchemaName, this );

        ExecuteFunction execF = [=]( std::shared_ptr<jsonio::ImpexFormatFile> inputIEFile,
                std::string dataFile, bool overwrite )
        {
            if( inputIEFile.get() == nullptr )
                return; // only for defined types

            curRecord_ = "";
            if( !resetBson(curRecord_, inputIEFile->getDataName()) )
                jsonio::jsonioErr( inputIEFile->getDataName() , "Try to read another schema format file" );

            startProcess();
            emit cmImportFormat(inputIEFile, dataFile.c_str(), overwrite);
        };

        dlg->setExecuteFunction(execF);
        dlg->show();

    }
    catch(jsonio::jsonio_exception& e)
    {
        QMessageBox::critical( this, e.title(), e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Write bson record to format file fileName
void VertexWidget::CmExportFormat()
{
    try {

        // Select keys to export
        auto aValList = keysTable->tableValues();
        if( aValList.empty() )
            return;

        SelectDialog selDlg( true, this, "Please, select a records to export.", aValList,
                               {}, TMatrixTable::tbNoMenu|TMatrixTable::tbSort );
        if( !selDlg.exec() )
            return;

        std::vector<int> selNdx;
        selDlg.getSelection(selNdx);
        QVector<QString> keysDocument;
        for( std::size_t ii=0; ii<selNdx.size(); ii++ )
            keysDocument.push_back( aValList[selNdx[ii]][0].toString() );


        // Select format&export file
        FormatImpexWidget* dlg = new FormatImpexWidget(
                    FormatImpexWidget::runModeExport, curSchemaName, this );


        ExecuteFunction execF = [=]( std::shared_ptr<jsonio::ImpexFormatFile> inputIEFile,
                std::string dataFile, bool /*overwrite*/ )
        {
            if( inputIEFile.get() == nullptr )
                return; // only for defined types

            // select main schema
            curRecord_ = "";
            if( !resetBson(curRecord_, inputIEFile->getDataName()) )
                jsonio::jsonioErr( inputIEFile->getDataName() , "Try to write another schema format file" );

            startProcess();
            emit cmExportFormat(inputIEFile, dataFile.c_str(), keysDocument);
        };

        dlg->setExecuteFunction(execF);
        dlg->show();
    }
    catch(jsonio::jsonio_exception& e)
    {
        QMessageBox::critical( this, e.title(), e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}
#endif

void VertexWidget::CmDisplaySearchResult()
{
    try
    {
        dbKeysModel->showResult("Vertex Query Result window");
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "CmDisplaySearchResult", e.what() );
    }
}

void VertexWidget::CmSearchQuery()
{
    try
    {
        std::vector<std::string> schemalst;
        schemalst.push_back(curSchemaName);
        dbKeysModel->showQuery( "Vertex Query Widget", schemalst );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "CmSearchQuery", e.what() );
    }
}

void VertexWidget::setQuery( QueryWidget* queryW  )
{
    startProcess();
    dbKeysModel->setOneQuery(queryW);
}

/// Read multiple records from file fileName
void VertexWidget::CmRestoreMultipleRecordsfromFile()
{
    try{
        std::string fileName;
        if(  ChooseFileOpen( this, fileName,
                             "Please, select a file with json object", domFilters  ))
        {
            startProcess();
            emit cmRestoreMultipleRecordsfromFile(fileName.c_str());
        }
    }
    catch(jsonio::jsonio_exception& e)
    {
        QMessageBox::critical( this, e.title(), e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Write multiple records to file fileName
void VertexWidget::CmBackupQueriedRecordstoFile()
{
    try {
        std::string fileName = fileShemaExt(curSchemaName, "json");
        if(  ChooseFileSave( this, fileName,
                             "Please, select a file to write (document) records", domFilters, fileName  ))
        {
            // Select keys to delete
            auto aValList = keysTable->tableValues();
            if( aValList.empty() )
                return;

            SelectDialog selDlg( true, this, "Please, select a records to backup.", aValList,
                                 {}, TMatrixTable::tbNoMenu|TMatrixTable::tbSort );
            if( !selDlg.exec() )
                return;

            std::vector<int> selNdx;
            selDlg.getSelection(selNdx);
            QVector<QString> keysDocument;
            for( std::size_t ii=0; ii<selNdx.size(); ii++ )
                keysDocument.push_back( aValList[selNdx[ii]][0].toString() );

            startProcess();
            emit cmBackupQueriedRecordstoFile(fileName.c_str(),keysDocument);
        }
    }
    catch(jsonio::jsonio_exception& e)
    {
        QMessageBox::critical( this, e.title(), e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Show connect Edjes window
void VertexWidget::CmFollowInEdges()
{
    try
    {
        std::string _id;
        model_schema->getDom()->findValue("_id",_id);

        std::shared_ptr<jsonio::TDBEdgeDocument> edges( jsonio::documentAllEdges( uiSettings().database() ));
        auto queryJson = edges->inEdgesQuery( _id );
        if( !edges->existKeysByQuery( queryJson ) )
        {
            QMessageBox::StandardButton reply;
            reply = QMessageBox::question( this, "Follow Incoming Edges",
                                           "No Incoming Edges. Create new?",
                                           QMessageBox::Yes|QMessageBox::No);
            if (reply == QMessageBox::No)
                return;
        }
        showOtherWindow( false, "", "",  queryJson);
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "Follow edges", e.what() );
    }
}


/// Show connect Edjes window
void VertexWidget::CmFollowOutEdges()
{
    try
    {
        std::string _id;
        model_schema->getDom()->findValue("_id",_id);

        std::shared_ptr<jsonio::TDBEdgeDocument> edges( jsonio::documentAllEdges( uiSettings().database() ));
        auto queryJson = edges->outEdgesQuery( _id );
        if( !edges->existKeysByQuery( queryJson ) )
        {
            QMessageBox::StandardButton reply;
            reply = QMessageBox::question( this, "Follow Outgoing Edges",
                                           "No Outgoing Edges. Create new?",
                                           QMessageBox::Yes|QMessageBox::No);
            if (reply == QMessageBox::No)
                return;
        }
        showOtherWindow( false, "", "",  queryJson);
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "Follow edges", e.what() );
    }
}

/// Read multiple records from file fileName
void VertexWidget::CmRestoreGraphfromFile()
{
    try{
        std::string fileName;
        if(  ChooseFileOpen( this, fileName,
                             "Please, select a file with json object", jsonFilters  ))
        {
            startProcess();
            emit cmRestoreGraphfromFile(fileName.c_str());
        }
    }
    catch(jsonio::jsonio_exception& e)
    {
        QMessageBox::critical( this, e.title(), e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Write multiple records to file fileName
void VertexWidget::CmBackupGraphtoFile()
{
    try {
        std::string fileName = fileShemaExt(curSchemaName, "json");
        if(  ChooseFileSave( this, fileName,
                             "Please, select a file to write (document) records", jsonFilters, fileName  ))
        {
            // Select keys to delete
            auto aValList = keysTable->tableValues();
            if( aValList.empty() )
                return;

            SelectDialog selDlg( true, this, "Please, select a records to backup graph.", aValList,
                                 {}, TMatrixTable::tbNoMenu|TMatrixTable::tbSort );
            if( !selDlg.exec() )
                return;

            std::vector<int> selNdx;
            selDlg.getSelection(selNdx);
            std::vector<std::string> keysDocument;
            for( std::size_t ii=0; ii<selNdx.size(); ii++ )
                keysDocument.push_back( aValList[selNdx[ii]][0].toString().toStdString() );

            startProcess();
            emit cmBackupGraphtoFile(fileName.c_str(), keysDocument);
        }
    }
    catch(jsonio::jsonio_exception& e)
    {
        QMessageBox::critical( this, e.title(), e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

} // namespace jsonui

// end of VertexMenu.cpp
