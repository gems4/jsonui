//  This is JSONUI library+API (https://bitbucket.org/gems4/jsonui)
//
/// \file HelpMainWindow.cpp
/// Implementation of classes HelpMainWindow - Help system main dialog
//
// JSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2017 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONUI depends on the following open-source software products:
// JSONIO (https://bitbucket.org/gems4/jsonio); Qt5 (https://qt.io).
//

#include <iostream>
#include <QLabel>
#include <QMessageBox>
#include <QFileInfo>
#include "HelpMainWindow.h"
#include "ui_HelpMainWindow.h"
#include "helpgenerator.h"
#include "helpdata.h"
#include "helpBrowser.h"
#include "SelectDialog.h"
#include "HelpEditDialog.h"
#include "jsonio/jsondomfree.h"

namespace jsonui {

extern const char *_GEMIPM_version_stamp;

bool HelpMainWindow::editHelp = false;

void helpWin( const std::string& name, const std::string& item )
{
    if( HelpMainWindow::pDia )
    {
        HelpMainWindow::pDia->showHelp( name, item );
        HelpMainWindow::pDia->show();
        HelpMainWindow::pDia->raise();
    }
}


//-------------------------------------------------------

HelpMainWindow* HelpMainWindow::pDia = nullptr;


HelpMainWindow::HelpMainWindow(  QWidget* parent):
    QMainWindow( parent ), ui(new Ui::HelpWindowData), other_thread_dbhelp(nullptr)
{
    ui->setupUi(this);
    ui->indexWidget->hide();

    //  ui->splitter->setStretchFactor(0, 1);
    ui->splitter->setStretchFactor(1, 500);

    helpView = new HelpBrowser(ui->helpWidget);
    helpView->setObjectName(QString::fromUtf8("helpView"));
    ui->verticalLayout_2->addWidget(helpView);

#ifdef USE_MARKDOWN

    tocView = new QTextBrowser(ui->tocWidget);
    tocView->setObjectName(QString::fromUtf8("tocView"));

    ui->verticalLayout->addWidget(tocView);
    connect( tocView, SIGNAL( anchorClicked(QUrl) ), this, SLOT(tocLinkClicked(QUrl)));
    ui->splitter->setStretchFactor(2, 1);

#endif

    std::string titl = " Help Viewer ";
    setWindowTitle( tr(titl.c_str()) );
    pDia = this;

    setActions();

    connect( helpView, SIGNAL( newLink(QUrl) ), this, SLOT(helpLinkClicked(QUrl)));
    connect( helpView, SIGNAL( loadFinished(bool) ), this, SLOT(scrollToAnchor(bool)));

    setupHtmlGenerator();
}

void HelpMainWindow::setActions()
{
    connect( ui->actionHome, SIGNAL( triggered()), this, SLOT( actionHome()) );
    connect( ui->actionBack, SIGNAL( triggered()), this, SLOT( actionBack()));
    connect( ui->actionForward, SIGNAL( triggered()), this, SLOT( actionForward()));
    connect( this, SIGNAL( forwardAvailable(bool) ), ui->actionForward, SLOT(setEnabled(bool)));
    connect( this, SIGNAL( backwardAvailable(bool)), ui->actionBack, SLOT(setEnabled(bool)));
    ui->actionForward->setEnabled(false);
    ui->actionBack->setEnabled(false);

    connect( ui->action_Find, SIGNAL( triggered()), this, SLOT(actionFind()));
    connect( ui->actionFind_Next, SIGNAL( triggered()), this, SLOT(actionFindNext()));
    connect( ui->actionFind_Previous, SIGNAL( triggered()), this, SLOT(actionFindPrevious()));

    connect( ui->actionZoom_In, SIGNAL( triggered()), this, SLOT(actionZoomIn()));
    connect( ui->actionZoom_Out, SIGNAL( triggered()), this, SLOT(actionZoomOut()));

    connect( ui->action_About, SIGNAL( triggered()), this, SLOT(helpAbout()));
    connect( ui->actionVersion, SIGNAL( triggered()), this, SLOT(helpVersion()));
    connect( ui->actionHelp_on_Help, SIGNAL( triggered()), this, SLOT(helpOnHelp()));
    connect( ui->action_Print, SIGNAL( triggered()), this, SLOT(helpPrint()));

    // Database
    if( editHelp )
    {
        connect( ui->action_Read_another_Docpage_from_DB, SIGNAL( triggered()), this, SLOT(CmRead()));
        connect( ui->action_Edit_DocPage, SIGNAL( triggered()), this, SLOT(CmEdit()));
        connect( ui->actionCreate_Update_Docpage_into_DB, SIGNAL( triggered()), this, SLOT(CmUpdate()));
        connect( ui->action_Delete_Docpages_from_DB, SIGNAL( triggered()), this, SLOT(CmDeleteList()));
    }
    else
    {
       ui->menu_Database->menuAction()->setVisible(false);
    }

    QLabel *label_2 = new QLabel(ui->toolFind);
    label_2->setText(tr("Find:"));
    ui->toolFind->addWidget( label_2 );

    findLine = new QLineEdit( ui->toolFind );
    findLine->setEnabled( true );
    findLine->setFocusPolicy( Qt::ClickFocus );
    ui->toolFind->addWidget( findLine );
    ui->toolFind->addAction(ui->actionFind_Previous);
    ui->toolFind->addAction(ui->actionFind_Next);

    if( editHelp )
    {
        QLabel *label = new QLabel(ui->toolAddress);
        label->setText(tr("Address:"));
        ui->toolAddress->addWidget( label );

        adressLine = new QLineEdit( ui->toolAddress );
        adressLine->setEnabled( true );
        adressLine->setFocusPolicy( Qt::ClickFocus );
        adressLine->setReadOnly( true );
        adressLine->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed );
        ui->toolAddress->addWidget( adressLine );
    }

}

HelpMainWindow::~HelpMainWindow()
{
    generatorThread.quit();
    generatorThread.wait();
    delete tocView;
    delete helpView;
    delete ui;
}

void HelpMainWindow::setupHtmlGenerator()
{

    HelpGenerator2 *worker = new HelpGenerator2;

    worker->moveToThread(&generatorThread);
    connect(&generatorThread, &QThread::finished, worker, &QObject::deleteLater);

    connect(this, &HelpMainWindow::updateHelpRecord, worker, &HelpGenerator2::setHelpRecord);
    connect(this, &HelpMainWindow::updateDBClient, worker, &HelpGenerator2::resetDBClient);
    emit updateDBClient();

    if( editHelp )
    {
        connect(worker, &HelpGenerator2::textChanged, [&]( const QString &newtext ) {  md_text = newtext; } );
        connect(this, &HelpMainWindow::updateDocPage, worker, &HelpGenerator2::setText );
    }

#ifdef USE_MARKDOWN
    connect(worker, &HelpGenerator2::htmlResultReady, this, &HelpMainWindow::htmlResultReady);
    connect(worker, &HelpGenerator2::tocResultReady, this, &HelpMainWindow::tocResultReady);
#else

    connect(this, SIGNAL(html(QString)), this, SLOT(handleHtml(QString)));
    //connect( helpView, SIGNAL( htmlLoadFinished() ), this, SLOT(scrollToAnchor()));
    helpView->setWebChannel(worker);
    htmlResultReady(htmlTemplate());
    //ui->helpView->setUrl(QUrl("qrc:/index.html"));
#endif
    generatorThread.start();
}


void HelpMainWindow::showAddres( const QString& name )
{
    if( editHelp )
    {
        adressLine->setText( name );
    }
}

void HelpMainWindow::showHelp(const std::string& name, const std::string& item )
{
    std::cout << name << "  " << item << std::endl;
    std::string keywd =  "docpages/"+name+";md";// name+":md:0:";
    anchorItem = item.c_str();
    currentKeyChange( keywd );
}

// must be key: "docpages/<name>;md"
void HelpMainWindow::showDocument(const char* keywd )
{
    std::cout << keywd << std::endl;
    anchorItem = "";
    currentKeyChange( keywd );
}

// Slots ----------------------------------------------

// show html preview
void HelpMainWindow::htmlResultReady(const QString &html)
{
    //std::cout << "html " << std::endl << html.toStdString() << std::endl;
    helpView->setHtmlData( html );
}

void HelpMainWindow::tocResultReady(const QString& toc)
{
    //std::cout << "toc =" << toc.toStdString() << std::endl;
    if( tocView )
        tocView->setHtml(toc);
}

void HelpMainWindow::scrollToAnchor( bool ok)
{
    //std::cout << "scrollToAnchor =" << anchorItem.toStdString() << std::endl;
    if( !ok || anchorItem.isEmpty() )
        return;
    helpView->scrollToAnchor(anchorItem);
}

void HelpMainWindow::tocLinkClicked(const QUrl& url)
{
    QString anchor = url.toString();
    anchor = anchor.remove("#");
    anchorItem = anchor;
    scrollToAnchor( true);
    //std::cout << "url = " << url.toString().toStdString() << "script =" << javascript.toStdString() << std::endl;
}

void HelpMainWindow::helpLinkClicked(const QUrl& url)
{
    currentKeyChange( keyFromUrl(url.url()) );
}

void HelpMainWindow::helpVersion()
{
    QMessageBox::information(this,
                         #ifdef __unix
                         #ifdef __APPLE__
                             tr("Title"), ("GEMS4.2 (MacOS X >10.6 64 clang)\n\n")+
                         #else
                             "GEMS4.2 (Linux 32/64 gcc4.7 Qt5)",
                         #endif
                         #else
                             "GEMS4.2 (Windows 7 MinGW 32 gcc4.7",
                         #endif
                             tr("\nThis is GEM-Selektor code package\n\n")+
                             //   _GEMS_version_stamp  +  "\n\nusing " +
                             //   _GEMIPM_version_stamp  +
                              "\n\n\nFor GEMS R&D community\n\n"
                                     "(c) 2013, GEMS Development Team\n\n"
                                     "          PSI-UH-ETHZ"  );
}

void HelpMainWindow::helpAbout()
{
    showHelp( "helpAbout", "" );
}

void HelpMainWindow::helpOnHelp()
{
    showHelp( "helpOnHelp", "" );
}

void HelpMainWindow::helpPrint()
{
    helpView->helpPrint();
}

void HelpMainWindow::actionZoomIn()
{
    helpView->zoomIn();
    if(tocView )
        tocView->zoomIn();
}

void HelpMainWindow::actionZoomOut()
{
    helpView->zoomOut();
    if(tocView )
        tocView->zoomOut();
}

void HelpMainWindow::actionFind()
{
    actionFindNext();
}

void HelpMainWindow::actionFindNext()
{
    if( !findLine )
        return;

    helpView->actFind( findLine->text(), ui->action_Case_sensetiv->isChecked() );
}

void HelpMainWindow::actionFindPrevious()
{
    if( !findLine )
        return;

    helpView->actFind( findLine->text(), ui->action_Case_sensetiv->isChecked(), true );
}

void HelpMainWindow::actionHome()
{
    showHelp( "start", "" );
}

void HelpMainWindow::actionBack()
{
    if( history.canGoBack() )
    {
        history.back();
        changeLink();
    }
}

void HelpMainWindow::actionForward()
{
    if( history.canGoForward() )
    {
        history.forward();
        changeLink();
    }
}

void HelpMainWindow::CmEdit()
{
    if( !helpReadytoEdit() )
        return;

    try{
        auto old_text = md_text;
        HelpEditDialog dlg( other_thread_dbhelp.get(), md_text, this);
        connect( &dlg, SIGNAL( updateMarkdown(QString) ), this, SIGNAL( updateDocPage(QString) ) );

        if( dlg.exec() )
        {
            ui->actionCreate_Update_Docpage_into_DB->setEnabled(true);
        }
        else
        {
            updateDocPage( old_text );
        }

    }
    catch(jsonio::jsonio_exception& e)
    {
        QMessageBox::critical( this, e.title(), e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }

}

void HelpMainWindow::CmRead()
{
    if( !helpReadytoEdit() )
        return;
    try{
        std::vector<std::string> aKeyList;
        std::vector<std::vector<std::string>> aValList;
        other_thread_dbhelp->lastQueryData()->getKeyValueList( aKeyList, aValList, { "type" }, { "0" } );
        if( aKeyList.empty() )
            return;

        jsonui::SelectDialog selDlg( false, this, "Please, select a record to edit", aValList );
        if( !selDlg.exec() )
            return;
        size_t selNdx =  selDlg.getSelectedIndex();
        showDocument( aKeyList[selNdx].c_str() );

    }
    catch(jsonio::jsonio_exception& e)
    {
        QMessageBox::critical( this, e.title(), e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

void HelpMainWindow::CmUpdate()
{
    if( !helpReadytoEdit() )
        return;

    try{
        DocPagesData data;
        data.setMarkdownRecord( adressLine->text().toStdString(), md_text.toStdString() );
        std::shared_ptr<jsonio::JsonDomFree> domdata(jsonio::JsonDomFree::newObject());
        data.toJsonNode(domdata.get());
        std::string jsondocpage = domdata->toString(true);
        other_thread_dbhelp->UpdateFromJson( jsondocpage, true );
        ui->actionCreate_Update_Docpage_into_DB->setEnabled(false);
    }
    catch(jsonio::jsonio_exception& e)
    {
        QMessageBox::critical( this, e.title(), e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

void HelpMainWindow::CmDeleteList()
{
    if( !helpReadytoEdit() )
        return;
    try
    {
        std::vector<std::string> aKeyList;
        std::vector<std::vector<std::string>> aValList;
        std::vector<size_t> selNdx;
        other_thread_dbhelp->lastQueryData()->getKeyValueList( aKeyList, aValList );
        if( aKeyList.empty() )
            return;

        jsonui::SelectDialog selDlg( true, this, "Please, select a record to delete", aValList );
        if( !selDlg.exec() )
            return;
        selDlg.getSelection(selNdx);

        for( uint ii=0; ii<selNdx.size(); ii++ )
        {
            other_thread_dbhelp->Delete( aKeyList[selNdx[ii]] );
        }
        emit updateDBClient(); // to update images
    }
    catch(jsonio::jsonio_exception& e)
    {
        QMessageBox::critical( this, e.title(), e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

//------------------------------------------------

bool HelpMainWindow::helpReadytoEdit()
{
    if( !editHelp )
        return false;

    if( other_thread_dbhelp.get() == nullptr )
    {
        other_thread_dbhelp.reset(  newDBHelpClient( jsonio::emptyQuery ));
    }
    return other_thread_dbhelp.get() != nullptr;
}


QString HelpMainWindow::htmlTemplate()
{
    QString htmlTemplate;
    QFile f(":/index.html");
    if (f.open(QIODevice::ReadOnly | QIODevice::Text)) {
        htmlTemplate = f.readAll();
    }
    return htmlTemplate;
}

void HelpMainWindow::generateHtml( const std::string& key )
{
    // generate HTML from record
    emit updateHelpRecord( QString(key.c_str()), anchorItem );
#ifndef USE_MARKDOWN
    //ui->helpView->setUrl(QUrl("qrc:/index.html"));
    //pageHelp->toHtml([this](const QString& result) mutable {emit html(result);});
#endif

    if( editHelp )
        ui->actionCreate_Update_Docpage_into_DB->setEnabled(false);
}

void HelpMainWindow::handleHtml(QString sHtml)
{
    std::cout <<"myhtml :"<< sHtml.toStdString() << std::endl;
}


void HelpMainWindow::currentKeyChange( const std::string& key )
{
    if( editHelp &&  ui->actionCreate_Update_Docpage_into_DB->isEnabled())
    {
        QString msg = QString("Confirm saving changes to %1 document?").arg( adressLine->text() );
        auto reply = QMessageBox::question( this, "Save docpage", msg,
                                            QMessageBox::Yes|QMessageBox::No);
        if (reply == QMessageBox::Yes)
            CmUpdate();
    }

    generateHtml( key );
    addLink( History::DBRecord, key, anchorItem.toStdString() );
}

void HelpMainWindow::addLink( int atype, const std::string& alink, const std::string& anchor )
{
    history.addLink( atype, alink, anchor );
    showAddres( alink.c_str() );
    emit backwardAvailable( history.canGoBack());
    emit forwardAvailable( history.canGoForward() );
}

void HelpMainWindow::changeLink()
{
    if( tocView )
        tocView->setHtml("");
    switch( history.type() )
    {
    case History::DBRecord:

        anchorItem = history.anchor().c_str();
        generateHtml( history.link() );
        break;
    case History::ExternLink:
        helpView->loadHistory( history.link().c_str() );
        break;
    }
    showAddres( history.link().c_str() );
    emit forwardAvailable( history.canGoForward() );
    emit backwardAvailable( history.canGoBack() );
}

void HelpMainWindow::resetDBClient()
{
    history.clear();
    emit updateDBClient();
    other_thread_dbhelp.reset();
}

std::string HelpMainWindow::keyFromUrl( const QString& urllink )
{
    anchorItem = urllink.section('#', 1, 1);
    QString  filePath= urllink.section('#', 0, 0);
    DocPagesData adata;
    adata.setFileInfo( filePath, false );
    return adata.recordKey();
}

} // namespace jsonui


