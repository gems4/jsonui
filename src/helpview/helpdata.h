//  This is JSONUI library+API (https://bitbucket.org/gems4/jsonui)
//
/// \file helpdata.h
/// Declarations of DocPagesData class description of  Help record
/// ( Markdown or image file )
//
// JSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2017 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONUI depends on the following open-source software products:
// JSONIO (https://bitbucket.org/gems4/jsonio); Qt5 (https://qt.io).
//

#ifndef HELPDATA_H
#define HELPDATA_H

#include <string>
#include <QString>
#include <QDateTime>

namespace jsonio {
class JsonDom;
}

namespace jsonui {


/** Description of  Help record ( Markdown or image file ) */
struct DocPagesData {

    /// Types of Help files
    enum DocFileTypes {
        /// Markdown text file
        Markdown,
        /// Image binary file
        Image
    };

    /* id of this help record or 0 if unknown */
    //std::string _id;

    /// name of file (used as key field)
    std::string name;
    /// extension of file (used as key field)
    std::string ext;
    /// type of content (used as key field)
    int type;
    /// markdown content
    std::string markdown;
    /// image content
    std::string image;

    // work data

    /// Last modified
    QDateTime lastModif;

    std::string fileName() const;

    // must be key: "docpages/<name>;md"
    std::string recordKey() const
    {
        return "docpages/"+name+";"+ext;
    }
    void setRecordKey( const std::string& akey );
    void setMarkdownRecord( const std::string& akey, const std::string& mdtext );

    void toJsonNode( jsonio::JsonDom *object ) const;
    void fromJsonNode( const jsonio::JsonDom *object );

    void setFileInfo( const QString& fileName, bool testModif = true );
    void readFile( int atype, const QString& fileName );
    void saveFile( const QString& helpDir);
    void deleteFile( const QString& helpDir );

};

} // namespace jsonui

#endif // HELPDATA_H
