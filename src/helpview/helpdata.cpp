//  This is JSONUI library+API (https://bitbucket.org/gems4/jsonui)
//
/// \file helpdata.cpp
/// Implementation of DocPagesData class description of  Help record
/// ( Markdown or image file )
//
// JSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2017 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONUI depends on the following open-source software products:
// JSONIO (https://bitbucket.org/gems4/jsonio); Qt5 (https://qt.io).
//

#include <iostream>
#include <QFileInfo>
#include "helpdata.h"
#include "jsonio/jsondom.h"

namespace jsonui {

std::string DocPagesData::fileName() const
{
    std::string fname;
    switch( type )
    {
    case Image : fname += "images/";
        break;
    case Markdown:
    default:     break;
    }
    fname += name+"."+ext;
    return fname;
}

// must be key: "docpages/<name>;<ext>"
// for all markdown must be md
//(*.markdown *.md *.mdown)
void DocPagesData::setRecordKey( const std::string& akey )
{
    auto pos_ext = akey.find_last_of(';');
    auto name_start = akey.find_first_of('/');

    jsonio::jsonioErrIf( name_start == std::string::npos, "setRecordKey", "Illegal record key: " + akey  );

    if( pos_ext != std::string::npos )
    {
        name = akey.substr(name_start+1, pos_ext-name_start-1);
        ext = akey.substr(pos_ext+1);
    }
    else
    {
        name = akey.substr( name_start+1 );
        ext = "";
    }

    if( ext == "md" || ext == "markdown" || ext == "mdown" || ext.empty() )
        type = 0;
    else
        type = 1;    // image file

    markdown = "";
    image = "";
    // lastModif =  QDateTime::currentDateTime(););
}

void DocPagesData::setMarkdownRecord(const std::string &akey, const std::string &mdtext)
{
    setRecordKey( akey );
    markdown = mdtext;
}

// set data to bson
void DocPagesData::toJsonNode( jsonio::JsonDom *object ) const
{
    object->appendString( "name", name );
    object->appendString( "ext", ext );
    object->appendInt( "type", type );
    object->appendString( "markdown", markdown );
    object->appendString( "image", image );
}

void DocPagesData::fromJsonNode( const jsonio::JsonDom *object )
{
    if(!object->findValue( "name", name ) )
        name = "undefined";
    if(!object->findValue( "ext", ext ) )
        ext = "md";
    if(!object->findValue( "type", type ) )
        type = Markdown;
    if(!object->findValue( "markdown", markdown ) )
        markdown = "";
    if(!object->findValue( "image", image ) )
        image = "";
}

void DocPagesData::setFileInfo( const QString& filePath, bool testModif )
{
    QFileInfo finfo(filePath);

    name = finfo.baseName().toStdString(); // baseName
    ext = finfo.suffix().toStdString();
    if( filePath.contains("images/"))
        type = Image;
    else
        type = Markdown;

    markdown = "";
    image = "";
    if( testModif )
        lastModif =  finfo.lastModified();
}

void DocPagesData::readFile( int atype, const QString& filePath )
{
    QFile infile(filePath);
    type = atype;

    switch (type)
    {
    case Image:     // read binary image
        if (infile.open(QFile::ReadOnly ))
        {
            QByteArray barr = infile.readAll();
            QByteArray barr_64 = barr.toBase64();
            image = barr_64.data();
        }
        break;
    case Markdown:  // read md text file
    default:
        if( infile.open(QFile::ReadOnly | QFile::Text) )
        {
            QString content = infile.readAll();
            markdown = content.toStdString();
        }
        break;
    }
    //  cout << name << "." << ext << endl << markdown << image.size() << endl;
}

void DocPagesData::saveFile( const QString& helpDir )
{
    QString fName = helpDir+"/"+fileName().c_str();
    QFile file(fName);

    switch (type)
    {
    case Image:     // read binary image
        if( !image.empty())
        {
            file.open(QIODevice::WriteOnly );
            QByteArray imgbase64 = image.c_str();
            file.write( QByteArray::fromBase64(imgbase64) );
        }
        break;
    case Markdown:  // read md text file
    default:
        if( !markdown.empty() )
        {
            file.open(QIODevice::WriteOnly | QIODevice::Text);
            file.write( markdown.c_str() );
        }
        break;
    }
}


void DocPagesData::deleteFile( const QString& helpDir )
{
    QString fName = helpDir+"/"+fileName().c_str();
    QFile file(fName);
    file.remove();
}


// http://stackoverflow.com/questions/27854979/how-to-load-base64-image-data-from-json-in-qt
/*

QString strReply = (QString)reply->readAll(); // json data from a servlet (created using gson library)
QJsonDocument jsonResponse = QJsonDocument::fromJson(strReply.toUtf8());
QJsonObject jsonObj = jsonResponse.object();
QByteArray imgbase64 = jsonObj["photo"]; // image data
QImage img;
img.loadFromData(QByteArray::fromBase64(imgbase64));
ui->outputImage->setPixmap(QPixmap::fromImage(img));
ui->outputImage->setScaledContents( true );
ui->outputImage->setSizePolicy( QSizePolicy::Ignored, QSizePolicy::Ignored );
*/

} // namespace jsonui
