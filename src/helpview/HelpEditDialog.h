//  This is JSONUI library+API (https://bitbucket.org/gems4/jsonui)
//
/// \file HelpEditDialog.h
/// Declaration of HelpEditDialog widget - dialog for editing help pages in markdown format
//
// JSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2017 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONUI depends on the following open-source software products:
// JSONIO (https://bitbucket.org/gems4/jsonio); Qt5 (https://qt.io).
//

#ifndef HELPEDITDIALOG_H
#define HELPEDITDIALOG_H

#include <QDialog>
#include "jsonio/dbschemadoc.h"

namespace Ui {
class HelpEditDialog;
}

namespace jsonui {

class HelpEditDialog : public QDialog
{
    Q_OBJECT

signals:

    /// Reset current md text
    void updateMarkdown(  QString new_mdtext );

protected slots:

    void applyChanges();
    void previewChanges();

    void insertImage();
    void addImage();
    void insertDocpage();
    void addDocPage();

    void showContextMenu(const QPoint &pt);

public:

    explicit HelpEditDialog( jsonio::TDBSchemaDocument* database, const QString& mdtext, QWidget *parent = nullptr);
    ~HelpEditDialog();

private:

    Ui::HelpEditDialog *ui;
    jsonio::TDBSchemaDocument *help_client;

    void imageLink(const std::string key);
    void docLink(const std::string key);
    std::string saveFiletoDB(int fileType, const QString &fileName, const QString &filePath);
};

} // namespace jsonui

#endif // HELPEDITDIALOG_H
