//  This is JSONUI library+API (https://bitbucket.org/gems4/jsonui)
//
/// \file HelpEditDialog.h
/// Declaration of HelpBrowser class provides an object to view help documents
//
// JSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2017 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONUI depends on the following open-source software products:
// JSONIO (https://bitbucket.org/gems4/jsonio); Qt5 (https://qt.io).
//

#ifndef HELPBROWSER_H
#define HELPBROWSER_H

#include <QtPrintSupport/QPrinter>

#ifndef NO_QWEBENGINE

#include <QWebEngineView>

#else

#include <QTextBrowser>

#endif

namespace jsonui {

class HelpGenerator2;

#ifndef NO_QWEBENGINE

class PreviewPage : public QWebEnginePage
{
    Q_OBJECT

signals:
    void newLink(const QUrl& url);

public:
    explicit PreviewPage(QObject *parent = nullptr) : QWebEnginePage(parent) {}

protected:
    bool acceptNavigationRequest(const QUrl &url, NavigationType type, bool isMainFrame);
};


class HelpBrowser : public QWebEngineView
{

    Q_OBJECT

signals:
    void newLink(const QUrl& url);
    //void htmlLoadFinished();

public slots:

   void htmlResultReady(const QString& html);

public:

    HelpBrowser(QWidget *parent = Q_NULLPTR);
    virtual ~HelpBrowser();

    void setWebChannel(HelpGenerator2 *worker);

    void setHtmlData(const QString &html )
    {
       pageHelp->setHtml( html, baseUrl );
       //setContent(html.toUtf8(), "text/html;charset=UTF-8", baseUrl);
    }

    void scrollToAnchor( const QString& anchorItem );
    void helpPrint();

    void zoomIn()
    {
        setZoomFactor(zoomFactor()*1.1);
    }

    void zoomOut()
    {
        setZoomFactor(zoomFactor()*0.9);
    }

    void actFind( const QString& findText, bool caseSensitively, bool findBackward = false );

    void loadHistory(const QString &history)
    {
        load( QUrl(history) );
    }

protected:

    QUrl baseUrl;
    QPrinter printer;
    PreviewPage *pageHelp = nullptr;

};

#else


class HelpBrowser : public QTextBrowser
{

    Q_OBJECT

signals:
    void newLink(const QUrl& url);
    void loadFinished(bool ok=true);

public slots:

    void htmlResultReady(const QString& html);
    void setSource(const QUrl &name);

public:

    HelpBrowser(QWidget *parent = Q_NULLPTR);
    virtual ~HelpBrowser();

    void setWebChannel(HelpGenerator2 *worker);

    void helpPrint();
    void actFind( const QString& findText, bool caseSensitively, bool findBackward = false );
    void loadHistory(const QString &history);
    void setHtmlData(const QString &html )
    {
      setHtml(html);
      emit loadFinished();
    }

   // from base class
   // void scrollToAnchor( const QString& anchorItem );
   // void zoomIn()
   // void zoomOut()


protected:

    QString convertDocument(const QString &mdtexts);

};

#endif

} // namespace jsonui

#endif // HELPBROWSER_H
