//  This is JSONUI library+API (https://bitbucket.org/gems4/jsonui)
//
/// \file HelpEditDialog.h
/// Implementation of HelpEditDialog widget - dialog for editing help pages in markdown format
//
// JSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2017 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONUI depends on the following open-source software products:
// JSONIO (https://bitbucket.org/gems4/jsonio); Qt5 (https://qt.io).
//

#include <QPushButton>
#include <QMessageBox>
#include <QMenu>

#include "HelpEditDialog.h"
#include "ui_HelpEditDialog.h"
#include "helpdata.h"
#include "HelpFileDialog.h"
#include "SelectDialog.h"
#include "jsonio/jsondomfree.h"

namespace jsonui {

HelpEditDialog::HelpEditDialog(jsonio::TDBSchemaDocument *database, const QString &mdtext, QWidget *parent):
    QDialog(parent), ui(new Ui::HelpEditDialog), help_client(database)
{
    ui->setupUi(this);

    ui->editor->setPlainText(mdtext);

    QObject::connect( ui->previewButton, SIGNAL( clicked() ), this, SLOT( previewChanges() ));
    QObject::connect( ui->undoButton, SIGNAL( clicked() ), ui->editor, SLOT( undo() ));
    QObject::connect( ui->redoButton, SIGNAL( clicked() ), ui->editor, SLOT( redo() ));
    QObject::connect( ui->editor, SIGNAL( undoAvailable(bool) ), ui->undoButton, SLOT( setEnabled(bool) ));
    QObject::connect( ui->editor, SIGNAL( redoAvailable(bool) ), ui->redoButton, SLOT( setEnabled(bool) ));

    QObject::connect( ui->insertImgButton, SIGNAL( clicked() ), this, SLOT( insertImage() ));
    QObject::connect( ui->addmgButton, SIGNAL( clicked() ), this, SLOT( addImage() ));
    QObject::connect( ui->insertDocButton, SIGNAL( clicked() ), this, SLOT( insertDocpage() ));
    QObject::connect( ui->addDocButton, SIGNAL( clicked() ), this, SLOT( addDocPage() ));

    connect(ui->buttons->button(QDialogButtonBox::Apply), &QPushButton::clicked, this, &HelpEditDialog::applyChanges);
    connect(ui->buttons, &QDialogButtonBox::rejected, this, &QDialog::reject);

    ui->editor->setContextMenuPolicy(Qt::CustomContextMenu);
    connect( ui->editor, SIGNAL(customContextMenuRequested(const QPoint&)),
             this, SLOT(showContextMenu(const QPoint &)) );
}

HelpEditDialog::~HelpEditDialog()
{
    delete ui;
}

void HelpEditDialog::previewChanges()
{
    emit updateMarkdown( ui->editor->toPlainText() );
}

void HelpEditDialog::applyChanges()
{
    previewChanges();
    accept();
}


void HelpEditDialog::insertImage()
{
    try{
        std::vector<std::string> aKeyList;
        std::vector<std::vector<std::string>> aValList;
        help_client->lastQueryData()->getKeyValueList( aKeyList, aValList, { "type" }, { "1" } );
        if( aKeyList.empty() )
            return;

        jsonui::SelectDialog selDlg( false, this, "Please, select an image record to link", aValList );
        if( !selDlg.exec() )
            return;
        size_t selNdx =  selDlg.getSelectedIndex();
        imageLink( aKeyList[selNdx] );
    }
    catch(jsonio::jsonio_exception& e)
    {
        QMessageBox::critical( this, e.title(), e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

void HelpEditDialog::addImage()
{
    try{
        HelpFileDialog addDlg( DocPagesData::Image, this );

        if( addDlg.exec() )
        {
            QString initFile = addDlg.sourceFile();
            QString nameFile = addDlg.linkName();
            if( initFile.isEmpty() || nameFile.isEmpty() )
                return;

            // save to DB
            std::string newkey = saveFiletoDB( DocPagesData::Image, nameFile, initFile);
            // copy to images
            help_client->Read(newkey);
            DocPagesData data;
            data.fromJsonNode( help_client->getDom());
            data.saveFile(".");
            // add link
            imageLink( newkey );
        }
    }
    catch(jsonio::jsonio_exception& e)
    {
        QMessageBox::critical( this, e.title(), e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

void HelpEditDialog::insertDocpage()
{
    try{
        std::vector<std::string> aKeyList;
        std::vector<std::vector<std::string>> aValList;
        help_client->lastQueryData()->getKeyValueList( aKeyList, aValList, { "type" }, { "0" } );
        if( aKeyList.empty() )
            return;

        jsonui::SelectDialog selDlg( false, this, "Please, select a help page to link", aValList );
        if( !selDlg.exec() )
            return;
        size_t selNdx =  selDlg.getSelectedIndex();
        docLink( aKeyList[selNdx] );
    }
    catch(jsonio::jsonio_exception& e)
    {
        QMessageBox::critical( this, e.title(), e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

void HelpEditDialog::addDocPage()
{
    try{
        HelpFileDialog addDlg( DocPagesData::Markdown, this );

        if( addDlg.exec() )
        {
            QString initFile = addDlg.sourceFile();
            QString nameFile = addDlg.linkName();
            if( nameFile.isEmpty() )
                return;

            // save to DB
            std::string newkey = saveFiletoDB( DocPagesData::Markdown, nameFile, initFile);
            // add link
            docLink( newkey );
        }
    }
    catch(jsonio::jsonio_exception& e)
    {
        QMessageBox::critical( this, e.title(), e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }}


void HelpEditDialog::imageLink( const std::string key )
{
    DocPagesData  data;
    data.setRecordKey( key );
    QString link = QString(" ![%1](images/%1.%2) ").arg(data.name.c_str()).arg(data.ext.c_str());
    ui->editor->insertPlainText(link);
}


void HelpEditDialog::docLink( const std::string key )
{
    DocPagesData  data;
    data.setRecordKey( key );
    QString link = QString(" [%1](%1.%2) ").arg(data.name.c_str()).arg(data.ext.c_str());
    ui->editor->insertPlainText(link);
}

std::string HelpEditDialog::saveFiletoDB( int fileType, const QString& fileName, const QString& filePath )
{
   DocPagesData data;
   if( !filePath.isEmpty() )
   {
       data.setFileInfo(filePath);
       data.readFile( fileType, filePath );
   }
   else
   {
       data.ext = "md";
       data.type = DocPagesData::Markdown;
   }
   data.name = fileName.toStdString();

   // save changes
   std::shared_ptr<jsonio::JsonDomFree> domdata(jsonio::JsonDomFree::newObject());
   data.toJsonNode(domdata.get());
   std::string jsondocpage = domdata->toString(true);
   auto keyhandler = help_client->CreateFromJson( jsondocpage, true );
   return keyhandler;
}

void HelpEditDialog::showContextMenu(const QPoint &pt)
{
    QMenu *menu = ui->editor->createStandardContextMenu();
    menu->addSeparator();

    auto act =  new QAction(tr("&Insert Image"), this);
    act->setShortcut(tr("Ctrl+I"));
    act->setStatusTip(tr("Insert existing image link to document."));
    connect(act, SIGNAL(triggered()), this, SLOT(insertImage()));
    menu->addAction(act);

    act =  new QAction(tr("&Add Image"), this);
    act->setShortcut(tr("Ctrl+A"));
    act->setStatusTip(tr("Add new image to database and insert link to document."));
    connect(act, SIGNAL(triggered()), this, SLOT(addImage()));
    menu->addAction(act);

    act =  new QAction(tr("Insert &Docpage"), this);
    act->setShortcut(tr("Ctrl+D"));
    act->setStatusTip(tr("Insert existing docpage link to document."));
    connect(act, SIGNAL(triggered()), this, SLOT(insertDocpage()));
    menu->addAction(act);

    act =  new QAction(tr("Add Docpage"), this);
    //act->setShortcut(tr("Ctrl+I"));
    act->setStatusTip(tr("Add new docpage to DB and insert link to document."));
    connect(act, SIGNAL(triggered()), this, SLOT(addDocPage()));
    menu->addAction(act);

    menu->exec(ui->editor->mapToGlobal(pt));
    delete menu;
}

} // namespace jsonui
