//  This is JSONUI library+API (https://bitbucket.org/gems4/jsonui)
//
/// \file HelpEditDialog.h
/// Implementation of HelpBrowser class provides an object to view help documents
//
// JSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2017 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONUI depends on the following open-source software products:
// JSONIO (https://bitbucket.org/gems4/jsonio); Qt5 (https://qt.io).
//

#include <QtPrintSupport/QPrintDialog>
#include <QMessageBox>
#include <QUrl>
#include "helpgenerator.h"
#include "helpBrowser.h"

#ifndef NO_QWEBENGINE
#include <QtWebEngineWidgets>
#include <QDesktopServices>
#else
extern "C" {
#ifdef Q_OS_WIN
#include <Windows.h>
#endif
#include <mkdio.h>
}
#include <QCoreApplication>
#endif

namespace jsonui {

#ifndef NO_QWEBENGINE

bool PreviewPage::acceptNavigationRequest(const QUrl &url,
                                          QWebEnginePage::NavigationType /*type*/,
                                          bool /*isMainFrame*/)
{
    if( url.isLocalFile() )
    {
        emit newLink( url );
        return false;
    }
    // Only allow qrc:/index.html.
    if (url.scheme() == QString("qrc")   || url.scheme() == QString("data"))
        return true;
    QDesktopServices::openUrl(url);
    return false;
}


HelpBrowser::HelpBrowser(QWidget *parent): QWebEngineView(parent)
{
    baseUrl = QUrl::fromLocalFile(qApp->applicationDirPath() + "/");
    //std::cout << "baseUrl " << baseUrl.toString().toStdString() << std::endl;

    pageHelp = new PreviewPage(parent);
    setPage(pageHelp);
    connect( page(), SIGNAL( newLink(QUrl) ), this, SIGNAL( newLink(QUrl) ) );
}

HelpBrowser::~HelpBrowser()
{
    delete pageHelp;
}

void HelpBrowser::setWebChannel(HelpGenerator2 *worker)
{
    QWebChannel *channel = new QWebChannel(this);
    channel->registerObject(QStringLiteral("content"), worker);
    pageHelp->setWebChannel(channel);
    connect(worker, &HelpGenerator2::htmlResultReady, this, &HelpBrowser::htmlResultReady);
}


void HelpBrowser::htmlResultReady(const QString& /*html*/)
{
  //std::cout << "htmlResultReady " << html.toStdString() << std::endl;
  emit loadFinished(true);
}

void HelpBrowser::scrollToAnchor( const QString& anchorItem )
{
    //std::cout << "anchorItem = " << anchorItem.toStdString() << std::endl;
    QString javascript = QString("location.hash = '#%1';").arg(anchorItem);
#ifndef USE_MARKDOWN
    javascript = javascript.toCaseFolded();
#endif
    std::cout << "javascript = " << javascript.toStdString() << std::endl;
    page()->runJavaScript( javascript );
}


void HelpBrowser::helpPrint()
{

    QPrintDialog dlg(  &printer, parentWidget() );
    dlg.setWindowTitle(tr("Print Help Page"));
    if ( hasSelection() )
        dlg.addEnabledOption(QAbstractPrintDialog::PrintSelection);
    if( dlg.exec() )
        page()->print( &printer, [](const QVariant &result){ qDebug() << result;
        return result; } ) ;
}

void HelpBrowser::actFind( const QString& findText, bool caseSensitively, bool findBackward )
{
    QWebEnginePage::FindFlags flg;

    if( findBackward )
        flg |= QWebEnginePage::FindBackward;
    if( caseSensitively )
        flg |= QWebEnginePage::FindCaseSensitively;

    //--if(ui->action_words_beginnings->isChecked() )
    //--    ; flg |=QWebEnginePage::FindAtWordBeginningsOnly;

    page()->findText( findText, flg, [this](bool found) {
        if (!found) QMessageBox::information(this, QString(), QStringLiteral("No occurrences found"));
    });
}

#else

//----------------------------------------------------------------------


HelpBrowser::HelpBrowser(QWidget *parent): QTextBrowser(parent)
{
    setOpenExternalLinks(true);
    setSearchPaths( { qApp->applicationDirPath()} );
    //connect( this, SIGNAL(anchorClicked(const QUrl &link) ), this, SIGNAL( newLink(QUrl) ) );
    //connect( this, SIGNAL( textChanged() ), this, SIGNAL( loadFinished() ) );
}

HelpBrowser::~HelpBrowser()
{

}

void HelpBrowser::setSource(const QUrl &url)
{
   if( url.isRelative() )
   {
       //QTextBrowser::setSource( QUrl() );
       emit newLink( url );
       return;
   }
   QTextBrowser::setSource( url );

}

void HelpBrowser::setWebChannel(HelpGenerator2 *worker)
{
   connect(worker, &HelpGenerator2::textChanged, this, &HelpBrowser::htmlResultReady);
}

void HelpBrowser::htmlResultReady(const QString& markdown )
{
  //std::cout << "htmlResultReady " << markdown.toStdString() << std::endl;
  auto html = convertDocument( markdown );
  //std::cout << "htmlResultReady " << html.toStdString() << std::endl;
  setHtml(html);
  emit loadFinished(true);
}


void HelpBrowser::helpPrint()
{
    QPrinter printer;
    QPrintDialog dlg(  &printer, parentWidget() );
    dlg.setWindowTitle(tr("Print Help Page"));
    if ( textCursor().hasSelection() )
        dlg.addEnabledOption(QAbstractPrintDialog::PrintSelection);
    if( dlg.exec() )
        print( &printer ) ;
}

void HelpBrowser::actFind( const QString& findText, bool caseSensitively, bool findBackward )
{
    QTextDocument::FindFlags flg;

    if( findBackward )
        flg |= QTextDocument::FindBackward;
    if( caseSensitively )
        flg |= QTextDocument::FindCaseSensitively;

    //--if(ui->action_words_beginnings->isChecked() )
    //--    ; flg |=QTextDocument::FindWholeWords;

    find( findText, flg);
}



void HelpBrowser::loadHistory(const QString &history)
{
    setSource( QUrl(history) );
}


unsigned int translateConverterOptions()
{
//#ifndef __unix
//    unsigned int converterOptions = 0;
//#else
    unsigned int converterOptions = MKD_TOC | MKD_NOSTYLE;

//    converterOptions |= MKD_NODLDISCOUNT;

    // autolink
    //if (options.testFlag(MarkdownConverter::AutolinkOption)) {
        converterOptions |= MKD_AUTOLINK;
    //}

    /* strikethrough
    if (options.testFlag(MarkdownConverter::NoStrikethroughOption)) {
        converterOptions |= MKD_NOSTRIKETHROUGH;
    }

    // alphabetic lists
    if (options.testFlag(MarkdownConverter::NoAlphaListOption)) {
        converterOptions |= MKD_NOALPHALIST;
    }

    // definition lists
    if (options.testFlag(MarkdownConverter::NoDefinitionListOption)) {
        converterOptions |= MKD_NODLIST;
    }*/

    // SmartyPants
    //if (options.testFlag(MarkdownConverter::NoSmartypantsOption)) {
        converterOptions |= MKD_NOPANTS;
    //}

    // Footnotes
    //if (options.testFlag(MarkdownConverter::ExtraFootnoteOption)) {
        converterOptions |= MKD_EXTRA_FOOTNOTE;
    //}

    /* Superscript
    if (options.testFlag(MarkdownConverter::NoSuperscriptOption)) {
        converterOptions |= MKD_NOSUPERSCRIPT;
    }*/
//#endif
    return converterOptions;
}


QString HelpBrowser::convertDocument(const QString &text )
{
    QString html;

//#ifndef __unix
//   html = text;   //temporally not convert
//#else
    MMIOT *doc = nullptr;

    if (text.length() > 0)
    {
        QString markdownText(text);

        // text has to always end with a line break,
        // otherwise characters are missing in HTML
        if (!markdownText.endsWith('\n')) {
            markdownText.append('\n');
        }

        unsigned int converterOptions = translateConverterOptions();

        QByteArray utf8Data = markdownText.toUtf8();
        doc = mkd_string(utf8Data, utf8Data.length(), converterOptions);

        mkd_compile(doc, converterOptions);
        char *out;
        mkd_document(doc, &out);

        html = QString::fromUtf8(out);
        mkd_cleanup(doc);

    }
//#endif

    return html;
}
#endif

} // namespace jsonui

