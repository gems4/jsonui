//  This is JSONUI library+API (https://bitbucket.org/gems4/jsonui)
//
/// \file helpgenerator.cpp
/// Implementation of HelpGenerator class is used for extract data from Help database and
/// generate Help HTML from internal Markdown format
//
// JSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2017 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONUI depends on the following open-source software products:
// JSONIO (https://bitbucket.org/gems4/jsonio); Qt5 (https://qt.io).
//

#include <iostream>
#include <QSettings>
#include <QDir>
#include "helpgenerator.h"
#include "helpdata.h"
#include "preferences.h"

#ifdef USE_MARKDOWN

#include <converter/markdownconverter.h>
#include <template/template.h>
#include <converter/markdowndocument.h>
#include <converter/discountmarkdownconverter.h>
#include <converter/revealmarkdownconverter.h>

#endif

namespace jsonui {

/// could be changed to read only, but change part for loading images
/// it will be more quickly
jsonio::TDBSchemaDocument* newDBHelpClient()
{
    jsonio::TDBSchemaDocument* newClient =  jsonio::TDBSchemaDocument::newSchemaDocument(
                jsonui::uiSettings().resources_database(),  "DocPages", "docpages" );
    return newClient;
}

jsonio::TDBSchemaDocument* newDBHelpClient(  const jsonio::DBQueryData& query  )
{
    jsonio::TDBSchemaDocument* newClient =  jsonio::TDBSchemaDocument::newSchemaDocumentQuery(
                jsonui::uiSettings().resources_database(),  "DocPages", "docpages",  query );
    return newClient;
}

//-----------------------------------------------------
//  TreadGeneratorPrivate
//-----------------------------------------------------

#ifdef USE_MARKDOWN

class HelpGenerator2Private
{

public:

    enum MarkdownConvert { CDiscountMarkdownConverter, CRevealMarkdownConverter };

    explicit HelpGenerator2Private( ):
        document(nullptr), converter(nullptr), markdownConverter(CDiscountMarkdownConverter)
    {
        setMarkdownConverter(  markdownConverter );
        setCodeHighlightingStyle("default");
     }

    ~HelpGenerator2Private()
    {
        delete document;
        delete converter;
    }

    // main functions
    void setMarkdownConverter( MarkdownConvert aconverter );
    void setCodeHighlightingStyle(const QString &style)
    {
      converter->templateRenderer()->setCodeHighlightingStyle(style);
    }

    QString generateHtmlFromMarkdown()
    {
        if( !document )
            return QString();
        QString html = converter->templateRenderer()->render(converter->renderAsHtml(document), renderOptions());
        return html;
    }

    QString generateTableOfContents()
    {
        if( !document )
            return QString();
        QString toc = converter->renderAsTableOfContents(document);
        //QString styledToc = QString("<html><head>\n<style type=\"text/css\">ul { list-style-type: none; padding: 0; margin-left: 1em; } a { text-decoration: none; }</style>\n</head><body>%1</body></html>").arg(toc);
        QString styledToc = QString("<html><head>\n<style type=\"text/css\">ul li {  text-align: left; list-style-type: none; padding: 0; margin-left: -2em; } a { text-decoration: none; }</style>\n</head><body>%1</body></html>").arg(toc);
        return styledToc;
    }

    void setMarkdownText(const QString &text)
    {
        //if( !text.isEmpty() )
        {
            delete document;
            // set new markdown document
            document = converter->createDocument(text, converterOptions());
        }
    }


    // addition function
    QString exportHtml(const QString &styleSheet, const QString &highlightingScript, const QString &highlightStyle);

private:

    MarkdownDocument *document;
    MarkdownConverter *converter;
    MarkdownConvert markdownConverter =  CDiscountMarkdownConverter;

    MarkdownConverter::ConverterOptions converterOptions() const;
    Template::RenderOptions renderOptions() const;

};


MarkdownConverter::ConverterOptions HelpGenerator2Private::converterOptions() const
{
    MarkdownConverter::ConverterOptions parserOptionFlags(
         MarkdownConverter::TableOfContentsOption | MarkdownConverter::NoStyleOption);

    // autolink
    parserOptionFlags |= MarkdownConverter::AutolinkOption;
    // strikethrough
    //?parserOptionFlags |= MarkdownConverter::NoStrikethroughOption;
    // alphabetic lists
    //?parserOptionFlags |= MarkdownConverter::NoAlphaListOption;
    // definition lists
    //?parserOptionFlags |= MarkdownConverter::NoDefinitionListOption;
     // SmartyPants
    parserOptionFlags |= MarkdownConverter::NoSmartypantsOption;
    // Footnotes
    parserOptionFlags |= MarkdownConverter::ExtraFootnoteOption;
    // Superscript
    //?parserOptionFlags |= MarkdownConverter::NoSuperscriptOption;

    return parserOptionFlags;
}

Template::RenderOptions HelpGenerator2Private::renderOptions() const
{
    Template::RenderOptions renderOptionFlags;

    // math support
    ///renderOptionFlags |= Template::MathSupport;

    // inline math support
    renderOptionFlags |= Template::MathInlineSupport;

    // diagram support
    renderOptionFlags |= Template::DiagramSupport;

    // code highlighting
    renderOptionFlags |= Template::CodeHighlighting;

    return renderOptionFlags;
}

QString HelpGenerator2Private::exportHtml( const QString &styleSheet,
                  const QString &highlightingScript, const QString &highlightStyle )
{
    if (!document) return QString();

    QString header;
    if( !styleSheet.isEmpty() )
        header += QString("\n<style>%1</style>").arg(styleSheet);

    if( !highlightingScript.isEmpty() && !highlightStyle.isEmpty() )
    {
        header += QString("\n<style>%1</style>").arg(highlightStyle);
        header += QString("\n<script>%1</script>").arg(highlightingScript);
        header += "\n<script>hljs.initHighlightingOnLoad();</script>";
    }

    return converter->templateRenderer()->exportAsHtml(header, converter->renderAsHtml(document), renderOptions());
}


void HelpGenerator2Private::setMarkdownConverter(  MarkdownConvert aconverter )
{
    QString style;

    if( markdownConverter == aconverter  && converter )
        return;

    markdownConverter = aconverter;

    if (converter)
    {
        style = converter->templateRenderer()->codeHighlightingStyle();
        delete converter;
    }

    switch (markdownConverter) {
    case  CRevealMarkdownConverter:
        converter = new RevealMarkdownConverter();
        converter->templateRenderer()->setCodeHighlightingStyle(style);
        break;

    case  CDiscountMarkdownConverter:
    //default:
        converter = new DiscountMarkdownConverter();
        converter->templateRenderer()->setCodeHighlightingStyle(style);
        break;
    }
}

#else

class HelpGenerator2Private
{

public:

    explicit HelpGenerator2Private( )  { }

    ~HelpGenerator2Private() {  }

    // main functions

    QString generateHtmlFromMarkdown() { return QString(); }

    QString generateTableOfContents() { return QString(); }

    void setMarkdownText(const QString &/*text*/)
    { }

private:

};

#endif

//-----------------------------------------------------
//  HelpGenerator2
//-----------------------------------------------------


HelpGenerator2::HelpGenerator2( QObject *parent ):
    QObject(parent), m_dbhelp(nullptr), pdata( new HelpGenerator2Private() )
{
    // move to other thread - must be run after allocator
    // resetDBClient();
}

HelpGenerator2::~HelpGenerator2()
{
 delete pdata;
}

void HelpGenerator2::setHelpRecord( const QString& key, const QString& anchor )
{
    m_key = key;
    m_anchor = anchor;
    QString code = readRecord( key );
    // std::cout << "Oldhtml: " << code.toUtf8().data() << std::endl;

    setText( code );
}

void HelpGenerator2::setText(const QString &text)
{
    //if (text == m_text)
    //    return;
    m_text = text;
    emit textChanged( m_text );

#ifdef USE_MARKDOWN

    pdata->setMarkdownText(m_text);

    const QString html = pdata->generateHtmlFromMarkdown();
    m_htmltext = html;
    //std::cout << "Oldhtml: " << m_htmltext.toUtf8() << std::endl;
    emit htmlResultReady(html);

    QString toc = pdata->generateTableOfContents();
    emit tocResultReady(toc);

#endif

}

// ---------------------- db ------------


QString HelpGenerator2::readRecord( const QString& _key )
{
  if( m_dbhelp.get() == nullptr )
      return "";

  if( _key.isEmpty() )
      return "";
  try
   {
      std::string key = _key.toStdString();
      if( m_dbhelp->Find( key ) )
      {
          std::string markdown;
          m_dbhelp->Read( key );
          m_dbhelp->getValue("markdown", markdown);
          return QString(markdown.c_str());
      }
   }
  catch(...) {}

  return "";
}

void HelpGenerator2::resetDBClient()
{
    try{

        jsonio::TDBSchemaDocument* newClient =  newDBHelpClient();
        if( newClient == nullptr )
            m_dbhelp.reset();
        else
            m_dbhelp.reset( newClient );
        copyImageRecordsToFileStructure(".");
    }
    catch(std::exception& e)
    {
        std::cout << "Internal comment: " << e.what() << std::endl;
        // throw;
    }
}

//??????????????? new keys structure - must be fix
void HelpGenerator2::saveRecordToFile( const QString& key, const QString& helpDir )
{
   if( m_dbhelp.get() == nullptr )
      return;

  DocPagesData data;
  m_dbhelp->Read( key.toStdString() );
  data.fromJsonNode(m_dbhelp->getDom());
  data.saveFile(helpDir);
}

void HelpGenerator2::copyImageRecordsToFileStructure( const QString& dirPath )
{
    if( m_dbhelp.get() == nullptr )
      return;

    std::cout << "start copyImageRecordsToFileStructure" << std::endl;

    QDir  dir(dirPath);
    dir.mkpath("images");
    clearDir( dirPath+"/images");

    jsonio::DBQueryData queryImages("{ \"type\": 1 }", jsonio::DBQueryData::qTemplate );

    jsonio::SetReadedFunction setfnc = [dirPath]( const std::string& jsondata )
    {
        std::shared_ptr<jsonio::JsonDomFree> domnode(jsonio::JsonDomFree::newObject());
        jsonio::parseJsonToNode( jsondata, domnode.get());
        DocPagesData data;
        data.fromJsonNode( domnode.get() );
        data.saveFile(dirPath);
    };

    m_dbhelp->runQuery( queryImages, setfnc );

    std::cout << "finish copyImageRecordsToFileStructure" << std::endl;
}

void HelpGenerator2::clearDir( const QString& dirPath )
{
    QDir dir( dirPath );
    dir.setFilter( QDir::NoDotAndDotDot | QDir::Files );
    foreach( QString dirItem, dir.entryList() )
            dir.remove( dirItem );
}

} // namespace jsonui
