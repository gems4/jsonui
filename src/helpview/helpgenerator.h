//  This is JSONUI library+API (https://bitbucket.org/gems4/jsonui)
//
/// \file helpgenerator.h
/// Declarations of HelpGenerator class is used for extract data from Help database and
/// generate Help HTML from internal Markdown format
//
// JSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2017 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONUI depends on the following open-source software products:
// JSONIO (https://bitbucket.org/gems4/jsonio); Qt5 (https://qt.io).
//

#ifndef HELPGENERATOR_H
#define HELPGENERATOR_H

#include <QObject>
#include <QString>
#include <QtCore/QMutex>
#include <QtCore/QThread>
#include <QtCore/QQueue>
#include <QtCore/QWaitCondition>

#include "jsonio/dbschemadoc.h"

// patch -p 1 < cutemarked.patch

namespace jsonui {

/// Allocate Help DB Schema
jsonio::TDBSchemaDocument* newDBHelpClient();
jsonio::TDBSchemaDocument* newDBHelpClient(  const jsonio::DBQueryData& query  );

class HelpGenerator2Private;

/// \class HelpGenerator is used for extract data from Help database and
/// generate Help HTML from internal Markdown format
class HelpGenerator2: public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString text MEMBER m_text NOTIFY textChanged FINAL)
    Q_PROPERTY(QString htmltext MEMBER m_htmltext NOTIFY htmlResultReady FINAL)
    //Q_PROPERTY(QString anchor MEMBER m_anchor FINAL)

signals:
    void textChanged(const QString &text);
    void htmlResultReady(const QString& html);
    void tocResultReady(const QString& toc);

public slots:
    /// Set markdown text from record
    void setHelpRecord( const QString& key, const QString& anchor );

    /// Reset/change Database
    void resetDBClient();

    /// Set markdown text
    void setText(const QString &text);

public:

    explicit HelpGenerator2(QObject *parent = nullptr);
    virtual ~HelpGenerator2();

    // markdown functions -----------------------

    /// Set html text
    void setHtmlText(const QString &htmltext);

private:

    /// internal help data connection
    std::shared_ptr<jsonio::TDBSchemaDocument> m_dbhelp;
    /// Path to help images data
    QString m_docPagesImagePath = "/images";
    /// Key of readed record
    QString m_key;
    /// Text written using the Markdown (https://en.wikipedia.org/wiki/Markdown) syntax.
    QString m_text;
    /// Text written using the Markdown (https://en.wikipedia.org/wiki/Markdown) syntax.
    QString m_anchor;
    /// Html format help page
    QString m_htmltext;

    // CuteMarlEd data
    HelpGenerator2Private* pdata = nullptr;

    // work with DB ------------------------------------

    /// Read record from database
    QString readRecord( const QString& key );

    /// Save record to file
    void saveRecordToFile( const QString& key, const QString& helpDir );

    /// Download image records to dirPath
    void copyImageRecordsToFileStructure( const QString& dirPath );

    /// Clear directory
    void clearDir( const QString& dirPath );
};

// http://doc.qt.io/qt-5/threads-technologies.html

} // namespace jsonui

#endif // HELPGENERATOR_H
