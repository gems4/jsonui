//  This is JSONUI library+API (https://bitbucket.org/gems4/jsonui)
//
/// \file HelpMainWindow.h
/// Declaration of classes HelpMainWindow - Help system main dialog
//
// JSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2017 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONUI depends on the following open-source software products:
// JSONIO (https://bitbucket.org/gems4/jsonio); Qt5 (https://qt.io).
//

#ifndef HelpWindow_included
#define HelpWindow_included

#include <vector>
#include <string>
#include <memory>
#include <QMainWindow>
#include <QTextBrowser>
#include <QLineEdit>
#include <QThread>
#include "jsonio/dbschemadoc.h"
// http://doc.qt.io/qt-5/qtwebenginewidgets-qtwebkitportingguide.html

namespace Ui {
class HelpWindowData;
}

namespace jsonui {

void helpWin( const std::string& name, const std::string& item );

class HelpBrowser;

class History
{
    std::vector<std::string> links;
    std::vector<std::string> anchors;
    std::vector<int> types;
    std::size_t current = 0;

public:

    enum _types { DBRecord, ExternLink, Undefined };

    bool canGoBack() const
    {
        return current > 1;
    }

    bool canGoForward() const
    {
        return current < links.size();
    }

    void back()
    {
        if( canGoBack() )
            current--;
    }

    void forward()
    {
        if( canGoForward() )
            current++;
    }

    int type() const
    {
        if( current > 0 )
            return types[current-1];
        return Undefined;
    }

    std::string link() const
    {
        if( current  > 0 )
            return links[current-1];
        return "";
    }

    std::string anchor() const
    {
        if( current  > 0 )
            return anchors[current-1];
        return "";
    }


    void addLink( int atype, const std::string& alink, const std::string& anchor )
    {
        types.resize(current);
        links.resize(current);
        types.push_back(atype);
        links.push_back(alink);
        anchors.push_back(anchor);
        current++;
    }

    void clear()
    {
        types.clear();
        links.clear();
        current = 0;
    }

};


class HelpMainWindow : public QMainWindow
{
    Q_OBJECT

    // internal functions
    void setActions();
    void setupHtmlGenerator();

    void currentKeyChange( const std::string& key );
    void generateHtml( const std::string& key );
    void addLink( int atype, const std::string& alink, const std::string& anchor);
    void changeLink();
    std::string keyFromUrl( const QString& urllink );
    QString htmlTemplate();

    bool helpReadytoEdit();

signals:
    void updateHelpRecord( const QString& key, const QString& anchor );
    void updateDocPage( QString md_text );
    void updateDBClient();

    void forwardAvailable(bool);
    void backwardAvailable(bool);
    void html(QString sHtml);

public slots:

    void resetDBClient();

protected slots:

    void scrollToAnchor( bool scroll = true );
    void showAddres( const QString& name );
    void htmlResultReady(const QString& html);
    void tocResultReady(const QString& toc);
    void tocLinkClicked(const QUrl& url);
    void helpLinkClicked(const QUrl& url);

    void helpPrint();

    void actionHome();
    void actionBack();
    void actionForward();

    void actionFind();
    void actionFindNext();
    void actionFindPrevious();

    void actionZoomIn();
    void actionZoomOut();

    void helpAbout();
    void helpVersion();
    void helpOnHelp();
    void handleHtml(QString sHtml);

    // Record
    void CmEdit();
    void CmRead();
    void CmUpdate();
    void CmDeleteList();


public:

    /// Extern flag to edit docpages
    static bool editHelp;

    static HelpMainWindow* pDia;

    HelpMainWindow( QWidget* parent = nullptr );
    virtual ~HelpMainWindow();

    void showDocument( const char* keywd );
    void showHelp( const std::string& name, const std::string& item );

protected:

     Ui::HelpWindowData *ui;
     HelpBrowser *helpView = nullptr;
     QTextBrowser *tocView = nullptr;
     QLineEdit *adressLine = nullptr;
     QLineEdit *findLine = nullptr;

     QString anchorItem = "";
     History history;

     QThread generatorThread;

     /// internal help data connection
     std::shared_ptr<jsonio::TDBSchemaDocument> other_thread_dbhelp;
     /// Text written using the Markdown (https://en.wikipedia.org/wiki/Markdown) syntax.
     QString md_text;
};

} // namespace jsonui

#endif // HelpWindow_included

