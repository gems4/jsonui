//  This is JSONUI library+API (https://bitbucket.org/gems4/jsonui)
//
/// \file model_node.cpp
/// Implementation of TSchemaNodeModel, TSchemaNodeDelegate
/// implements a tree view of internal DOM based on our JSON schemas
//
// JSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONUI depends on the following open-source software products:
// JSONIO (https://bitbucket.org/gems4/jsonio); Qt5 (https://qt.io).
//

#include <iostream>
#include <QLineEdit>
#include <QComboBox>
#include <QMessageBox>
#include "model_dom.h"
#include "SelectDialog.h"
#include "ModelLineDialog.h"
#include "jsonio/jsondomfree.h"
#include "jsonio/tf_json.h"
using namespace std;
using namespace jsonio;

namespace jsonui {

//--------------------------------------------------------------------------------------
//  class TJsonDomModel
//  class for represents the data set and is responsible for fetchin
//  the data is neaded for viewing and for writing back any changes.
//  Reading/writing data from/to TObject and TObjList classes
//---------------------------------------------------------------------------------------

TJsonDomModel::TJsonDomModel( const string& ajson, const QStringList& aHeaderData, QObject* parent ):
        TJsonAbstractModel(parent), hdData( aHeaderData), rootNode(JsonDomFree::newObject())
{
  setupModelData(ajson, "");
  connect( this, SIGNAL(dataChanged( const QModelIndex&, const QModelIndex& )),
            parent,  SLOT(objectChanged()) );
}

TJsonDomModel::~TJsonDomModel()
{
}

JsonDom* TJsonDomModel::lineFromIndex(const QModelIndex &index) const
{
    if (index.isValid()) {
        return static_cast<JsonDom*>(index.internalPointer());
    } else {
        return rootNode.get();
    }
}

void TJsonDomModel::setupModelData( const string& ajson, const string&  )
{
    try{
         beginResetModel();
         rootNode->clearField();
         parseJsonToNode( ajson, rootNode.get() );
         endResetModel();
       }
    catch( ... )
    {
       endResetModel();
       throw;
    }
}

QModelIndex TJsonDomModel::index(int row, int column, const QModelIndex &parent) const
{
    if (!rootNode.get())
        return QModelIndex();
    auto parentItem = lineFromIndex( parent );
    if( parentItem->getChildrenCount() > 0 )
       return createIndex(row, column, parentItem->getChild( row ));
    else
       return QModelIndex();
}

QModelIndex TJsonDomModel::parent(const QModelIndex& child) const
{
    if (!child.isValid())
        return QModelIndex();

    auto childItem = lineFromIndex(child);
    auto parentItem = childItem->getParent();
    if ( parentItem == rootNode.get() )
            return QModelIndex();
    return createIndex(parentItem->getNdx(), 0, parentItem);
}

int TJsonDomModel::rowCount( const QModelIndex& parent ) const
{
   if (!rootNode.get())
       return 0;
  //if (parent.column() > 0) bugfix
  //   return 0;
  auto parentItem = lineFromIndex( parent );
  return parentItem->getChildrenCount();
}

int TJsonDomModel::columnCount( const QModelIndex& /*parent*/ ) const
{
  return 2;
}

Qt::ItemFlags TJsonDomModel::flags( const QModelIndex& index ) const
{
  Qt::ItemFlags flags = QAbstractItemModel::flags(index);
  auto item = lineFromIndex( index );
  if( index.column() == 1 && (!item->isStruct() && !item->isArray() ) )
   {  flags |= Qt::ItemIsEditable;
             return flags;
   }
  else
     return (flags & ~Qt::ItemIsEditable);
}

QVariant TJsonDomModel::headerData( int section, Qt::Orientation orientation, int role ) const
{
  if( role == Qt::DisplayRole  && orientation == Qt::Horizontal && section<hdData.size())
      return hdData[section];
  return QVariant();
}

QVariant TJsonDomModel::data( const QModelIndex& index, int role ) const
{
   if(!index.isValid())
     return QVariant();

   switch( role )
   { case Qt::DisplayRole:
     case Qt::EditRole:
            {  auto item = lineFromIndex( index );
               if( index.column()== 0 )
                   return item->getKey().c_str();
               else
                   if( index.column()== 1 )
                       return item->getFieldValue().c_str();
                   //else
                   //    return getDescription( index );
           }
           break;
      case Qt::ToolTipRole:
      case Qt::StatusTipRole:
          return  getDescription(index );
      case Qt::ForegroundRole:
          if ( index.column() == 0 )
               {
                 return QVariant( QColor( Qt::darkCyan ) );
               }
          //return QVariant( QColor( Qt::black ) );
            break;
      default: break;
   }

   return QVariant();
}

bool TJsonDomModel::setData( const QModelIndex& index, const QVariant& value, int role)
{
    if( index.isValid() && ( role == Qt::EditRole ) )
    {
      if(  index.column()== 1)
          {
             auto line =lineFromIndex(index);
             string newval =  QVariant(value).toString().toStdString();

             if( line->isStruct() || line->isArray() ) // ?? could be json string
                beginResetModel();
             line->setValue(newval);
             if( line->isStruct() || line->isArray() )
                endResetModel();
          }
     return true;
    }
    return false;
}

string TJsonDomModel::getFieldPath( const QModelIndex& index ) const
{
    auto line =  lineFromIndex(index);
    return line->getFieldPath();
}

string TJsonDomModel::getFieldData( const QModelIndex& index ) const
{
    auto line =  lineFromIndex(index);
    return line->toString(); // json data for object and array
}

void  TJsonDomModel::setFieldData( const QModelIndex& index, const string& data)
{
    auto line =  lineFromIndex(index);
    try {
          beginResetModel();
          line->clearField();
          line->setValue(data); // could be json data
          endResetModel();
    }
    catch( ... )
      {
         endResetModel();
         throw;
       }
 }

QString TJsonDomModel::getDescription( const QModelIndex& index ) const
{
    auto item = lineFromIndex( index );
    return item->getDescription().c_str();
}

string TJsonDomModel::helpName( const QModelIndex& index ) const
{
    auto item = lineFromIndex( index );
    return item->getHelpName();
}

//*****************************************************************

void TJsonDomModel::resizeArray( QWidget* parentWidget, const QModelIndex& index )
{
    auto line =  lineFromIndex(index);
    auto oldsize =  line->getChildrenCount();

    ModelLineDialog dlg( this, index, index, line->getKey(), parentWidget);
    if( !dlg.exec() )
       return;
    string new_value = dlg.getDefValue();
    auto new_sizes = dlg.getArraySizes();

    // resizeline item
    if( new_sizes[0] == oldsize )
        return;

    // delete if smaler
    if( new_sizes[0] < oldsize )
    {
     beginRemoveRows( index, new_sizes[0], oldsize-new_sizes[0]);
     line->resizeArray( new_sizes, new_value );
     endRemoveRows();
     return;
    }

    // add new elements
    beginInsertRows( index, oldsize, new_sizes[0]-1);
    line->resizeArray( new_sizes, new_value );
    endInsertRows();
}

void TJsonDomModel::delObject( QWidget* parentWidget, const QModelIndex& index )
{
    auto line =  lineFromIndex(index);
    QString msg = QString("Confirm deletion of %1 data object?").arg( line->getKey().c_str() );
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question( parentWidget->window(), "Data object to delete", msg,
             QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::No)
       return;

    beginRemoveRows( parent(index), index.row(), index.row());
    line->removeField();
    endRemoveRows();
}

void TJsonDomModel::delObjects( QWidget * parentWidget, const QModelIndex& cindex )
{
    //uint row;
    QModelIndex parentIndex= parent(cindex);
    auto parent_ = lineFromIndex(parentIndex);

    // select fields to be added
    std::vector<std::string> sch_lst = parent_->getUsedKeys();
    if( sch_lst.empty() )
      return;

    SelectDialog selDlg( true, parentWidget, "Please, select objects to be deleted", sch_lst );
    if( !selDlg.exec() )
      return;

    std::vector<std::size_t> sel;
    selDlg.getSelection( sel );

    // delete from list
    try {
         for(std::size_t ii=0; ii<sel.size(); ii++ )
         {
              auto child = parent_->field( sch_lst[sel[ii]] );
              int rowa = child->getNdx();
              beginRemoveRows( parentIndex, rowa, rowa);
              child->removeField();
              endRemoveRows();
         }
     } catch( ... )
       {
        throw;
       }
}

const QModelIndex TJsonDomModel::addObjects( QWidget * parentWidget, const QModelIndex& cindex )
{
    return addObject( parentWidget, cindex );
}

// add object to and of object
const QModelIndex TJsonDomModel::addObject( QWidget* parentWidget, const QModelIndex& cindex )
{
    int row;
    QModelIndex parentIndex;
    auto line =  lineFromIndex(cindex);
    if(line->isStruct() && line->getChildrenCount() < 1 )
    {
        parentIndex = cindex.siblingAtColumn(0);
        row = 0;
    }
    else
    {
        parentIndex = parent(cindex).siblingAtColumn(0);
        row =  rowCount( parentIndex ); //line->parent->children.size();
    }

    ModelLineDialog dlg( this, cindex, parentIndex, "", parentWidget);
    if( !dlg.exec() )
       return cindex;
    string new_object = dlg.getName();
    int new_type = dlg.getType();
    string new_value = dlg.getDefValue();

    try{
         // start insert row
        auto parent =  lineFromIndex(parentIndex);
        beginInsertRows( parentIndex, row, row );
        parent->appendNode(new_object, new_type, new_value);
        endInsertRows();
    } catch( ... )
      {
       endInsertRows();
       throw;
      }
    return index( row, 0, parentIndex);
}

// add object to and of object
const QModelIndex TJsonDomModel::cloneObject( QWidget*, const QModelIndex& cindex )
{
    QModelIndex parentIndex = parent(cindex);
    auto line =  lineFromIndex(cindex);

    try{
        auto parent =  lineFromIndex(parentIndex);
        if( !parent->isArray() )
          return cindex;
        int row =  rowCount( parentIndex );
        beginInsertRows( parentIndex, row, row );
        auto newitem = parent->appendNode( std::to_string(row), line->getType(),  "" );
        newitem->setValue( line->toString() );
        endInsertRows();
        return index( row, 0, parentIndex);

    } catch( ... )
      {
       endInsertRows();
       throw;
      }
   //return cindex;
}


//-------------------------------------------------------------------------------------
// TJsonDomDelegate -  individuak items in views are rendered and edited using delegates
//-------------------------------------------------------------------------------------

TJsonDomDelegate::TJsonDomDelegate( QObject * parent ):
    QItemDelegate( parent )
{ }

// Editing QTreeView for objects in System page
QWidget *TJsonDomDelegate::createEditor(QWidget *parent,
        const QStyleOptionViewItem &option,
        const QModelIndex &index) const
{
    auto line =  (dynamic_cast<const TJsonDomModel *>(index.model() ))->lineFromIndex(index);
    if( index.column() > 0 )
    {
        switch( line->getType()  )
        {
        case JSON_BOOL:
                { QComboBox *accessComboBox = new QComboBox(parent);
                  accessComboBox->addItem(tr("false"));
                  accessComboBox->addItem(tr("true"));
                  return accessComboBox;
                 }
        case JSON_INT:
        case JSON_DOUBLE:
               { QLineEdit *lineEdit = new QLineEdit(parent);
                 lineEdit->setValidator(new QDoubleValidator(lineEdit));
                 //lineEdit->setValidator(new QDoubleValidator(-999.0,
                 //            999.0, 2, lineEdit));
                 return lineEdit;
               }
        case JSON_NULL:
        case JSON_STRING:
              { QLineEdit *lineEdit = new QLineEdit(parent);
                return lineEdit;
              }
        // main constructions
       case JSON_OBJECT:
       case JSON_ARRAY:
       default:
             jsonioErr("TBsonDelegate" ,"can't print type " );

       }
    }
   return QAbstractItemDelegate::createEditor( parent, option,  index );
}


} // namespace jsonui

//---------------------- End of file  model_node.cpp ---------------------------
