//  This is JSONUI library+API (https://bitbucket.org/gems4/jsonui)
//
/// \file VertexWidget.cpp
/// Implementation of class VertexWidget - Widget to work vertex
/// internal DOM based on our JSON schemas data
//
// JSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2018 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONUI depends on the following open-source software products:
// JSONIO (https://bitbucket.org/gems4/jsonio); Qt5 (https://qt.io).
//

#include <iostream>
#include <QMessageBox>
#include <QKeyEvent>
#include <QHeaderView>
#include "VertexWidget.h"
#include "ui_VertexWidget.h"
#include "waitingspinnerwidget.h"

namespace jsonui {

void VertexWidget::closeEvent(QCloseEvent* e)
{
    if( dbKeysModel )
        dbKeysModel->Close();

    if( !onCloseEvent(this) )
        e->ignore();
    else
        QWidget::closeEvent(e);
}

bool VertexWidget::event(QEvent * e) // overloading event(QEvent*) method of QMainWindow
{
    switch(e->type())
    {
        case QEvent::WindowActivate :
            changeKeyList();
            break ;
        default: break;
    }
    return QMainWindow::event(e) ;
}

VertexWidget::VertexWidget( const std::string& aschemaName, QWidget *parent) :
    JSONUIBase( aschemaName, parent), ui(new Ui::VertexWidget),
    dbKeysModel(nullptr), keysTable(nullptr), curRecord_("")
{

    ui->setupUi(this);
    ui->keySplitter->setStretchFactor(0, 2);
    ui->keySplitter->setStretchFactor(1, 1);
    ui->mainSplitter->setStretchFactor(0, 1);
    ui->mainSplitter->setStretchFactor(1, 4);

    setAttribute(Qt::WA_DeleteOnClose); // automatically delete itself when window is closed
    QString title = qApp->applicationName()+" Structured Data Editor and Database Browser";
    setWindowTitle(title);

    // define edit tree view
    QStringList aHeaderData;
    aHeaderData << "key" << "value" << "comment" ;
    fieldTable =  new TJsonView(  ui->bsonWidget );
    model_schema = new TJsonSchemaModel(  curRecord_, curSchemaName, aHeaderData, this/*ui->centralWidget*/ );
    deleg_schema = new TJsonSchemaDelegate();
    fieldTable->setModel(model_schema);
    fieldTable->setItemDelegate(deleg_schema);
    fieldTable->setColumnWidth( 0, 250 );
    fieldTable->expandToDepth(0);
    ui->verticalLayout->addWidget(fieldTable);
    CmNew(); // set up default data

    // define key table
    keysTable = new TKeyTable( ui->keylistWidget, [&]( const QModelIndex& index ){openRecordKey( index );});
    ui->horizontalLayout->addWidget(keysTable);
    connect( keysTable, SIGNAL( clicked(const QModelIndex& ) ), this, SLOT(openRecordKey( const QModelIndex& )));

    // define menu
    setActions();
    waitDialog = new WaitingSpinnerWidget(this, true/*Qt::ApplicationModal*/, true);

    // Open db connection and load keys table
    resetDBClient( curSchemaName );
}

VertexWidget::~VertexWidget()
{
    dbThread.quit();
    dbThread.wait();

    if( dbKeysModel )
        delete dbKeysModel;
    if( keysTable )
        delete keysTable;
    if( waitDialog )
        delete waitDialog;

    delete ui;
    //cout << "~VertexWidget" << endl;
}

//-----------------------------------------------------

bool VertexWidget::resetBson( const std::string& curRecord, const std::string& schemaName )
{
    // test legal schema name
    if( !schemaName.empty() && schemaName != curSchemaName )
        return false;
    fieldTable->updateModelData( curRecord, curSchemaName );
    fieldTable->update();
    return true;
}

void VertexWidget::openRecordKey(  const QModelIndex& rowindex    )
{
    // find key
    int row = rowindex.row();
    QModelIndex index = rowindex.sibling( row, 0);
    std::string key = keysTable->model()->data(index).toString().toStdString();
    openRecordKey(  key  );
}

void VertexWidget::openRecordKey(  const std::string& reckey, bool  )
{
    emit cmReadDocument( reckey.c_str() );
}

/// Change current View menu selections
void VertexWidget::updtViewMenu()
{
    ui->action_Show_comments->setChecked( TJsonSchemaModel::showComments );
    ui->action_Display_enums->setChecked(TJsonSchemaModel::useEnumNames);
    ui->action_Edit_id->setChecked(TJsonSchemaModel::editID );
    ui->actionKeep_Data_Fields_Expanded->setChecked(TJsonView::expandedFields );
}

/// Change model ( show/hide comments)
void VertexWidget::updtModel()
{
    model_schema->updateModelData();
    fieldTable->header()->resizeSection(0, 250);
    fieldTable->header()->resizeSection(1, 250);
    fieldTable->expandToDepth(0);
}

/// Change table (Show Enum Names Instead of Values)
void VertexWidget::updtTable()
{
    fieldTable->hide();
    fieldTable->show();
}

/// Update after change DB locations
void VertexWidget::updtDB()
{
    ///dbgraph->runQuery();
    std::string key = getKeyFromDom( model_schema->getDom() );
    dbKeysModel->resetModel(key.c_str());
    CmNew();
}

// new stuff ----------------------------------------------------

// Get current record key from bson structure
std::string VertexWidget::getKeyFromDom( const jsonio::JsonDom* domdata ) const
{
    std::string keyStr = "";
    if( !domdata->findKey( "_id", keyStr ) )
        keyStr = "";
    return keyStr;
}


// Exception when database command execution
void VertexWidget::get_exception( const QString& titl, const QString& msg )
{
    QMessageBox::critical( this, titl, msg );
}

// After open new document
void VertexWidget::openDocument(  QString jsonDocument  )
{
    try{
        curRecord_ = jsonDocument.toStdString();
        resetBson(curRecord_, "" );
        std::string key = getKeyFromDom( model_schema->getDom() );
        dbKeysModel->movetoKey( key );
        contentsChanged = false;
    }
    catch( jsonio::jsonio_exception& e)
    {
        QMessageBox::critical( this, e.title(), e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Update db document query
void VertexWidget::changeDBClientData(  QString query )
{
      ui->queryEdit->setText( query );
}

void VertexWidget::resetDBClient(const std::string& schemaName )
{

    qRegisterMetaType<QVector<QString> >("QVector<QString>");
    qRegisterMetaType<std::vector<std::string> >("std::vector<std::string>");
    qRegisterMetaType<std::shared_ptr<jsonio::DBQueryDef> >("std::shared_ptr<jsonio::DBQueryDef>");
#ifndef IMPEX_OFF
    qRegisterMetaType<std::shared_ptr<jsonio::ImpexFormatFile> >("std::shared_ptr<jsonio::ImpexFormatFile>");
#endif

  try{
      dbKeysModel = new DBKeysListModel( nullptr, keysTable, this );
      dbDocumentModel = new DBDocumentModel(  schemaName, false  );


      connect( dbKeysModel, &DBKeysListModel::updateQuery, dbDocumentModel, &DBDocumentModel::dbChangeQuery );
      connect( dbDocumentModel, &DBDocumentModel::changedDBClient,  dbKeysModel, &DBKeysListModel::changeDBClient );
      connect( dbDocumentModel, &DBDocumentModel::changedModel,  dbKeysModel, &DBKeysListModel::resetModel );

      // link from GUI

      connect( this, &VertexWidget::cmResetClient, dbDocumentModel, &DBDocumentModel::dbResetClient );
      //connect( this, &VertexWidget::cmChangeQuery, dbDocumentModel, &DBDocumentModel::dbChangeQuery );
      connect( this, &VertexWidget::cmReadDocument, dbDocumentModel, &DBDocumentModel::dbReadDocument );
      connect( this, &VertexWidget::cmUpdate, dbDocumentModel, &DBDocumentModel::dbUpdate );
      connect( this, &VertexWidget::cmDelete, dbDocumentModel, &DBDocumentModel::dbDelete );
      connect( this, &VertexWidget::cmDeleteList, dbDocumentModel, &DBDocumentModel::dbDeleteList );
      connect( this, &VertexWidget::cmRestoreMultipleRecordsfromFile, dbDocumentModel, &DBDocumentModel::dbRestoreMultipleRecordsfromFile );
      connect( this, &VertexWidget::cmBackupQueriedRecordstoFile, dbDocumentModel, &DBDocumentModel::dbBackupQueriedRecordstoFile );
#ifndef IMPEX_OFF
      connect( this, &VertexWidget::cmImportFormat, dbDocumentModel, &DBDocumentModel::dbImportFormat );
      connect( this, &VertexWidget::cmExportFormat, dbDocumentModel, &DBDocumentModel::dbExportFormat );
#endif
      connect( this, &VertexWidget::cmBackupGraphtoFile, dbDocumentModel, &DBDocumentModel::dbBackupGraphtoFile );
      connect( this, &VertexWidget::cmRestoreGraphfromFile, dbDocumentModel, &DBDocumentModel::dbRestoreGraphfromFile );
      connect( this, &VertexWidget::cmReloadQuery, dbDocumentModel, &DBDocumentModel::dbReloadQuery );

      // link to GUI

      connect( dbDocumentModel, &DBDocumentModel::changedQuery, this, &VertexWidget::changeDBClientData );
      connect( dbDocumentModel, &DBDocumentModel::finished, this, &VertexWidget::finishProcess );
      connect( dbDocumentModel, &DBDocumentModel::isException, this, &VertexWidget::get_exception );
      connect( dbDocumentModel, &DBDocumentModel::updatedOid, this, &VertexWidget::updateOid );

      connect( dbDocumentModel, &DBDocumentModel::openedDocument, this, &VertexWidget::openDocument );
      connect( dbDocumentModel, &DBDocumentModel::deletedDocument, this, &VertexWidget::afterDeleteDocument );
      connect( dbDocumentModel, &DBDocumentModel::loadedGraph, this, &VertexWidget::afterLoadGraph );
      /*connect( dbDocumentModel, &DBDocumentModel::changedDBClient,
               [&](jsonio::TDBVertexDocument* newClient)
                     {  openRecordKey( newClient->lastQueryData()->getFirstKeyFromList().c_str() );
                     } );*/

     // thread functions

      dbDocumentModel->moveToThread(&dbThread);
      connect(&dbThread, &QThread::finished, dbDocumentModel, &QObject::deleteLater);
      dbThread.start();

      // might be execute signals it other thread

      startProcess();

      emit cmResetClient( schemaName.c_str() );

      /*if(  keysTable->model()->rowCount() > 1 )
          openRecordKey( keysTable->model()->index(0,0) ); // read first line */

   }
   catch(std::exception& e)
      {
         std::cout << "Internal comment: " << e.what() << std::endl;
         throw;
      }
}

/// Start long database process
void VertexWidget::startProcess()
{
    // possible open Waiting Spinner
    std::cout << "startProcess "  << std::endl;
    waitDialog->start();
}

/// Finish long database process
void VertexWidget::finishProcess()
{
    // possible close Waiting Spinner
    waitDialog->stop();
    std::cout << "finishProcess "  << std::endl;
}


} // namespace jsonui
