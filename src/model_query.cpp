//  This is JSONUI library+API (https://bitbucket.org/gems4/jsonui)
//
/// \file model_query.cpp
/// Implementation of TQueryModel, TQueryDelegate and TQueryView
/// implements a tree view of query structure
//
// JSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONUI depends on the following open-source software products:
// JSONIO (https://bitbucket.org/gems4/jsonio); Qt5 (https://qt.io).
//

#include <iostream>
#include <QKeyEvent>
#include <QComboBox>
#include <QMenu>
#include <QAction>
#include <QHeaderView>
#include <QInputDialog>
#include <QMessageBox>
#include "QueryWidget.h"
#include "preferences.h"
#include "model_query.h"
#include "model_query.h"
#include "model_query.h"

namespace jsonui {

//--------------------------------------------------------------------------------------
//  class TQueryModel
//  class for represents the data set and is responsible for fetchin
//  the data is neaded for viewing and for writing back any changes.
//---------------------------------------------------------------------------------------

TQueryModel::TQueryModel( QObject* parent ):
    QAbstractItemModel(parent), rootNode(nullptr)
{
    // define edit tree view
    hdData << "key" << "value"  ;
    rootNode.reset( new AQLline(0, "root", AQLline::Top,nullptr ));
    connect( this, SIGNAL(dataChanged( const QModelIndex&, const QModelIndex& )),
             parent,  SLOT(objectChanged()) );
}

void TQueryModel::clearModelData()
{
    beginResetModel();
    rootNode.reset( new AQLline(0, "root", AQLline::Top, nullptr ) );
    endResetModel();
}


AQLline *TQueryModel::lineFromIndex(const QModelIndex &index) const
{
    if (index.isValid()) {
        return static_cast<AQLline *>(index.internalPointer());
    } else {
        return rootNode.get();
    }
}


QModelIndex TQueryModel::index(int row, int column, const QModelIndex &parent) const
{
    if (!rootNode)
        return QModelIndex();
    AQLline *parentItem = lineFromIndex( parent );
    if(parentItem->children.size() > 0 )
        return createIndex(row, column, parentItem->children[row].get());
    else
        return QModelIndex();
}

QModelIndex TQueryModel::parent(const QModelIndex& child) const
{
    if (!child.isValid())
        return QModelIndex();

    AQLline *childItem = lineFromIndex(child);
    AQLline *parentItem = childItem->parent;
    if (parentItem == rootNode.get() )
        return QModelIndex();
    return createIndex(parentItem->ndx, 0, parentItem);
}

int TQueryModel::rowCount( const QModelIndex& parent ) const
{
    if (!rootNode)
        return 0;
    AQLline *parentItem = lineFromIndex( parent );
    return static_cast<int>(parentItem->children.size());
}	

int TQueryModel::columnCount( const QModelIndex& /*parent*/ ) const
{
    return 2;
}	

Qt::ItemFlags TQueryModel::flags( const QModelIndex& index ) const
{
    Qt::ItemFlags flags = QAbstractItemModel::flags(index);
    AQLline *item = lineFromIndex( index );
    if( index.column() == 1 &&  item && item->type == AQLline::Field  )
    {
        flags |= Qt::ItemIsEditable;
        return flags;
    }
    else
        return (flags & ~Qt::ItemIsEditable);
}

QVariant TQueryModel::headerData( int section, Qt::Orientation orientation, int role ) const
{
    if( role == Qt::DisplayRole  && orientation == Qt::Horizontal )
        return hdData[section];
    return QVariant();
}

QVariant TQueryModel::data( const QModelIndex& index, int role ) const
{
    if(!index.isValid())
        return QVariant();

    switch( role )
    {
    case Qt::DisplayRole:
    case Qt::EditRole:
    {
        AQLline *item = lineFromIndex( index );
        if( index.column()== 0 )
            return item->keyname.c_str();
        else
            if( index.column()== 1 )
                return item->value.c_str();
    }
        break;
    case Qt::ForegroundRole:
        if ( index.column() == 0 )
        {
            return QVariant( QColor( Qt::darkCyan ) );
        }
        break;
    default: break;
    }
    return QVariant();
}

bool TQueryModel::setData( const QModelIndex& index, const QVariant& value_, int role)
{
    if( index.isValid() && ( role == Qt::EditRole ) )
    {
        if(  index.column()== 1)
        {
            AQLline *line =lineFromIndex(index);
            if( line )
                line->value = value_.toString().toStdString();
        }
        return true;
    }
    return false;
}

bool TQueryModel::canLogical(const QModelIndex &index, bool AQL_mode ) const
{
    if( !AQL_mode )
        return false;
    auto item = lineFromIndex( index );
    return( item && ( ( item->children.empty() && item->type==AQLline::Top)   ||
                      item->type==AQLline::LogicalTwo ||
                      (item->children.empty() && item->type==AQLline::LogicalNot) ));
}

bool TQueryModel::canOperator(const QModelIndex &index, bool AQL_mode ) const
{
    if( !AQL_mode )
        return false;
    auto item = lineFromIndex( index );
    return( item && ( ( item->children.empty() && item->type==AQLline::Top)
                      || item->type==AQLline::LogicalTwo
                      || ( item->children.empty() && item->type==AQLline::LogicalNot ) ));
}

bool TQueryModel::canField(const QModelIndex &index, bool AQL_mode) const
{
    if( !AQL_mode )
        return true;
    auto item = lineFromIndex( index );
    return( item && item->children.empty() && item->type==AQLline::Operator );
}


const QModelIndex TQueryModel::addField( const std::string& flds_name,
                                         jsonio::ThriftFieldDef* fld_data, const QModelIndex& parentIndex )
{
    AQLline *line =  lineFromIndex(parentIndex);
    if(!line)
        return parentIndex;

    int row = static_cast<int>(line->children.size());
    beginInsertRows( parentIndex, row, row );
    AQLline* newline = new AQLline( line->children.size(), flds_name, AQLline::Field, line );
    newline->field_data.thrifttype = fld_data->fTypeId.front();
    newline->field_data.enumname = fld_data->className;
    endInsertRows();
    return index( row, 0 , parentIndex);
}

const QModelIndex TQueryModel::addObject( const std::string& akey, bool isoperator, const QModelIndex& parentIndex )
{
    AQLline *line =  lineFromIndex(parentIndex);
    if(!line)
        return parentIndex;

    int row = static_cast<int>(line->children.size());
    beginInsertRows( parentIndex, row, row );
    auto type = ( isoperator ? AQLline::Operator : ( akey=="NOT" ? AQLline::LogicalNot: AQLline::LogicalTwo ) );
    new AQLline(line->children.size(), akey, type, line );
    endInsertRows();
    return index( row, 0 , parentIndex);
}

void TQueryModel::delObject( const QModelIndex& index )
{
    AQLline *line =  lineFromIndex(index);
    if(!line)
        return;

    const QModelIndex parentNdx = parent(index);
    AQLline *parent_ = line->parent;
    beginRemoveRows( parentNdx, index.row(), index.row());
    parent_->children.erase( parent_->children.begin() + index.row() );
    endRemoveRows();
}

jsonio::DBQueryData TQueryModel::getQuery( int queryType, const std::string& collection )
{
    std::string query_string;

    if( rootNode->children.size()<1 )
        return jsonio::emptyQuery;

    if( queryType == jsonio::DBQueryData::qTemplate )
        query_string = rootNode->toTemplate();
    else if( queryType == jsonio::DBQueryData::qAQL )
        query_string = rootNode->toAQL(collection);
    else  queryType = jsonio::DBQueryData::qUndef;

    return jsonio::DBQueryData(query_string, queryType);
}

std::string TQueryModel::getFILTER()
{
    std::shared_ptr<jsonio::JsonDomFree> domdata(jsonio::JsonDomFree::newObject());
    rootNode->toDom( domdata.get() );
    return domdata->toString(true);
}

void TQueryModel::setFILTER(const std::string &filter_generator)
{
    std::shared_ptr<jsonio::JsonDomFree> domdata(jsonio::JsonDomFree::newObject());
    if( !filter_generator.empty() && parseJsonToNode( filter_generator, domdata.get() ) )
    {
        rootNode->fromDom( domdata.get() );
    }
}


//-------------------------------------------------------------------------------------
// TQueryDelegate -  individual items in views are rendered and edited using delegates
//-------------------------------------------------------------------------------------

TQueryDelegate::TQueryDelegate(  QObject * parent ):
    QItemDelegate( parent )
{ }

// Editing QTreeView for objects in System page
QWidget *TQueryDelegate::createEditor(QWidget *parent,
                                      const QStyleOptionViewItem &option,
                                      const QModelIndex &index) const
{
    AQLline *line =  (dynamic_cast< const TQueryModel *>(index.model() ))->lineFromIndex(index);
    if( index.column() > 0 )
    {
        switch( line->valueType() )
        {
        case jsonio::JSON_BOOL:
        {
            QComboBox *accessComboBox = new QComboBox(parent);
            accessComboBox->addItem(tr("false"), tr("false"));
            accessComboBox->addItem(tr("true"), tr("true"));
            return accessComboBox;
        }
        case jsonio::JSON_INT:
        {
            std::string enumName = line->field_data.enumname;
            if( !enumName.empty() )
            {
                jsonio::ThriftEnumDef* enumdef = jsonio::ioSettings().Schema()->getEnum( enumName );
                if(enumdef != nullptr )
                {
                    auto lst = enumdef->getNamesList();
                    QComboBox *accessComboBox = new QComboBox(parent);
                    for( size_t ii=0; ii<lst.size(); ii++ )
                    {
                        std::string itname =lst[ii];
                        accessComboBox->addItem(tr(itname.c_str()), enumdef->getId(itname) );
                        accessComboBox->setItemData(ii, enumdef->getDoc(itname).c_str(), Qt::ToolTipRole);
                    }
                    return accessComboBox;
                }
            }
            QLineEdit *lineEdit = new QLineEdit(parent);
            QIntValidator *ivalid = new QIntValidator(lineEdit);
            if( line->field_data.thrifttype == jsonio::Th_U64 )
                ivalid->setBottom(0);
            lineEdit->setValidator(ivalid);
            return lineEdit;
        }
        case jsonio::JSON_DOUBLE:
        {
            QLineEdit *lineEdit = new QLineEdit(parent);
            QDoubleValidator *dvalid = new QDoubleValidator(lineEdit);
            lineEdit->setValidator(dvalid);
            return lineEdit;
        }
        case jsonio::JSON_STRING:
        default:
        {
            QLineEdit *lineEdit = new QLineEdit(parent);
            return lineEdit;
        }
        }
    }
    return QAbstractItemDelegate::createEditor( parent, option,  index );
}


//-------------------------------------------------------------------------------------
// class TQueryView implements a tree view that displays items from a model to query.
//-------------------------------------------------------------------------------------

TQueryView::TQueryView( QWidget * parent ):
    QTreeView( parent )
{
    setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    header()->setSectionsClickable(true);
    header()->setSectionResizeMode( /*QHeaderView::ResizeToContents*/QHeaderView::Interactive );

    setEditTriggers( QAbstractItemView::DoubleClicked|QAbstractItemView::AnyKeyPressed );
    setFocusPolicy(Qt::StrongFocus);
    setTabKeyNavigation( false );
    setContextMenuPolicy(Qt::CustomContextMenu);
    setSelectionMode( QAbstractItemView::SingleSelection );
    setSelectionBehavior( QAbstractItemView::SelectRows );
    setIndentation( 30 );

    connect( this, SIGNAL(customContextMenuRequested(QPoint)),
             this, SLOT(slotPopupContextMenu(QPoint)));
    connect( header(), SIGNAL(sectionClicked(int)),
             this, SLOT(changeCurrent(int)));
}

void TQueryView::changeCurrent( int section )
{
    if( !currentIndex().isValid() )
    {
        QModelIndex index = model()->index( 0, section, rootIndex());
        setCurrentIndex(index);
    }
}

void TQueryView::slotPopupContextMenu(const QPoint &pos)
{
    QModelIndex index = indexAt( pos );
    QMenu *menu = contextMenu( index);
    if( menu)
    {
        menu->exec( viewport()->mapToGlobal(pos) );
        delete menu;
    }
}

QMenu* TQueryView::contextMenu(const QModelIndex &index)
{
    const TQueryModel* model_ =dynamic_cast<const TQueryModel*>(model());

    if( !model_ )
        return nullptr;

    QMenu *menu = new QMenu(this);
    QAction* act;

    if(  model_->canField(index, AQL_mode) )
    {
        act =  new QAction(tr("Add &Fieldpath"), this);
        act->setShortcut(tr("Ctr+F"));
        act->setStatusTip(tr("Add the Next Fieldpath"));
        connect(act, SIGNAL(triggered()), this, SLOT(CmAddField()));
        menu->addAction(act);
        menu->addSeparator();
    }

    if(  model_->canLogical(index, AQL_mode) )
    {
        act =  new QAction(tr("Add '&AND'"), this);
        act->setShortcut(tr("Ctr+A"));
        act->setStatusTip(tr("Add '&&' joined condition"));
        connect(act, SIGNAL(triggered()), this, SLOT(CmAddAnd()));
        menu->addAction(act);

        act =  new QAction(tr("Add '&OR'"), this);
        act->setShortcut(tr("Ctr+O"));
        act->setStatusTip(tr("Add '||' joined condition"));
        connect(act, SIGNAL(triggered()), this, SLOT(CmAddOr()));
        menu->addAction(act);

        act =  new QAction(tr("Add '&NOT'"), this);
        act->setShortcut(tr("Ctr+N"));
        act->setStatusTip(tr("Add '!' condition"));
        connect(act, SIGNAL(triggered()), this, SLOT(CmAddNot()));
        menu->addAction(act);

        menu->addSeparator();
    }

    if(  model_->canOperator(index, AQL_mode) )
    {
        act =  new QAction(tr("Add '==' operator"), this);
        act->setShortcut(tr("="));
        act->setStatusTip(tr("Value equality comparison operator"));
        connect(act, SIGNAL(triggered()), this, SLOT(CmAddEq()));
        menu->addAction(act);

        act =  new QAction(tr("Add '!=' operator"), this);
        act->setShortcut(tr("!"));
        act->setStatusTip(tr("Value inequality comparison operator"));
        connect(act, SIGNAL(triggered()), this, SLOT(CmAddNotEq()));
        menu->addAction(act);

        act =  new QAction(tr("Add '>' operator"), this);
        act->setShortcut(tr(">"));
        act->setStatusTip(tr("Value greater than"));
        connect(act, SIGNAL(triggered()), this, SLOT(CmAddGt()));
        menu->addAction(act);

        act =  new QAction(tr("Add '>=' operator"), this);
        act->setShortcut(tr("Ctr+G"));
        act->setStatusTip(tr("Value greater than or equal to"));
        connect(act, SIGNAL(triggered()), this, SLOT(CmAddGte()));
        menu->addAction(act);

        act =  new QAction(tr("Add '<' operator"), this);
        act->setShortcut(tr("<"));
        act->setStatusTip(tr("Value lesser than "));
        connect(act, SIGNAL(triggered()), this, SLOT(CmAddLt()));
        menu->addAction(act);

        act =  new QAction(tr("Add '<=' operator"), this);
        act->setShortcut(tr("Ctr+E"));
        act->setStatusTip(tr(" Value lesser than or equal to"));
        connect(act, SIGNAL(triggered()), this, SLOT(CmAddLte()));
        menu->addAction(act);

        menu->addSeparator();

        act =  new QAction(tr("Add 'IN'"), this);
        act->setShortcut(tr("Ctr+I"));
        act->setStatusTip(tr("Test if a value is contained in an array"));
        connect(act, SIGNAL(triggered()), this, SLOT(CmAddIn()));
        menu->addAction(act);

        act =  new QAction(tr("Add 'NOT IN'"), this);
        act->setStatusTip(tr("Test if a value is not contained in an array"));
        connect(act, SIGNAL(triggered()), this, SLOT(CmAddNin()));
        menu->addAction(act);

        act =  new QAction(tr("Add '&LIKE'"), this);
        act->setShortcut(tr("Ctr+L"));
        act->setStatusTip(tr("Tests if a string value matches a pattern"));
        connect(act, SIGNAL(triggered()), this, SLOT(CmAddLike()));
        menu->addAction(act);

        act =  new QAction(tr("Add '=~'"), this);
        act->setShortcut(tr("Ctr+R"));
        act->setStatusTip(tr("Tests if a string value matches a regular expression"));
        connect(act, SIGNAL(triggered()), this, SLOT(CmAddRegexp()));
        menu->addAction(act);

        menu->addSeparator();
        act =  new QAction(tr("Add '!~'"), this);
        act->setStatusTip(tr("Tests if a string value does not match a regular expression"));
        connect(act, SIGNAL(triggered()), this, SLOT(CmAddNotRegexp()));
        menu->addAction(act);

        menu->addSeparator();
    }

    act =  new QAction(tr("Remove current line"), this);
    act->setShortcut(tr("F8"));
    act->setStatusTip(tr("Delete current line from object"));
    connect(act, SIGNAL(triggered()), this, SLOT(CmDelLine()));
    menu->addAction(act);

    return menu;
}

void TQueryView::keyPressEvent(QKeyEvent* e)
{
    if ( e->modifiers() & Qt::ControlModifier )
    {
        switch ( e->key() )
        {
        case Qt::Key_F:
            CmAddField();
            return;

        case Qt::Key_A:
            CmAddAnd();
            return;
        case Qt::Key_O:
            CmAddOr();
            return;
        case Qt::Key_N:
            CmAddNot();
            return;

        case Qt::Key_Equal:
            CmAddEq();
            return;
        case Qt::Key_Exclam:
            CmAddNotEq();
            return;
        case Qt::Key_Greater:
            CmAddGt();
            return;
        case Qt::Key_G:
            CmAddGte();
            return;
        case Qt::Key_Less:
            CmAddLt();
            return;
        case Qt::Key_E:
            CmAddLte();
            return;

        case Qt::Key_I:
            CmAddIn();
            return;
        case Qt::Key_L:
            CmAddLike();
            return;
        case Qt::Key_R:
            CmAddRegexp();
            return;
        }
    }

    switch( e->key() )
    {
    case Qt::Key_F8:
        CmDelLine();
        return;
    }
    QTreeView::keyPressEvent(e);
}

void TQueryView::addObject( const std::string& akey, bool isoperator )
{
    TQueryModel* qmodel = dynamic_cast<TQueryModel*>(model());

    try {
        QModelIndex index = currentIndex();
        if( !index.isValid())
            index = rootIndex();
        if( qmodel && ( ( !isoperator  && qmodel->canLogical(index, AQL_mode) ) ||
                        ( isoperator  && qmodel->canOperator(index, AQL_mode) ) ))
        {
            QModelIndex ndx = qmodel->addObject( akey, isoperator, index);
            selectionModel()->setCurrentIndex( ndx, QItemSelectionModel::ClearAndSelect );
        }
        else
            QMessageBox::information( window(), "Add to FILTER",
                                      "Could not execute action." );

    }
    catch(jsonio::jsonio_exception& e)
    {
        QMessageBox::critical( window(), e.title(), e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( window(), "std::exception", e.what() );
    }
}

void TQueryView::CmAddField()
{
    TQueryModel* qmodel = dynamic_cast<TQueryModel*>(model());

    try {
        QModelIndex index = currentIndex();
        if( !index.isValid() || !AQL_mode )
            index = rootIndex();
        if( qmodel && qmodel->canField(index, AQL_mode) )
        {
            QueryWidget* parentdlg = dynamic_cast<QueryWidget*>(window());
            std::string fldpath;
            jsonio::ThriftFieldDef* flddata=nullptr;
            parentdlg->selectLine(fldpath, &flddata);

            QModelIndex ndx = qmodel->addField( fldpath, flddata, index);
            selectionModel()->setCurrentIndex( ndx, QItemSelectionModel::ClearAndSelect );
        }
        else
            QMessageBox::information( window(), "Add to FILTER",
                                      "Could not execute action." );
    }
    catch(jsonio::jsonio_exception& e)
    {
        QMessageBox::critical( window(), e.title(), e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( window(), "std::exception", e.what() );
    }
}


void TQueryView::CmAddObjects( const std::vector<std::string>& fields_list,
                               const std::vector<jsonio::ThriftFieldDef*>& fields_types )
{
    TQueryModel* qmodel = dynamic_cast<TQueryModel*>(model());

    try {
        QModelIndex index = rootIndex();
        if( qmodel && qmodel->canField(index, AQL_mode) )
        {
            QModelIndex ndx = index;
            for(size_t ii=0; ii<fields_list.size(); ii++)
                ndx = qmodel->addField( fields_list[ii], fields_types[ii], index);
            selectionModel()->setCurrentIndex( ndx, QItemSelectionModel::ClearAndSelect );
        }
        else
            QMessageBox::information( window(), "Add to FILTER", "Could not execute action." );
    }
    catch(jsonio::jsonio_exception& e)
    {
        QMessageBox::critical( window(), e.title(), e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( window(), "std::exception", e.what() );
    }
}

void TQueryView::CmDelLine()
{
    QModelIndex index = currentIndex();
    auto qmodel = dynamic_cast<const TQueryModel*>(index.model());
    try {
        if(  qmodel )
        {
            selectionModel()->clear();
            collapse(index);
            const_cast<TQueryModel*>(qmodel)->delObject( index );
        } else
            QMessageBox::information( window(), "Delete from Object",
                                      "Please, determine the key-value pair to be deleted" );
    }
    catch(jsonio::jsonio_exception& e)
    {
        QMessageBox::critical( window(), e.title(), e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( window(), "std::exception", e.what() );
    }
}


//-----------------------------------------------------------------

AQLline::AQLline( size_t andx, const std::string& akey, AQLline::fieldType atype, AQLline* parentline ):
    ndx( static_cast<int>(andx)), type( atype ), keyname( akey), value( "" )
{
    //type = typeFromKey( keyname );
    if( !parentline )
        type = Top;
    parent = parentline;
    if(  parent )
    {
        parent->children.push_back( std::unique_ptr<AQLline>(this) );
        field_data = parent->field_data;
    }
}


AQLline::~AQLline()
{
    children.clear();
}

int AQLline::valueType()
{
    int bstype = jsonio::jsonTypeFromThrift( field_data.thrifttype );
    if( parent && parent->type == AQLline::Operator &&
            ( parent->keyname == "IN" || parent->keyname == "NOT IN" ) )
        bstype = jsonio::JSON_ARRAY;

    if( parent && parent->type == AQLline::Operator &&
            ( parent->keyname == "LIKE" || parent->keyname == "=~" || parent->keyname == "!~" ) )
        bstype = jsonio::JSON_STRING;

    return bstype;
}

void AQLline::value2dom(  jsonio::JsonDom* domobj )
{
    std::string fldkey = keyname;
    int bstype = valueType();

    switch (bstype)
    {
    // impotant datatypes
    case jsonio::JSON_NULL:
        domobj->appendNull( fldkey  );
        break;
    case jsonio::JSON_BOOL:
        domobj->appendBool( fldkey,  ( value == "true"? true : false));
        break;
    case jsonio::JSON_INT:
    {
        int val;
        getValue( val );
        if( !field_data.enumname.empty() )
        {
            jsonio::ThriftEnumDef* enumdef = jsonio::ioSettings().Schema()->getEnum( field_data.enumname );
            if( enumdef != nullptr )
                val = enumdef->getId( value );
        }
        domobj->appendInt( fldkey, val );
    }
        break;
    case jsonio::JSON_DOUBLE:
    {
        double val;
        getValue( val );
        domobj->appendDouble(  fldkey, val );
    }
        break;
    case jsonio::JSON_OBJECT:
    {
        auto obj = domobj->appendObject(fldkey);
        obj->setValue( value );
    }
        break;
    case jsonio::JSON_ARRAY:
    {
        auto arr = domobj->appendArray(fldkey);
        arr->setValue( value );
    }
        break;
    case jsonio::JSON_STRING:
        domobj->appendString( fldkey, value );
        break;
    default:
        break;
    }
}

void AQLline::query2dom( jsonio::JsonDom* domobj )
{
    switch (type)
    {
    // impotant datatypes
    case Top:
        domobj->clearField();
        for(size_t ii1=0; ii1< children.size(); ii1++ )
            children[ii1]->query2dom( domobj );
        break;
    case Field:
        if(!value.empty())
        {
            value2dom( domobj );
        }
        break;
    default: break;
    }
}

std::string AQLline::toTemplate()
{
    std::string jsonquery;
    std::shared_ptr<jsonio::JsonDomFree> domdata(jsonio::JsonDomFree::newObject());
    query2dom( domdata.get() );
    printNodeToJson( jsonquery, domdata.get() );
    return jsonquery;
}


std::string AQLline::value2AQL(  const std::string& the_operator  )
{
    int bstype = valueType();
    std::string filter( "u." + keyname + " " + the_operator + " ");

    switch (bstype)
    {
    case jsonio::JSON_STRING:
    {    auto str =value;
        jsonio::convertStringToWrite( value );
        filter += "'"+value+"'";
    }
        break;
    case jsonio::JSON_INT:
    {
        int val=0;
        if( !field_data.enumname.empty() )
        {
            jsonio::ThriftEnumDef* enumdef = jsonio::ioSettings().Schema()->getEnum(field_data.enumname );
            if( enumdef != nullptr )
                val = enumdef->getId( value );
            filter += std::to_string(val);
        }
        else
            filter += value;
    }
        break;
    default:
        filter += value;
        break;
    }

    filter += " ";
    return filter;
}


std::string AQLline::query2AQL()
{
    std::string filter;

    switch (type)
    {
    case Top:
        for(size_t ii=0; ii< children.size(); ii++ )
            filter += children[ii]->query2AQL();
        break;
    case LogicalTwo:
    {
        filter += "( ";
        for( size_t ii=0; ii< children.size(); ii++ )
        {
            if( ii > 0)
                filter += " "+ keyname +" ";
            filter += children[ii]->query2AQL();
        }
        filter += " )";
    }
        break;
    case LogicalNot:
        if( !children.empty())
        {
            filter += " "+ keyname + "  ";
            filter += children[0]->query2AQL();
            filter += " ";
        }
        break;
    case Operator:
        if( !children.empty() && children[0]->type == Field )
        {
            filter += " ";
            filter += children[0]->value2AQL( keyname );
            filter += " ";
        }
        break;
    case Field:
    {
        filter += value2AQL( "==" );
    }
        break;
    }
    return filter;
}

std::string AQLline::toAQL( const std::string& collection  )
{
    std::string aql_query("FOR u IN " );
    aql_query += collection;
    aql_query += "\nFILTER ";
    aql_query += query2AQL();
    aql_query += "\nRETURN u ";
    return aql_query;
}

void AQLline::toDom(jsonio::JsonDom *domobj)
{
    domobj->appendInt(  "type",  type);
    domobj->appendString(  "keyname",  keyname);
    domobj->appendString(  "value",  value );
    if( type == AQLline::Field )
    {
        domobj->appendInt(  "thrifttype",   field_data.thrifttype);
        domobj->appendString(  "enumname",  field_data.enumname);
    }
    auto arr = domobj->appendArray("children");
    for( size_t ii=0; ii< children.size(); ii++ )
    {
        auto obj = arr->appendObject( std::to_string(ii) );
        children[ii]->toDom(obj);
    }
}

void AQLline::fromDom(const jsonio::JsonDom *domobj)
{
    children.clear();
    int atype = Operator;
    domobj->findValue(  "type",  atype);
    domobj->findValue(  "keyname",  keyname);
    domobj->findValue(  "value",  value );
    type = static_cast<AQLline::fieldType>(atype);
    if( type == AQLline::Field )
    {
        domobj->findValue(  "thrifttype",  field_data.thrifttype);
        domobj->findValue(  "enumname",  field_data.enumname);
    }
    auto arr  = domobj->field( "children" );
    if(arr == nullptr)
        return;

    for(size_t ii=0; ii<arr->getChildrenCount(); ii++)
    {
        auto newline = new AQLline( children.size(), "undef", AQLline::Field, this );
        newline->fromDom( arr->getChild(ii) );
    }
}


} // namespace jsonui

//---------------------- End of file  model_query.cpp ---------------------------
