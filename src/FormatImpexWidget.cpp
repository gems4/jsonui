//  This is JSONUI library+API (https://bitbucket.org/gems4/jsonui)
//
/// \file FormatImpexWidget.cpp
/// Implementation of class FormatImpexWidget - work with Foreign Import/Export File Format
//
// JSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2018 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONUI depends on the following open-source software products:
// JSONIO (https://bitbucket.org/gems4/jsonio); Qt5 (https://qt.io).
//

#include <QMessageBox>
#include <QKeyEvent>
#include <QLineEdit>
#include <QHeaderView>
#include "FormatImpexWidget.h"
#include "ui_FormatImpexWidget.h"
#include "SelectDialog.h"
#include "jsonio/jsondomfree.h"
#ifndef IMPEX_OFF
  #include "jsonimpex/yamlxml2file.h"
  #include "jsonimpex/thrift_impex.h"
#endif
using namespace jsonio;


namespace jsonui {

TDBSchemaDocument* newDBImpexClient(  const DBQueryData& query  )
{
    return TDBSchemaDocument::newSchemaDocumentQuery(
                uiSettings().resources_database(), "ImpexFormat", "impexdefs",  query);
}

#ifndef IMPEX_OFF

ImpexFormatFile*  readFormatStructDataFile( int mode, bool useDB,
     TDBSchemaDocument *formatdb,  const std::string& format, const std::string& impexSchemaName )
{
    std::shared_ptr<JsonDomFree> formatdata(JsonDomFree::newObject());
    if( useDB )
    {
        formatdb->Read( format );
        std::string impexstr;
        formatdb->getValue( "impex", impexstr );
        parseJsonToNode(  impexstr, formatdata.get() );
    }
    else
       {
        FJson file( format); // type from extension
        file.LoadJson( formatdata.get() );
       }

    //Create input structure
    ImpexFormatFile*  inputIEFile=  new  ImpexFormatFile(
                mode/*ImpexFormatFile::modeImport*/, impexSchemaName/*"FormatStructDataFile"*/, formatdata.get() );
    return inputIEFile;
 }

#endif

void FormatImpexWidget::closeEvent(QCloseEvent* e)
{
    if( !onCloseEvent(this) )
        e->ignore();
    else
        QWidget::closeEvent(e);
}


FormatImpexWidget::FormatImpexWidget(  int mode, const std::string& arunschemaName,  QWidget *parent) :
    JSONUIBase( "", parent),
    _mode(mode), parentSchemaName(arunschemaName), ui(new Ui::FormatImpexWidget)
{
    ui->setupUi(this);
    ui->splitter->setStretchFactor(0, 1);
    ui->splitter->setStretchFactor(1, 2);

    setAttribute(Qt::WA_DeleteOnClose); // automatically delete itself when window is closed

    QString title = qApp->applicationName()+" Structured Data Editor and Database Browser";
    setWindowTitle(title);

    //ui->direcBox->setEnabled(false);

    //set up main parameters
    curRecord_ = "";
    ThriftStructDef* strDef = ioSettings().Schema()->getStruct( "FormatStructDataFile" );
    if( strDef != nullptr )
         ui->impexschema->addItem("FormatStructDataFile");
    strDef = ioSettings().Schema()->getStruct( "FormatKeyValueFile" );
    if( strDef != nullptr )
         ui->impexschema->addItem("FormatKeyValueFile");
    strDef = ioSettings().Schema()->getStruct( "FormatTableFile" );
    if( strDef != nullptr )
         ui->impexschema->addItem("FormatTableFile");
    strDef = ioSettings().Schema()->getStruct( "FormatTextFile" );
    if( strDef != nullptr )
        ui->impexschema->addItem("FormatTextFile");

    jsonio::jsonioErrIf(ui->impexschema->count()<1, "FormatImpexWidget", "No impex formats defined");
    curSchemaName = ui->impexschema->currentText().toStdString();

    //define schema checkbox
    auto vertexNames = ioSettings().Schema()->getVertexesList();
    for( uint ii=0; ii<vertexNames.size(); ii++ )
        ui->schemaBox->addItem(vertexNames[ii].c_str());
    vertexNames = ioSettings().Schema()->getEdgesList();
    for( uint ii=0; ii<vertexNames.size(); ii++ )
       ui->schemaBox->addItem(vertexNames[ii].c_str());


    if( !parentSchemaName.empty() )
        ui->schemaBox->setCurrentText(parentSchemaName.c_str());

    if( _mode )
        ui->direcBox->setCurrentIndex(_mode-1);

    // define edit tree view
    aHeaderData << "key" << "value" << "comment"  ;
    fieldTable =  new TJsonView(  ui->bsonWidget );
    model_schema = new TJsonSchemaModel(  curRecord_,
                                          curSchemaName, aHeaderData, this/*ui->centralWidget*/ );
    deleg_schema = new TJsonSchemaDelegate();

    fieldTable->setModel(model_schema);
    fieldTable->setItemDelegate(deleg_schema);
    fieldTable->setColumnWidth( 0, 250 );
    fieldTable->expandToDepth(0);
    ui->gridLayout_3->addWidget(fieldTable, 1, 0, 1, 2);

    // define menu
    _execF = [](std::shared_ptr<jsonio::ImpexFormatFile>, std::string, bool ){ };
    setActions();
    resetDBClient();

    // set up default data
    CmNew();
}

FormatImpexWidget::~FormatImpexWidget()
{
    delete ui;

    //cout << "~FormatImpexWidget" << endl;
}

bool FormatImpexWidget::resetBson( const std::string& curRecord, const std::string& schemaName )
{
    // test legal schema name
    if( !curSchemaName.empty() &&
            !schemaName.empty() && schemaName != curSchemaName )
        return false;
    fieldTable->updateModelData( curRecord, curSchemaName );
    fieldTable->update();
    return true;
}

//-------------------------------------------------------------


/// Change current View menu selections
void FormatImpexWidget::updtViewMenu()
{
    ui->action_Show_comments->setChecked( TJsonSchemaModel::showComments );
    ui->action_Display_enums->setChecked(TJsonSchemaModel::useEnumNames);
    ui->action_Edit_id->setChecked(TJsonSchemaModel::editID );
    ui->actionKeep_Data_Fields_Expanded->setChecked(TJsonView::expandedFields );
}

/// Change model ( show/hide comments)
void FormatImpexWidget::updtModel()
{
    if( !curSchemaName.empty() )
    {
        model_schema->updateModelData();
        fieldTable->header()->resizeSection(0, 250);
        fieldTable->header()->resizeSection(1, 250);
        fieldTable->expandToDepth(0);
    }
}

/// Change table (Show Enum Names Instead of Values)
void FormatImpexWidget::updtTable()
{
    fieldTable->hide();
    fieldTable->show();
}

/// Update after change DB locations
void FormatImpexWidget::updtDB()
{
    //resetDBClient();
    dbquery->updateQuery();
}

void FormatImpexWidget::CmEditID( bool editid )
{
    ui->keyNoEdit->setReadOnly(!editid);
}

//--------------------------------------------------------------------

//  Connect all actions
void FormatImpexWidget::setActions()
{
    connect( ui->impexschema, SIGNAL(currentIndexChanged(const QString&)),
             this, SLOT(schemaChanged(const QString&)));
    QObject::connect( ui->datafileButton, SIGNAL(clicked()), this, SLOT(CmSelectDataFile()));

    // File
    connect( ui->actionNew , SIGNAL( triggered()), this, SLOT(CmNew()));
    connect( ui->actionClone, SIGNAL( triggered()), this, SLOT(CmClone()));
    connect( ui->actionE_xit, SIGNAL( triggered()), this, SLOT(close()));
    connect( ui->actionExport_File, SIGNAL( triggered()), this, SLOT(CmExportJSON()));
    connect( ui->actionImport_File, SIGNAL( triggered()), this, SLOT(CmImportJSON()));
    connect( ui->actionSelect_Data_File, SIGNAL( triggered()), this, SLOT(CmSelectDataFile()));
#ifndef IMPEX_OFF
    connect( ui->action_Run_script, SIGNAL( triggered()), this, SLOT(CmRunScript()));
#else
    ui->action_Run_script->setDisabled(true);
#endif

    // Edit
    connect(ui->actionAdd_one_field, SIGNAL(triggered()), fieldTable, SLOT(CmAddObject()));
    connect(ui->action_Clone_Current, SIGNAL(triggered()), fieldTable, SLOT(CmCloneObject()));
    connect(ui->action_Delete_field, SIGNAL(triggered()), fieldTable, SLOT(CmDelObject()));
    connect(ui->action_Delete_fields, SIGNAL(triggered()), fieldTable, SLOT(CmDelObjects()));
    connect(ui->actionRemove_Alternatives_Union, SIGNAL(triggered()), fieldTable, SLOT(CmDelObjectsUnion()));
    connect(ui->action_Add_fields, SIGNAL(triggered()), fieldTable, SLOT(CmAddObjects()));
    connect(ui->actionReset_Data_to_Defaults, SIGNAL(triggered()), fieldTable, SLOT(CmResetObject()));
    connect(ui->action_Resize_array, SIGNAL(triggered()), fieldTable, SLOT(CmResizeArray()));
    connect(ui->action_Calculator, SIGNAL(triggered()), fieldTable, SLOT(CmCalc()));
    connect(ui->actionCopy_Field_Path, SIGNAL(triggered()), fieldTable, SLOT(CopyFieldPath()));
    connect(ui->actionCopy_Field, SIGNAL(triggered()), fieldTable, SLOT(CopyField()));
    connect(ui->actionPaste_Field, SIGNAL(triggered()), fieldTable, SLOT(PasteField()));

    // Help
    connect( ui->action_Help_About, SIGNAL( triggered()), this, SLOT(CmHelpAbout()));
    connect( ui->actionContents, SIGNAL( triggered()), this, SLOT(CmHelpContens()));
    connect( ui->actionAuthors, SIGNAL( triggered()), this, SLOT(CmHelpAuthors()));
    connect( ui->actionLicense, SIGNAL( triggered()), this, SLOT(CmHelpLicense()));

    // View
    connect( ui->action_Show_comments, SIGNAL( toggled(bool)), &uiSettings(), SLOT(CmShowComments(bool)));
    connect( ui->action_Display_enums, SIGNAL( toggled(bool)), &uiSettings(), SLOT(CmDisplayEnums(bool)));
    //connect( ui->action_Edit_id, SIGNAL(toggled(bool)), &uiSettings(), SLOT(CmEditID(bool)));
    connect( ui->actionKeep_Data_Fields_Expanded, SIGNAL( toggled(bool)), &uiSettings(), SLOT(CmEditExpanded(bool)));

    connect( ui->action_Show_comments, SIGNAL( toggled(bool)), this, SLOT(updateModel()));
    connect( ui->action_Display_enums, SIGNAL( toggled(bool)), this, SLOT(updateTable()));
    connect( ui->action_Edit_id, SIGNAL(toggled(bool)), this, SLOT(CmEditID(bool)));
    CmEditID( TJsonSchemaModel::editID );
    updateViewMenu();

    connect( ui->action_Update, SIGNAL( triggered()), this, SLOT(CmUpdate()));
    connect( ui->action_Delete, SIGNAL( triggered()), this, SLOT(CmDelete()));
    connect( ui->action_Read, SIGNAL( triggered()), this, SLOT(CmRead()));

    if( _mode == editMode)
    {    ui->runToolBar->hide();
        ui->action_Run_script->setDisabled(true);
        ui->actionSelect_Data_File->setDisabled(true);
        ui->datafileButton->setDisabled(true);
        ui->datafileEdit->setDisabled(true);
        ui->overwriteBox->setDisabled(true);
    }
    //else
    //    ui->editToolBar->hide();
}

// Menu commands -----------------------------------------------------------

/// Change current Edge
void FormatImpexWidget::schemaChanged( const QString& text )
{
    try {
        curSchemaName = text.toStdString();
        std::string curscript = model_schema->saveToJson();
        model_schema->setupModelData( curscript, curSchemaName);
        fieldTable->expandToDepth(0);
    }
    catch(jsonio_exception& e)
    {
        QMessageBox::critical( this, e.title(), e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Set default bson record
void FormatImpexWidget::CmNew()
{
    try{
        ui->keyNoEdit->setText("");
        ui->nameEdit->setText("new_impex_name");
        ui->descEdit->setText("");
        resetBson("", "");
    }
    catch(jsonio_exception& e)
    {
        QMessageBox::critical( this, e.title(), e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

// Clone bson record (clear _id)
void FormatImpexWidget::CmClone()
{
    try{
        ui->keyNoEdit->setText("");
    }
    catch(jsonio_exception& e)
    {
        QMessageBox::critical( this, e.title(), e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/* Save current record to DB file as new
void FormatImpexWidget::CmCreateInsert()
{
    try
    {
        if( dbquery.get() == 0 )
          return;

        curRecord_ =impexToJson();
        std::string key = dbquery->recFromJson( curRecord_ );
        dbquery->Create( key );
        contentsChanged = false;
    }
   catch(jsonio_exception& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}*/

/// Read new record from DB
void FormatImpexWidget::CmRead()
{
    try
    {
        if( dbquery.get() == nullptr )
            return;

        // Select keys to load
        std::vector<std::string> aKeyList;
        std::vector<std::vector<std::string>> aValList;
        dbquery->lastQueryData()->getKeyValueList( aKeyList, aValList );
        if( aKeyList.empty() )
            return;

        SelectDialog selDlg( false, this, "Please, select a record to read/view", aValList );
        if( !selDlg.exec() )
            return;

        std::string reckey = aKeyList[selDlg.getSelectedIndex()];
        // Read Record
        dbquery->Read( reckey );
        impexFromJson( dbquery->getDom() );
        contentsChanged = false;
    }
    catch(jsonio_exception& e)
    {
        QMessageBox::critical( this, e.title(), e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Save current record to DB file
void FormatImpexWidget::CmUpdate()
{
    try
    {
        if( dbquery.get() == nullptr )
            return;
        curRecord_ =impexToJson();
        dbquery->UpdateFromJson( curRecord_, true );
        contentsChanged = false;
    }
    catch(jsonio_exception& e)
    {
        QMessageBox::critical( this, e.title(), e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Delete list of documents from DB
void FormatImpexWidget::CmDelete()
{
    try
    {
        if( dbquery.get() == nullptr )
            return;

        // Select keys to delete
        std::vector<std::string> aKeyList;
        std::vector<std::vector<std::string>> aValList;
        dbquery->lastQueryData()->getKeyValueList( aKeyList, aValList );
        if( aKeyList.empty() )
            return;

        SelectDialog selDlg( true, this, "Please, select documents to delete", aValList, {},
                                  TMatrixTable::tbNoMenu|TMatrixTable::tbSort );
        if( !selDlg.exec() )
            return;

        std::vector<std::size_t> selNdx;
        selDlg.getSelection( selNdx );
        for( std::size_t ii=0; ii<selNdx.size(); ii++ )
        {
            dbquery->Delete( aKeyList[selNdx[ii]] );
        }
    }
    catch(jsonio_exception& e)
    {
        QMessageBox::critical( this, e.title(), e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}


/// Read bson record from json file fileName
void FormatImpexWidget::CmImportJSON()
{
    try{
        std::string fileName;
        if(  ChooseFileOpen( this, fileName,
                             "Please, select a file with format", domFilters  ))
        {
#ifndef IMPEX_OFF
            FJsonYamlXml file( fileName);
            //file.setType( FileTypes::Json_ );
#else
            FJson file( fileName);
#endif
            auto newscript =  file.LoadtoJsonString();
            if( !resetBson( newscript, schemafromName( fileName )) )
                jsonioErr( fileName , "Try to read another schema format file" );
            contentsChanged = true;
        }
    }
    catch(jsonio_exception& e)
    {
        QMessageBox::critical( this, e.title(), e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Write bson record to json file fileName
void FormatImpexWidget::CmExportJSON()
{
    try {
        std::string fileName = fileShemaExt( curSchemaName, "json");
        if(  ChooseFileSave( this, fileName,
                             "Please, select a file to write the format", domFilters, fileName  ))
        {
#ifndef IMPEX_OFF
            FJsonYamlXml file( fileName);
#else
            FJson file( fileName);
#endif
            file.SaveJson( model_schema->getDom() );
        }
    }
    catch(jsonio_exception& e)
    {
        QMessageBox::critical( this, e.title(), e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

void FormatImpexWidget::CmSelectDataFile()
{
    try{
        std::string fileName = ui->datafileEdit->text().toStdString();
        if(  ChooseFileOpen( this, fileName,
                             "Select Input File Path", textFilters  ))
        {
            ui->datafileEdit->setText( fileName.c_str() );
        }
    }
    catch(jsonio_exception& e)
    {
        QMessageBox::critical( this, e.title(), e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

void FormatImpexWidget::CmRunScript()
{
#ifndef IMPEX_OFF
    try{
        std::shared_ptr<ImpexFormatFile> inputIEFile( connectFormatFile( model_schema->getDom() ) );
        std::string datafile = ui->datafileEdit->text().toStdString();
        jsonioErrIf(datafile.empty(), "Undefined Data to Import","Please, select file with data to import");
        bool overwrite = ui->overwriteBox->isChecked();
        _execF(inputIEFile, datafile, overwrite);

        close();
    }
    catch(jsonio_exception& e)
    {
        QMessageBox::critical( this, e.title(), e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
#endif
}


//-------------internal

#ifndef IMPEX_OFF
ImpexFormatFile* FormatImpexWidget::connectFormatFile( const JsonDom* object )
{
    int mode = ui->direcBox->currentIndex();
    ImpexFormatFile*  inputIEFile=  new  ImpexFormatFile( mode, curSchemaName, object );
    return inputIEFile;
}
#endif

// set data to bson
std::string FormatImpexWidget::impexToJson()
{
    std::string jsonstr;
    std::shared_ptr<JsonDomFree> domdata(JsonDomFree::newObject());

    domdata->appendString( "_id", ui->keyNoEdit->text().toStdString() );
    domdata->appendString( "name", ui->nameEdit->text().toStdString() );
    domdata->appendString( "impexschema", ui->impexschema->currentText().toStdString() );
    domdata->appendString( "schema", ui->schemaBox->currentText().toStdString() );
    domdata->appendString(  "comment", ui->descEdit->text().toStdString() );
    domdata->appendInt(  "direction", ui->direcBox->currentIndex() );
    std::string impexstr = model_schema->saveToJson();
    domdata->appendString( "impex", impexstr  );

    // not used now
    //    domdata->appendString( "format", "" );
    //    domdata->appendString( "extension", "" );
    printNodeToJson( jsonstr, domdata.get() );
    return jsonstr;
}

void FormatImpexWidget::impexFromJson( const JsonDom *obj )
{
    // setup data from bson
    std::string getdata;

    if(!obj->findValue( "_id", getdata ) )
        getdata = "";
    ui->keyNoEdit->setText(getdata.c_str());

    if(!obj->findValue( "name", getdata ) )
        getdata = "new_impex";
    ui->nameEdit->setText(getdata.c_str());
    if(!obj->findValue( "impexschema", getdata ) )
        getdata = "";
    ui->impexschema->setCurrentText( getdata.c_str());
    if(!obj->findValue( "schema", getdata ) )
        getdata = "";
    ui->schemaBox->setCurrentText( getdata.c_str());
    if(!obj->findValue( "comment", getdata ) )
        getdata = "";
    ui->descEdit->setText(getdata.c_str());

    int direc;
    if(!obj->findValue( "direction", direc ) )
        direc = 0;
    ui->direcBox->setCurrentIndex(direc);

    // read impex
    if( obj->findValue( "impex", getdata ))
    {
        curRecord_ = getdata;
        resetBson(curRecord_, "");
    }
    // not used now
    if(!obj->findValue( "format", getdata ) )
        getdata = "";
    if(!obj->findValue( "extension", getdata ) )
        getdata = "";
}

void FormatImpexWidget::resetDBClient()
{
    if( curCollectionName.empty())
        return;

    try{
        std::string queryString = "";
        if( _mode!=editMode && !parentSchemaName.empty() )
        {
            queryString = "{ \"schema\": \"";
            queryString += parentSchemaName;
            queryString += "\", ";
            if( _mode == runModeImport )
                queryString += " \"direction\": 0 }";
            else
                queryString += " \"direction\": 1 }";
        }

        DBQueryData qrdata(queryString, DBQueryData::qTemplate );
        TDBSchemaDocument* newClient = TDBSchemaDocument::newSchemaDocumentQuery(
                    uiSettings().resources_database(), "ImpexFormat", "impexdefs", qrdata );
        // no schema
        if( newClient == nullptr )
            dbquery.reset();
        else
            dbquery.reset( newClient );

    }
    catch(std::exception& e)
    {
        std::cout << "Internal comment: " << e.what() << std::endl;
        throw;
    }
}


} // namespace jsonui

// end of FormatImpexWidget.cpp
