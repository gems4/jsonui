//  This is JSONUI library+API (https://bitbucket.org/gems4/jsonui)
//
/// \file JSONUIWidget.cpp
/// JSONUIWidget - Widget to work with json data or
/// internal DOM based on our JSON schemas data
//
// JSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONUI depends on the following open-source software products:
// JSONIO (https://bitbucket.org/gems4/jsonio); Qt5 (https://qt.io).
//

#include <QMessageBox>
#include <QKeyEvent>
#include <QLineEdit>
#include <QHeaderView>
#include "JSONUIWidget.h"
#include "ui_JSONUIWidget.h"
#include "SelectDialog.h"
#include "model_dom.h"
#include "model_schema.h"
#include "jsonio/dbjsondoc.h"
#ifndef IMPEX_OFF
  #include "jsonimpex/yamlxml2file.h"
#endif
using namespace jsonio;

namespace jsonui {


void JSONUIWidget::closeEvent(QCloseEvent* e)
{
    if( !onCloseEvent(this) )
           e->ignore();
       else
           QWidget::closeEvent(e);
}


JSONUIWidget::JSONUIWidget( const std::string& aschemaName, const std::string& collectName, QWidget *parent) :
    JSONUIBase( aschemaName, parent),
    curCollectionName(collectName), ui(new Ui::JSONUIWidget)
{

   ui->setupUi(this);
   setAttribute(Qt::WA_DeleteOnClose); // automatically delete itself when window is closed

   QString title = qApp->applicationName()+" Structured Data Editor and Database Browser";
   setWindowTitle(title);

   //set up main parameters
   curRecord_ = "";

   // define edit tree view
   aHeaderData << "key" << "value"  ;
   fieldTable =  new TJsonView(  ui->bsonWidget );
   if( curSchemaName.empty() )
   {
     model_schema = new TJsonDomModel(  curRecord_, aHeaderData, this/*ui->centralWidget*/ );
     deleg_schema =  new TJsonDomDelegate();
   }
   else
   {
     aHeaderData << "comment" ;
     model_schema = new TJsonSchemaModel(  curRecord_,
        curSchemaName, aHeaderData, this/*ui->centralWidget*/ );
     deleg_schema = new TJsonSchemaDelegate();
   }
   fieldTable->setModel(model_schema);
   fieldTable->setItemDelegate(deleg_schema);
   fieldTable->setColumnWidth( 0, 250 );
   fieldTable->expandToDepth(0);
   ui->verticalLayout->addWidget(fieldTable);

   // define menu
   setActions();
   resetDBClient();
   // set up default data
   CmNew();
}

JSONUIWidget::~JSONUIWidget()
{
    delete ui;
    //cout << "~JSONUIWidget" << endl;
}

bool JSONUIWidget::resetBson( const std::string& curRecord, const std::string& schemaName )
{
   // test legal schema name
   if( !curSchemaName.empty() &&
       !schemaName.empty() && schemaName != curSchemaName )
       return false;
   fieldTable->updateModelData( curRecord, curSchemaName );
   fieldTable->update();
   return true;
}

//-------------------------------------------------------------


/// Change current View menu selections
void JSONUIWidget::updtViewMenu()
{
    ui->action_Show_comments->setChecked( TJsonSchemaModel::showComments );
    ui->action_Display_enums->setChecked(TJsonSchemaModel::useEnumNames);
    ui->action_Edit_id->setChecked(TJsonSchemaModel::editID );
    ui->actionKeep_Data_Fields_Expanded->setChecked(TJsonView::expandedFields );
}

/// Change model ( show/hide comments)
void JSONUIWidget::updtModel()
{
  if( !curSchemaName.empty() )
  {
      model_schema->updateModelData();
      fieldTable->header()->resizeSection(0, 250);
      fieldTable->header()->resizeSection(1, 250);
      fieldTable->expandToDepth(0);
  }
}

/// Change table (Show Enum Names Instead of Values)
void JSONUIWidget::updtTable()
{
    fieldTable->hide();
    fieldTable->show();
}

/// Update after change DB locations
void JSONUIWidget::updtDB()
{
  //resetDBClient();
  if(dbquery.get() )
    dbquery->updateQuery();
}


//--------------------------------------------------------------------

//  Connect all actions
void JSONUIWidget::setActions()
{
    // File
    connect( ui->actionNew , SIGNAL( triggered()), this, SLOT(CmNew()));
    connect( ui->action_Clone_Structured_Data_Object , SIGNAL( triggered()), this, SLOT(CmClone()));
    connect( ui->actionE_xit, SIGNAL( triggered()), this, SLOT(close()));
    connect( ui->actionExport_Json_File, SIGNAL( triggered()), this, SLOT(CmExportJSON()));
    connect( ui->actionImport_Json_File, SIGNAL( triggered()), this, SLOT(CmImportJSON()));
#ifndef IMPEX_OFF
    connect( ui->action_Export_YAML_File, SIGNAL( triggered()), this, SLOT(CmExportYAML()));
    connect( ui->actionImport_YAML_File, SIGNAL( triggered()), this, SLOT(CmImportYAML()));
    connect( ui->actionExport_XML_File, SIGNAL( triggered()), this, SLOT(CmExportXML()));
    connect( ui->actionImport_XML_File, SIGNAL( triggered()), this, SLOT(CmImportXML()));
#else
    ui->action_Export_YAML_File->setVisible(false);
    ui->actionImport_YAML_File->setVisible(false);
    ui->actionExport_XML_File->setVisible(false);
    ui->actionImport_XML_File->setVisible(false);
#endif

    // Edit
    connect(ui->actionAdd_one_field, SIGNAL(triggered()), fieldTable, SLOT(CmAddObject()));
    connect(ui->action_Clone_Current, SIGNAL(triggered()), fieldTable, SLOT(CmCloneObject()));
    connect(ui->action_Delete_field, SIGNAL(triggered()), fieldTable, SLOT(CmDelObject()));
    connect(ui->action_Delete_fields, SIGNAL(triggered()), fieldTable, SLOT(CmDelObjects()));
    if( !curSchemaName.empty() )
    {   connect(ui->actionRemove_Alternatives_Union, SIGNAL(triggered()), fieldTable, SLOT(CmDelObjectsUnion()));
        connect(ui->action_Add_fields, SIGNAL(triggered()), fieldTable, SLOT(CmAddObjects()));
        connect(ui->actionReset_Data_to_Defaults, SIGNAL(triggered()), fieldTable, SLOT(CmResetObject()));
    }else
      {  ui->actionRemove_Alternatives_Union->setVisible(false);
         ui->action_Add_fields->setVisible(false);
         ui->actionReset_Data_to_Defaults->setVisible(false);
      }
    connect(ui->action_Resize_array, SIGNAL(triggered()), fieldTable, SLOT(CmResizeArray()));
    connect(ui->action_Calculator, SIGNAL(triggered()), fieldTable, SLOT(CmCalc()));
    connect(ui->actionCopy_Field_Path, SIGNAL(triggered()), fieldTable, SLOT(CopyFieldPath()));
    connect(ui->actionCopy_Field, SIGNAL(triggered()), fieldTable, SLOT(CopyField()));
    connect(ui->actionPaste_Field, SIGNAL(triggered()), fieldTable, SLOT(PasteField()));

    // Help
    connect( ui->action_Help_About, SIGNAL( triggered()), this, SLOT(CmHelpAbout()));
    connect( ui->actionContents, SIGNAL( triggered()), this, SLOT(CmHelpContens()));
    connect( ui->actionAuthors, SIGNAL( triggered()), this, SLOT(CmHelpAuthors()));
    connect( ui->actionLicense, SIGNAL( triggered()), this, SLOT(CmHelpLicense()));

    // View
    connect( ui->action_Show_comments, SIGNAL( toggled(bool)), &uiSettings(), SLOT(CmShowComments(bool)));
    connect( ui->action_Display_enums, SIGNAL( toggled(bool)), &uiSettings(), SLOT(CmDisplayEnums(bool)));
    connect( ui->action_Edit_id, SIGNAL(toggled(bool)), &uiSettings(), SLOT(CmEditID(bool)));
    connect( ui->actionKeep_Data_Fields_Expanded, SIGNAL( toggled(bool)), &uiSettings(), SLOT(CmEditExpanded(bool)));
    updateViewMenu();

    if( !curCollectionName.empty())
    { connect( ui->action_Update, SIGNAL( triggered()), this, SLOT(CmUpdate()));
      connect( ui->action_Create, SIGNAL( triggered()), this, SLOT(CmCreateInsert()));
      connect( ui->action_Delete, SIGNAL( triggered()), this, SLOT(CmDelete()));
      connect( ui->action_Read, SIGNAL( triggered()), this, SLOT(CmRead()));
    }
    else
      ui->menu_Database->menuAction()->setVisible(false);

   if( !curSchemaName.empty() )
   { QLineEdit* pLineTask = new QLineEdit( ui->nameToolBar );
     pLineTask->setEnabled( true );
     pLineTask->setFocusPolicy( Qt::ClickFocus );
     pLineTask->setReadOnly( true );
     pLineTask->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed );
     QString title =  curSchemaName.c_str();
     pLineTask->setText(title);
     ui->nameToolBar->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed );
     ui->nameToolBar->addWidget( pLineTask ); // setStretchableWidget( pLine );
   }
   else
   {
       ui->mainToolBar->hide();
   }
}

// Menu commands -----------------------------------------------------------


/// Set default bson record
void JSONUIWidget::CmNew()
{
  try{
       curRecord_="";
       resetBson( curRecord_, "");
    }
   catch(jsonio_exception& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Clone bson record (clear _id)
void JSONUIWidget::CmClone()
{
  try{
        TJsonSchemaModel* model = dynamic_cast<TJsonSchemaModel*>(model_schema);
        if( model)
        {
           model->setOid( "" );
           model->setValue("_rev", std::string(""));
        }
        else
          if( !curCollectionName.empty() )
          {  TJsonDomModel* modeljs = dynamic_cast<TJsonDomModel*>(model_schema);
             if( modeljs)
             {
               modeljs->setOid( "" );
               modeljs->setValue("_rev", std::string(""));
             }
          }
        fieldTable->hide();
        fieldTable->show();
    }
   catch(jsonio_exception& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

// Save current record to DB file as new
void JSONUIWidget::CmCreateInsert()
{
    try
    {
        if( dbquery.get() == nullptr )
          return;
        curRecord_ =model_schema->saveToJson();
        std::string key = dbquery->recFromJson( curRecord_ );
        std::string rid = dbquery->Create( key );
        model_schema->setupModelData( dbquery->GetJson(), curSchemaName );
    }
   catch(jsonio_exception& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}


/// Read new record from DB
void JSONUIWidget::CmRead()
{
    try
    {
        if( dbquery.get() == nullptr )
          return;

        // Select keys to load
        std::vector<std::string> aKeyList;
        std::vector<std::vector<std::string>> aValList;
        dbquery->lastQueryData()->getKeyValueList( aKeyList, aValList );
        if( aKeyList.empty() )
          return;

        SelectDialog selDlg( false, this, "Please, select a record to read/view", aValList );
         if( !selDlg.exec() )
          return;

        std::string reckey = aKeyList[selDlg.getSelectedIndex()];
        // Read Record
        dbquery->Read( reckey );
        curRecord_ = dbquery->GetJson(true);
        resetBson(curRecord_, "");
        contentsChanged = true;
   }
   catch(jsonio_exception& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Save current record to DB file
void JSONUIWidget::CmUpdate()
{
    try
    {
       if( dbquery.get() == nullptr )
          return;
        curRecord_ =model_schema->saveToJson();
        std::string key = dbquery->recFromJson( curRecord_ );
        dbquery->Update( key );
        contentsChanged = false;
    }
   catch(jsonio_exception& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Delete current record from DB
void JSONUIWidget::CmDelete()
{
    try
    {
        if( dbquery.get() == nullptr )
          return;
        // get current key
        curRecord_ =model_schema->saveToJson();
        std::string key = dbquery->recFromJson( curRecord_ );
        QString msg = QString("Confirm deletion of %1 record?").arg( key.c_str() );
        QMessageBox::StandardButton reply;
        reply = QMessageBox::question( this, "Record to delete", msg,
                 QMessageBox::Yes|QMessageBox::No);
        if (reply == QMessageBox::No)
           return;
        dbquery->Delete( key );
        contentsChanged = false;
    }
   catch(jsonio_exception& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}


/// Read bson record from json file fileName
void JSONUIWidget::CmImportJSON()
{
  try{
        std::string fileName;
        if(  ChooseFileOpen( this, fileName,
                     "Please, select a file with JSON object", jsonFilters  ))
       {
#ifndef IMPEX_OFF
            FJsonYamlXml file( fileName);
            file.setType( FileTypes::Json_ );
#else
            FJson file( fileName);
#endif
          curRecord_ =  file.LoadtoJsonString();
          if( !resetBson( curRecord_, schemafromName( fileName )) )
            jsonioErr( fileName , "Try to read another schema format file" );
          contentsChanged = true;
       }
    }
   catch(jsonio_exception& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Write bson record to json file fileName
void JSONUIWidget::CmExportJSON()
{
   try {
         std::string fileName = fileShemaExt( curSchemaName, "json");
         if(  ChooseFileSave( this, fileName,
                     "Please, select a file to write the data", jsonFilters, fileName  ))
         {
#ifndef IMPEX_OFF
            FJsonYamlXml file( fileName);
            file.setType( FileTypes::Json_ );
#else
            FJson file( fileName);
#endif
            file.SaveJson( model_schema->getDom() );
         }
    }
   catch(jsonio_exception& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

#ifndef IMPEX_OFF

/// Read bson record from yaml file fileName
void JSONUIWidget::CmImportYAML()
{
  try{
        std::string fileName;
        if(  ChooseFileOpen( this, fileName,
                     "Please, select a file with YAML data object", yamlFilters  ))
       {
          FJsonYamlXml file( fileName);
          file.setType( FileTypes::Yaml_ );
          curRecord_ =  file.LoadtoJsonString();
          if( !resetBson( curRecord_, schemafromName( fileName )) )
            jsonioErr( fileName , "Try to read another schema format file" );
          contentsChanged = true;
       }
    }
   catch(jsonio_exception& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Write bson record to YAML file fileName
void JSONUIWidget::CmExportYAML()
{
   try {
        std::string fileName = fileShemaExt(curSchemaName, "yaml");
        if(  ChooseFileSave( this, fileName,
                     "Please, select a file to write the data", yamlFilters, fileName  ))
         {
            FJsonYamlXml file( fileName);
            file.setType( FileTypes::Yaml_ );
            file.SaveJson( model_schema->getDom() );
         }
    }
   catch(jsonio_exception& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Read bson record from xml file fileName
void JSONUIWidget::CmImportXML()
{
  try{
        std::string fileName;
        if(  ChooseFileOpen( this, fileName,
                     "Please, select a file with xml object to read", xmlFilters  ))
       {
          FJsonYamlXml file( fileName);
          file.setType( FileTypes::XML_ );
          curRecord_ =  file.LoadtoJsonString();
          if( !resetBson( curRecord_, schemafromName( fileName )) )
            jsonioErr( fileName , "Try to read another schema format file" );
          contentsChanged = true;
       }
    }
   catch(jsonio_exception& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Write bson record to file fileName
void JSONUIWidget::CmExportXML()
{
   try {
        std::string fileName = fileShemaExt(curSchemaName, "xml");
        if(  ChooseFileSave( this, fileName,
                     "Please, select a file to write the data", xmlFilters, fileName  ))
         {
            FJsonYamlXml file( fileName);
            file.setType( FileTypes::XML_ );
            file.SaveJson( model_schema->getDom() );
         }
    }
   catch(jsonio_exception& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

#endif
void JSONUIWidget::resetDBClient()
{
  if( curCollectionName.empty())
       return;

  try{
      TDBDocumentBase* newClient;
      if( curSchemaName.empty() )
      {
          newClient = TDBJsonDocument::newJsonDocumentQuery(
            uiSettings().database(), curCollectionName );
      }
      else
       {
         // std::vector<std::string> keyFldsInf = { "name" };
         newClient = TDBSchemaDocument::newSchemaDocumentQuery(
           uiSettings().database(), curSchemaName, curCollectionName );
       }
      // no schema
      if( newClient == nullptr )
       dbquery.reset();
      else
       dbquery.reset( newClient );

    }
   catch(std::exception& e)
    {
       std::cout << "Internal comment: " << e.what() << std::endl;
       throw;
    }
 }

} // namespace jsonui

// end of JSONUIWidget.cpp
