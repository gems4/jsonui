//  This is JSONUI library+API (https://bitbucket.org/gems4/jsonui)
//
/// \file model_dom.h
/// Declaration of TSchemaNodeModel, TSchemaNodeDelegate
/// implements a tree view of internal DOM based on our JSON schemas
//
// JSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONUI depends on the following open-source software products:
// JSONIO (https://bitbucket.org/gems4/jsonio); Qt5 (https://qt.io).
//

#ifndef _model_dom_h
#define _model_dom_h

#include <memory>
#include "model_view.h"
#include "jsonio/jsondom.h"

namespace jsonui {


/// \class TJsonDomModel
/// Class for represents the data set and is responsible for fetching the data
/// is needed for viewing and for writing back any changes.
/// Reading/writing data from/to bson objects
class TJsonDomModel: public TJsonAbstractModel
{
    Q_OBJECT

    QStringList hdData;
    std::shared_ptr<jsonio::JsonDom> rootNode;

    QString getDescription( const QModelIndex& index ) const;

 public:

  TJsonDomModel( const std::string& ajson,
                  const QStringList& aHeaderData,   QObject* parent = nullptr );
  ~TJsonDomModel();
  void setupModelData( const std::string& ajson, const std::string& schema );

  QModelIndex index(int row, int column, const QModelIndex& parent) const;
  QModelIndex parent(const QModelIndex& child) const;
  int rowCount ( const QModelIndex& parent ) const;
  int columnCount ( const QModelIndex& parent  ) const;
  QVariant data ( const QModelIndex& index, int role ) const;
  bool setData ( const QModelIndex& index, const QVariant& value, int role );
  QVariant headerData ( int section, Qt::Orientation orientation, int role ) const;
  Qt::ItemFlags flags ( const QModelIndex& index ) const;
  jsonio::JsonDom* lineFromIndex(const QModelIndex& index) const;

  std::string helpName( const QModelIndex& index ) const;

  bool isNumber( const QModelIndex& index ) const
  {
    return  lineFromIndex(index)->isNumber();
  }
  bool isArray( const QModelIndex& index ) const
  {
     return  lineFromIndex(index)->isArray();
  }
  bool isCanBeRemoved( const QModelIndex& index ) const
  {
      auto line = lineFromIndex(index);
      return  !line->isTop() && line->getParent()->isStruct() ;
  }
  bool isCanBeCloned( const QModelIndex& index ) const
  {
      auto line = lineFromIndex(index);
      return  !line->isTop() && ( line->getParent()->isStruct() || line->getParent()->isArray() ) ;
  }
  bool isStruct( const QModelIndex& index ) const
  {
      auto line = lineFromIndex(index);
      return ( line->isTop() || line->getParent()->isStruct() ||
              ( line->isStruct()  && line->getChildrenCount() < 1  )) ;
  }
  ///bool isUnion( const QModelIndex&  index ) const;

  void resizeArray( QWidget* parent, const QModelIndex& index );
  void delObject( QWidget* parent, const QModelIndex& index );
  void delObjects( QWidget* parent, const QModelIndex& index  );
  const QModelIndex addObjects( QWidget* parent, const QModelIndex& index );
  const QModelIndex addObject( QWidget* parent, const QModelIndex& index );
  const QModelIndex cloneObject( QWidget* parent, const QModelIndex& index  );

  /// void delObjectsUnion( QWidget* , const QModelIndex&  );
  /// void resetObject( QWidget* , const QModelIndex&  );

  std::string  getFieldPath( const QModelIndex& index ) const;
  std::string  getFieldData( const QModelIndex&  index ) const;
  void  setFieldData( const QModelIndex& , const std::string& );

  /// Return internal data to json std::string
  virtual std::string saveToJson()
  {
    return  rootNode->toString();
  }

  const jsonio::JsonDom* getDom() const
  {
      return rootNode.get();
  }

  /// Set Value to Dom
  template <class T>
  void setValue( const std::string& fldpath, const T& value  )
  {
       auto data = rootNode->field(  fldpath );
      if( data != nullptr)
        data->setValue(value);
  }

  /// Set _id to Node
  void setOid( const std::string& _oid  )
  {
      rootNode->setOid_( _oid );
  }
};


/// \class TJsonDomDelegate individual items in views are rendered and edited using delegates
class TJsonDomDelegate: public QItemDelegate
{
    Q_OBJECT

public:
     TJsonDomDelegate( QObject * parent = nullptr );
     QWidget *createEditor(QWidget *parent,
                           const QStyleOptionViewItem &option,
                           const QModelIndex &index) const;
};


} // namespace jsonui

#endif   // _model_dom_h
