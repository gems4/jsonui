//  This is JSONUI library+API (https://bitbucket.org/gems4/jsonui)
//
/// \file QueryWidget.h
/// Declarations of class QueryWidget widget to work with database queries
//
// JSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONUI depends on the following open-source software products:
// JSONIO (https://bitbucket.org/gems4/jsonio); Qt5 (https://qt.io).
//

#ifndef QUERYWIDGET_H
#define QUERYWIDGET_H

#include <QMainWindow>
#include <QLineEdit>
#include "jsonio/dbschemadoc.h"


namespace Ui {
class QueryWidget;
}

namespace jsonui {

class TStringVectorModel;
class TQueryView;
class TQueryModel;

/// \class QueryWidget window to insert/edit  database query
class QueryWidget : public QMainWindow
{
    Q_OBJECT

protected slots:

    void objectChanged(){}
    void schemaChanged(const QString & text);
    void AQLtoggled(bool checked);

    // Files
    void CmSaveQuery();
    void CmLoadQuery();
    void CmExecute();
    void CmApplay()
    {
      CmExecute();
      close();
    }

    // Select part
    void CmSelectFields();
    void CmAddSelectFields();
    void CmSelectRestore();

    // Where part
    void CmWhereApply();
    void CmWhereRestore();
    void CmClearTree();

    // Menu
    void CmAddWhereFields();

    // Record
    void CmClone();
    void CmRead();
    void CmUpdate();
    void CmDeleteList();

    // Help
    void CmHelp();
    void CmHelpAbout();

public:

    explicit QueryWidget( const char* title,  const std::vector<std::string>& schemaNames,
                          const jsonio::DBQueryDef& query,   QWidget *parent );
    ~QueryWidget();

    /// Return query
    const jsonio::DBQueryDef& getQueryDef();

    /// Return query schema
    std::string getQuerySchema() const;

    /// Update query
    void UpdateQuery( const std::vector<std::string>& schemaNames, const jsonio::DBQueryDef& query );

    /// Reset current DB client
    void resetDBClient();

private:

    friend class TQueryView;

    std::string schema_name;
    //std::string _id;
    std::shared_ptr<jsonio::TDBSchemaDocument> dbquery;
    jsonio::DBQueryDef default_query;
    jsonio::DBQueryDef query_description;

    ///  Connect all actions
    void setActions();

    void showQuery();
    std::string queryToJson();
    int getQueryType() const;
    void queryFromNode( const jsonio::JsonDom *object );
    bool selectLine( std::string& fldpath, jsonio::ThriftFieldDef** flddata );
    jsonio::DBQueryData newSchemaQuery();

    Ui::QueryWidget *ui;
    TStringVectorModel* lstmodel;
    TQueryView* queryEdit;
    TQueryModel* queryModel;
    QWidget* resultWidget;
    QLineEdit* qId;

};

} // namespace jsonui

#endif // QUERYWIDGET_H
