//  This is JSONUI library+API (https://bitbucket.org/gems4/jsonui)
//
/// \file preferences.cpp
/// Implementation of UIPreferences object for monitoring changes
//
// JSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2017 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONUI depends on the following open-source software products:
// JSONIO (https://bitbucket.org/gems4/jsonio); Qt5 (https://qt.io).
//

#include <QMessageBox>
#include <iostream>
#include "model_schema.h"
#include "PreferencesJSONUI.h"
#include "HelpMainWindow.h"
#ifndef IMPEX_OFF
  #include "jsonimpex/dbclient.h"
#endif

namespace jsonui {

UIPreferences& uiSettings()
{
    static  UIPreferences data;
    return data;
}

UIPreferences::UIPreferences():
    _iosettings(jsonio::ioSettings()), _jsonuiGroup(_iosettings.group("jsonui")),
    work_database(nullptr), resourse_database(nullptr)
{
    setDefValues();
    updateDatabase();
}

void UIPreferences::setDefValues()
{
    // load main programm settingth
    TJsonSchemaModel::showComments = _jsonuiGroup.value("ShowComments", TJsonSchemaModel::showComments);
    TJsonSchemaModel::useEnumNames = _jsonuiGroup.value("ShowEnumNames", TJsonSchemaModel::useEnumNames);
    TJsonSchemaModel::editID = _jsonuiGroup.value("CanEdit_id", TJsonSchemaModel::editID);
    TJsonView::expandedFields = _jsonuiGroup.value("KeepExpanded", TJsonView::expandedFields);

    HelpMainWindow::editHelp = _jsonuiGroup.value("CanEditDocPages", HelpMainWindow::editHelp);
}

//  Create empty collections if they are not present (for all vertexes and edges)
void UIPreferences::createCollectionsIfNoExist( jsonio::TAbstractDBDriver* dbDriver)
{
    try{
        const std::map<std::string, std::string>& vertexcolls =
                                   _iosettings.Schema()->usedVertexCollections();
        for( auto vertexCol: vertexcolls )
            dbDriver->createCollection( vertexCol.second.c_str(), "vertex");

        const std::map<std::string, std::string>& edgecolls =
                                    _iosettings.Schema()->usedEdgeCollections();
        for( auto edgeCol: edgecolls )
            dbDriver->createCollection( edgeCol.second.c_str(), "edge");
    }
    catch(std::exception& e)
    {
        std::cout << "Error connection to localhost: " <<  e.what() << std::endl;
    }
}

bool UIPreferences::testCreateLocalDatabase(const std::string& theDBname, const std::string& user)
{
    try {  // could be not exist local ArangoDB or illegal root password

        jsonio::ArangoDBConnect rootConnect( jsonio::local_server_endpoint,  jsonio::local_root_username,
                                             jsonio::local_root_password,  jsonio::local_root_database  );
        rootConnect.getFromSettings( _iosettings.groupArangoDB("ArangoDBLocal"), true );
        std::shared_ptr<jsonio::TArangoDBRootClient> rootclient(  new jsonio::TArangoDBRootClient( rootConnect ) );


        if( !rootclient->ExistDatabase( theDBname ) )
        {
            QString msg = QString("The database %1 does not exist.\n Do you wants to create it?").arg(theDBname.c_str());
            auto reply = QMessageBox::question( 0, "Create database", msg, QMessageBox::Yes|QMessageBox::No);
            if (reply == QMessageBox::Yes)
                return true;

            jsonio::jsonioErr( "DBArango: ", "The database "+theDBname+" does not exist." );
        }
    }
    catch(std::exception& e)
    {
        std::cout << "Error connection to localhost: " <<  e.what() << std::endl;
    }
    return false;
}

void UIPreferences::updateDatabase()
{

    std::shared_ptr<jsonio::TAbstractDBDriver> dbDriver;
    int dbConnection = getCurrentDBConnection( );//_jsonuiGroup.value("CurrentDBConnection", 0);

    switch( dbConnection )
    {
    default:
    case 0: // localDB
    {
        jsonio::ArangoDBConnect localConnect;
        localConnect.getFromSettings( _iosettings.groupArangoDB("ArangoDBLocal") );
        dbDriver.reset( new jsonio::TArangoDBClient( localConnect ));
        if( testCreateLocalDatabase( localConnect.databaseName, localConnect.user.name ) )
            createLocalDatabaseIfNoExist( localConnect.databaseName );
        createCollectionsIfNoExist( dbDriver.get() );
    }
        break;
    case 1: // remoteDB
    {
        jsonio::ArangoDBConnect remoteConnect;
        remoteConnect.getFromSettings( _iosettings.groupArangoDB("ArangoDBRemote") );
        dbDriver.reset( new jsonio::TArangoDBClient( remoteConnect ));
    }
        break;
#ifndef IMPEX_OFF
    case 2: // localDB
    {
        std::string host = _iosettings.value( "jsonimpex.ThriftDBSocketHost", jsonio::defHost );
        int port = _iosettings.value("jsonimpex.ThriftDBSocketPort", jsonio::defPort );
        dbDriver.reset( new jsonio::TDBClient( host, port ));
    }
        break;
#endif
    }

    if( work_database.get() == nullptr )
        work_database.reset( new jsonio::TDataBase( dbDriver) );
    else
        work_database->updateDriver(dbDriver);

    updateResourceDatabase();
}

void UIPreferences::updateResourceDatabase()
{

    std::shared_ptr<jsonio::TAbstractDBDriver> dbDriver;
    int dbConnection = getCurrentDBConnection( );//_jsonuiGroup.value("CurrentDBConnection", 0);

    switch( dbConnection )
    {
    default:
    case 0: // localDB
    {
        createLocalDatabaseIfNoExist(resourcesDatabaseName);
        jsonio::ArangoDBConnect localConnect;
        localConnect.getFromSettings( _iosettings.groupArangoDB("ArangoDBLocal") );
        localConnect.databaseName = resourcesDatabaseName;
        dbDriver.reset( new jsonio::TArangoDBClient( localConnect ));
        //dbDriver->createCollection( "impexdefs", "vertex");
        //dbDriver->createCollection( "queries", "vertex");
        //dbDriver->createCollection( "docpages", "vertex");
    }
        break;
    case 1: // remoteDB
    {
        jsonio::ArangoDBConnect remoteConnect;
        remoteConnect.getFromSettings( _iosettings.groupArangoDB("ArangoDBRemote") );
        remoteConnect.databaseName = resourcesDatabaseName;
        dbDriver.reset( new jsonio::TArangoDBClient( remoteConnect ));
    }
        break;
#ifndef IMPEX_OFF
    case 2: // localDB
    {
        std::string host = _iosettings.value( "jsonimpex.ThriftDBSocketHost", jsonio::defHost );
        int port = _iosettings.value("jsonimpex.ThriftDBSocketPort", jsonio::defPort );
        dbDriver.reset( new jsonio::TDBClient( host, port ));
    }
        break;
#endif
    }

    if( resourse_database.get() == nullptr )
        resourse_database.reset( new jsonio::TDataBase( dbDriver) );
    else
        resourse_database->updateDriver(dbDriver);
}

void UIPreferences::createLocalDatabaseIfNoExist( const std::string& theDBname )
{
    try {  // could be not exist local ArangoDB or illegal root password

        jsonio::ArangoDBConnect localConnect;
        localConnect.getFromSettings( _iosettings.groupArangoDB("ArangoDBLocal") );
        jsonio::ArangoDBConnect rootConnect( jsonio::local_server_endpoint,  jsonio::local_root_username,
                                             jsonio::local_root_password,  jsonio::local_root_database  );
        rootConnect.getFromSettings( _iosettings.groupArangoDB("ArangoDBLocal"), true );
        std::shared_ptr<jsonio::TArangoDBRootClient> rootclient(  new jsonio::TArangoDBRootClient( rootConnect ) );

        rootclient->CreateDatabase( theDBname, { localConnect.user });
    }
    catch(std::exception& e)
    {
        std::cout << "Error connection to localhost: " <<  e.what() << std::endl;
    }
}



bool UIPreferences::testCurrentDriver(int dbConnection )
{

    if( work_database.get() == nullptr )
        return true;

    switch( dbConnection )
    {
    default:
    case 0: // localDB
    {
        jsonio::ArangoDBConnect localConnect;
        localConnect.getFromSettings(_iosettings.groupArangoDB("ArangoDBLocal"));
        jsonio::TArangoDBClient* client = dynamic_cast<jsonio::TArangoDBClient*>(work_database->theDriver());
        return ( !client || localConnect != client->dbConnection() );
    }
        //break;
    case 1: // remoteDB
    {
        jsonio::ArangoDBConnect remoteConnect;
        remoteConnect.getFromSettings(_iosettings.groupArangoDB("ArangoDBRemote"));
        jsonio::TArangoDBClient* client = dynamic_cast<jsonio::TArangoDBClient*>(work_database->theDriver());
        return ( !client || remoteConnect != client->dbConnection() );
    }
        //break;
#ifndef IMPEX_OFF
    case 2: // localDB
    {
        std::string host = _iosettings.value( "jsonimpex.ThriftDBSocketHost", jsonio::defHost );
        int port = _iosettings.value("jsonimpex.ThriftDBSocketPort", jsonio::defPort );
        jsonio::TDBClient* client = dynamic_cast<jsonio::TDBClient*>(work_database->theDriver());
        return ( !client || host != client->host() || port != client->port()  );
    }
        //break;
#endif
    }
    //return false;
}


// Update "CurrentDBConnection" value.
void UIPreferences::setCurrentDBConnection( int ndx )
{
    std::string dbConnection = "ArangoDBLocal";
    switch( ndx )
    {
    default:
    case 0: // localDB
        dbConnection = "ArangoDBLocal";
        _iosettings.setValue("jsonio.UseArangoDBInstance", "ArangoDBLocal");
        break;
    case 1: // remoteDB
        dbConnection = "ArangoDBRemote";
        _iosettings.setValue("jsonio.UseArangoDBInstance", "ArangoDBRemote");
        break;
    case 2: // localDB
        dbConnection = "ThriftDBSocket";
        break;
    }
    _jsonuiGroup.setValue("CurrentDBConnection", dbConnection );
}

// Get "CurrentDBConnection" index
int UIPreferences::getCurrentDBConnection( )
{
    auto dbconnection = _jsonuiGroup.value("CurrentDBConnection", "ArangoDBLocal" );
    if( dbconnection == "ArangoDBRemote" )
        return 1;
#ifndef IMPEX_OFF
    if( dbconnection == "ThriftDBSocket" )
        return 2;
#endif
    return 0;
}

//------------------------------------------------------------

void UIPreferences::CmSettingth()
{
    try
    {
        // define new preferences
        PreferencesJSONUI dlg(_iosettings);
        // signal to reset database
        QObject::connect( &dlg, SIGNAL(dbdriveChanged()), this, SIGNAL( dbChanged()));

        if( !dlg.exec() )
            return;

        if( _iosettings.updateSchemaDir() )
            emit schemaChanged();

        emit viewMenuChanged();
        emit modelChanged();
        emit tableChanged();

    }
    catch(jsonio::jsonio_exception& e)
    {
        std::cout << "CmSettingth " <<  e.title() << e.what() << std::endl;
        emit errorSettings(e.what());
    }
    catch(std::exception& e)
    {
        std::cout << "CmSettingth " <<  e.what() << std::endl;
        emit errorSettings(e.what());
    }

}

void UIPreferences::CmShowComments( bool checked )
{
    TJsonSchemaModel::showComments = checked;
    _jsonuiGroup.setValue("ShowComments",  TJsonSchemaModel::showComments );
    emit viewMenuChanged();
    emit modelChanged();
}

void UIPreferences::CmDisplayEnums( bool checked )
{
    TJsonSchemaModel::useEnumNames = checked;
    _jsonuiGroup.setValue("ShowEnumNames",  TJsonSchemaModel::useEnumNames );
    emit viewMenuChanged();
    emit tableChanged();
}

void UIPreferences::CmEditID( bool checked )
{
    TJsonSchemaModel::editID = checked;
    _jsonuiGroup.setValue("CanEdit_id",  TJsonSchemaModel::editID );
    emit viewMenuChanged();
}

void UIPreferences::CmEditExpanded( bool checked )
{
    TJsonView::expandedFields = checked;
    _jsonuiGroup.setValue("KeepExpanded",  TJsonView::expandedFields );
    // signal to update menu all
    emit viewMenuChanged();
}

void UIPreferences::CmSetUserDir( QString dirPath)
{
    _iosettings.setUserDir( dirPath.toStdString() );
}

} // namespace jsonui
