//  This is JSONUI library+API (https://bitbucket.org/gems4/jsonui)
//
/// \file EdgesWidget.cpp
/// Implementation of class EdgesWidget - Widget to work with edges -
/// internal DOM based on our JSON schemas data
//
// JSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2018 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONUI depends on the following open-source software products:
// JSONIO (https://bitbucket.org/gems4/jsonio); Qt5 (https://qt.io).
//

#include <iostream>
#include <QMessageBox>
#include <QKeyEvent>
#include <QHeaderView>
#include <QLineEdit>
#include "EdgesWidget.h"
#include "ui_EdgesWidget.h"
#include "waitingspinnerwidget.h"

namespace jsonui {

//----------------------------------------------------------------------

void EdgesWidget::closeEvent(QCloseEvent* e)
{
    if( dbKeysModel )
        dbKeysModel->Close();
    if( inKeysModel )
        inKeysModel->Close();
    if( outKeysModel )
        outKeysModel->Close();

    if( !onCloseEvent(this) )
        e->ignore();
    else
        QWidget::closeEvent(e);
}

bool EdgesWidget::event(QEvent * e) // overloading event(QEvent*) method of QMainWindow
{
    switch(e->type())
    {
    case QEvent::WindowActivate :
        changeKeyList();
        break ;
    default: break;
    }

    return QMainWindow::event(e) ;
}

EdgesWidget::EdgesWidget( const std::string& schemaName, const jsonio::DBQueryData& query, QWidget *parent) :
    JSONUIBase(schemaName, parent),
    ui(new Ui::EdgesWidget),
    defaultQuery(query),   dbKeysModel(nullptr), keysTable(nullptr),
    inKeysModel(nullptr), inKeysTable(nullptr), outKeysModel(nullptr), outKeysTable(nullptr),
    isDefaultQuery(!query.empty())
{

    auto edgeNames = jsonio::ioSettings().Schema()->getEdgesList();
    jsonio::jsonioErrIf(edgeNames.empty(), "EdgesWidget", "No edges defined");

    ui->setupUi(this);
    ui->keySplitter->setStretchFactor(0, 1);
    ui->mainSplitter->setStretchFactor(0, 0);
    ui->mainSplitter->setStretchFactor(1, 0);
    ui->mainSplitter->setStretchFactor(2, 1);
    ui->typeBox->setEnabled(!isDefaultQuery);

    setAttribute(Qt::WA_DeleteOnClose); // automatically delete itself when window is closed
    QString title = qApp->applicationName()+" Structured Data Editor and Database Browser";
    setWindowTitle(title);

    // set up default data
    if( curSchemaName.empty() && !edgeNames.empty() )
            curSchemaName = edgeNames[0];

    //set up main parameters
    curRecord_ = "";

    //define schema checkbox
    for( uint ii=0; ii<edgeNames.size(); ii++ )
        ui->typeBox->addItem(edgeNames[ii].c_str());
    ui->typeBox->setCurrentText(curSchemaName.c_str());

    // define edit tree view
    QStringList aHeaderData;
    aHeaderData << "key" << "value"  << "comment" ;
    fieldTable =  new TJsonView(  ui->bsonWidget );
    model_schema = new TJsonSchemaModel(  curRecord_,
                                          curSchemaName, aHeaderData, this/*ui->centralWidget*/ );
    deleg_schema = new TJsonSchemaDelegate();
    fieldTable->setModel(model_schema);
    fieldTable->setItemDelegate(deleg_schema);
    fieldTable->setColumnWidth( 0, 150 );
    fieldTable->expandToDepth(0);
    ui->gridLayout->addWidget(fieldTable, 1, 0, 1, 2);
    CmNew(); // set up default data

    // define all keys tables
    keysTable = new TKeyTable( ui->keySplitter,[&]( const QModelIndex& index ){openRecordKey( index );});
    ui->keySplitter->insertWidget(0, keysTable);
    ui->keySplitter->setStretchFactor(0, 4);
    connect( keysTable, SIGNAL( clicked(const QModelIndex& ) ), this, SLOT(openRecordKey( const QModelIndex& )));

    // define table of incoming keys
    inKeysTable = new TKeyTable(ui->inWidget,
                                []( const QModelIndex&  ){;} );
    ui->verticalLayout_2->addWidget(inKeysTable);
    connect( inKeysTable, SIGNAL( doubleClicked(const QModelIndex& ) ),
             this, SLOT(addIncoming( const QModelIndex& )));

    // define table of outgoing keys
    outKeysTable = new TKeyTable(ui->outWidget,
                                 []( const QModelIndex&  ){;} );
    ui->verticalLayout->addWidget(outKeysTable);
    connect( outKeysTable, SIGNAL( doubleClicked(const QModelIndex& ) ),
             this, SLOT(addOutgoing( const QModelIndex& )));


    // define menu
    setActions();
    waitDialog = new WaitingSpinnerWidget(this, true/*Qt::ApplicationModal*/, true);

    // Open db connection and load keys table
    resetDBClient( curSchemaName );
}

EdgesWidget::~EdgesWidget()
{
    dbThread.quit();
    dbThread.wait();

    if(dbKeysModel)
        delete dbKeysModel;
    if( keysTable )
        delete keysTable;

    // free incoming table data
    if(inKeysModel)
        delete inKeysModel;
    if( inKeysTable )
        delete inKeysTable;

    // free outgoing table data
    if(outKeysModel)
        delete outKeysModel;
    if( outKeysTable )
        delete outKeysTable;

    if( waitDialog )
        delete waitDialog;

    delete ui;
    //cout << "~EdgesWidget" << endl;
}

//-----------------------------------------------------

void EdgesWidget::resetTypeBox( const QString& text )
{
    disconnect( ui->typeBox, SIGNAL(currentTextChanged(const QString&)),
                this, SLOT(typeChanged(const QString&)));
    ui->typeBox->setCurrentText(text);
    pLineTask->setText(text);
    curSchemaName = text.toStdString();
    connect( ui->typeBox, SIGNAL(currentTextChanged(const QString&)),
             this, SLOT(typeChanged(const QString&)));
}


bool EdgesWidget::resetBson( const std::string& curRecord, const std::string& schemaName )
{
    // test legal schema name
    if( !curSchemaName.empty() &&
            !schemaName.empty() && schemaName != curSchemaName )
        return false;
    fieldTable->updateModelData( curRecord, curSchemaName );
    fieldTable->update();
    return true;
}

void EdgesWidget::openRecordKey(  const QModelIndex& rowindex , bool resetInOutQuery  )
{
    // find key
    int row = rowindex.row();
    QModelIndex index = rowindex.sibling( row, 0);
    std::string key = keysTable->model()->data(index).toString().toStdString();

    // Read Record
    openRecordKey(  key, resetInOutQuery  );
}

void EdgesWidget::openRecordKey(  const std::string& reckey, bool  )
{
    emit cmReadDocument( reckey.c_str() );
}

void EdgesWidget::changeKeyList()
{
    std::string key = getKeyFromDom( model_schema->getDom() );
    dbKeysModel->resetModel( key.c_str() );
    // select lines into lists
    std::string inV, outV;
    model_schema->getDom()->findValue( "_to", inV );
    model_schema->getDom()->findValue( "_from", outV );
    inKeysModel->resetModel( inV.c_str() );
    outKeysModel->resetModel( outV.c_str() );
}

/// Change current View menu selections
void EdgesWidget::updtViewMenu()
{
    ui->action_Show_comments->setChecked( TJsonSchemaModel::showComments );
    ui->action_Display_enums->setChecked(TJsonSchemaModel::useEnumNames);
    ui->action_Edit_id->setChecked(TJsonSchemaModel::editID );
    ui->actionKeep_Data_Fields_Expanded->setChecked(TJsonView::expandedFields );
}

/// Change model ( show/hide comments)
void EdgesWidget::updtModel()
{
    model_schema->updateModelData();
    fieldTable->header()->resizeSection(0, 150);
    fieldTable->header()->resizeSection(1, 250);
    fieldTable->expandToDepth(0);
}

/// Change table (Show Enum Names Instead of Values)
void EdgesWidget::updtTable()
{
    fieldTable->hide();
    fieldTable->show();
}

/// Update after change DB locations
void EdgesWidget::updtDB()
{
    changeKeyList();
    CmNew();
}

void EdgesWidget::addIncoming(  const QModelIndex& rowindex )
{
    // find key
    int row = rowindex.row();
    QModelIndex index = rowindex.sibling( row, 0);
    std::string key = index.model()->data(index).toString().toStdString();

    std::string id_;
    // only to unsaved records
    if( !model_schema->getDom()->findValue("_id",id_) || id_.empty()  )
    {
        model_schema->setValue("_to", key);
        fieldTable->hide();
        fieldTable->show();
    }
}

void EdgesWidget::addOutgoing(  const QModelIndex& rowindex )
{
    // find key
    int row = rowindex.row();
    QModelIndex index = rowindex.sibling( row, 0);
    std::string key = index.model()->data(index).toString().toStdString();

    std::string id_;
    // only to unsaved records
    if( !model_schema->getDom()->findValue("_id",id_) || id_.empty()  )
    {
        model_schema->setValue("_from",key);
        fieldTable->hide();
        fieldTable->show();
    }
}

// new stuff ----------------------------------------------------

// Get current record key from bson structure
std::string EdgesWidget::getKeyFromDom( const jsonio::JsonDom* domdata ) const
{
    std::string keyStr = "";
    if( !domdata->findKey( "_id", keyStr ) )
        keyStr = "";
    return keyStr;
}

// Exception when database command execution
void EdgesWidget::get_exception( const QString& titl, const QString& msg )
{
    QMessageBox::critical( this, titl, msg );
}

// After open new document
void EdgesWidget::openDocument(  QString jsonDocument  )
{
    try{
        curRecord_ = jsonDocument.toStdString();
        resetBson( curRecord_, "" );
        std::string key = getKeyFromDom( model_schema->getDom() );
        dbKeysModel->movetoKey( key );

        // select lines into lists
        std::string inV, outV;
        model_schema->getDom()->findValue( "_to", inV );
        model_schema->getDom()->findValue( "_from", outV );
        inKeysModel->movetoKey( inV );
        outKeysModel->movetoKey( outV );

        contentsChanged = false;
    }
    catch( jsonio::jsonio_exception& e)
    {
        QMessageBox::critical( this, e.title(), e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}


/// Change current Edge
void EdgesWidget::typeChanged( const QString& text )
{
    try {
        pLineTask->setText(text);
        curSchemaName = text.toStdString();
        curRecord_="";
        model_schema->setupModelData( curRecord_, curSchemaName );
        fieldTable->expandToDepth(0);
        startProcess();
        emit cmResetSchema( text );
    }
    catch(jsonio::jsonio_exception& e)
    {
        QMessageBox::critical( this, e.title(), e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

std::string  EdgesWidget::extractSchemaFromId( const std::string& id  )
{
    std::queue<std::string> names = jsonio::split(id, "/");
    if(names.size()<=1)
      return "";

    std::string label = names.front();
    label.pop_back(); // delete last "s"
    return jsonio::ioSettings().Schema()->getVertexName( label );
}

void EdgesWidget::resetDBClient(const std::string& schemaName )
{
    qRegisterMetaType<QVector<QString> >("QVector<QString>");
    qRegisterMetaType<std::vector<std::string> >("std::vector<std::string>");
    qRegisterMetaType<std::shared_ptr<jsonio::DBQueryDef> >("std::shared_ptr<jsonio::DBQueryDef>");
//    qRegisterMetaType<std::shared_ptr<jsonio::ImpexFormatFile> >("std::shared_ptr<jsonio::ImpexFormatFile>");

  try{

        dbKeysModel = new DBKeysListModel( nullptr, keysTable, this );
        inKeysModel = new DBKeysListModel( nullptr, inKeysTable, this );
        outKeysModel = new DBKeysListModel( nullptr, outKeysTable, this );

        dbDocumentModel = new DBEdgeDocumentModel(  schemaName, defaultQuery  );


        connect( dbKeysModel, &DBKeysListModel::updateQuery, dbDocumentModel, &DBEdgeDocumentModel::dbChangeQuery );
        connect( inKeysModel, &DBKeysListModel::updateQuery, dbDocumentModel, &DBEdgeDocumentModel::dbChangeIncomingQuery );
        connect( outKeysModel, &DBKeysListModel::updateQuery, dbDocumentModel, &DBEdgeDocumentModel::dbChangeOutgoingQuery );

        connect( dbDocumentModel, &DBEdgeDocumentModel::changedDBClient,  dbKeysModel, &DBKeysListModel::changeDBClient );
        connect( dbDocumentModel, &DBEdgeDocumentModel::changedIncomingDBClient,  inKeysModel, &DBKeysListModel::changeDBClient );
        connect( dbDocumentModel, &DBEdgeDocumentModel::changedOutgoingDBClient,  outKeysModel, &DBKeysListModel::changeDBClient );

      connect( dbDocumentModel, &DBEdgeDocumentModel::changedModel,  dbKeysModel, &DBKeysListModel::resetModel );
      connect( dbDocumentModel, &DBEdgeDocumentModel::changedIncomingModel,  inKeysModel, &DBKeysListModel::resetModel );
      connect( dbDocumentModel, &DBEdgeDocumentModel::changedOutgoingModel,  outKeysModel, &DBKeysListModel::resetModel );

      // link from GUI

      connect( this, &EdgesWidget::cmResetClient, dbDocumentModel, &DBEdgeDocumentModel::dbResetClient );
      connect( this, &EdgesWidget::cmResetSchema, dbDocumentModel, &DBEdgeDocumentModel::dbResetSchema );
      connect( this, &EdgesWidget::cmReadDocument, dbDocumentModel, &DBEdgeDocumentModel::dbReadDocument );
      connect( this, &EdgesWidget::cmUpdate, dbDocumentModel, &DBEdgeDocumentModel::dbUpdate );
      connect( this, &EdgesWidget::cmDelete, dbDocumentModel, &DBEdgeDocumentModel::dbDelete );
      connect( this, &EdgesWidget::cmDeleteList, dbDocumentModel, &DBEdgeDocumentModel::dbDeleteList );
      connect( this, &EdgesWidget::cmRestoreMultipleRecordsfromFile, dbDocumentModel, &DBEdgeDocumentModel::dbRestoreMultipleRecordsfromFile );
      connect( this, &EdgesWidget::cmBackupQueriedRecordstoFile, dbDocumentModel, &DBEdgeDocumentModel::dbBackupQueriedRecordstoFile );
      //connect( this, &VertexWidget::cmImportFormat, dbDocumentModel, &DBDocumentModel::dbImportFormat );
      //connect( this, &VertexWidget::cmExportFormat, dbDocumentModel, &DBDocumentModel::dbExportFormat );
      connect( this, &EdgesWidget::cmBackupGraphtoFile, dbDocumentModel, &DBEdgeDocumentModel::dbBackupGraphtoFile );
      connect( this, &EdgesWidget::cmRestoreGraphfromFile, dbDocumentModel, &DBEdgeDocumentModel::dbRestoreGraphfromFile );
      connect( this, &EdgesWidget::cmReloadQuery, dbDocumentModel, &DBEdgeDocumentModel::dbReloadQuery );

      // link to GUI

      connect( dbDocumentModel, &DBEdgeDocumentModel::changedQuery, this, &EdgesWidget::changeDBClientData );
      connect( dbDocumentModel, &DBEdgeDocumentModel::finished, this, &EdgesWidget::finishProcess );
      connect( dbDocumentModel, &DBEdgeDocumentModel::isException, this, &EdgesWidget::get_exception );
      connect( dbDocumentModel, &DBEdgeDocumentModel::updatedOid, this, &EdgesWidget::updateOid );

      connect( dbDocumentModel, &DBEdgeDocumentModel::openedDocument, this, &EdgesWidget::openDocument );
      connect( dbDocumentModel, &DBEdgeDocumentModel::deletedDocument, this, &EdgesWidget::afterDeleteDocument );
      connect( dbDocumentModel, &DBEdgeDocumentModel::loadedGraph, this, &EdgesWidget::afterLoadGraph );

      connect( dbDocumentModel, &DBEdgeDocumentModel::resetTypeBox, this, &EdgesWidget::resetTypeBox );

      connect( dbDocumentModel, &DBEdgeDocumentModel::fixIncoming,
               [&]() { ui->actionIncoming_Vertex_Query->setDisabled(true); } );
      connect( dbDocumentModel, &DBEdgeDocumentModel::fixOutgoing,
               [&]() { ui->actionOutgoing_Vertex_Query->setDisabled(true); } );

     // thread functions

      dbDocumentModel->moveToThread(&dbThread);
      connect(&dbThread, &QThread::finished, dbDocumentModel, &QObject::deleteLater);
      dbThread.start();

      // might be execute signals it other thread

      startProcess();

      emit cmResetClient( schemaName.c_str() );

      /*if(  keysTable->model()->rowCount() > 1 )
          openRecordKey( keysTable->model()->index(0,0) ); // read first line */

   }
   catch(std::exception& e)
      {
         std::cout << "Internal comment: " << e.what() << std::endl;
         throw;
      }

}

/// Update db document query
void EdgesWidget::changeDBClientData(  QString query )
{
      ui->edgeQuery->setText( query );
}

/// Start long database process
void EdgesWidget::startProcess()
{
    // possible open Waiting Spinner
    std::cout << "startProcess "  << std::endl;
    waitDialog->start();
}

/// Finish long database process
void EdgesWidget::finishProcess()
{
    // possible close Waiting Spinner
    waitDialog->stop();
    std::cout << "finishProcess "  << std::endl;
}

} // namespace jsonui
