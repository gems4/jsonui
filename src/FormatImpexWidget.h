//  This is JSONUI library+API (https://bitbucket.org/gems4/jsonui)
//
/// \file FormatImpexWidget.h
/// Declaration of FormatImpexWidget - work with Foreign Import/Export File Format
//
// JSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONUI depends on the following open-source software products:
// JSONIO (https://bitbucket.org/gems4/jsonio); Qt5 (https://qt.io).
//

#ifndef IMPEXWIDGET_H
#define IMPEXWIDGET_H

#include <QCloseEvent>
#include "JSONUIBase.h"
#include "model_schema.h"
#include "jsonio/dbschemadoc.h"

namespace jsonio {
   class ImpexFormatFile;
}

namespace Ui {
class FormatImpexWidget;
}

namespace jsonui {

using  ExecuteFunction = std::function<void( std::shared_ptr<jsonio::ImpexFormatFile> inputIEFile,
                                             std::string dataFile, bool overwrite )>;

jsonio::TDBSchemaDocument* newDBImpexClient(  const jsonio::DBQueryData& query  );

#ifndef IMPEX_OFF

/// Read impex format from database or file
jsonio::ImpexFormatFile*  readFormatStructDataFile( int mode, bool useDB,
     jsonio::TDBSchemaDocument *formatdb,  const std::string& format, const std::string& impexSchemaName );

#endif

/*jsonio::StructDataIEFile* connectFormatStructDataFile( int amode,
                  const jsonio::ThriftSchema* schema, const jsonio::JsonDom* object, const std::string& lua_lib_path );
jsonio::KeyValueIEFile* connectFormatKeyValueFile( int amode,
         const jsonio::ThriftSchema* schema, const jsonio::JsonDom* object, const std::string& lua_lib_path );
jsonio::TableIEFile* connectFormatTableFile( int amode,
         const jsonio::ThriftSchema* schema, const jsonio::JsonDom* object, const std::string& lua_lib_path );

jsonio::AbstractIEFile* connectFormatDataFile( int amode, const std::string& impexSchemaName, const jsonio::JsonDom* object );
*/

/// \class FormatImpexWidget - Widget to work with Foreign Import/Export File Format
class FormatImpexWidget : public JSONUIBase
{
    Q_OBJECT

    // Internal data
    int _mode;
    //std::string curSchemaName = ""; ///< Current impex format
    const std::string curCollectionName = "impexdefs";
    std::shared_ptr<jsonio::TDBSchemaDocument> dbquery;
    std::string curRecord_;
    bool contentsChanged = false;
    std::string parentSchemaName;

    // Work functions
    ExecuteFunction _execF;

    /// Set up menu commands
    void setActions();
    /// Set up current bson data to view model
    bool resetBson( const std::string& curRecord, const std::string& schemaName );

    void closeEvent(QCloseEvent* e);

    // update after change preferences
    virtual void updtViewMenu();
    virtual void updtModel();
    virtual void updtTable();
    virtual void updtDB();

    void resetDBClient();
    std::string impexToJson();
    void impexFromJson( const jsonio::JsonDom *obj );
#ifndef IMPEX_OFF
    jsonio::ImpexFormatFile* connectFormatFile( const jsonio::JsonDom* object );
#endif

public slots:

    // internal slots
    void openRecordKey(  const std::string& , bool  = false  ) {}
    void openRecordKey(  const QModelIndex&  ) {}
    void changeKeyList() {}
    void objectChanged()
       { contentsChanged = true; }
    void schemaChanged(const QString & text);

    // File
    void CmNew();
    void CmClone();
    void CmEditID(bool);

    void CmExportJSON();
    void CmImportJSON();
    void CmSelectDataFile();
    void CmRunScript();

    // Record
    void CmRead();
    void CmUpdate();
    void CmDelete();

public:

    enum MODE_ {
         editMode = 0, runModeImport = 1, runModeExport = 2
       };

    explicit FormatImpexWidget( int mode, const std::string& aschemaName = "", QWidget *parent = nullptr);
    ~FormatImpexWidget();

    /// Set function to execute script
    void setExecuteFunction(ExecuteFunction afunc )
    {
       _execF = afunc;
    }

    void setQuery( QueryWidget*  ) {}

private:

    Ui::FormatImpexWidget *ui;

    // tree view
    QStringList aHeaderData;
    TJsonSchemaModel* model_schema;
    QItemDelegate* deleg_schema;
    TJsonView* fieldTable;

};

} // namespace jsonui

#endif // IMPEXWIDGET_H
