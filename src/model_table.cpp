//  This is JSONUI library+API (https://bitbucket.org/gems4/jsonui)
//
/// \file model_table.cpp
/// Implementation of classes TAbstractDataContainer, TMatrixModel,
/// TMatrixDelegate and TMatrixTable - implements flexible approach
/// provided by Qt's model/view architecture according TAbstractDataContainer
/// interface.
//
// JSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONUI depends on the following open-source software products:
// JSONIO (https://bitbucket.org/gems4/jsonio); Qt5 (https://qt.io).
//

#include <iostream>
#include <QMenu>
#include <QApplication>
#include <QClipboard>
#include <QHeaderView>
#include <QKeyEvent>
#include <QLineEdit>
#include <QTextStream>
#include "model_table.h"
#include "model_view.h"
#include "CalcDialog.h"
#include "TCSVPage.h"

namespace jsonui {


QVector<QString> fromStdStringVector(const std::vector<std::string>& strvector )
{
    QVector<QString> tmp;

    for( auto row: strvector)
        tmp.append(QString(row.c_str()));
    return tmp;
}

std::vector<std::string> toStdStringVector(const QVector<QString>& strvector )
{
    std::vector<std::string> tmp;

    for( auto row: strvector)
        tmp.push_back(row.toStdString());
    return tmp;
}

QVector<QVector<QString>> fromStdStringTable(const jsonio::ValuesTable& strtable )
{
    QVector<QVector<QString>> tmp;

    for( auto row: strtable)
        tmp.append(fromStdStringVector( row ));
    return tmp;
}

jsonio::ValuesTable toStdStringTable(const QVector<QVector<QString>>& strtable )
{
    jsonio::ValuesTable tmp;

    for( auto row: strtable)
        tmp.push_back(toStdStringVector(row));
    return tmp;
}

/// Internal data for selection
class Selection
{
  public:

    int N1;
    int N2;
    int M1;
    int M2;

    Selection(int n1, int n2, int m1, int m2 ):
     N1(n1), N2(n2), M1(m1), M2(m2)
     {}

    Selection(const Selection& sel):
     N1(sel.N1), N2(sel.N2), M1(sel.M1), M2(sel.M2)
     {}

};

TAbstractDataContainer::~TAbstractDataContainer()
{}

StringTable::~StringTable()
{}

//--------------------------------------------------------------------------------------
//  class TMatrixModel

QVariant TMatrixModel::data( const QModelIndex& index, int role ) const
{
    if(!index.isValid())
        return QVariant();

    switch( role )
    {
    case TypeRole:
        return datacontainer->getType( index.row(), index.column() );
    case Qt::DisplayRole:
    case Qt::EditRole:
        return datacontainer->data( index.row(), index.column() );
    case Qt::ToolTipRole:
    case Qt::StatusTipRole:
        return datacontainer->getToolTip( index.row(), index.column() );
    case  Qt::ForegroundRole:
        if( datacontainer->useColor( index.row(), index.column() ) )
            return datacontainer->getColor( index.row(), index.column()  );
        break;
    case Qt::TextAlignmentRole:
    {
        int type_ = datacontainer->getType( index.row(), index.column() );
        if( type_ == ftNumeric || type_ == ftDouble )
            return int(Qt::AlignRight | Qt::AlignVCenter);
        else return int(Qt::AlignLeft | Qt::AlignVCenter);
    }
    case  Qt::SizeHintRole:
    default: break;
    }
    return QVariant();
}

bool TMatrixModel::setData( const QModelIndex &index, const QVariant& value, int role)
{
    if( index.isValid() && ( role == Qt::EditRole ) )
	{
       datacontainer->setData( index.row(), index.column(), value );
       emit dataChanged(index, index);
       return true;
	} 
	return false;
}

QVariant TMatrixModel::headerData( int section, Qt::Orientation orientation, int role ) const
{
    switch( role )
    { case Qt::DisplayRole:
         if( orientation == Qt::Horizontal )
             return datacontainer->headerData( section );
          else
          return QVariant(section);

        case Qt::TextAlignmentRole:
               return int(Qt::AlignRight | Qt::AlignVCenter);
        default:
        break;
     }

    return QVariant();
}

Qt::ItemFlags TMatrixModel::flags( const QModelIndex & index ) const
{
    Qt::ItemFlags flags = QAbstractTableModel::flags(index);
    if( datacontainer->IsEditable( index.row(), index.column() )  )
      {  flags |= Qt::ItemIsEditable;
         return flags;
     }
   return (flags & ~Qt::ItemIsEditable);
}

// internal part
void TMatrixModel::resetMatrixData()
{
    beginResetModel();
    datacontainer->resetData();
    endResetModel();
}

//-------------------------------------------------------------------------------------
// TMatrixDelegate -  individuak items in views are rendered and edited using delegates
//-------------------------------------------------------------------------------------

// Editing QTreeView for objects in System page
QWidget *TMatrixDelegate::createEditor(QWidget *parent,
        const QStyleOptionViewItem &option,
        const QModelIndex &index) const
{
    int type = index.model()->data(index, TypeRole).toInt();
    switch( type  )
    {
          case ftNumeric:
            {   QLineEdit *lineEdit = new QLineEdit(parent);
                lineEdit->setValidator(new QIntValidator);
               return lineEdit;
             }
          case ftDouble:
               { QLineEdit *lineEdit = new QLineEdit(parent);
                 lineEdit->setValidator(new QDoubleValidator);
                 //lineEdit->setValidator(new QDoubleValidator(-999.0,
                 //            999.0, 2, lineEdit));
                 return lineEdit;
               }
          case ftString:
              { QLineEdit *lineEdit = new QLineEdit(parent);
                return lineEdit;
              }
         default:return QAbstractItemDelegate::createEditor( parent, option,  index );
   }
}

//-------------------------------------------------------------------------------------
// class TMatrixTable implements a table view that displays items from a model.
//-------------------------------------------------------------------------------------

TMatrixTable::TMatrixTable( QWidget * parent, int mode  ):
     QTableView( parent ), _modeUsing(mode)
{

#if QT_VERSION >= 0x050000

//    verticalHeader()->setSectionResizeMode( /*QHeaderView::ResizeToContents*/QHeaderView::Stretch );
//    horizontalHeader()->setSectionResizeMode( /*QHeaderView::ResizeToContents*/QHeaderView::Stretch );
     verticalHeader()->setSectionResizeMode( QHeaderView::ResizeToContents );
     horizontalHeader()->setSectionResizeMode( QHeaderView::ResizeToContents );
#else
    verticalHeader()->setResizeMode( QHeaderView::ResizeToContents/*QHeaderView::Stretch*/ );
//    horizontalHeader()->setResizeMode( QHeaderView::ResizeToContents/*QHeaderView::Stretch*/ );

#endif

    //setFocusPolicy(Qt::WheelFocus/*Qt::StrongFocus*/);
    //setTabKeyNavigation( false );

    setVerticalScrollMode( QAbstractItemView::ScrollPerItem );
    setHorizontalScrollMode( QAbstractItemView::ScrollPerItem );
    setEditTriggers( QAbstractItemView::DoubleClicked|QAbstractItemView::AnyKeyPressed );
    setSortingEnabled( _modeUsing & tbSort );

    setContextMenuPolicy(Qt::CustomContextMenu);
    if( !(_modeUsing & tbNoMenu) )
      connect( this, SIGNAL(customContextMenuRequested(QPoint)),
            this, SLOT(slotPopupContextMenu(QPoint)));
}

void TMatrixTable::makePopupContextMenu(QMenu *menu, QModelIndex index )
{
    QAction* act;
    /*act =  new QAction(tr("&Help"), this);
    act->setIcon(QIcon(":/menu/Icons/ShowHelpWindowIcon.png"));
    act->setShortcut(tr("F1"));
    act->setStatusTip(tr("Help to the specified cell"));
    connect(act, SIGNAL(triggered()), this, SLOT(CmHelp()));
    menu->addAction(act);*/

    if( _modeUsing & tbEdit )
    {
      int type_ = model()->data(index, TypeRole ).toInt();
      if( type_ == ftNumeric || type_ == ftDouble  )
      {
        act =  new QAction(tr("&Calculator"), this);
        act->setShortcut(tr("F8"));
        act->setStatusTip(tr("Use calculator  to the specified cells"));
        connect(act, SIGNAL(triggered()), this, SLOT(CmCalc()));
        menu->addAction(act);
        menu->addSeparator();
      }
    }

    act =  new QAction(tr("Select &row"), this);
    act->setShortcut(tr("Ctrl+R"));
    act->setStatusTip(tr("Select current row"));
    connect(act, SIGNAL(triggered()), this, SLOT(SelectRow()));
    menu->addAction(act);

    act =  new QAction(tr("Select co&lumn"), this);
    act->setShortcut(tr("Ctrl+L"));
    act->setStatusTip(tr("Select current column"));
    connect(act, SIGNAL(triggered()), this, SLOT(SelectColumn()));
    menu->addAction(act);

    act =  new QAction(tr("Select &all"), this);
    act->setShortcut(tr("Ctrl+A"));
    act->setStatusTip(tr("Select all cells"));
    connect(act, SIGNAL(triggered()), this, SLOT(SelectAll()));
    menu->addAction(act);

    menu->addSeparator();

    act =  new QAction(tr("&Copy"), this);
    act->setShortcut(tr("Ctrl+C"));
    act->setStatusTip(tr("Copy selected cells"));
    connect(act, SIGNAL(triggered()), this, SLOT(CopyData()));
    menu->addAction(act);

    act =  new QAction(tr("Copy wit&h names"), this);
    act->setShortcut(tr("Ctrl+H"));
    act->setStatusTip(tr("Copy selected header&cells"));
    connect(act, SIGNAL(triggered()), this, SLOT(CopyDataHeader()));
    menu->addAction(act);

    if( _modeUsing & tbEdit )
    {
       menu->addSeparator();

       act =  new QAction(tr("C&ut"), this);
       act->setShortcut(tr("Ctrl+U"));
       act->setStatusTip(tr("Cut selected cells"));
       connect(act, SIGNAL(triggered()), this, SLOT(CutData()));
       menu->addAction(act);

       act =  new QAction(tr("&Paste"), this);
       act->setShortcut(tr("Ctrl+V"));
       act->setStatusTip(tr("Paste to selected area"));
       connect(act, SIGNAL(triggered()), this, SLOT(PasteData()));
       menu->addAction(act);

       act =  new QAction(tr("Paste &transposed"), this);
       act->setShortcut(tr("Ctrl+T"));
       act->setStatusTip(tr("Paste transposed to selected area"));
       connect(act, SIGNAL(triggered()), this, SLOT(PasteTransposedData()));
       menu->addAction(act);

       act =  new QAction(tr("&Clear"), this);
       act->setShortcut(tr("Ctrl+Del"));
       act->setStatusTip(tr("Clear selected cells"));
       connect(act, SIGNAL(triggered()), this, SLOT(ClearData()));
       menu->addAction(act);
    }
}

 void TMatrixTable::slotPopupContextMenu(const QPoint &pos)
 {
     if( _modeUsing & tbNoMenu )
       return;

     QModelIndex index = indexAt( pos );

     QMenu *menu = new QMenu(this);
     makePopupContextMenu( menu, index );
     menu->exec( viewport()->mapToGlobal(pos) );
     delete menu;
 }

 void TMatrixTable::keyPressEvent(QKeyEvent* e)
 {
     if( _modeUsing & tbNoMenu )
     {
       QTableView::keyPressEvent(e);
       return;
     }

    if ( e->modifiers() & Qt::ControlModifier ) 
    {
		switch ( e->key() ) 
		{
          case Qt::Key_R:
            SelectRow();
            return;
          case Qt::Key_L:
            SelectColumn();
            return;
          case Qt::Key_A:
		    SelectAll();
		    return;
		  case Qt::Key_U:
		    CutData();
		    return;
          case Qt::Key_C:
            CopyData();
            return;
          case Qt::Key_H:
            CopyDataHeader();
            return;
          case Qt::Key_V:
            PasteData();
            return;
		  case Qt::Key_T:
            PasteTransposedData();
            return;
		  case Qt::Key_Delete:
		    ClearData();
            return;
		}
	}
    switch( e->key() )
 	{
//      case Qt::Key_F1:
//            CmHelp();
//            return;
      case Qt::Key_F8:
             CmCalc();
              return;
     }
 	QTableView::keyPressEvent(e);
 }


// Calculator on F8 pressed on data field
void TMatrixTable::CmCalc()
{
    if( !(_modeUsing & tbEdit) )
     return;

    QString res;
    CalcDialog calc( window() );
    if( calc.exec() )
    {
       foreach( QModelIndex ndx,  selectedIndexes()  )
       {
           res = calc.computeFunctionString( ndx.data(Qt::EditRole).toDouble() );
           model()->setData(ndx, res,  Qt::EditRole);
       }
   }
}

void TMatrixTable::SelectRow()
{
    selectionModel()->select( currentIndex(),
                QItemSelectionModel::Rows|QItemSelectionModel::ClearAndSelect );
 }


void TMatrixTable::SelectColumn()
{
    selectionModel()->select( currentIndex(),
                QItemSelectionModel::Columns|QItemSelectionModel::ClearAndSelect );
}

void TMatrixTable::SelectAll()
{
  selectAll();
}

void TMatrixTable::CutData()
{
  if( !(_modeUsing & tbEdit) )
     return;
  CopyData();
  ClearData();
}

void TMatrixTable::ClearData()
{
    if( !(_modeUsing & tbEdit) )
     return;

    foreach( QModelIndex ndx,  selectedIndexes()  )
           model()->setData(ndx, ""/*emptiness.c_str()*/,  Qt::EditRole);
}

 void TMatrixTable::CopyData()
 {
	Selection sel = getSelectionRange();
    QString clipText = createString( sel, splitCol );
    QApplication::clipboard()->setText(clipText/*, QClipboard::Clipboard*/);
 }

 void TMatrixTable::CopyDataHeader()
 {
    Selection sel = getSelectionRange();
    QString clipText = createHeader(splitCol);
    clipText += createString( sel, splitCol );
    QApplication::clipboard()->setText(clipText/*, QClipboard::Clipboard*/);
 }

 // Save selected data to csv format file
 void TMatrixTable::saveSelectedToCSV(const std::string& fileName)
 {
     Selection sel = getSelectionRange();
     QString clipText = createHeader(',');
     clipText += createString( sel, ',' );

     // save data to file
     QFile outFile(fileName.c_str());
     outFile.open(QIODevice::WriteOnly | QIODevice::Text);

     if(!outFile.isOpen()){
         std::cout << fileName << "- Error, unable to open for output" << std::endl;
     } else
      {
        QTextStream outStream(&outFile);
        outStream << clipText;
        outFile.close();
      }
 }


 void TMatrixTable::PasteData()
 {
     if( !(_modeUsing & tbEdit) )
      return;

    Selection sel = getSelectionRange( true );
    pasteIntoArea( sel, false, splitCol);
 }

 void TMatrixTable::PasteTransposedData()
 {
     if( !(_modeUsing & tbEdit) )
      return;

    Selection sel = getSelectionRange( true );
    pasteIntoArea( sel, true, splitCol );
 }
 
 //------------------------------------------------------------------------ 

 QString TMatrixTable::createHeader( char _splitCol )
 {
   QString cText;
   QString clipText;
   int col;
   bool frst = true;
   for( col = 0; col < model()->columnCount( rootIndex() ); col++ )
   {
       if( selectionModel()->columnIntersectsSelection( col,  rootIndex() ) )
       {
         if( !frst )
           clipText += _splitCol;
         frst = false;
         cText = model()->headerData( col, Qt::Horizontal, Qt::DisplayRole ).toString();
         if( cText.isEmpty() /* == emptiness.c_str()*/ )
             cText = "  ";//"\r";
         clipText += cText;
       }
    }
    if( !frst )
       clipText += splitRow;
   return clipText;
 }


 QString TMatrixTable::createString( Selection& sel, char _splitCol )
  {
      int ii, jj;
	  QModelIndex index = currentIndex();
	  QString cText;
	  QString clipText;
	  
	  for(  ii=sel.N1; ii<=sel.N2; ii++ )
	  {
		if( ii > sel.N1 )
          clipText += splitRow;
  
		for( jj=sel.M1; jj<=sel.M2; jj++ )
		{
		  QModelIndex wIndex = 	index.sibling( ii, jj );
		  // selected all region if( selmodel->isSelected( wIndex ) )
          if( jj > sel.M1 )
            clipText += _splitCol;
		  cText = wIndex.data(Qt::EditRole).toString();
          if( cText.isEmpty() /*cText == emptiness.c_str()*/ )
			  cText = "  ";//"\r"; 
	     clipText += cText;
		}
	 }
	return clipText;  
  }

 Selection TMatrixTable::getSelectionRange( bool paste_ )
 {
     QModelIndex index = currentIndex();

     if(  !index.isValid() )
         return Selection( 0, 0, 0, 0 );

     if( !paste_  && !selectionModel()->hasSelection()  )
         return Selection( index.row(), index.row(), index.column(), index.column() );

     int N1=-1, N2=0, M1=-1, M2=0;
     foreach( QModelIndex ndx,  selectedIndexes()  )
     {
         if( N1 == -1 || M1 == -1)
         {
             N1 = ndx.row();
             M1 = ndx.column();
         }
         if( N1 > ndx.row() ) N1 = ndx.row();
         if( N2 < ndx.row() ) N2 = ndx.row();
         if( M1 > ndx.column() ) M1 = ndx.column();
         if( M2 < ndx.column() ) M2 = ndx.column();
     }

     if( paste_ && ( !selectionModel()->hasSelection() ||
                     ( N1==N2 && M1==M2 ) ) ) // only one selected => all for end of table
         return Selection( index.row(), index.model()->rowCount( index )-1,
                           index.column(), index.model()->columnCount( index )-1 );

     return Selection( N1, N2, M1, M2 );
 }

 void  TMatrixTable::setFromString(char splitrow, const QString& str,
                                   Selection sel, bool transpose)
 {
     //QSortFilterProxyModel *sortmodel = (QSortFilterProxyModel *)currentIndex().model();
     //TMatrixModel* _model =(TMatrixModel *)sortmodel->sourceModel();

     if( str.isEmpty() )
         return;

     QModelIndex wIndex;
     const QStringList rows = str.split(splitrow, QString::KeepEmptyParts/*Qt::KeepEmptyParts*/);

     int ii, jj;
     int rowNum = sel.N1;
     const int nLimit = (transpose) ? (sel.N1 + sel.M2-sel.M1) : sel.N2;

     for( int it = 0; it < rows.count() && rowNum <= nLimit; it++, rowNum++)
     {
         //if( rows[it].isEmpty() ) sd 29/10/2008
         // continue;

         const QStringList cells = rows[it].split('\t', QString::KeepEmptyParts/*Qt::KeepEmptyParts*/);
         int cellNum = sel.M1;
         const int mLimit = (transpose) ? (sel.M1 + sel.N2-sel.N1) : sel.M2;
         for( int cellIt = 0;  cellIt < cells.count() && cellNum <= mLimit; cellIt++, cellNum++)
         {
             QString str_ = cells[ cellIt ];
             str_ = str_.trimmed();  // strip
             //if( value.empty() /*|| value == emptiness*/ )
             //    value = "";//"---";

             if( transpose )
             { ii = (cellNum-sel.M1)+sel.N1;
                 jj = (rowNum-sel.N1)+sel.M1;
             }
             else
             { ii = rowNum;
                 jj = cellNum;
             }
             wIndex = currentIndex().sibling( ii, jj );
             model()->setData(wIndex, str_, Qt::EditRole);
         }
     }
 }

  void TMatrixTable::pasteIntoArea( Selection& sel, bool transpose, char _splitCol )
  {
      QString clipboard = QApplication::clipboard()->text(QClipboard::Clipboard);
      char splitrow = splitRow;

      int lastCR = clipboard.lastIndexOf(splitrow);
      if( lastCR < 0 )
      {
#ifdef __APPLE__
       splitrow = '\n';
#else
      splitrow = '\r';
#endif
         lastCR = clipboard.lastIndexOf(splitrow);
      }
      if( lastCR == clipboard.length() - 1 )
  	  clipboard.remove(lastCR, 1);
      QString undoString;

         const QStringList rows = clipboard.split(splitrow);
         const int clipN = rows.count();
         const bool largerN = transpose ?
           (clipN > (sel.M2 - sel.M1 + 1)) : (clipN > (sel.N2 - sel.N1 +1 ));
      
         int rowNum = sel.N1;
     	 bool largerM = false;
         for(int it = 0; it < rows.count(); it++, rowNum++)
  	     {
           int clipM = rows[it].count(_splitCol) + 1;
  	       largerM = transpose ? (clipM > (sel.N2 - sel.N1 + 1 )) : (clipM > (sel.M2 - sel.M1 + 1));
  	       if( largerM )
  		      break;
  	     }
         if( largerN || largerM )
         {
           std::cout <<  "Object paste" <<
            "Pasting contents has larger dimensions then the object!" << std::endl;
         }
         undoString = createString( sel, splitCol );
         setFromString(splitrow, clipboard, sel, transpose);
  }


  // Get header data
  QVector< QString > TMatrixTable::headerData() const
  {
      QVector< QString > headerD;

      for(int col = 0; col < model()->columnCount(); col++ )
          headerD.push_back(model()->headerData( col, Qt::Horizontal, Qt::DisplayRole ).toString());

      return headerD;
  }

  // Get table data
  QVector< QVector<QVariant> > TMatrixTable::tableValues() const
  {
      QVector< QVector<QVariant> > valuesD;

      for( int  row =0; row< model()->rowCount(); row++)
      {
          QVector<QVariant> rowData;
          for(int col = 0; col < model()->columnCount(); col++ )
              rowData.push_back( model()->data( model()->index(row, col) ) );
          valuesD.push_back(rowData);
      }
      return valuesD;
  }

  // -----------------------------------------------  TMatrixUniqueSelection

  TMatrixTableProxy::TMatrixTableProxy(QWidget *parent, int mode):
      TMatrixTable( parent, mode)
  { }

  void TMatrixTableProxy::setModel(QAbstractItemModel *model)
  {
      if( _modeUsing & TMatrixTable::tbSort)
      {
          TSortFilterProxyModel *proxyModel = new TSortFilterProxyModel();
          proxyModel->setSourceModel( model );
          TMatrixTable::setModel(proxyModel);
      }
      else
      {
          TMatrixTable::setModel(model);
      }
  }

  void TMatrixTableProxy::setCurrentRow(int row)
  {
      if( model()->rowCount() <= 0 )
          return;
      if(  row > model()->rowCount()  )
          row = 0;

      if( _modeUsing & TMatrixTable::tbSort)
      {
        TSortFilterProxyModel *proxyModel = dynamic_cast<TSortFilterProxyModel *>(model());
        if( proxyModel )
         {
            setCurrentIndex(  proxyModel->mapFromSource( proxyModel->sourceModel()->index(row,0) ) );
            return;
         }
      }
      setCurrentIndex( model()->index(row,0) );
  }

  std::size_t TMatrixTableProxy::getCurrentRow() const
  {
      auto selndx = selectedRow();
      jsonio::jsonioErrIf( selndx<0, "SelectionError", "Undefined single selection" );
      return static_cast<size_t>(selndx);
  }

  int TMatrixTableProxy::selectedRow() const
  {
      if( model()->rowCount() <= 0 || !currentIndex().isValid() )
          return -1;
      if( _modeUsing & TMatrixTable::tbSort)
      {
          TSortFilterProxyModel *proxyModel = dynamic_cast<TSortFilterProxyModel *>(model());
          if( proxyModel )
              return proxyModel->mapToSource(currentIndex()).row();
      }
      return currentIndex().row();
  }

  void TMatrixTableProxy::selectRows(const std::set<std::size_t> &rows)
  {
      TSortFilterProxyModel *proxyModel = nullptr;
      auto tmodel = model();
      if( _modeUsing & TMatrixTable::tbSort)
      {
          proxyModel = dynamic_cast<TSortFilterProxyModel *>(tmodel);
          if( proxyModel )
              tmodel =  proxyModel->sourceModel();
      }

      QItemSelection selitems;
      int startRow=-1, endRow=-1;
      for( int row: rows )
      {
          if( row >= tmodel->rowCount() )
              break;

          if( startRow == -1 )
              startRow = endRow = row;
          else
              if( endRow+1 == row )
                  endRow++;
              else
              {
                  selitems.merge(QItemSelection(tmodel->index(startRow, 0),
                                                tmodel->index(endRow, 0)),
                                 QItemSelectionModel::Select);
                  startRow = endRow = row;
              }
      }

      if(startRow != -1 )
          selitems.merge(QItemSelection(tmodel->index(startRow, 0),
                                        tmodel->index(endRow, 0)),
                         QItemSelectionModel::Select);

      if( (_modeUsing & TMatrixTable::tbSort) && proxyModel )
      {
          selitems =  proxyModel->mapSelectionFromSource(selitems);
      }

      selectionModel()->select(selitems,QItemSelectionModel::Rows|
                               QItemSelectionModel::ClearAndSelect );
  }

  std::set<std::size_t> TMatrixTableProxy::allSelectedRows()
  {
      std::set<std::size_t> arr;
      QItemSelection selitems = selectionModel()->selection();
      if( _modeUsing & TMatrixTable::tbSort)
      {
        TSortFilterProxyModel *proxyModel = dynamic_cast<TSortFilterProxyModel *>(model());
        if( proxyModel )
         selitems =  proxyModel->mapSelectionToSource(selitems);
       }
      QModelIndexList selection = selitems.indexes();

      // Multiple rows can be selected
      for(int i=0; i< selection.count(); i++)
      {
          QModelIndex index = selection.at(i);
          if(index.column() == 0 )
            arr.insert( index.row() );
      }
      return arr;
  }


  TMatrixUniqueSelection::TMatrixUniqueSelection( const std::vector<int>& colUniq, QWidget* parent, int mode ):
      TMatrixTableProxy( parent, mode), _columnUnique(colUniq)
  { }

   void TMatrixUniqueSelection::slotSelectionChange(const QItemSelection & selected, const QItemSelection &/*deselected*/)
   {
       bool equal;
       std::set<int> testedrows;
       std::set<int> rows;
       auto insexes = selected.indexes();
       for( auto ind: insexes )
           rows.insert(ind.row());

       QModelIndexList selection = selectionModel()->selectedRows();

       for( auto row_: rows )
       {
         for(int i=0; i< selection.count(); i++)
         {
           QModelIndex index = selection.at(i);

           if( index.row() == row_ || testedrows.find(index.row())!= testedrows.end() )
               continue;

           equal = true;
           for( auto colmn: _columnUnique )
           {
               if( index.sibling( index.row(),colmn).data() != index.sibling( row_, colmn).data()    )
               {
                 equal = false;
                 break;
               }

           }
           if( equal )
             selectionModel()->select(index, QItemSelectionModel::Rows|QItemSelectionModel::Deselect);
        }
        testedrows.insert(row_);
       }
  }


// -----------------------------------------------  List model

  QVariant TStringVectorModel::data(const QModelIndex &index, int role) const
  {
      if (index.row() < 0 || index.row() >= lst.size())
          return QVariant();

      if (role == Qt::DisplayRole || role == Qt::EditRole)
          return QString(lst[index.row()]);

      return QVariant();
  }

  bool TStringVectorModel::setData(const QModelIndex &index,
         const QVariant &value, int role )
  {
      if (index.row() >= 0 && index.row() < lst.size()
          && (role == Qt::EditRole || role == Qt::DisplayRole))
      {
          lst[index.row()] = value.toString().toUtf8().data();
          return true;
      }
      return false;
  }

  void TStringVectorModel::setStringList(const std::vector<std::string>& strs)
  {
      beginResetModel();
      lst =  fromStdStringVector(strs);
      endResetModel();
  }


} // namespace jsonui

//---------------------- End of file  model_table.cpp ---------------------------
