//  This is JSONUI library+API (https://bitbucket.org/gems4/jsonui)
//
/// \file preferences.h
/// Declaration of UIPreferences object for monitoring changes
//
// JSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2017 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONUI depends on the following open-source software products:
// JSONIO (https://bitbucket.org/gems4/jsonio); Qt5 (https://qt.io).
//

#ifndef PREFERENCES_H
#define PREFERENCES_H

#include <QObject>
#include "jsonio/io_settings.h"
#include "jsonio/dbconnect.h"

namespace jsonui {

void helpWin( const std::string& name, const std::string& item );

const std::string resourcesDatabaseName = "resources";

/// \class Preferences - monitoring preferences into JSONIO
class UIPreferences: public QObject
{
    friend UIPreferences& uiSettings();
    jsonio::JsonioSettings& _iosettings;

    jsonio::GroupSettings _jsonuiGroup;

    /// Current work Database
    std::shared_ptr<jsonio::TDataBase> work_database;
    /// Current resourse Database
    std::shared_ptr<jsonio::TDataBase> resourse_database;

    Q_OBJECT

    /// Read/Init main settings
    void setDefValues();

    ///  Create empty collections if they are not present (for all vertexes and edges)
    void createCollectionsIfNoExist( jsonio::TAbstractDBDriver* dbDriver);

    /// Constructor
    UIPreferences();

    /// Link new resource Driver
    void updateResourceDatabase();
    /// Create resource Database
    void createLocalDatabaseIfNoExist( const std::string& theDBname );

    bool testCreateLocalDatabase( const std::string& theDBname, const std::string& user );

signals:

    /// Changed Data Base
    void dbChanged();
    /// Changed thrift schemas Dir
    void schemaChanged();
    /// Signal to update "View" menu
    void viewMenuChanged();
    /// Signal to update editors model
    void modelChanged();
    /// Signal to update models tableview
    void tableChanged();
    /// Error editing settings
    void errorSettings(std::string);

public slots:

    /// Run update Preferences Dialog
    void CmSettingth();
    /// On/off extern flag to show schema comments into editor
    void CmShowComments( bool checked );
    /// On/off extern flag to show enum names into editor
    void CmDisplayEnums( bool checked );
    /// On/off extern flag to edit system data
    void CmEditID( bool checked );
    /// On/off extern flag to keep data fields expanded
    void CmEditExpanded( bool checked );
    /// Define new input/output directory
    void CmSetUserDir( QString dirPath);

public:

    void updateUiSettings()
    {
        setDefValues();
    }

    const jsonio::TDataBase* database() const
    {
        return work_database.get();
    }

    const std::shared_ptr<jsonio::TDataBase>& dbclient() const
    {
        return work_database;
    }

    const jsonio::TDataBase* resources_database() const
    {
        return resourse_database.get();
    }

    /// Link new Driver
    void updateDatabase();


    /// Test changes in current driver data
    bool testCurrentDriver( int dbConnection );

    /// Update "CurrentDBConnection" value.
    void setCurrentDBConnection( int ndx );

    /// Get "CurrentDBConnection" index
    int getCurrentDBConnection( );


};

/// Function to connect to only one Preferences object
extern UIPreferences& uiSettings();

} // namespace jsonui

#endif // PREFERENCES_H
