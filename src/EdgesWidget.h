//  This is JSONUI library+API (https://bitbucket.org/gems4/jsonui)
//
/// \file EdgesWidget.h
/// Declaration of class EdgesWidget - Widget to work with edges -
/// internal DOM based on our JSON schemas data
//
// JSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2018 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONUI depends on the following open-source software products:
// JSONIO (https://bitbucket.org/gems4/jsonio); Qt5 (https://qt.io).
//

#ifndef EDGESWIDGET_H
#define EDGESWIDGET_H

#include <QThread>
#include "JSONUIBase.h"
#include "model_schema.h"
#include "dbdocumentmodel.h"

namespace Ui {
class EdgesWidget;
}

class WaitingSpinnerWidget;

namespace jsonui {


/// \class EdgesWidget - window to insert/update  Edges (TDBGraph)
class EdgesWidget : public JSONUIBase
{
    Q_OBJECT

    void closeEvent(QCloseEvent* e);

    bool event(QEvent * e); // overloading event(QEvent*) method of QMainWindow

    // update after change preferences
    virtual void updtViewMenu();
    virtual void updtModel();
    virtual void updtTable();
    virtual void updtDB();

signals:

    /// Emit signal, when delete recod(s)
    void edgeDeleted();

    /// Emit signal, when loaded graph
    void graphLoaded();

    // signals to DBDocumentModel

    /// Reset current DB client
    void cmResetClient(  QString aschemaName );

    /// Reset current DB schema
    void cmResetSchema(  QString aschemaName );

    /// Reload query
    void cmReloadQuery();

    /// Open document
    void cmReadDocument(  QString reckey  );

    /// Save json Document to database
    void cmUpdate( QString jsonDocument );

    /// Delete keyDocument document from database
    void cmDelete( QString keyDocument );

    /// Delete documents from database
    void cmDeleteList( QVector<QString> keysDocument );

    /// Read multiple records from file fileName
    void cmRestoreMultipleRecordsfromFile( QString fileName );

    /// Write multiple records to file fileName
    void cmBackupQueriedRecordstoFile( QString fileName, QVector<QString> keysDocument);

    /// Write graph to file fileName
    void cmBackupGraphtoFile(QString fileName, std::vector<std::string> keysDocument);

    /// Read multiple records from file fileName
    void cmRestoreGraphfromFile( QString fileName );

    /// Update query
    void cmChangeQueryToId( QString id );

    /// Update query
    void cmResetInOutQuery( QString id );

public slots:

    // internal slots
    void openRecordKey(  const std::string& key, bool resetInOutQuery = false  );
    void openRecordKey(  const QModelIndex& index, bool resetInOutQuery = false );
    void changeKeyList();
    void updateQuery()
    {
      startProcess();
      emit cmReloadQuery();
    }

    /// Add Incoming key to edited document
    void addIncoming(  const QModelIndex& index );
    /// Add Outgoing key to edited document
    void addOutgoing(  const QModelIndex& index );

    // Record
    void CmRead();
    void CmUpdate();
    void CmDelete();
    void CmDeleteSelect();
    void CmDisplaySearchResult();
    void CmSearchQuery();
    void CmBackupQueriedRecordstoFile();
    void CmRestoreMultipleRecordsfromFile();
    void CmBackupGraphtoFile();
    void CmRestoreGraphfromFile();

    // File
    void CmNew();
    void CmClone();
    void CmExportJSON();
    void CmImportJSON();
    void CmIncomingQuery();
    void CmOutgoingQuery();

    // View
    void CmFollowInVertex();
    void CmFollowOutVertex();

    void CmHelpContens();

protected slots:

    void typeChanged(const QString& text);
    void objectChanged()
    { contentsChanged = true; }

    /// Start long database process
    void startProcess();

    /// Finish long database process
    void finishProcess();

    /// Exception when database command execution
    void get_exception( const QString& titl, const QString& msg );

    /// Update oid
    void updateOid( QString oid )
    {
        model_schema->setOid(oid.toStdString());
        if( oid.isEmpty() )
            model_schema->setValue("_rev", std::string(""));
        fieldTable->hide();
        fieldTable->show();
    }

    /// After open new document
    void openDocument( QString jsonDocument  );

    /// Update db document query
    void changeDBClientData(  QString query );

    /// Possible delete edges
    void afterDeleteDocument()
    {
        contentsChanged = false;
        emit edgeDeleted();
    }

    /// Load records to different collections
    void afterLoadGraph()
    {
        emit graphLoaded();
    }

public:

    explicit EdgesWidget( const std::string& schemaName, const jsonio::DBQueryData& query, QWidget *parent = nullptr);
    ~EdgesWidget();

    /// Update query
    void setQuery( QueryWidget* queryW  );

    std::string currentKey() const
    {
      return  getKeyFromDom( model_schema->getDom() );
    }

private:

    /// EdgesWidget.ui description
    Ui::EdgesWidget *ui;

    // tree view editor

    /// Tree view editor model
    TJsonSchemaModel* model_schema = nullptr;
    /// Tree view editor delegate
    QItemDelegate *deleg_schema = nullptr;
    /// Tree view editor widget
    TJsonView* fieldTable = nullptr;

    // database API

    /// Current edge query
    jsonio::DBQueryData defaultQuery;
    /// Database connection object
    DBEdgeDocumentModel* dbDocumentModel=nullptr;
    // std::shared_ptr<jsonio::TDBVertexDocument> dbgraph;
    /// Database keys list model
    DBKeysListModel *dbKeysModel;
    /// Keys list table
    TKeyTable *keysTable;

    // Incoming vertexes list data

    /// Incoming vertexes keys list model
    DBKeysListModel *inKeysModel;
    /// Incoming vertexes keys list table
    TKeyTable *inKeysTable;

    // Outgoing vertexes list data

    /// Outgoing vertexes keys list model
    DBKeysListModel *outKeysModel;
    /// Outgoing vertexes keys list table
    TKeyTable *outKeysTable;

    // Internal data

    QLineEdit* pLineTask;
    WaitingSpinnerWidget *waitDialog;
    QThread dbThread;

    bool isDefaultQuery = false;

    std::string curRecord_;
    //bool _resetInOutQuery = false;
    bool contentsChanged = false;

    // Work functions

    /// Set up menu commands
    void setActions();
    /// Set up current bson data to view model
    bool resetBson( const std::string& curRecord, const std::string& schemaName );
    /// Reset current DB client
    void resetDBClient(const std::string& schemaName );
    void resetTypeBox( const QString& text );

    /// Get current record key from dom structure
    std::string getKeyFromDom( const jsonio::JsonDom* domdata ) const;

    std::string extractSchemaFromId( const std::string& id  );
};

} // namespace jsonui

#endif // EDGESWINDOW_H
