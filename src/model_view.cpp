//  This is JSONUI library+API (https://bitbucket.org/gems4/jsonui)
//
/// \file model_bson.cpp
/// Implementation of TBsonModel, TBsonDelegate and TBsonView
/// implements a tree view of bson structure
//
// JSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONUI depends on the following open-source software products:
// JSONIO (https://bitbucket.org/gems4/jsonio); Qt5 (https://qt.io).
//

#include <iostream>
#include <memory>
#include <QKeyEvent>
#include <QMenu>
#include <QAction>
#include <QHeaderView>
#include <QApplication>
#include <QClipboard>
#include <QMessageBox>
#include "jsonio/ar2base.h"
#include "model_view.h"
#include "CalcDialog.h"
#include "preferences.h"
using namespace jsonio;

namespace jsonui {

bool TJsonView::expandedFields = true;

/// Internal structure to save expanded fields
class ExpandedLevelLine
{
public:

    QString keyname;           ///< Name of field
    ExpandedLevelLine *parent;
    std::vector<std::unique_ptr<ExpandedLevelLine>> children;

    ExpandedLevelLine( const QString& akey, ExpandedLevelLine* parentline ):
        keyname( akey), parent( parentline )
    {
       if( parent != nullptr )
            parent->children.push_back( std::unique_ptr<ExpandedLevelLine>(this) );
    }

    ~ExpandedLevelLine()
    {   children.clear();   }

    ExpandedLevelLine* findKey( const QString& akey )
    {
        auto it = children.begin();
        while( it != children.end() )
        {
          if( it->get()->keyname == akey)
           return it->get();
          it++;
        }
        return nullptr;
    }
};


//-------------------------------------------------------------------------------------
// class TJsonView implements a tree view that displays items from a model to json data.
//-------------------------------------------------------------------------------------

TJsonView::TJsonView( QWidget * parent ):
     QTreeView( parent )
{
     setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
#if QT_VERSION >= 0x050000
     header()->setSectionsClickable(true);
     header()->setSectionResizeMode( /*QHeaderView::ResizeToContents*/QHeaderView::Interactive );
#else
     header()->setResizeMode( /*QHeaderView::ResizeToContents*/QHeaderView::Interactive );
#endif
	    setEditTriggers( QAbstractItemView::DoubleClicked|QAbstractItemView::AnyKeyPressed );
	    setFocusPolicy(Qt::StrongFocus);
	    setTabKeyNavigation( false );
        setContextMenuPolicy(Qt::CustomContextMenu);
        setSelectionMode( QAbstractItemView::/*MultiSelection*/ExtendedSelection );
        setSelectionBehavior( QAbstractItemView::SelectItems );
        setIndentation( 30 );

        connect( this, SIGNAL(customContextMenuRequested(QPoint)),
            this, SLOT(slotPopupContextMenu(QPoint)));
        connect( header(), SIGNAL(sectionClicked(int)),
             this, SLOT(changeCurrent(int)));
}

/*virtual void TJsonView::drawRow(QPainter* p_painter, const QStyleOptionViewItem& option,
                     const QModelIndex& index) const
{
    QStyleOptionViewItem optionCustom = option;
    if (index.column() == 0)
    {
        optionCustom.palette.setColor(QPalette::Text, Qt::green);
    }
    QTreeView::drawRow(p_painter, optionCustom, index);
 }*/

void TJsonView::changeCurrent( int section )
{
    if( !currentIndex().isValid() )
    { QModelIndex index = model()->index( 0, section, rootIndex());
      setCurrentIndex(index);
    }
}

 void TJsonView::slotPopupContextMenu(const QPoint &pos)
 {
    QModelIndex index = indexAt( pos );
    const TJsonAbstractModel* model =dynamic_cast<const TJsonAbstractModel*>(index.model());
    QMenu *menu = new QMenu(this);
  
     QAction* act =  new QAction(tr("&Help"), this);
     act->setIcon(QIcon(":/menu/ShowHelpWindowIcon.png"));
     act->setShortcut(tr("F1"));
     act->setStatusTip(tr("Help for specified cell"));
     connect(act, SIGNAL(triggered()), this, SLOT(CmHelp()));
     menu->addAction(act);

     menu->addSeparator();

     if(  model && model->isStruct(index) )
         {
            //menu->addSeparator();
            act =  new QAction(tr("Add Structured Fields ..."), this);
            act->setShortcut(QKeySequence(Qt::Key_F7));
            //act->setShortcutVisibleInContextMenu(true);
            act->setStatusTip(tr("Add new line(s) into object"));
            connect(act, SIGNAL(triggered()), this, SLOT(CmAddObjects()));
            menu->addAction(act);
         }

     if(  model && model->isStruct(index) )
         {
            //menu->addSeparator();
            act =  new QAction(tr("Add &One Field ..."), this);
            act->setShortcut(tr("Ctr+O"));
            act->setStatusTip(tr("Add new line into object"));
            connect(act, SIGNAL(triggered()), this, SLOT(CmAddObject()));
            menu->addAction(act);
         }

     if( model && model->isStruct(index) )
         {
            //menu->addSeparator();
            act =  new QAction(tr("Reset Data Object to &Defaults"), this);
            act->setShortcut(tr("Ctrl+D"));
            act->setStatusTip(tr("Setup default values to object"));
            connect(act, SIGNAL(triggered()), this, SLOT(CmResetObject()));
            menu->addAction(act);
         }

     if( model && model->isCanBeRemoved(index) )
         {
            //menu->addSeparator();
            act =  new QAction(tr("Select and Remove Fields"), this);
            act->setShortcut(QKeySequence(Qt::Key_F8));
            //act->setShortcutVisibleInContextMenu(true);
            act->setStatusTip(tr("Delete selected lines from object"));
            connect(act, SIGNAL(triggered()), this, SLOT(CmDelObjects()));
            menu->addAction(act);
         }
     if( model && model->isCanBeRemoved(index) )
         {
            //menu->addSeparator();
            act =  new QAction(tr("&Remove Selected Field"), this);
            act->setShortcut(tr("Ctr+R"));
            act->setStatusTip(tr("Delete current line from object"));
            connect(act, SIGNAL(triggered()), this, SLOT(CmDelObject()));
            menu->addAction(act);
         }
     if( model && model->isUnion(index) )
         {
            //menu->addSeparator();
            act =  new QAction(tr("Remove Alternatives (&Union)"), this);
            act->setShortcut(tr("Ctr+U"));
            act->setStatusTip(tr("Delete other of current line from object"));
            connect(act, SIGNAL(triggered()), this, SLOT(CmDelObjectsUnion()));
            menu->addAction(act);
         }

     if(  model && model->isArray(index) )
         {
            menu->addSeparator();
            act =  new QAction(tr("Resize Selected List/Map/Set ..."), this);
            act->setShortcut(QKeySequence(Qt::Key_F4));
            //act->setShortcutVisibleInContextMenu(true);
            act->setStatusTip(tr("Change current array size"));
            connect(act, SIGNAL(triggered()), this, SLOT(CmResizeArray()));
            menu->addAction(act);
         }

     if(  model && model->isCanBeCloned( index ) )
         {
            //menu->addSeparator();
            act =  new QAction(tr("Clone Selected Field"), this);
            act->setShortcut(tr("Ctr+C"));
            act->setStatusTip(tr("Add copy of field to end of list/map"));
            connect(act, SIGNAL(triggered()), this, SLOT(CmCloneObject()));
            menu->addAction(act);
         }


     if( index.column()==1 && model && model->isNumber(index) )
         {
            menu->addSeparator();
            act =  new QAction(tr("Calculator"), this);
            act->setShortcut(QKeySequence(Qt::Key_F5));
            //act->setShortcutVisibleInContextMenu(true);
            act->setStatusTip(tr("Use Calculator for specified cells"));
       	    connect(act, SIGNAL(triggered()), this, SLOT(CmCalc()));
            menu->addAction(act);
         }

      /*menu->addSeparator();
         
      act =  new QAction(tr("Select &row"), this);
      act->setShortcut(tr("Ctrl+R"));
      act->setStatusTip(tr("Select current row"));
      connect(act, SIGNAL(triggered()), this, SLOT(SelectRow()));
      menu->addAction(act);
 
      act =  new QAction(tr("Select co&lumn"), this);
      act->setShortcut(tr("Ctrl+L"));
      act->setStatusTip(tr("Select current column"));
      connect(act, SIGNAL(triggered()), this, SLOT(SelectColumn()));
      menu->addAction(act);
       
      act =  new QAction(tr("Select &group"), this);
      act->setShortcut(tr("Ctrl+G"));
      act->setStatusTip(tr("Select current item with children"));
      connect(act, SIGNAL(triggered()), this, SLOT(SelectGroup()));
      menu->addAction(act);

      act =  new QAction(tr("Select &all"), this);
      act->setShortcut(tr("Ctrl+A"));
      act->setStatusTip(tr("Select all"));
      connect(act, SIGNAL(triggered()), this, SLOT(SelectAll()));
      menu->addAction(act);

      menu->addSeparator();

      act =  new QAction(tr("&Copy"), this);
      act->setShortcut(tr("Ctrl+C"));
      act->setStatusTip(tr("Copy selected data"));
      connect(act, SIGNAL(triggered()), this, SLOT(CopyData()));
      menu->addAction(act);

      act =  new QAction(tr("Copy &with names"), this);
      act->setShortcut(tr("Ctrl+W"));
      act->setStatusTip(tr("Copy selected header&cells"));
      connect(act, SIGNAL(triggered()), this, SLOT(CopyDataHeader()));
      menu->addAction(act);
      */
      menu->addSeparator();

      act =  new QAction(tr("Copy Field &Path"), this);
      act->setShortcut(tr("Ctrl+P"));
      act->setStatusTip(tr("Copy selected field path"));
      connect(act, SIGNAL(triggered()), this, SLOT(CopyFieldPath()));
      menu->addAction(act);

      act =  new QAction(tr("Copy &Field"), this);
      act->setShortcut(tr("Ctrl+F"));
      act->setStatusTip(tr("Copy selected field data"));
      connect(act, SIGNAL(triggered()), this, SLOT(CopyField()));
      menu->addAction(act);

      act =  new QAction(tr("Paste Field &Value"), this);
      act->setShortcut(tr("Ctrl+V"));
      //act->setShortcutVisibleInContextMenu(true);
      act->setStatusTip(tr("Paste data to field"));
      connect(act, SIGNAL(triggered()), this, SLOT(PasteField()));
      menu->addAction(act);

     menu->exec( viewport()->mapToGlobal(pos) );
     delete menu;
 }

  void TJsonView::keyPressEvent(QKeyEvent* e)
 {
	    if ( e->modifiers() & Qt::ControlModifier ) 
	    {
			switch ( e->key() ) 
			{
              /*case Qt::Key_R:
			    SelectRow();
			    return;
			  case Qt::Key_L:
			    SelectColumn();
			    return;
			  case Qt::Key_G:
			    SelectGroup();
			    return;
			  case Qt::Key_A:
			    SelectAll();
			    return;
			  case Qt::Key_C:
			    CopyData();
			    return;
             case Qt::Key_W:
                CopyDataHeader();
                return;*/
            case Qt::Key_R:
               CmDelObject();
               return;
            case Qt::Key_U:
               CmDelObjectsUnion();
               return;
            case Qt::Key_O:
                CmAddObject();
               return;
            case Qt::Key_D:
                CmResetObject();
               return;
            case Qt::Key_P:
                CopyFieldPath();
               return;
            case Qt::Key_F:
                CopyField();
               return;
            case Qt::Key_V:
                PasteField();
               return;
            case Qt::Key_C:
                CmCloneObject();
               return;
            }
		}

      switch( e->key() )
 	  {
 	    case Qt::Key_F1:
 	        CmHelp();
 	        return;
        case Qt::Key_F5:
           CmCalc();
          return;
        case Qt::Key_F7:
           CmAddObjects();
          return;
        case Qt::Key_F8:
           CmDelObjects();
          return;
       case Qt::Key_F4:
           CmResizeArray();
          return;
    }
 	QTreeView::keyPressEvent(e);
 }

  bool TJsonView::testCurrentIndex( )
  {
    if( currentIndex().isValid() )
     return  true;
   QMessageBox::information( window(), "View object",
        "Please, determine the current index" );
   return false;
  }

  void TJsonView::_SelectRow()
  {
    if( testCurrentIndex() )
      selectionModel()->select( currentIndex(),
            QItemSelectionModel::Rows|QItemSelectionModel::ClearAndSelect );
  }

  void TJsonView::_SelectColumn()
  {
    if( testCurrentIndex() )
      selectionModel()->select( currentIndex(),
              QItemSelectionModel::Columns|QItemSelectionModel::ClearAndSelect );
  }

  void TJsonView::_SelectAll()
  {
    if( testCurrentIndex() )
     selectWithChildren(rootIndex());
     //   selectAll();
  }

  void TJsonView::selectWithChildren(const QModelIndex& parIndex)
  {
     selectionModel()->select( parIndex,
             QItemSelectionModel::Rows|QItemSelectionModel::Select );

     for(int row = 0; row < model()->rowCount( parIndex ); row++ )
     {
        QModelIndex  childIndex = model()->index( row, 0, parIndex);
        selectWithChildren(childIndex);
     }
 }

  void TJsonView::_SelectGroup()
  {
     if( !testCurrentIndex() )
        return;

     selectWithChildren(currentIndex());
  }


  void TJsonView::copyWithChildren( const QModelIndex& parIndex,
      const QModelIndexList& selIndexes, QString& clipText )
  {
      QModelIndex index;
      QString cText;
      bool frst = true;
      int row = parIndex.row(),  col;

      for (col = 0; col < model()->columnCount( parIndex ); col++ )
      {
          index = parIndex.sibling( row, col);
          if( selIndexes.contains( index ) )
          {
            if( !frst )
               clipText += splitCol;
             frst = false;
             cText = model()->data(index).toString();
             if( cText == emptiness.c_str() )
                   cText = "  ";//"\r";
             clipText += cText;
          }
     }
     if( !frst )
       clipText += splitRow;

     for(int rw = 0; rw < model()->rowCount( parIndex ); rw++ )
     {
        index = model()->index( rw, 0, parIndex);
        copyWithChildren( index, selIndexes, clipText );
     }
 }

  QString TJsonView::createString()
  {
    QString clipText;
    QModelIndexList selIndexes = selectedIndexes();
    copyWithChildren( rootIndex(),selIndexes, clipText );
	return clipText;  
  }

  QString TJsonView::createHeader()
  {
    QString cText;
	QString clipText;
    int col;
    bool frst = true;
    for( col = 0; col < model()->columnCount( rootIndex() ); col++ )
    {
        if( selectionModel()->columnIntersectsSelection( col,  rootIndex() ) )
        {
            if( !frst )
                clipText += splitCol;
            frst = false;
            cText = model()->headerData( col, Qt::Horizontal, Qt::DisplayRole ).toString();
            if( cText == emptiness.c_str() )
                cText = "  ";//"\r";
            clipText += cText;
        }
    }
    if( !frst )
        clipText += splitRow;
    return clipText;
  }

  void TJsonView::_CopyData()
  {
 	QString clipText = createString();
    QApplication::clipboard()->setText(clipText/*, QClipboard::Clipboard*/);
  }

  void TJsonView::_CopyDataHeader()
  {
 	QString clipText = createHeader();
	clipText += createString();
    QApplication::clipboard()->setText(clipText/*, QClipboard::Clipboard*/);
  }

  void TJsonView::CmCalc()
  {
    QModelIndex index = currentIndex();
    const TJsonAbstractModel* model = dynamic_cast<const TJsonAbstractModel* >( index.model());
    if( index.column()==1 && model && model->isNumber(index) )
    {
       CalcDialog calc( window() );
       if( calc.exec() )
       {
            QString res = calc.computeFunctionString( index.data(Qt::EditRole).toDouble() );
            const_cast<TJsonAbstractModel*>(model)->setData(index, res, Qt::EditRole);
       }
    }
   }

  void TJsonView::CmAddObjects()
  {
    QModelIndex index = currentIndex();
    TJsonAbstractModel* model_;
    if (index.isValid())
        model_ = const_cast<TJsonAbstractModel*>(dynamic_cast< const TJsonAbstractModel*>(index.model()));
    else
        model_ = dynamic_cast<TJsonAbstractModel*>(model());

    try {
        if(  model_ && model_->isStruct(index) )
        {  const QModelIndex ndx = model_->addObjects( window(), index);
           selectionModel()->setCurrentIndex( ndx, QItemSelectionModel::ClearAndSelect );
        }
       else
        QMessageBox::information( window(), "Add to Object",
             "Please, determine object to be expanded" );
    }
    catch(jsonio_exception& e)
    {
        QMessageBox::critical( window(), e.title(), e.what() );
    }
    catch(std::exception& e)
     {
        QMessageBox::critical( window(), "std::exception", e.what() );
     }
  }


  void TJsonView::CmAddObject()
  {
    QModelIndex index = currentIndex();
    //TBsonAbstractModel* model_ =  (TBsonAbstractModel*)(index.model());
    TJsonAbstractModel* model_ =  dynamic_cast<TJsonAbstractModel*>(model());

    try {
         if(  model_ && model_->isStruct(index) )
         {
             QModelIndex ndx = model_->addObject( window(), index);
             selectionModel()->setCurrentIndex( ndx, QItemSelectionModel::ClearAndSelect );
         }
        else
           QMessageBox::information( window(), "Add to Object",
               "Please, determine object to be expanded" );
   }
   catch(jsonio_exception& e)
   {
       QMessageBox::critical( window(), e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( window(), "std::exception", e.what() );
    }
   }


  void TJsonView::CmCloneObject()
  {
      QModelIndex index = currentIndex();
      const TJsonAbstractModel* model_=nullptr;
      if (index.isValid())
          model_ =dynamic_cast<const TJsonAbstractModel*>(index.model());

    try {
         if(  model_ && model_->isCanBeCloned( index ) )
         {
             QModelIndex ndx = const_cast<TJsonAbstractModel*>(model_)->cloneObject( window(), index);
             selectionModel()->setCurrentIndex( ndx, QItemSelectionModel::ClearAndSelect );
         }
        else
           QMessageBox::information( window(), "Clone Object",
               "Please, determine object to be expanded" );
   }
   catch(jsonio_exception& e)
   {
       QMessageBox::critical( window(), e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( window(), "std::exception", e.what() );
    }
   }


  void TJsonView::CmResetObject()
  {
    QModelIndex index = currentIndex();
    //TBsonAbstractModel* model_ =  (TBsonAbstractModel*)(index.model());
    TJsonAbstractModel* model_ =  dynamic_cast<TJsonAbstractModel*>(model());

    try {
         if(  model_ && model_->isStruct(index) )
         {   // collapse(index);
             model_->resetObject( window(), index);
             // selectionModel()->setCurrentIndex( index, QItemSelectionModel::ClearAndSelect );
         }
        else
           QMessageBox::information( window(), "Reset Object",
               "Please, determine object to be expanded" );
   }
   catch(jsonio_exception& e)
   {
       QMessageBox::critical( window(), e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( window(), "std::exception", e.what() );
    }
   }

  void TJsonView::CmDelObject()
  {
    QModelIndex index = currentIndex();
    const TJsonAbstractModel* model =dynamic_cast< const TJsonAbstractModel*>(index.model());
    try {
          if(  model && model->isCanBeRemoved(index) )
          {
              selectionModel()->clear();
              collapse(index);
              const_cast<TJsonAbstractModel*>(model)->delObject( window(), index);
      //update();
         } else
          QMessageBox::information( window(), "Delete from Object",
             "Please, determine the key-value pair to be deleted" );
    }
    catch(jsonio_exception& e)
    {
        QMessageBox::critical( window(), e.title(), e.what() );
    }
    catch(std::exception& e)
     {
        QMessageBox::critical( window(), "std::exception", e.what() );
     }
    catch(...)
     {
        QMessageBox::critical( window(), "std::exception", "Undefined exception" );
     }
  }

  void TJsonView::CmDelObjects()
  {
    QModelIndex index = currentIndex();
    const TJsonAbstractModel* model = dynamic_cast< const TJsonAbstractModel*>(index.model());
    try {
          if(  model && model->isCanBeRemoved(index) )
          {
              selectionModel()->clear();
              collapse(index);
              const_cast<TJsonAbstractModel*>(model)->delObjects( window(), index);
      //update();
         } else
          QMessageBox::information( window(), "Delete from Object",
             "Please, determine the key-value level to be deleted" );
    }
    catch(jsonio_exception& e)
    {
        QMessageBox::critical( window(), e.title(), e.what() );
    }
    catch(std::exception& e)
     {
        QMessageBox::critical( window(), "std::exception", e.what() );
     }
    catch(...)
     {
        QMessageBox::critical( window(), "std::exception", "Undefined exception" );
     }
  }

  void TJsonView::CmDelObjectsUnion()
  {
    QModelIndex index = currentIndex();
    const TJsonAbstractModel* model = dynamic_cast< const TJsonAbstractModel*>(index.model());
    try {
          if(  model && model->isUnion(index) )
          {
              collapse(index);
              const_cast<TJsonAbstractModel*>(model)->delObjectsUnion( window(), index);
      //update();
         } else
          QMessageBox::information( window(), "Delete from Object",
             "Please, determine the key-value pair to be deleted" );
    }
    catch(jsonio_exception& e)
    {
        QMessageBox::critical( window(), e.title(), e.what() );
    }
    catch(std::exception& e)
     {
        QMessageBox::critical( window(), "std::exception", e.what() );
     }
    catch(...)
     {
        QMessageBox::critical( window(), "std::exception", "Undefined exception" );
     }
  }

  void TJsonView::CmResizeArray()
  {
    QModelIndex index = currentIndex();
    const TJsonAbstractModel* model = dynamic_cast<const TJsonAbstractModel*>(index.model());
    try {
        if(  model && model->isArray(index) )
       {
            // collapse(index);
            const_cast<TJsonAbstractModel*>(model)->resizeArray( window(), index);
       }
       else
          QMessageBox::information( window(), "Resize Array",
             "Please, determine arrays key to be resized" );
    }
    catch(jsonio_exception& e)
    {
        QMessageBox::critical( window(), e.title(), e.what() );
    }
    catch(std::exception& e)
     {
        QMessageBox::critical( window(), "std::exception", e.what() );
     }
   }

  // Help on F1 pressed on data field
 void TJsonView::CmHelp()
 {
      QModelIndex index = currentIndex();
      const TJsonAbstractModel* model =dynamic_cast<const TJsonAbstractModel*>(index.model());
      std::string hlp= "", schema= "undefined";
      if(model)
      {
          hlp =  model->helpName( index );
          auto pos = hlp.find_first_of('#');
          if( pos != std::string::npos )
          {
            schema = hlp.substr(0, pos);
            hlp = hlp.substr(pos+1);
          }
          std::cout << schema << "#"<< hlp << std::endl;
          helpWin( schema, hlp );
      }
 }

// Save expand level  -----------------------------------------


void TJsonView::updateModelData( const std::string& ajson, const std::string& curSchemaName )
{
    TJsonAbstractModel* model_table = dynamic_cast<TJsonAbstractModel*>( model() );
    if( !model_table )
      return;

    // save old expand level
    int sec0size = 250, sec1size = 250;
    ExpandedLevelLine *topList = nullptr;
    if( expandedFields )
    {
      sec0size = header()->sectionSize(0);
      sec1size = header()->sectionSize(1);
      topList = new ExpandedLevelLine("root", nullptr);
      saveExpandedState( topList );
    }

    // update model
    model_table->setupModelData( ajson, curSchemaName );

    // restore expand level
    if( expandedFields )
      restoreExpandedState( topList );
    else
       expandToDepth(0);
    header()->resizeSection(0, sec0size);
    header()->resizeSection(1, sec1size);
    if( topList )
      delete topList;

}

void TJsonView::saveExpandedState( ExpandedLevelLine *topList )
{
    for(int row = 0; row < model()->rowCount(); ++row)
        saveExpandedOnLevel( model()->index(row,0), topList);
}

void TJsonView::restoreExpandedState( ExpandedLevelLine *topList )
{
    setUpdatesEnabled(false);
    for(int row = 0; row < model()->rowCount(); ++row)
        restoreExpandedOnLevel( model()->index(row,0), topList );
    setUpdatesEnabled(true);
}

void TJsonView::saveExpandedOnLevel(const QModelIndex& index, ExpandedLevelLine *parent )
{
    if( index.isValid() && isExpanded(index))
    {
        QString key = index.data(Qt::EditRole).toString();
        ExpandedLevelLine *child = new ExpandedLevelLine(key, parent);
        for(int row = 0; row < model()->rowCount(index); ++row)
            saveExpandedOnLevel( index.model()->index(row,0, index), child );
    }
}

void TJsonView::restoreExpandedOnLevel(const QModelIndex& index, ExpandedLevelLine *topData )
{
    if( index.isValid() )
    {
      QString key = index.data(Qt::EditRole).toString();
      ExpandedLevelLine *child = topData->findKey(key);
      if(child != nullptr )
      {
        setExpanded(index, true);
        for(int row = 0; row < model()->rowCount(index); ++row)
            restoreExpandedOnLevel(index.model()->index(row,0, index), child );
      }
   }
}

void TJsonView::CopyFieldPath()
{
  QModelIndex index = currentIndex();
  const TJsonAbstractModel* model = dynamic_cast<const TJsonAbstractModel*>(index.model());
  try {
        if(  model )
        {
            QString clipText = model->getFieldPath( index ).c_str();
            QApplication::clipboard()->setText(clipText/*, QClipboard::Clipboard*/);
        }
  }
  catch(jsonio_exception& e)
  {
      QMessageBox::critical( window(), e.title(), e.what() );
  }
  catch(std::exception& e)
   {
      QMessageBox::critical( window(), "std::exception", e.what() );
   }
  catch(...)
   {
      QMessageBox::critical( window(), "std::exception", "Undefined exception" );
   }
}


void TJsonView::CopyField()
{
  QModelIndex index = currentIndex();
  const TJsonAbstractModel* model = dynamic_cast<const TJsonAbstractModel*>(index.model());
  try {
        if(  model )
        {
            QString clipText = model->getFieldData( index ).c_str();
            QApplication::clipboard()->setText(clipText/*, QClipboard::Clipboard*/);
        }
  }
  catch(jsonio_exception& e)
  {
      QMessageBox::critical( window(), e.title(), e.what() );
  }
  catch(std::exception& e)
   {
      QMessageBox::critical( window(), "std::exception", e.what() );
   }
  catch(...)
   {
      QMessageBox::critical( window(), "std::exception", "Undefined exception" );
   }
}

void TJsonView::PasteField()
{
  auto clipText = QApplication::clipboard()->text(QClipboard::Clipboard).toStdString();
  ExpandedLevelLine *topList = nullptr;

  if(clipText.empty())
    return;
  QModelIndex index = currentIndex();
  const TJsonAbstractModel* model = dynamic_cast<const TJsonAbstractModel*>(index.model());

  try {
        if(  model )
        {
            // save old expand level
            int sec0size = 250, sec1size = 250;
            sec0size = header()->sectionSize(0);
            sec1size = header()->sectionSize(1);
            topList = new ExpandedLevelLine("root", nullptr);
            saveExpandedState( topList );

            const_cast<TJsonAbstractModel*>(model)->setFieldData( index, clipText );

            restoreExpandedState( topList );
            header()->resizeSection(0, sec0size);
            header()->resizeSection(1, sec1size);
        }
  }
  catch(jsonio_exception& e)
  {
      QMessageBox::critical( window(), e.title(), e.what() );
  }
  catch(std::exception& e)
   {
      QMessageBox::critical( window(), "std::exception", e.what() );
   }
  catch(...)
   {
      QMessageBox::critical( window(), "std::exception", "Undefined exception" );
   }
  if( topList )
    delete topList;
}

} // namespace jsonui

//---------------------- End of file  model_view.cpp ---------------------------
