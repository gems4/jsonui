//  This is JSONUI library+API (https://bitbucket.org/gems4/jsonui)
//
/// \file JSONUIBase.h
/// JSONUIBase - Base Widget to work with bson
/// or internal DOM based on our JSON schemas data
//
// JSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONUI depends on the following open-source software products:
// JSONIO (https://bitbucket.org/gems4/jsonio); Qt5 (https://qt.io).
//

#ifndef JSONUIBASE_H
#define JSONUIBASE_H

#include <QMainWindow>
#include <QSettings>
#include "preferences.h"
#include "jsonio/dbquerydef.h"

namespace jsonui {

class JSONUIBase;
class QueryWidget;
using  onEventfunction = std::function<bool(JSONUIBase*)>;
using  ShowWidgetFunction = std::function<void( bool isVertex, const std::string& testschema,
       const std::string& recordkey, const jsonio::DBQueryData& query )>;

/// \class JSONUIBase base window to organize update/close events
class JSONUIBase : public QMainWindow
{
    Q_OBJECT

 protected:

    // Internal data
    std::string curSchemaName = "";
    onEventfunction onCloseEvent;       ///< Run function when close window
    ShowWidgetFunction showOtherWindow; ///< Run function to open new window

    // Work functions

    /// Build schema file extension
    virtual std::string fileShemaExt(const std::string& curSchemaName,const char* ext);
    /// Get schema name from extension
    virtual std::string schemafromName( const std::string& fname );

    // update after change preferences
    virtual void updtViewMenu()=0;
    virtual void updtModel()=0;
    virtual void updtTable()=0;
    virtual void updtDB()=0;

public slots:

    // internal slots
    virtual void openRecordKey(  const std::string& key, bool resetInOutQuery=false  ) = 0;
    virtual void changeKeyList() = 0;

    // update after change preferences
    void updateViewMenu() { updtViewMenu();}
    void updateModel() { updtModel(); }
    void updateTable() { updtTable(); }
    void updateDB()    { updtDB(); }

    // Help
    virtual void  CmHelpAbout();
    virtual void  CmHelpAuthors();
    virtual void  CmHelpLicense();
    virtual void  CmHelpContens();

public:

    explicit JSONUIBase(  const std::string& aschemaName, QWidget *parent = nullptr):
        QMainWindow(parent), curSchemaName(aschemaName)
     {
        onCloseEvent = [](QMainWindow*){ return true; };
     }

    virtual ~JSONUIBase() {}

    /// Run query
    virtual void setQuery( QueryWidget* queryW  ) = 0;

    /// Set condition function into CloseEvent
    void setOnCloseEventFunction(onEventfunction acond )
    {
       onCloseEvent = acond;
    }

    /// Set function to open other window
    void setShowWidgetFunction(ShowWidgetFunction afunc )
    {
       showOtherWindow = afunc;
    }

    virtual std::string currentKey() const
    {
      return "";
    }

};

} // namespace jsonui

#endif // TJSONUIBASE_H
