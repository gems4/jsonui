//  This is JSONUI library+API (https://bitbucket.org/gems4/bsonui)
//
/// \file FormatImportDialog.cpp
/// Implementation of FormatImportDialog to define data for
/// import of foreign format files
//
// JSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONUI depends on the following open-source software products:
// JSONIO (https://bitbucket.org/gems4/jsonio); Qt5 (https://qt.io);
// Qwtplot (http://qwt.sourceforge.net).
//

#include <QFileDialog>
#include "FormatImportDialog.h"
#include "ui_FormatImportDialog.h"
#include "preferences.h"

namespace jsonui {

FormatImportDialog::FormatImportDialog(QSettings *aSet, QWidget *parent) :
    QDialog(parent), settings(aSet),
    ui(new Ui::FormatImportDialog)
{
    ui->setupUi(this);

    formats = new QButtonGroup();
    formats->addButton(ui->rbText, 0);
    formats->addButton(ui->rbTable, 1);
    formats->addButton(ui->rbKeyValue, 2);
    formats->addButton(ui->rbStructure, 3);

    if( settings ) //load old settings
    {
        QAbstractButton* btn = formats->button(settings->value("ImportFileFormat", "3").toInt());
        if( btn )
            btn->setChecked(true);
        ui->formatEdit->setText( settings->value("FormatFilePath", "").toString() );
        ui->fileEdit->setText( settings->value("InputFilePath", ".").toString() );
        ui->overwriteBox->setChecked( settings->value("ImportFormatOverwrite", false).toBool() );
    }

    QObject::connect( ui->buttonBox, SIGNAL(accepted()), this, SLOT(CmSave()));
    QObject::connect( ui->buttonBox, SIGNAL(helpRequested()), this, SLOT(CmHelp()));
    QObject::connect( ui->formatButton, SIGNAL(clicked()), this, SLOT(CmFormatPath()));
    QObject::connect( ui->fileButton, SIGNAL(clicked()), this, SLOT(CmFilePath()));
}

FormatImportDialog::~FormatImportDialog()
{
    delete formats;
    delete ui;
}

void FormatImportDialog::CmSave()
{
    if( !settings )
       return;

    settings->setValue("ImportFileFormat", formats->checkedId() );
    settings->setValue("FormatFilePath",   ui->formatEdit->text() );
    settings->setValue("InputFilePath",   ui->fileEdit->text() );
    settings->setValue("ImportFormatOverwrite",    ui->overwriteBox->isChecked() );
    settings->sync();
    accept();
}

void FormatImportDialog::CmFormatPath()
{

   QString fn = QFileDialog::getOpenFileName( this, "Select Format File Path",
      ui->formatEdit->text(), "Files (*.json *.yaml *.xml)",
      nullptr, QFileDialog::DontConfirmOverwrite );

  if( !fn.isEmpty() )
     ui->formatEdit->setText( fn );
}

void FormatImportDialog::CmFilePath()
{

   QString fn = QFileDialog::getOpenFileName( this, "Select Input File Path",
      ui->fileEdit->text(), "All Files (*)",
      nullptr, QFileDialog::DontConfirmOverwrite );

  if( !fn.isEmpty() )
     ui->fileEdit->setText( fn );
}

void FormatImportDialog::CmHelp()
{
   helpWin( "FormatImportDialogJsonui", "" );
}

} // namespace jsonui
