//  This is JSONUI library+API (https://bitbucket.org/gems4/jsonui)
//
/// \file model_node.cpp
/// Implementation of TJsonSchemaModel, TJsonSchemaDelegate
/// implements a tree view of internal DOM based on our JSON schemas
//
// JSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONUI depends on the following open-source software products:
// JSONIO (https://bitbucket.org/gems4/jsonio); Qt5 (https://qt.io).
//

#include <iostream>
#include <QLineEdit>
#include <QComboBox>
#include <QMessageBox>
#include "model_schema.h"
#include "SelectDialog.h"
#include "ModelLineDialog.h"
#include "jsonio/jsondomschema.h"
#include "jsonio/tf_json.h"
#include "jsonio/io_settings.h"

using namespace jsonio;
//using namespace ::apache::thrift::protocol;

namespace jsonui {

bool TJsonSchemaModel::showComments = false;
bool TJsonSchemaModel::useEnumNames = false;
bool TJsonSchemaModel::editID = false;

//--------------------------------------------------------------------------------------
//  class TJsonDomModel
//  class for represents the data set and is responsible for fetchin
//  the data is neaded for viewing and for writing back any changes.
//  Reading/writing data from/to TObject and TObjList classes
//---------------------------------------------------------------------------------------

TJsonSchemaModel::TJsonSchemaModel( const std::string& ajson, const std::string& schema,
                                    const QStringList& aHeaderData, QObject* parent ):
        TJsonAbstractModel(parent), hdData( aHeaderData),
        currentschema(schema), rootNode(JsonDomSchema::newSchemaObject(schema))
{
  setupModelData(ajson, schema);
  connect( this, SIGNAL(dataChanged( const QModelIndex&, const QModelIndex& )),
            parent,  SLOT(objectChanged()) );
}

TJsonSchemaModel::~TJsonSchemaModel()
{
}

JsonDomSchema* TJsonSchemaModel::lineFromIndex(const QModelIndex &index) const
{
    if (index.isValid()) {
        return static_cast<JsonDomSchema*>(index.internalPointer());
    } else {
        return rootNode.get();
    }
}

void TJsonSchemaModel::setupModelData( const std::string& ajson, const std::string&  schema )
{
    try{
         beginResetModel();
         if( currentschema != schema )
             rootNode.reset( JsonDomSchema::newSchemaObject(schema) );
          rootNode->clearField();
          parseJsonToNode( ajson, rootNode.get() );
          endResetModel();
       }
    catch( ... )
    {
       endResetModel();
       throw;
    }
}


QModelIndex TJsonSchemaModel::index(int row, int column, const QModelIndex &parent) const
{
    if (!rootNode.get())
        return QModelIndex();
    auto parentItem = lineFromIndex( parent );
    if( parentItem->getChildrenCount() > 0 )
       return createIndex(row, column, parentItem->getChild( row ));
    else
       return QModelIndex();
}

QModelIndex TJsonSchemaModel::parent(const QModelIndex& child) const
{
    if (!child.isValid())
        return QModelIndex();

    auto childItem = lineFromIndex(child);
    auto parentItem = childItem->getParent();
    if ( parentItem == rootNode.get() )
            return QModelIndex();
    return createIndex(parentItem->getNdx(), 0, parentItem);
}

int TJsonSchemaModel::rowCount( const QModelIndex& parent ) const
{
   if (!rootNode.get())
       return 0;
  //if (parent.column() > 0)
  //    return 0;
  auto parentItem = lineFromIndex( parent );
  return parentItem->getChildrenCount();
}

int TJsonSchemaModel::columnCount( const QModelIndex& /*parent*/ ) const
{
    if( showComments )
      return 3;
    return 2;
}

Qt::ItemFlags TJsonSchemaModel::flags( const QModelIndex& index ) const
{
  Qt::ItemFlags flags = QAbstractItemModel::flags(index);
  auto item = lineFromIndex( index );

  if( !editID && item->getKey()[0] == '_' )
  {
    return (flags & ~Qt::ItemIsEditable);
  }

  if( index.column() == 0 && !item->isTop() && item->getParent()->isMap() )
  {
      flags |= Qt::ItemIsEditable;
      return flags;
  }

  if( index.column() == 1 &&
      !( item->isStruct() || item->isArray() || item->getMapEnumdef() != nullptr ) )
  {
      flags |= Qt::ItemIsEditable;
      return flags;
  }

  return (flags & ~Qt::ItemIsEditable);
}

QVariant TJsonSchemaModel::headerData( int section, Qt::Orientation orientation, int role ) const
{
  if( role == Qt::DisplayRole  && orientation == Qt::Horizontal && section<hdData.size())
      return hdData[section];
  return QVariant();
}

QString TJsonSchemaModel::getValue( int column, const JsonDomSchema *node ) const
{
   switch( column )
   {
     case 0: return  QString(node->getKey().c_str());
     case 2: return  QString(node->getDescription().c_str());
     case 1: return  QString(node->getFieldValue().c_str());
   }
   return QString("");
}


QVariant TJsonSchemaModel::data( const QModelIndex& index, int role ) const
{
    if(!index.isValid())
        return QVariant();

    auto node = lineFromIndex( index );
    switch( role )
    {
    case Qt::DisplayRole:
    {
        std::string enumName;
        if( useEnumNames && !(enumName = node->getEnumI32Name()).empty() )
        {
            ThriftEnumDef* enumdef = ioSettings().Schema()->getEnum( enumName );
            if( enumdef != nullptr )
            {
                int idx;
                node->getValue(idx);
                std::string name = enumdef->getNamebyId(idx);

                if( index.column() == 1 )
                    return  QString(name.c_str());
                if( index.column() == 2 )
                    return  QString(enumdef->getDoc(name).c_str());
            }
        }
        else
            if( index.column() == 2 )
            {
                auto mapenum = node->getMapEnumdef();
                if( mapenum != nullptr )
                {
                    std::string name;
                    node->getValue(name);
                    return  QString(mapenum->getDoc(name).c_str());
                }
            }
        return getValue( index.column(), node );
    }
         //break;
    case Qt::EditRole:
        return getValue( index.column(), node );
    case Qt::ToolTipRole:
    case Qt::StatusTipRole:
        return  QString(node->getFullDescription().c_str() );
    case Qt::ForegroundRole:
    {
        if ( index.column() == 0 && !node->isTop() && !node->getParent()->isMap()  )
        {
            return QVariant( QColor( Qt::darkCyan ) );
        }
        //return QVariant( QColor( Qt::black ) );
    }
        break;
    default: break;
    }
    return QVariant();
}

bool TJsonSchemaModel::setData( const QModelIndex& index, const QVariant& value, int role)
{
    if( index.isValid() && ( role == Qt::EditRole ) )
    {
       auto node = lineFromIndex( index );
       std::string val = value.toString().toStdString();
       switch( index.column() )
       {
         case 0: node->setMapKey( val );
                break;
         case 1:
                node->setValue(val);
                break;
         default:
                break;
       }
      return true;
    }
    return false;
}

std::string TJsonSchemaModel::getFieldPath( const QModelIndex& index ) const
{
    auto line =  lineFromIndex(index);
    return line->getFieldPath();
}

std::string TJsonSchemaModel::getFieldData( const QModelIndex& index ) const
{
    auto line =  lineFromIndex(index);
    return line->toString(); // json data for object and array
}

void  TJsonSchemaModel::setFieldData( const QModelIndex& index, const std::string& data)
{
    auto line =  lineFromIndex(index);
    if( !editID && line->getKey()[0] == '_' )
        return;
    try {
          beginResetModel();
          line->clearField();
          line->setValue(data); // could be json data
          endResetModel();
    }
    catch( ... )
      {
         endResetModel();
         throw;
       }
 }

QString TJsonSchemaModel::getDescription( const QModelIndex& index ) const
{
    auto item = lineFromIndex( index );
    return item->getDescription().c_str();
}

std::string TJsonSchemaModel::helpName( const QModelIndex& index ) const
{
    auto item = lineFromIndex( index );
    return item->getHelpName();
}

//*****************************************************************

void TJsonSchemaModel::resizeArray( QWidget* parentWidget, const QModelIndex& index )
{
    auto line =  lineFromIndex(index);
    ModelLineDialog dlg( this, index, index, line->getKey(), parentWidget);
    if( !dlg.exec() )
       return;

    std::string new_value = dlg.getDefValue();
    auto new_sizes = dlg.getArraySizes();

    // message to delete old data
    if(  rowCount( index ) )
    {
      beginRemoveRows( index.siblingAtColumn(0), 0, rowCount( index ) );
      endRemoveRows();
    }
    line->resizeArray( new_sizes,  new_value );
    // message to add lines
    if(new_sizes[0]>0)
    {
         beginInsertRows( index.siblingAtColumn(0), 0, static_cast<int>(new_sizes[0])-1 );
         endInsertRows();
    }
}

void TJsonSchemaModel::delObject( QWidget* parentWidget, const QModelIndex& index )
{
    auto line =  lineFromIndex(index);

    if( line->isReqiered( )  )
    {
       QMessageBox::warning(parentWidget, "Data object to delete",
                "Required data object cannot be deleted");
        return;
    }

    QString msg = QString("Confirm deletion of %1 data object?").arg( line->getKey().c_str() );
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question( parentWidget->window(), "Data object to delete", msg,
             QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::No)
       return;

    beginRemoveRows( parent(index), index.row(), index.row());
    line->removeField();
    endRemoveRows();
}

void TJsonSchemaModel::delObjects( QWidget * parentWidget, const QModelIndex& cindex )
{
    //uint row;
    QModelIndex parentIndex= parent(cindex);
    auto parent_ = lineFromIndex(parentIndex);

    // select fields to be added
    std::vector<std::string> sch_lst = parent_->getUsedKeys();
    if( sch_lst.empty() )
      return;

    SelectDialog selDlg( true, parentWidget, "Please, select objects to be deleted", sch_lst );
    if( !selDlg.exec() )
      return;

    std::vector<std::size_t> sel;
    selDlg.getSelection( sel );

    // delete from list
    try {
         for(std::size_t ii=0; ii<sel.size(); ii++ )
         {
              auto child = parent_->field( sch_lst[sel[ii]] );
              int rowa = child->getNdx();
              beginRemoveRows( parentIndex, rowa, rowa);
              child->removeField();
              endRemoveRows();
         }
     } catch( ... )
       {
        throw;
       }
}

void TJsonSchemaModel::resetObject( QWidget* parentWidget, const QModelIndex& index )
{
    auto line =  lineFromIndex(index);

    QString msg = QString("Confirm clear of %1 data object?").arg( line->getKey().c_str() );
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(parentWidget, "Data object to clear", msg,
             QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::No)
       return;

    if(  rowCount( index ) )
    {
      beginRemoveRows( index, 0, rowCount( index ) );
      endRemoveRows();
    }
    line->setComplexValue("");
    auto nchildr = line->getChildrenCount();
    if( nchildr )
    {
      beginInsertRows( index, 0, static_cast<int>(nchildr) );
      endInsertRows();
     }
 }

void TJsonSchemaModel::delObjectsUnion( QWidget* parentWidget, const QModelIndex& index )
{
    auto line =  lineFromIndex(index);

    QString msg = QString("Confirm deletion of all alternatives to %1 data object union variant?").arg(
                line->getKey().c_str() );
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(parentWidget, "Data objects to delete", msg,
             QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::No)
       return;

    // resizeline item
    auto parent_ = line->getParent();

    // remove rows after current
    size_t current_next = index.row()+1;
    if( current_next < parent_->getChildrenCount()  )
    {
       beginRemoveRows( parent(index), current_next, parent_->getChildrenCount()-1 );
       for(size_t ii=current_next; ii<parent_->getChildrenCount(); ii++ )
           parent_->getChild(ii)->removeField();
       endRemoveRows();
    }

    //remove rows before current
    current_next = index.row();
    if( current_next > 0  )
    {
       beginRemoveRows( parent(index), 0, index.row() );
       for(size_t ii=0; ii<current_next; ii++ )
           parent_->getChild(ii)->removeField();
       endRemoveRows();
    }
}


const QModelIndex TJsonSchemaModel::addObjects( QWidget * parentWidget, const QModelIndex& cindex )
{
    //uint row;
    QModelIndex parentIndex;
    auto line =  lineFromIndex(cindex);
    if( line->isStruct() && line->getChildrenCount() < 1 )
    {
        parentIndex = cindex.siblingAtColumn(0);
        //row = 0;
    }
    else
    {
        parentIndex = parent(cindex).siblingAtColumn(0);
        //row =  rowCount( parentIndex ); //line->parent->children.size();
    }
    auto parent_ = lineFromIndex(parentIndex);

    // select fields to be added
    std::vector<std::string> sch_lst = parent_->getNoUsedKeys();
    if( sch_lst.empty() )
      return cindex;

    SelectDialog selDlg( true, parentWidget, "Please, select objects to be added", sch_lst );
    if( !selDlg.exec() )
      return cindex;

    std::vector<std::size_t> sel;
    selDlg.getSelection( sel );

    // insert from list
    try {
         //beginInsertRows( parentIndex, row, row+sel.size() );
         for(std::size_t ii=0; ii<sel.size(); ii++ )
         {
              auto child = parent_->field( sch_lst[sel[ii]] );
              child->addField();
              int rowa = child->getNdx();
              beginInsertRows( parentIndex, rowa, rowa );
              endInsertRows();
         }
        //endInsertRows();
     } catch( ... )
       {
        //endInsertRows();
        throw;
       }
   return index( rowCount( parentIndex )-1, 0, parentIndex);
}

// add object to and of object
const QModelIndex TJsonSchemaModel::addObject( QWidget* parentWidget, const QModelIndex& cindex )
{
    int row;
    QModelIndex parentIndex;
    auto line =  lineFromIndex(cindex);
    if(line->isStruct() && line->getChildrenCount() < 1 )
    {
        parentIndex = cindex.siblingAtColumn(0);
        row = 0;
    }
    else
    {
        parentIndex = parent(cindex).siblingAtColumn(0);
        row =  rowCount( parentIndex ); //line->parent->children.size();
    }

    ModelLineDialog dlg( this, cindex, parentIndex, "", parentWidget);
    if( !dlg.exec() )
       return cindex;
    std::string new_object = dlg.getName();
    //int new_type = dlg.getType();
    std::string new_value = dlg.getDefValue();
    auto sizes = dlg.getArraySizes();

    auto parent_ =  lineFromIndex(parentIndex);
    try{
         // start insert row
        //beginInsertRows( parentIndex, row, row );
        auto child = parent_->field(new_object);
        if( !child->isArray())
           child->setValue( new_value );
        else
          {
            child->addField();
            child->resizeArray( sizes,  new_value );
          }
        int rowa = child->getNdx();
        beginInsertRows( parentIndex, rowa, rowa );
        endInsertRows();
    } catch( ... )
      {
       endInsertRows();
       throw;
      }

    return index( row, 0, parentIndex);
}

// add object to and of object
const QModelIndex TJsonSchemaModel::cloneObject( QWidget*, const QModelIndex& cindex )
{
    QModelIndex parentIndex = parent(cindex);
    auto line =  lineFromIndex(cindex);

    try{
        auto parent =  lineFromIndex(parentIndex);
        if( !parent->isArray() )
          return cindex;
        auto row =  rowCount( parentIndex );
        beginInsertRows( parentIndex, row, row );
        parent->appendNode( std::to_string(row), line->getType(),  line->toString() );
        endInsertRows();
        return index( row, 0, parentIndex);

    } catch( ... )
      {
       endInsertRows();
       throw;
      }
   //return cindex;
}

//-------------------------------------------------------------------------------------
// TJsonDomDelegate -  individuak items in views are rendered and edited using delegates
//-------------------------------------------------------------------------------------

TJsonSchemaDelegate::TJsonSchemaDelegate( QObject * parent ):
    QItemDelegate( parent )
{ }

QWidget *TJsonSchemaDelegate::createEditor(QWidget *parent,
        const QStyleOptionViewItem &/*option*/,
        const QModelIndex &index) const
{
    auto model = dynamic_cast<const TJsonSchemaModel *>(index.model() );
    auto node =  model->lineFromIndex(index);

    int type;
    // map key
    if( index.column() == 0 )
       type = node->getFieldType(true);
    else
       type = node->getFieldType();

    // if top level map with class - new edit table
    if( index.column() == 0 )
    { auto enumdef = node->getMapEnumdef();
      if( enumdef != nullptr )
      {
        std::vector<std::string> lst = enumdef->getNamesList();
        QComboBox *accessComboBox = new QComboBox(parent);
        for( size_t ii=0; ii<lst.size(); ii++ )
        {
          std::string itname =lst[ii];
          accessComboBox->addItem(tr(itname.c_str()), enumdef->getId(itname) );
          accessComboBox->setItemData( static_cast<int>(ii), enumdef->getDoc(itname).c_str(), Qt::ToolTipRole);
        }
        return accessComboBox;
     }
    }

    switch( type )
    {
          case Th_BOOL:
                {
                  QComboBox *accessComboBox = new QComboBox(parent);
                  accessComboBox->addItem(tr("false"), tr("false"));
                  accessComboBox->addItem(tr("true"), tr("true"));
                  return accessComboBox;
                 }
          case Th_I08:
          case Th_I16:
          case Th_I32:
                {
                   std::string enumName = node->getEnumI32Name();
                   if( !enumName.empty() )
                   {
                     ThriftEnumDef* enumdef = ioSettings().Schema()->getEnum( enumName );
                     if(enumdef != nullptr )
                     {
                         std::vector<std::string> lst = enumdef->getNamesList();
                         QComboBox *accessComboBox = new QComboBox(parent);
                         for( size_t ii=0; ii<lst.size(); ii++ )
                         {
                             std::string itname =lst[ii];
                             accessComboBox->addItem(tr(itname.c_str()), enumdef->getId(itname) );
                             accessComboBox->setItemData(static_cast<int>(ii), enumdef->getDoc(itname).c_str(), Qt::ToolTipRole);
                         }
                         return accessComboBox;
                     }
                  }
                }
          [[fallthrough]];
          case Th_I64:
          case Th_U64:
              { QLineEdit *lineEdit = new QLineEdit(parent);
                QIntValidator *ivalid = new QIntValidator(lineEdit);
                if( type == Th_U64 )
                   ivalid->setBottom(0);
                if( !IsDoubleEmpty( node->minValue() ) )
                  ivalid->setBottom( static_cast<int>(node->minValue()) );
                if( !IsDoubleEmpty( node->maxValue()) )
                  ivalid->setTop( static_cast<int>(node->maxValue()) );
                lineEdit->setValidator(ivalid);
                return lineEdit;
              }
         case Th_DOUBLE:
               { QLineEdit *lineEdit = new QLineEdit(parent);
                 QDoubleValidator *dvalid = new QDoubleValidator(lineEdit);
                 if( !IsDoubleEmpty( node->minValue()) )
                   dvalid->setBottom(node->minValue());
                 if( !IsDoubleEmpty( node->maxValue()) )
                   dvalid->setTop(node->maxValue());
                 lineEdit->setValidator(dvalid);
                 return lineEdit;
               }
          case Th_UTF8:
          case Th_STRING:
              { QLineEdit *lineEdit = new QLineEdit(parent);
                return lineEdit;
              }
          // main constructions
          default:
             jsonioErr("TBsonDelegate" ,"can't print type " );

  }

   //return QAbstractItemDelegate::createEditor( parent, option,  index );
}

void TJsonSchemaDelegate::setEditorData(QWidget *editor,
                                  const QModelIndex &index) const
{
   QComboBox *cellEdit = dynamic_cast<QComboBox*>(editor);
   if( cellEdit)
   {
       //if( currentData().isValid() )
       {
         int idx = cellEdit->findData(index.data(Qt::EditRole));
         if ( idx != -1 )  // -1 for not found
            cellEdit->setCurrentIndex(idx);
       }
       //else
       //    cellEdit->setCurrentIndex( index.data(Qt::EditRole) );
   }
    else
       QItemDelegate::setEditorData(editor, index);
}

void TJsonSchemaDelegate::setModelData(QWidget *editor,
                                 QAbstractItemModel *model,
                                 const QModelIndex &index) const
{
   QComboBox *cellEdit = dynamic_cast<QComboBox*>(editor);
   if( cellEdit)
   {
      model->setData(index, cellEdit->currentData(),  Qt::EditRole);
      if( index.column() == 0 )
          model->setData(index.sibling(index.row(), 1), cellEdit->currentText(),  Qt::EditRole);
   } else
      QItemDelegate::setModelData(editor, model, index);
 }

// http://www.qtcentre.org/threads/15577-ComboBox-with-multiple-columns


} // namespace jsonui

//---------------------- End of file  model_node.cpp ---------------------------
