//  This is JSONUI library+API (https://bitbucket.org/gems4/jsonui)
//
/// \file TableEditWindow.cpp
/// Implementation of class TableEditWidget - widget to work with table data.
//
// JSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONUI depends on the following open-source software products:
// JSONIO (https://bitbucket.org/gems4/jsonio); Qt5 (https://qt.io).
//

#include <QMessageBox>
#include <QFile>
#include "TableEditWindow.h"
#include "ui_TableEditWindow.h"
#include "TCSVPage.h"
#include "SelectDialog.h"
#include "GraphDialog.h"

namespace jsonui {

const QString csvFilters = "CSV files (*.csv);;All files (*)";

void removeComments( QString& valCsv );

TableEditWidget::TableEditWidget( const char* title,
    TAbstractDataContainer *adata, int mode, QWidget *parent):
    QMainWindow(parent), _title(title),
    _modeUsing(mode), _contn(nullptr),
    ui(new Ui::TableEditWidget), _graph_dlg(nullptr)
{
    setWidgetData(adata);
    setWindowTitle(_title);
    _page->initGraphic( mode );
}


TableEditWidget::TableEditWidget( const char* title,
      const std::string& fname, int mode, QWidget *parent):
    QMainWindow(parent), _title(title),
    _modeUsing(mode), _contn( new TCSVContainer("csv", "")),
    ui(new Ui::TableEditWidget), _graph_dlg(nullptr)
{
    setWidgetData(_contn.get());
    setWindowTitle(_title+"( "+fname.c_str()+" )");
    openNewCSV( fname );
    _page->initGraphic( mode );
}


void TableEditWidget::setWidgetData(TAbstractDataContainer *datacont)
{
    ui->setupUi(this);
    //setAttribute(Qt::WA_DeleteOnClose); // automatically delete itself when window is closed

    // define table
    _page = new TCSVPage(datacont, _modeUsing, this );
    ui->verticalLayout->addWidget(_page);
    disconnect( _page->_tableCSV, SIGNAL(customContextMenuRequested(QPoint)),
           _page->_tableCSV, SLOT(slotPopupContextMenu(QPoint)));
    connect( _page->_tableCSV, SIGNAL( customContextMenuRequested(QPoint) ),
             this, SLOT(slotPopupContextMenu(QPoint)));

    // define menu
    setActions();
}

TableEditWidget::~TableEditWidget()
{
    closeGraph();

    delete _page;
    delete ui;
    //std::cout << "~TableEditWidget" << std::endl;
}

//  Connect all actions
void TableEditWidget::setActions()
{
    // File
    connect( ui->action_Help, SIGNAL( triggered()), this, SLOT(CmHelp()));
    connect( ui->actionClose, SIGNAL( triggered()), this, SLOT(close()));
    connect( ui->actionExport_to_CSV_File, SIGNAL( triggered()), this, SLOT(CmExportCSV()));
    if( (_modeUsing & TMatrixTable::tbNoMenu) )
        ui->actionExport_Selected_to_CSV_File->setVisible(false);
    else
        connect( ui->actionExport_Selected_to_CSV_File, SIGNAL( triggered()), this, SLOT(CmExportSelectedCSV()));
    if( (_modeUsing & TMatrixTable::tbEdit) && !(_modeUsing & TMatrixTable::tbNoMenu) )
      connect( ui->actionImport_from_CVS, SIGNAL( triggered()), this, SLOT(CmImportCSV()));
    else
       ui->actionImport_from_CVS->setVisible(false);

    // Edit
    if( (_modeUsing & TMatrixTable::tbNoMenu) )
    {
        ui->actionSelect_row->setVisible(false);
        ui->actionSelect_co_lumn->setVisible(false);
        ui->actionSelect_all->setVisible(false);
        ui->action_Copy->setVisible(false);
        ui->actionCopy_wit_h_names->setVisible(false);
    }
    else
    { connect(ui->actionSelect_row, SIGNAL(triggered()), _page, SLOT(SelectRow()));
      connect(ui->actionSelect_co_lumn, SIGNAL(triggered()), _page, SLOT(SelectColumn()));
      connect(ui->actionSelect_all, SIGNAL(triggered()), _page, SLOT(SelectAll()));
      connect(ui->action_Copy, SIGNAL(triggered()), _page, SLOT(CopyData()));
      connect(ui->actionCopy_wit_h_names, SIGNAL(triggered()), _page, SLOT(CopyDataHeader()));
    }

    if( (_modeUsing & TMatrixTable::tbEdit) && !(_modeUsing & TMatrixTable::tbNoMenu) )
    { connect(ui->actionCalculator, SIGNAL(triggered()), _page, SLOT(CmCalc()));
      connect(ui->actionC_ut, SIGNAL(triggered()), _page, SLOT(CutData()));
      connect(ui->action_Paste, SIGNAL(triggered()), _page, SLOT(PasteData()));
      connect(ui->actionPaste_transposed, SIGNAL(triggered()), _page, SLOT(PasteTransposedData()));
      connect(ui->actionClear, SIGNAL(triggered()), _page, SLOT(ClearData()));
    } else
      {
        ui->actionCalculator->setVisible(false);
        ui->actionC_ut->setVisible(false);
        ui->action_Paste->setVisible(false);
        ui->actionPaste_transposed->setVisible(false);
        ui->actionClear->setVisible(false);
      }

    // tools
    if( (_modeUsing & TMatrixTable::tbGraph) && !(_modeUsing & TMatrixTable::tbNoMenu))
    { connect( ui->actionMark_Columns_as_X, SIGNAL( triggered()), this, SLOT(ToggleX()));
      connect( ui->actionMark_Columns_as_Y, SIGNAL( triggered()), this, SLOT(ToggleY()));
      connect( ui->actionPlot_Results, SIGNAL( triggered()), this, SLOT(CmPlotTable()));
    }else
      {
        ui->menu_Tools->menuAction()->setVisible(false);
        //ui->actionMark_Columns_as_X->setVisible(false);
        //ui->actionMark_Columns_as_Y->setVisible(false);
        //ui->actionPlot_Results->setVisible(false);
       }
}

void TableEditWidget::updateTable()
{
    closeGraph();
   _page->updateTable();
}


// Menu commands -----------------------------------------------------------

/// Write all table to csv file fileName
void TableEditWidget::CmExportCSV()
{
   try {
         std::string fileName;
         if(  ChooseFileSave( this, fileName,
                     "Please, select file to write", csvFilters  ))
            _page->saveAllToCSV(fileName);
    }
   catch(jsonio::jsonio_exception& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Write selected to csv file fileName
void TableEditWidget::CmExportSelectedCSV()
{
   try {
         std::string fileName;
         if(  ChooseFileSave( this, fileName,
                     "Please, select file to write", csvFilters  ))
            _page->saveSelectedToCSV(fileName);
    }
   catch(jsonio::jsonio_exception& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Read table from csv file fileName
void TableEditWidget::CmImportCSV()
{
  try{
        std::string fileName;
        if(  ChooseFileOpen( this, fileName,
                "Please, select file with csv object", csvFilters  ))
         openNewCSV( fileName );
    }
   catch(jsonio::jsonio_exception& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

void TableEditWidget::openNewCSV( const std::string& fileName )
{
    std::string _path = fileName;

     // read file
     QFile tmpStriam(_path.c_str());
     QString valCsv;
     if(tmpStriam.open( QIODevice::ReadOnly ))
     {
       valCsv = tmpStriam.readAll();
       tmpStriam.close();

       //delete all comments // #
       removeComments( valCsv );
       // remove all ' ' and '\t'
       valCsv.remove(QChar(' '), Qt::CaseInsensitive);
       valCsv.remove(QChar('\t'), Qt::CaseInsensitive);

       //set up data
       if( _contn.get() )
       {
          setWindowTitle(_title+"( "+fileName.c_str()+" )");
          _contn->setCsvString(valCsv);
       }
       updateTable();
     }
     else
       std::cout << "error open file " << _path << std::endl;
  }

// working with graph data --------------------------------------------------

void TableEditWidget::slotPopupContextMenu(const QPoint &pos)
{
    if( _modeUsing & TMatrixTable::tbNoMenu )
      return;

    QModelIndex index = _page->_tableCSV->indexAt( pos );

    QMenu *menu = new QMenu(this);
    _page->_tableCSV->makePopupContextMenu( menu, index );

    if( _modeUsing & TMatrixTable::tbGraph )
    {
      bool addsep = false;
      QAction* act;

       if( _page->canBeToggled( index, true ) )
       {
            menu->addSeparator();
            addsep = true;
            act =  new QAction(tr("Toggle &X"), this);
            act->setShortcut(tr("Ctrl+X"));
            act->setStatusTip(tr("Mark columns as X (toggle)"));
            connect(act, SIGNAL(triggered()), this, SLOT(ToggleX()));
            menu->addAction(act);
       }

       if( _page->canBeToggled( index, false ) )
       {
           if( !addsep )
               menu->addSeparator();
           act =  new QAction(tr("Toggle &Y"), this);
           act->setShortcut(tr("Ctrl+Y"));
           act->setStatusTip(tr("Mark columns as Y (toggle)"));
           connect(act, SIGNAL(triggered()), this, SLOT(ToggleY()));
           menu->addAction(act);
       }
    }

    menu->exec( mapToGlobal(pos) );
    delete menu;
}

void TableEditWidget::ToggleX()
{
  _page->toggleX();
   ///???     CmPlotTable();
}

void TableEditWidget::ToggleY()
{
    _page->toggleY();
    ///???      CmPlotTable();
}

void TableEditWidget::setXYaxis(int ndxX, std::vector<int> ndxY)
{
    _page->setXYaxis( ndxX, ndxY );
    ///???    CmPlotTable();
}

void TableEditWidget::showGraphData( const std::string& title )
{
   if( !_graph_dlg )
   {
      _graph_dlg = new GraphDialog( _page->_chartData, this );
      connect( _graph_dlg, SIGNAL( dataChanged(ChartData* ) ),
               _page,  SLOT( saveGraphData( ChartData* ) ) );
      connect( _page, SIGNAL( updateGraphWindow() ),
               _graph_dlg,  SLOT( UpdateAll() ) );
   }
   _graph_dlg->UpdateAll(title.c_str());
   _graph_dlg->show();
}

void TableEditWidget::CmPlotTable()
{
    try
    {
       std::string title = "Graph for window: ";
               title  += _title.toStdString();
       showGraphData( title );
    }
    catch(jsonio::jsonio_exception& e)
    {
        QMessageBox::critical( this, e.title(), e.what() );
    }
    catch(std::exception& e)
     {
        QMessageBox::critical( this, "std::exception", e.what() );
     }
}

void TableEditWidget::closeGraph()
{
    if( _graph_dlg )
    {
        _graph_dlg->close();
        delete  _graph_dlg;
        _graph_dlg = nullptr;
    }
}

void TableEditWidget::closeEvent(QCloseEvent* )
{
    closeGraph();
}

void TableEditWidget::CmHelp()
{
    helpWin("HelpTableWindow","");
}


} // namespace jsonui
