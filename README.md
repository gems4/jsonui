# JSONUI #

This [JSONIO](https://bitbucket.org/gems4/jsonio)- and [Qt5](http://www.qt.io/)-based widget C++ library and API is aimed at implementing 
lightweight local GUI applications for editing/viewing the structured data kept in a NoSQL database (ArangoDB), with rendering documents 
to/from JSON, YAML, XML files, and with importing/exporting data from/to arbitrary foreign formats. A widget for graphical presentation 
of tabular data (csv format fields) is also available.

* JSONUI is written in C/C++ using open-source Qt5 and JSONIO codes.
* Version: currently 0.0.
* Will be distributed as is (no liability) under the terms of Lesser GPL v.3 license.

### How to clone (download) the JSONUI source code ###

* In your home directory, make a folder named e.g. "jsonui".
* Cd into jsonui and clone this repository from  https://bitbucket.org/gems4/jsonui.git using git or a preinstalled free git client e.g. SmartGit (the best way on Windows). 
* Alternatively on Mac OS X or linux, open a terminal and in the command line (do not forget a period) run:
~~~
cd /home/your_user/jsonui
git clone https://bitbucket.org/gems4/jsonui.git . 
~~~

### How to build the JSONUI library on Linux or Mac OS ###

* Make sure that Qt5 and CMake are installed in your system. For (K)Ubuntu 18.04, this can be done using the following commands:
~~~
sudo apt-get install cmake
sudo apt-get install qt5-default qttools5-dev libqt5svg5 libqt5svg5-dev libqt5help5
~~~

  For MacOS, make sure that Homebrew is installed (see [Homebrew web site](http://brew.sh) and [Homebrew on Mac OSX El Capitan](http://digitizor.com/install-homebrew-osx-el-capitan/)


* Install Dependencies

In order to build the JSONUI library on (k)ubuntu linux 16.04/18.04 or MacOS, first execute the following: 

~~~
#!bash
cd ~/jsonui
sudo ./install-dependencies.sh
~~~

* Install the JSONUI library

Then navigate to the directory where this README.md file is located and type in terminal:

~~~
cd ~/jsonui
sudo ./install.sh
~~~

This will install JSONUI assuming the Qt5 toolkit available in the system (as in (K)Ubuntu). If Qt5 toolkit is installed locally in your user folder, use the following commands:

~~~
cd ~/jsonui
sudo ./install.sh /home/<you>/Qt/5.12.2/gcc_64/
~~~

or on MacOS

~~~
cd ~/jsonui
sudo ./install.sh /home/<you>/Qt/5.12.2/clang_64/
~~~

* After that, third-party libraries and their headers can be found in /usr/local/{include,lib}. 

### Install the current version of ArangoDB server locally (if not yet installed)

On (K)Ubuntu linux, install the current version of ArangoDB server locally [from here](https://www.arangodb.com/download-major/ubuntu/):

~~~
curl -OL https://download.arangodb.com/arangodb34/DEBIAN/Release.key
sudo apt-key add - < Release.key
echo 'deb https://download.arangodb.com/arangodb34/DEBIAN/ /' | sudo tee /etc/apt/sources.list.d/arangodb.list
sudo apt-get install apt-transport-https
sudo apt-get update
sudo apt-get install arangodb3=3.4.4-1
~~~

The updates will come together with other ubuntu packages that you have installed.

On MacOS Sierra or higher, [navigate here](https://docs.arangodb.com/3.4/Manual/Installation/MacOSX.html) and follow the instructions on how to install ArangoDB with homebrew. Basically, you have to open a terminal and run two commands:

~~~
brew update
brew install arangodb
~~~


## How to use JSONUI (use cases) ##
TBD